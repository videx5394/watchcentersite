@extends('layouts.app')

@section('content')
    <script src="{{asset('js')}}/order.js"></script>
        <style>
            .all-title {
                text-transform: uppercase;
                text-align: center;
            }
            .order_rules {
                text-align: left;
                padding: 10px;
                padding-left: 0;
                min-height: 350px;
            }
        </style>
        <h2 class="all-title">@lang('ordersNotInStock.orders_not_in_stock')</h2>

        <div class="row">
            <div class="xlarge-2 large-3 medium-3 columns sidebar-pos">
                <h2>@lang('ordersNotInStock.order_rules_title')</h2>
                <div>
                    <p class="order_rules">
                        @lang('ordersNotInStock.order_conditions_text')
                    </p>
                </div>
            </div>

            <div class="xlarge-10 large-9 medium-9 columns content-pos">
                <div class="top_bar_shop">

                    <div class="catalog-ordering">


                        <p class="woocommerce-result-count">
                            @lang('home.results',['from' => ($from+1),'to' => ($to<$total_count ? $to : $total_count), 'all' => $total_count])
                        </p>
                        <ul class="shop-ordering">
                            <li>
                                <div class="shop-layout-opts" data-display-type="grid">
                                    <a href="#" class="layout-opt tooltip" data-layout="grid"
                                       title="Grid Layout"><i class="grid-icon active"></i></a>
                                    <a href="#" class="layout-opt tooltip" data-layout="list"
                                       title="List Layout"><i class="list-icon "></i></a>
                                </div>
                            </li>
                            <li>


                                <form class="woocommerce-viewing" method="get">
                                    <select name="count" class="count">
                                        <option value="16" selected='selected'>16</option>
                                        @if($count=='28')
                                            <option selected="selected" value="28">28</option>
                                        @else
                                            <option value="28">28</option>
                                        @endif
                                        @if($count=='40')
                                            <option selected="selected" value="40">40</option>
                                        @else
                                            <option value="40">40</option>
                                        @endif
                                    </select>
                                    <input type="hidden" name="page" value=""/>
                                </form>
                            </li>
                            <li>
                                <form class="woocommerce-ordering" method="get">
                                    <select name="orderby" class="orderby"
                                            aria-label="Shop order">
                                        <option selected="selected"
                                                value="menu_order">@lang('home.default_sorting')</option>
                                        @if($orderby=='price-asc')
                                            <option selected="selected"
                                                    value="price-asc">@lang('home.sort_by_price_l_h')</option>
                                        @else
                                            <option value="price-asc">@lang('home.sort_by_price_l_h')</option>
                                        @endif
                                        @if($orderby=='price-desc')
                                            <option selected="selected"
                                                    value="price-desc">@lang('home.sort_by_price_h_l')</option>
                                        @else
                                            <option value="price-desc">@lang('home.sort_by_price_h_l')</option>
                                        @endif
                                    </select>
                                    <input type="hidden" name="page" value="1"/>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <!--catalog-ordering-->
                    <div class="clearfix"></div>
                </div><!-- .top_bar_shop-->
                <div class="woocommerce-notices-wrapper"></div>
                <div class="active_filters_ontop"></div>

                <ul id="products"
                    class="product-category-list products products-grid small-block-grid-2 medium-block-grid-3 large-block-grid-4 xlarge-block-grid-4 xxlarge-block-grid-4 columns-4 product-layout-grid">

                    @foreach ($products as $product)
                        <li
                                class="product-item  spinner-circle palign-left  product_hover_enable product_hover_mob_disable">
                            <figure class="product-inner">
                                <div class="image-container standart">
                                    <a href="{{route('product',['id' =>$product->id]) }}">
                                        <div class="product_thumbnail_wrapper">
                                            <div class="product_thumbnail with_second_image">
																	<span class="product_thumbnail_background"
                                                                          style="background-image:url('{{config('app.uploads_location')}}/{{$product->back_photo_small_url}}')"></span>
                                                <img width="350" height="380"
                                                     src="{{config('app.uploads_location')}}/{{$product->front_photo_small_url}}"
                                                     class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                     alt=""
                                                     sizes="(max-width: 350px) 100vw, 350px"/>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="ev-attr-swatches"></div>
                                </div>
                                <div class="category-discription-grid-list">
                                    <p class="product-category-listing">
                                        <a href="" class="product-category-link">
                                            @if($product->product_type == 1)
                                                {{__('home.watches')}}
                                            @else
                                                {{__('home.watch_boxes')}}
                                            @endif
                                        </a>
                                    </p>
                                    <h4><a class="product-title-link"
                                           href="{{route('product',['id' =>$product->id])}}"></a>{{$product->name}}</h4>
                                    <div class="archive-product-rating">
                                    </div>
                                    <p class="description-list">{{$product->description}}</p>
                                </div>
                                <div class="category-price-grid-list">
                                <span class="price">
                                    @if($product->sale_price>0)
                                        <del>
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price)}}
                                            </span>
                                        </del>
                                    @endif
                                    <ins>
                                        <span class="woocommerce-Price-amount amount">
                                            <span class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price - (int)$product->sale_price)}}
                                        </span>
                                    </ins>
                                </span>

                                    <div class="clearfix"></div>
                                    <a href="{{route('product',['id' =>$product->id])}}"
                                       class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                    </a>
                                    <a href="" onclick='makeOrder($(this),"{{route('makeOrder')}}","{{route('checkout')}}")' data-product-id="{{$product->id}}"
                                        class="wc-forward button product_type_simple ajax_add_to_cart added_to_cart_button"
                                        title="@lang('ordersNotInStock.make_order')">@lang('ordersNotInStock.make_order')</a>
                                    <div class="clearfix"></div>

                                </div>
                                <!--.category-price-grid-list-->


                                <div class="category-discription-grid">


                                    <p class="product-category-listing"><a href="" class="product-category-link">
                                            @if($product->product_type == 1)
                                                {{__('home.watches')}}
                                            @else
                                                {{__('home.watch_boxes')}}
                                            @endif
                                        </a></p>
                                    <h4><a class="product-title-link"
                                           href="{{route('product',['id' =>$product->id])}}">{{$product->name}}</a></h4>
                                    <div class="archive-product-rating">
                                    </div>


                                    <div class="product_after_shop_loop">


                                        <div class="product_after_shop_loop_switcher">

                                            <div class="product_after_shop_loop_price">

																	<span class="price">
                                                                        @if($product->sale_price>0)
                                                                            <del>
                                                                                <span
                                                                                        class="woocommerce-Price-amount amount">
                                                                                    <span
                                                                                            class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price)}}
                                                                                </span>
                                                                            </del>
                                                                        @endif
                                                                        <ins>
                                                                            <span
                                                                                    class="woocommerce-Price-amount amount">
                                                                                <span
                                                                                        class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price - (int)$product->sale_price)}}
                                                                            </span>
                                                                        </ins>
                                                                    </span>
                                            </div>

                                            <div class="product_after_shop_loop_buttons">
                                                <a href="" onclick='makeOrder($(this),"{{route('makeOrder')}}","{{route('checkout')}}")' data-product-id="{{$product->id}}"
                                                        class="wc-forward button product_type_simple ajax_add_to_cart added_to_cart_button"
                                                        title="@lang('ordersNotInStock.make_order')">@lang('ordersNotInStock.make_order')</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--.category-discription-grid-->

                                <div class="inner-desc">
                                    <p class="description-list">{{Str::limit($product->description, 60)}}</p>

                                </div>

                            </figure>
                            <!-- <div class="clearfix"></div> -->
                        </li>
                    @endforeach
                </ul>
                <div class="woocommerce-after-shop-loop-wrapper">
                    <nav class="woocommerce-pagination">
                        <ul class="page-numbers">
                            <li>
                                <span aria-current="page" class="page-numbers current">1</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div><!-- .large-9 or .large-12 -->


        </div><!-- .row -->

        <script>

            let full_url = "{!! $full_url !!}";

            document.querySelectorAll('a.page-numbers').forEach(el => {
                let page = el.getAttribute('data-page');
                el.setAttribute('href', updateQueryStringParameter(full_url, 'page', page));
            });



        </script>


@endsection

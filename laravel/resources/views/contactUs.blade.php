@extends('layouts.app')

@section('content')
        <style>
            .form_errors li{
                color: red;
                font-size: 20px;
            }
        </style>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

        <div class="row woocommerce">
            <div class="large-12 columns wc-notice">
            </div>
        </div>




        <article id="post-1586" class="post-1586 page type-page status-publish hentry">

            <div class="row">
                <div class="large-12 columns">
                    <div class="entry-content">
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false"
                             class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-flex">
                            <div class="wpb_column vc_column_container vc_col-sm-8">
                                <div class="vc_column-inner vc_custom_1456151127599">
                                    <div class="wpb_wrapper">
                                        <div class="vc_empty_space" style="height: 30px"><span
                                                class="vc_empty_space_inner"></span></div>
                                        <h1 style="font-size: 80px;color: #eeeeee;line-height: 80px;text-align: left;font-family:Lato;font-weight:900;font-style:normal"
                                            class="vc_custom_heading">@lang('contactUs.get')</h1>
                                        <h6 style="font-size: 30px;color: #333333;line-height: 30px;text-align: left"
                                            class="vc_custom_heading vc_custom_1456781570337">@lang('contactUs.in_touch')</h6>

                                        <div class="alert alert-danger ">
                                            <ul class="form_errors">

                                            </ul>
                                        </div>

                                        <div role="form" class="wpcf7" id="wpcf7-f2244-p1586-o1"
                                             lang="en-US" dir="ltr">
                                            <div class="screen-reader-response"></div>
                                            <form
                                                action="https://woodstock.temashdesign.com/electronics/contact-us/#wpcf7-f2244-p1586-o1"
                                                method="post" class="wpcf7-form"
                                                novalidate="novalidate">
                                                <div style="display: none;">
                                                    <input type="hidden" name="_wpcf7"
                                                           value="2244" />
                                                    <input type="hidden" name="_wpcf7_version"
                                                           value="5.1.6" />
                                                    <input type="hidden" name="_wpcf7_locale"
                                                           value="en_US" />
                                                    <input type="hidden" name="_wpcf7_unit_tag"
                                                           value="wpcf7-f2244-p1586-o1" />
                                                    <input type="hidden"
                                                           name="_wpcf7_container_post"
                                                           value="1586" />
                                                </div>
                                                <p><label>@lang('contactUs.your_name') <abbr
                                                            class="required">*</abbr></label><br />
                                                    <span
                                                        class="wpcf7-form-control-wrap your-name"><input
                                                            type="text" name="your-name"
                                                            value="" size="40"
                                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                            aria-required="true"
                                                            aria-invalid="false" /></span></p>
                                                <p><label>@lang('contactUs.your_email') <abbr
                                                            class="required">*</abbr></label><br />
                                                    <span
                                                        class="wpcf7-form-control-wrap your-email"><input
                                                            type="email" name="your-email"
                                                            value="" size="40"
                                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                            aria-required="true"
                                                            aria-invalid="false" /></span></p>
                                                <p><label>@lang('contactUs.subject')</label><br />
                                                    <span
                                                        class="wpcf7-form-control-wrap your-subject"><input
                                                            type="text" name="your-subject"
                                                            value="" size="40"
                                                            class="wpcf7-form-control wpcf7-text"
                                                            aria-invalid="false" /></span></p>
                                                <p><label>@lang('contactUs.message')</label><br />
                                                    <span
                                                        class="wpcf7-form-control-wrap your-message"><textarea
                                                            name="your-message" cols="40"
                                                            rows="10"
                                                            class="wpcf7-form-control wpcf7-textarea"
                                                            aria-invalid="false"></textarea></span>
                                                </p>
                                                <p><input type="button" onclick="contactUs()" value="@lang('contactUs.send')"
                                                          class="wpcf7-form-control wpcf7-submit" />
                                                </p>
                                                <div
                                                    class="wpcf7-response-output wpcf7-display-none">
                                                </div>
                                            </form>
                                        </div>
                                        <div class="vc_empty_space" style="height: 30px"><span
                                                class="vc_empty_space_inner"></span></div>
                                    </div>
                                </div>
                            </div>
                            <div
                                class="wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill">
                                <div class="vc_column-inner vc_custom_1456090660314">
                                    <div class="wpb_wrapper">
                                        <div class="vc_empty_space" style="height: 50px"><span
                                                class="vc_empty_space_inner"></span></div>
                                        <div
                                            class="vc_separator wpb_content_element vc_separator_align_left vc_sep_width_100 vc_sep_border_width_3 vc_sep_pos_align_left vc_separator-has-text">
																	<span class="vc_sep_holder vc_sep_holder_l"><span
                                                                            style="border-color:#333333;"
                                                                            class="vc_sep_line"></span></span>
                                                                            <i style="font-size: 40px; margin-right: 20px;" class="fa fa-list-alt" aria-hidden="true"></i><span
                                                class="vc_sep_holder vc_sep_holder_r"><span
                                                    style="border-color:#333333;"
                                                    class="vc_sep_line"></span></span>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <h3>Baku</h3>
                                                <p><strong>{{request()->contact_info['phone']}}</strong><br />
                                                    <a
                                                        href="mailto:{{request()->contact_info['email']}}">{{request()->contact_info['email']}}</a>
                                                </p>
                                            </div>
                                        </div>
                                        <div
                                            class="vc_separator wpb_content_element vc_separator_align_left vc_sep_width_100 vc_sep_border_width_3 vc_sep_pos_align_left vc_custom_1456090824236 vc_separator-has-text">
																	<span class="vc_sep_holder vc_sep_holder_l"><span
                                                                            style="border-color:#333333;"
                                                                            class="vc_sep_line"></span></span>
                                            <h4>@lang('contactUs.working_hours')</h4><span
                                                class="vc_sep_holder vc_sep_holder_r"><span
                                                    style="border-color:#333333;"
                                                    class="vc_sep_line"></span></span>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <p>
                                                    <strong>@lang('contactUs.everyday')</strong>: 24 / 7<br />

                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 50px"><span
                                                class="vc_empty_space_inner"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                    </div><!-- .entry-content -->

                </div><!-- .columns -->
            </div><!-- .row -->

        </article><!-- #post -->


        <div class="clearfix"></div>

        <script>
            let contactUs = async () => {

                addLoading($('form'));

                const data = {
                    'name' : $('[name="your-name"]').val(),
                    'subject' : $('[name="your-subject"]').val(),
                    'message' : $('[name="your-message"]').val(),
                    'email' : $('[name="your-email"]').val()
                };

                const response = await fetch("{{route('contactUsMail')}}", {
                    method: 'POST', // *GET, POST, PUT, DELETE, etc.
                    mode: 'cors', // no-cors, *cors, same-origin
                    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                    credentials: 'same-origin', // include, *same-origin, omit
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest',
                        'Content-Type': 'application/json',
                        'Accept':'application/json',
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    redirect: 'follow', // manual, *follow, error
                    referrerPolicy: 'no-referrer', // no-referrer, *client
                    body: JSON.stringify(data) // body data type must match "Content-Type" header
                });

                if(response.status === 200)
                {
                    Swal.fire({
                        title: "@lang('contactUs.form_success_title')",
                        text: "@lang('contactUs.form_success_message')",
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        window.location.href = '/';
                    })
                }
                else if(response.status === 422 )
                {
                    const result = await response.json();
                    const errors = result.errors;
                    $('.form_errors').html('');
                    for (fieldName in errors)
                    {
                        let field = errors[fieldName];
                        for (idx in field)
                        {
                            let error = field[idx].replace(fieldName,"'"+$("[name='your-"+fieldName+"']").closest('p').find('label').text()+"'");
                            $('.form_errors').append(`<li>${error}</li>`);
                        }
                    }
                }
                else
                {
                    Swal.fire({
                        title: "@lang('contactUs.form_error_title')",
                        text: "@lang('contactUs.form_error_message')",
                        icon: 'error',
                        confirmButtonText: 'OK'
                    });
                }
                removeLoading($('form'));
            };


        </script>
@endsection

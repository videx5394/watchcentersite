@extends('layouts.app')

@section('content')
    <style>
        .wrapper
        {
            margin: 10px auto;
            width: 80%;
        }
    </style>
    <div class="wrapper">

        <h2>Why us?</h2>

        <p class="text-paragraph">Watch Shop is an official stockist for all watch brands listed on this website. Established in 1991 on the high street, Watch Shop is a leading retailer of brand name designer watches and is also the UK's most popular watch website*. Watch Shop was the first independent watch retailer to advertise on national television, and we pride ourselves on having one of the most efficient shopping systems available with communication at every stage to inform you of your order status, as well as excellent 7-days sales, customer service and support team who are glad to assist you with any enquiry.</p>

        <cite>* Statistic based on independent traffic rankings supplied by Alexa and Hitwise.</cite>
        <h3 style="padding-top: 1rem; padding-bottom: 1rem;">Reasons why you should trust Watch Shop with your purchase:</h3>

        <h1>Price Promise</h1>

        <p>We don't want you to choose between the best price and the best service. That's why we price match every product we sell against any ACCREDITED retailer. Just supply us with the product you're interested in and the price you've found it at and we'll do the rest!</p>

        <p>Just make sure you've got this handy:
            <br> The product number and the price you've seen it listed at. The product must be identical (e.g., colour, style, model) and in stock at the competitor.
            <br> The website address of the retailer. We can only match against genuine accredited stockists based in the United Kingdom.</p>

        <ul style="line-height: 25px;">
            <li>Our price match INCLUDES discount codes!</li>
            <li>We work hard to price check our products daily, and have a dedicated team that work tirelessly to ensure we are great value!</li>
            <li>We only match against official, accredited retailers.</li>
            <li>When purchasing from international sites, e.g., in the United States, you should consider that you must pay customs duty and import tax on top of the watch price. For this reason, we can only match with retailers based in the United Kingdom.</li>
            <li>We do not match second hand goods or grey market imports.</li>
            <li>We do not match auction or marketplace retailers.</li>
            <li>We take into account any delivery charges that are applicable.</li>
            <li>Price match vouchers cannot be used in conjunction with other promotional codes.</li>
            <li>We reserve the right to decline a price match at our discretion.</li>
        </ul>

        <p>See for yourself! Submit a price match today and let our dedicated Customer Support Team take care of the rest.</p>
    </div>
@endsection

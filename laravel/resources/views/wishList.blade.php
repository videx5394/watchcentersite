@extends('layouts.app')

@section('content')

    <div class="row woocommerce">
        <div class="large-12 columns wc-notice">
        </div>
    </div>

    <article id="post-4" class="post-4 page type-page status-publish hentry">

        <div class="row">
            <div class="large-12 columns">

                <div class="entry-content">

                    <div class="row">
                        <div class="large-10 columns large-centered ">
                            <div id="yith-wcwl-messages"></div>


                            <form id="yith-wcwl-form"
                                  action="https://woodstock.temashdesign.com/electronics/wishlist/view/IBJU1AILKZ1B/"
                                  method="post" class="woocommerce yith-wcwl-form wishlist-fragment"
                                  data-fragment-options="{&quot;per_page&quot;:5,&quot;pagination&quot;:&quot;no&quot;,&quot;wishlist_id&quot;:241591,&quot;action_params&quot;:&quot;&quot;,&quot;no_interactions&quot;:false,&quot;layout&quot;:&quot;&quot;,&quot;is_default&quot;:true,&quot;is_custom_list&quot;:true,&quot;wishlist_token&quot;:&quot;IBJU1AILKZ1B&quot;,&quot;is_private&quot;:false,&quot;count&quot;:3,&quot;page_title&quot;:&quot;My wishlist on Woodstock Electronics&quot;,&quot;default_wishlsit_title&quot;:&quot;My wishlist on Woodstock Electronics&quot;,&quot;current_page&quot;:1,&quot;page_links&quot;:false,&quot;is_user_logged_in&quot;:true,&quot;is_user_owner&quot;:true,&quot;show_price&quot;:true,&quot;show_dateadded&quot;:false,&quot;show_stock_status&quot;:true,&quot;show_add_to_cart&quot;:true,&quot;show_remove_product&quot;:true,&quot;add_to_cart_text&quot;:&quot;Add to Cart&quot;,&quot;show_ask_estimate_button&quot;:false,&quot;ask_estimate_url&quot;:&quot;&quot;,&quot;price_excl_tax&quot;:true,&quot;show_cb&quot;:false,&quot;show_quantity&quot;:false,&quot;show_variation&quot;:false,&quot;show_price_variations&quot;:false,&quot;show_update&quot;:false,&quot;enable_drag_n_drop&quot;:false,&quot;enable_add_all_to_cart&quot;:false,&quot;move_to_another_wishlist&quot;:false,&quot;repeat_remove_button&quot;:false,&quot;show_last_column&quot;:true,&quot;heading_icon&quot;:&quot;<i class=\&quot;fa none\&quot;><\/i>&quot;,&quot;share_enabled&quot;:true,&quot;template_part&quot;:&quot;view&quot;,&quot;additional_info&quot;:false,&quot;available_multi_wishlist&quot;:false,&quot;form_action&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/wishlist\/view\/IBJU1AILKZ1B\/&quot;,&quot;ajax_loading&quot;:false,&quot;item&quot;:&quot;wishlist&quot;}">

                                <!-- TITLE -->
                                <div class="wishlist-title">
                                    <h2>@lang('wishList.my_wish_list')</h2>
                                </div>

                                <!-- WISHLIST TABLE -->
                                <table class="shop_table cart wishlist_table wishlist_view traditional responsive"
                                       data-pagination="no" data-per-page="5" data-page="1" data-id="241591"
                                       data-token="IBJU1AILKZ1B">
                                    <thead>
                                    <tr>
                                        <th class="product-remove"></th>

                                        <th class="product-thumbnail"></th>

                                        <th class="product-name">
                                            <span class="nobr">@lang('wishList.product_name')</span>
                                        </th>

                                        <th class="product-price">
                                            <span class="nobr">@lang('wishList.unit_price')</span>
                                        </th>

                                        <th class="product-add-to-cart"></th>
                                    </tr>
                                    </thead>

                                    <tbody class="wishlist-items-wrapper">
                                    @foreach ($products as $product)
                                        <tr id="yith-wcwl-row-{{$product->id}}" data-row-id="{{$product->id}}">
                                            <td class="product-remove">
                                                <div>
                                                    <a href="{{route('remove_from_wish_list')}}"
                                                       class="remove remove_from_wishlist"
                                                       title="Remove this product">×</a>
                                                </div>
                                            </td>

                                            <td class="product-thumbnail">
                                                <a href="{{route('product',['id' =>$product->id]) }}">
                                                    <img width="350" height="380"
                                                         src="{{config('app.uploads_location')}}/{{$product->front_photo_small_url}}"
                                                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                         alt=""
                                                         sizes="(max-width: 350px) 100vw, 350px">
                                                </a>
                                            </td>

                                            <td class="product-name">
                                                <a href="{{route('product',['id' =>$product->id]) }}">{{$product->name}}</a>


                                            </td>

                                            <td class="product-price">
                                                <span class="woocommerce-Price-amount amount"><span
                                                        class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price - (int)$product->sale_price)}}</span>
                                            </td>


                                            <td class="product-add-to-cart">
                                                <!-- Date added -->

                                                <!-- Add to cart button -->
                                                @if(!in_array($product->id,request()->shopping_cart['ids']))
                                                    <a href="{{route('add_to_card',['id' => $product->id])}}"
                                                       data-quantity="1"
                                                       class="button add_to_cart_button ajax_add_to_cart add_to_cart alt"
                                                       data-product_id="{{$product->id}}" data-product_sku="WS1040"
                                                       aria-label="Add “{{$product->name}}” to your cart"
                                                       rel="nofollow">@lang('wishList.add_to_cart')</a>
                                                @else
                                                    <button
                                                            style="padding:0;width: 100%;"
                                                       data-quantity="1"
                                                       class="button"
                                                       type="button"
                                                       onclick="removeFromCart($(this),'{{$product->id}}','{{$product->name}}','{{route('add_to_card',['id' => $product->id])}}')"
                                                       data-product_id="{{$product->id}}" data-product_sku="WS1040"
                                                       aria-label="Remove “{{$product->name}}” from your cart"
                                                       rel="nofollow">@lang('wishList.remove_from_cart')</button>
                                                @endif

                                                <!-- Change wishlist -->

                                                <!-- Remove from wishlist -->
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </form>
                        </div>
                    </div>
                </div><!-- .entry-content -->

            </div><!-- .columns -->
        </div><!-- .row -->

    </article><!-- #post -->


    <div class="clearfix"></div>

    <script>

        async function removeFromCart(element,product_id,product_name,route) {

            $('.wishlist_table').fadeTo("400", "0.6").block({
                message: null,
                overlayCSS: {
                    background: "transparent url(" + yith_wcwl_l10n.ajax_loader_url + ") no-repeat center",
                    backgroundSize: "40px 40px",
                    opacity: 1
                }
            });

            const data = {
                product_id: product_id
            };
            const result = await fetch("{{route('remove_from_card_ajax')}}", {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client
                body: JSON.stringify(data) // body data type must match "Content-Type" header
            });

            if(result.status === 200)
            {
                element.closest('td').html(` <a href="${route}"
                                                       data-quantity="1"
                                                       class="button add_to_cart_button ajax_add_to_cart add_to_cart alt"
                                                       data-product_id="${product_id}" data-product_sku="WS1040"
                                                       aria-label="Add “${product_name}” to your cart"
                                                       rel="nofollow">@lang('wishList.add_to_cart')</a>`);
            }

            $('.wishlist_table').stop(!0).css("opacity", "1").unblock();

        }
    </script>

@endsection

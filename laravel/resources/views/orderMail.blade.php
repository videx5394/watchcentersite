<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Order</title>
</head>
<body>
<h2>Client</h2>
<table style="text-align: center">
    <thead>
    <tr>
        <th>Field</th>
        <th>Value</th>
    </tr>
    </thead>
   <tbody>
   <tr>
       <td>First Name</td>
       <td>{{$request['firstName']}}</td>
   </tr>
   <tr>
       <td>Surname</td>
       <td>{{$request['surname']}}</td>
   </tr>
   <tr>
       <td>Address</td>
       <td>{{$request['address']}}</td>
   </tr>
   <tr>
       <td>Phone</td>
       <td>{{$request['phone']}}</td>
   </tr>
   <tr>
       <td>Email</td>
       <td>{{$request['email']}}</td>
   </tr>
   <tr>
       <td>Shipping</td>
       <td>{{$request['shipping']}}</td>
   </tr>
   <tr>
       <td>Payment</td>
       <td>{{$request['payment']}}</td>
   </tr>
   <tr>
       <td>Coupon</td>
       <td>{{$products[0]->coupon}}</td>
   </tr>
   <tr>
       <td>Total price</td>
       <td>{{$products[0]->total_price}}</td>
   </tr>
   </tbody>
</table>
<h2>Products</h2>
<table style="text-align: center">
    <thead>
    <tr>
        <th>Field</th>
        <th>Value</th>
    </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{$product->name}}</td>
                <td>{{$product->price}} ₼</td>
            </tr>
        @endforeach
    </tbody>
</table>

</body>
</html>

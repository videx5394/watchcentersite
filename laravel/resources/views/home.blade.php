@extends('layouts.app')

@section('content')


    <style>

        #best-sellers {
            margin-bottom: 0;
        }

        #best-sellers .product-item figure.product-inner {
            display: block;
            margin: 0;
            padding: 20px 20px 20px 20px;
            -webkit-box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
            -moz-box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
            box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            -ms-transition: all 0.3s;
            -o-transition: all 0.3s;
            transition: all 0.3s;
            z-index: 1;
            height: auto !important;
        }

        #best-sellers .owl-stage-outer {
            padding-top: 0px;
            display: block;
        }

        /*.owl-item*/
        /*{*/
        /*    visibility: hidden;*/
        /*}*/
        /*.owl-item.active*/
        /*{*/
        /*    visibility: visible;*/
        /*}*/


        .all-title {
            margin-top: 0px;
            font-size: 1.5rem;
            line-height: 2.4rem;
            text-transform: uppercase;
            margin-bottom: 20px;
            text-align: center;
            position: relative;
            top: -40px;
        }

        #best-sellers .product-item figure.product-inner:hover {
            background-color: #fff;
            -webkit-box-shadow: 0px 0px 40px 0px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 0px 0px 40px 0px rgba(0, 0, 0, 0.1);
            box-shadow: 0px 0px 40px 0px rgba(0, 0, 0, 0.1);
            -moz-transform: translateY(-4px);
            -o-transform: translateY(-4px);
            -ms-transform: translateY(-4px);
            -webkit-transform: translateY(-4px);
            transform: translateY(-4px);
            z-index: 9999;
        }

        #best-sellers li.product-item figure.product-inner p.product-category-listing a {
            color: rgba(0, 0, 0, 0.4);
            font-size: 10px;
            line-height: 16px;
            font-size: 0.625rem;
            line-height: 1rem;
            font-weight: bold;
            font-weight: 400;
            line-height: 12px;
            text-transform: uppercase;
            letter-spacing: 1px;
        }

        #best-sellers .product-item figure.product-inner .inner-desc p {
            font-size: 12px;
            line-height: 19.2px;
            font-size: 0.75rem;
            line-height: 1.2rem;
            color: rgba(0, 0, 0, 0.7);
        }

        #best-sellers li.product-item figure.product-inner h4 a:hover {
            color: rgba(0, 0, 0, 0.7);
        }

        #best-sellers li.product-item figure.product-inner h4 a {
            color: #000;
        }

        #best-sellers .product-item figure.product-inner .inner-desc {
            text-align: left;
            display: none;
            margin-top: 15px;
        }

        #best-sellers .product-item figure.product-inner.hover .inner-desc {
            -webkit-animation: fadeIn 0.6s;
            -moz-animation: fadeIn 0.6s;
            -ms-animation: fadeIn 0.6s;
            -o-animation: fadeIn 0.6s;
            animation: fadeIn 0.6s;
            display: block;
        }

        .carousel-title {
            text-align: center;
        }

        @media only screen and (max-width: 40em) {
            #best-sellers .product-item.product_hover_mob_disable figure.product-inner {
                display: block;
                -webkit-animation: none !important;
                -moz-animation: none !important;
                -ms-animation: none !important;
                -o-animation: none !important;
                animation: none !important;
                -webkit-box-shadow: none !important;
                -moz-box-shadow: none !important;
                box-shadow: none !important;
                -moz-transform: none !important;
                -o-transform: none !important;
                -ms-transform: none !important;
                -webkit-transform: none !important;
                transform: none !important;
            }

            #products-carousel .owl-stage-outer {
                padding: 30px 0 0px 0;
            }

            #best-sellers .product-item.product_hover_mob_disable figure.product-inner.hover {
                -webkit-animation: none;
                -moz-animation: none;
                -ms-animation: none;
                -o-animation: none;
                animation: none;
                -webkit-box-shadow: none !important;
                -moz-box-shadow: none !important;
                box-shadow: none !important;
                -moz-transform: none !important;
                -o-transform: none !important;
                -ms-transform: none !important;
                -webkit-transform: none !important;
                transform: none !important;
            }

            #best-sellers .product-item.product_hover_mob_disable figure.product-inner.hover .inner-desc {
                -webkit-animation: none !important;
                -moz-animation: none !important;
                -ms-animation: none !important;
                -o-animation: none !important;
                animation: none !important;
            }

            #best-sellers .product-item.product_hover_mob_disable figure.product-inner .product_after_shop_loop {
                height: auto;
            }

            #best-sellers .product-item.product_hover_mob_disable figure.product-inner .product_after_shop_loop_switcher {
                height: auto;
                -moz-transform: translateY(0px) !important;
                -o-transform: translateY(0px) !important;
                -ms-transform: translateY(0px) !important;
                -webkit-transform: translateY(0px) !important;
                transform: translateY(0px) !important;
            }

            #best-sellers .product-item.product_hover_mob_disable figure.product-inner .product_after_shop_loop_switcher .product_after_shop_loop_price {
                overflow: visible;
                -moz-transform: translateY(0px) !important;
                -o-transform: translateY(0px) !important;
                -ms-transform: translateY(0px) !important;
                -webkit-transform: translateY(0px) !important;
                transform: translateY(0px) !important;
            }

            #best-sellers .product-item.product_hover_mob_disable figure.product-inner .inner-desc {
                display: block;
            }

            #best-sellers .owl-stage-outer {
                display: block;
            }

            #best-sellers .owl-stage {
                margin-bottom: 100px;
            }
        }


    </style>

    <script type="text/javascript" src="/wp-content/plugins/woocommerce/assets/js/accounting/accounting.minaffb.js?ver=0.4.2"></script>
    <script type="text/javascript" src="/wp-includes/js/jquery/ui/mouse.mine899.js?ver=1.11.4"></script>


    <script type="text/javascript">
        /* <![CDATA[ */
        var woocommerce_price_slider_params = {
            "currency_format_num_decimals": "0",
            "currency_format_symbol": "₼",
            "currency_format_decimal_sep": ".",
            "currency_format_thousand_sep": ",",
            "currency_format": "%s%v"
        };
        /* ]]> */
    </script>
    <script type="text/javascript" src="/wp-includes/js/jquery/ui/slider.mine899.js?ver=1.11.4"></script>
    <script type="text/javascript" src="/wp-content/plugins/woocommerce/assets/js/frontend/price-slider.min7e2e.js?ver=3.8.1"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var mc4wp_forms_config = [];
        /* ]]> */
    </script>

    <div class="site_header with_featured_img row"
         style="max-width: 86.857rem;background-image: url({{asset('images/home_wallpaper.jpeg')}}); background-position: center -38px; opacity: 1;">

        <div class="site_header_overlay" style="opacity: 1;"></div>

        <div class="row">
            <div class="large-12 title-left large-centered columns">
                <div id="breadcrumbs">
                <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage"
                                                                      title="Go to Woodstock Watch."
                                                                      href="/"
                                                                      class="home"><span property="name">Watch Center Baku</span></a>
                <meta property="position" content="1">
                </span>
                </div>

                <h1 class="page-title on-shop">Watches</h1>

                <div class="term-description">
                    <p>The architects of time</p>
                </div>

            </div>
            <!-- .large-12 -->

        </div>
        <!-- .row -->
    </div>

    <div id="button_offcanvas_sidebar_left"><i class="sidebar-icon"></i></div>

    <div class="single_product_related">
        <div class="row">
            <div class="large-12 columns">
                <div id="products-carousel">

                    <h2 class="carousel-title">@lang('home.best_sellers')</h2>

                    <ul id="products" class="products products-grid product-layout-grid owl-carousel owl-theme">
                        @foreach ($best_sellers as $product)
                            <li
                                    class="product-item  spinner-circle palign-left  product_hover_enable product_hover_mob_disable">
                                <figure class="product-inner">
                                    <div class="image-container standart">
                                        <a href="{{route('product',['id' =>$product->id]) }}">
                                            <div class="product_thumbnail_wrapper">
                                                <div class="product_thumbnail with_second_image">
																	<span class="product_thumbnail_background"
                                                                          style="background-image:url('{{config('app.uploads_location')}}/{{$product->back_photo_small_url}}')"></span>
                                                    <img width="350" height="380"
                                                         src="{{config('app.uploads_location')}}/{{$product->front_photo_small_url}}"
                                                         class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                         alt=""
                                                         sizes="(max-width: 350px) 100vw, 350px"/>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="ev-attr-swatches"></div>
                                    </div>
                                    <div class="category-discription-grid-list">
                                        <p class="product-category-listing">
                                            <a href="" class="product-category-link">
                                                @if($product->product_type == 'watches')
                                                    {{__('home.watches')}}
                                                @else
                                                    {{__('home.watch_boxes')}}
                                                @endif
                                            </a>
                                        </p>
                                        <h4><a class="product-title-link"
                                               href="{{route('product',['id' =>$product->id])}}"></a>{{$product->name}}
                                        </h4>
                                        <div class="archive-product-rating">
                                        </div>
                                    </div>
                                    <div class="category-price-grid-list">
                                <span class="price">
                                   @if($product->sale_price>0)
                                        <del>
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price)}}
                                            </span>
                                        </del>
                                    @endif
                                    <ins>
                                        <span class="woocommerce-Price-amount amount">
                                            <span class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price - (int)$product->sale_price)}}
                                        </span>
                                    </ins>
                                </span>

                                        <div class="clearfix"></div>
                                        <a href="{{route('product',['id' =>$product->id])}}"
                                           class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                        </a>
                                        @if(in_array($product->id,request()->shopping_cart['ids']))
                                            <a
                                                    href="{{route('view_cart')}}"
                                                    class="wc-forward button product_type_simple ajax_add_to_cart added_to_cart_button"
                                                    title="@lang('home.view_cart')">@lang('home.view_cart')</a>
                                        @else
                                            <a href="{{route('add_to_card',['id' => $product->id])}}" data-quantity="1"
                                               class="button product_type_simple add_to_cart_button ajax_add_to_cart"
                                               data-product_id="{{$product->id}}" data-product_sku="WS1040"
                                               aria-label="Add &ldquo;{{$product->name}}&rdquo; to your cart"
                                               rel="nofollow">@lang('home.add_to_card')</a>
                                        @endif


                                        <div class="clearfix"></div>

                                        <div class="prod-plugins">
                                            <ul>
                                                <li>
                                                    <div
                                                            class="yith-wcwl-add-to-wishlist add-to-wishlist-369  wishlist-fragment on-first-load"
                                                            data-fragment-ref="369"
                                                            data-fragment-options="{&quot;base_url&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/product-category\/cellphones?product_cat=cellphones&quot;,&quot;wishlist_url&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/wishlist\/&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:369,&quot;parent_product_id&quot;:369,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse Wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in the wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:false,&quot;item&quot;:&quot;add_to_wishlist&quot;}">


                                                        @if(in_array($product->id,request()->wish_list['ids']))
                                                            <div class="yith-wcwl-wishlistaddedbrowse">
                                                                <span class="feedback">@lang('home.product_added_to_wish_list')</span>
                                                                <a href="{{route('wishList')}}" rel="nofollow" data-title="Browse Wishlist">Browse Wishlist	</a>
                                                            </div>
                                                        @else
                                                            <div class="yith-wcwl-add-button">
                                                                <a href="{{route('add_to_wish_list',['id' => $product->id])}}"
                                                                   rel="nofollow" data-product-id="{{$product->id}}"
                                                                   data-product-type="simple"
                                                                   data-original-product-id="{{$product->id}}"
                                                                   class="add_to_wishlist single_add_to_wishlist"
                                                                   data-title="Add to Wishlist">
                                                                    <span>@lang('home.add_to_wish_list')</span>
                                                                </a>
                                                            </div>
                                                        @endif



                                                        <!-- COUNT TEXT -->
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>


                                    </div>
                                    <!--.category-price-grid-list-->


                                    <div class="category-discription-grid">


                                        <p class="product-category-listing"><a href="" class="product-category-link">
                                                @if($product->product_type == 'watches')
                                                    {{__('home.watches')}}
                                                @else
                                                    {{__('home.watch_boxes')}}
                                                @endif
                                            </a></p>
                                        <h4><a class="product-title-link"
                                               href="{{route('product',['id' =>$product->id])}}">{{$product->name}}</a>
                                        </h4>
                                        <div class="archive-product-rating">
                                        </div>


                                        <div class="product_after_shop_loop">


                                            <div class="product_after_shop_loop_switcher">

                                                <div class="product_after_shop_loop_price">
																	<span class="price">
                                                                       @if($product->sale_price>0)
                                                                            <del>
                                                                                <span
                                                                                        class="woocommerce-Price-amount amount">
                                                                                    <span
                                                                                            class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price)}}
                                                                                </span>
                                                                            </del>
                                                                        @endif
                                                                        <ins>
                                                                            <span
                                                                                    class="woocommerce-Price-amount amount">
                                                                                <span
                                                                                        class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price - (int)$product->sale_price)}}
                                                                            </span>
                                                                        </ins>
                                                                    </span>
                                                </div>
                                                <div class="product_after_shop_loop_buttons">
                                                    @if(in_array($product->id,request()->shopping_cart['ids']))
                                                        <a
                                                                href="{{route('view_cart')}}"
                                                                class="wc-forward button product_type_simple ajax_add_to_cart added_to_cart_button"
                                                                title="@lang('home.view_cart')">@lang('home.view_cart')</a>
                                                    @else
                                                        <a href="{{route('add_to_card',['id' => $product->id])}}"
                                                           data-quantity="1"
                                                           class="button product_type_simple add_to_cart_button ajax_add_to_cart"
                                                           data-product_id="{{$product->id}}" data-product_sku="WS1040"
                                                           aria-label="Add &ldquo;{{$product->name}}&rdquo; to your cart"
                                                           rel="nofollow">@lang('home.add_to_card')</a>
                                                    @endif
                                                </div>


                                            </div>

                                        </div>

                                    </div>
                                    <!--.category-discription-grid-->

                                    <div class="inner-desc">
                                        <p class="description-list">{{Str::limit($product->description, 60)}}</p>
                                        <div class="prod-plugins">
                                            <ul>
                                                <li>
                                                    <div
                                                            class="yith-wcwl-add-to-wishlist add-to-wishlist-369  wishlist-fragment on-first-load"
                                                            data-fragment-ref="369"
                                                            data-fragment-options="{&quot;base_url&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/product-category\/cellphones?product_cat=cellphones&quot;,&quot;wishlist_url&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/wishlist\/&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:369,&quot;parent_product_id&quot;:369,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse Wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in the wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:false,&quot;item&quot;:&quot;add_to_wishlist&quot;}">

                                                        @if(in_array($product->id,request()->wish_list['ids']))
                                                            <div class="yith-wcwl-wishlistaddedbrowse">
                                                                <span class="feedback">@lang('home.product_added_to_wish_list')</span><a href="{{route('wishList')}}" rel="nofollow" data-title="Browse Wishlist">Browse Wishlist	</a>
                                                            </div>
                                                        @else
                                                            <div class="yith-wcwl-add-button">
                                                                <a href="{{route('add_to_wish_list',['id' => $product->id])}}"
                                                                   rel="nofollow" data-product-id="{{$product->id}}"
                                                                   data-product-type="simple"
                                                                   data-original-product-id="{{$product->id}}"
                                                                   class="add_to_wishlist single_add_to_wishlist"
                                                                   data-title="Add to Wishlist">
                                                                    <span>@lang('home.add_to_wish_list')</span>
                                                                </a>
                                                            </div>
                                                        @endif
                                                        <!-- COUNT TEXT -->

                                                    </div>
                                                </li>

                                            </ul>
                                        </div>

                                    </div>

                                </figure>
                                <!-- <div class="clearfix"></div> -->
                            </li>
                        @endforeach
                    </ul>

                </div>

                <script>
                    jQuery(document).ready(function ($) {

                        "use strict";

                        var owl = $('#products-carousel #products');
                        owl.owlCarousel({
                            items: 5,
                            lazyLoad: true,
                            dots: true,
                            responsiveClass: true,
                            nav: true,
                            mouseDrag: true,
                            navText: [
                                "",
                                ""
                            ],
                            responsive: {
                                0: {
                                    items: 2,
                                    nav: false,
                                },
                                600: {
                                    items: 3,
                                    nav: false,
                                },
                                1000: {
                                    items: 4,
                                    nav: true,
                                    dots: false,
                                },
                                1200: {
                                    items: 5,
                                    nav: true,
                                    dots: false,
                                }
                            }
                        });

                    });
                </script>
            </div>
        </div><!-- .row -->
    </div><!-- .single_product_summary_related -->


    <h2 class="all-title">@lang('home.all_products')</h2>

    <div class="row">

        <div class="catalog_top">
            <div class="woocommerce-notices-wrapper"></div>
        </div>


        <div class="xlarge-2 large-3 medium-3 columns sidebar-pos">
            <div class="shop_sidebar wpb_widgetised_column">
                <aside class="widget woocommerce widget_product_categories">
                    <h3 class="widget-title">@lang('home.filters')</h3>
                    <ul class="product-categories">
                        <li class="cat-item cat-item-167 @if(Request::get('product_type')=='watches') current-cat @endif cat-parent">
                            <a
                                    data-filter="product_type"
                                    data-value="watches"
                                    href="">@lang('home.watches')</a> <span
                                    class="count">({{$watches_count}})</span>
                            <ul class='children'>
                                <li class="cat-item cat-item-168 @if(Request::get('watch_type')=='mens') current-cat @endif">
                                    <a
                                            data-filter="watch_type"
                                            data-value="mens"
                                            href="">@lang('home.mens_watches')</a>
                                    <span class="count">({{$mens_watches_count}})</span></li>
                                <li class="cat-item cat-item-169 @if(Request::get('watch_type')=='ladies') current-cat @endif">
                                    <a
                                            data-filter="watch_type"
                                            data-value="ladies"
                                            href="">@lang('home.ladies_watches')</a>
                                    <span
                                            class="count">({{$womens_watches_count}})</span></li>
                            </ul>
                        </li>
                        <li class="cat-item cat-item-180 @if(Request::get('product_type')=='watch_boxes') current-cat  @endif "><a
                                    href="?product_type=watch_boxes">@lang('home.watch_boxes')</a> <span
                                    class="count">({{$watch_boxes_count}})</span></li>
                    </ul>
                </aside>
                <aside class="widget woocommerce widget_price_filter">
                    <h3 class="widget-title">@lang('home.filter_by_price')</h3>

                    <form method="get"
                          action="/">
                        <div class="price_slider_wrapper">
                            <div class="price_slider" style="display:none;"></div>
                            <div class="price_slider_amount" data-step="10">
                                <input type="number" id="min_price" name="min_price"
                                       value="{{ $min_price }}" data-min="0" placeholder="Min price"/>
                                <input type="number" id="max_price" name="max_price"
                                       value="{{ $max_price }}" data-max="1500" placeholder="Max price"/>
                                <button type="button" class="button filter_price"
                                        onclick='document.querySelector(".shop_sidebar [data-filter]").click();'>@lang('home.filter')</button>
                                <div class="price_label" style="display:none;">
                                    <span class="from"></span> &mdash; <span
                                            class="to"></span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </form>

                </aside>
                <aside
                        class="widget woocommerce woodstock_attributes_filter widget_layered_nav">
                    <h3 class="widget-title">@lang('home.filter_by_color')</h3>
                    <ul>
                        <li class="wc-layered-nav-term show-color @if(in_array('black',$colors) ) chosen @endif "
                            data-color="black"><a
                                    data-filter="filter_colors"
                                    data-value="black"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#000000;color:rgba(0,0,0,0.5);"
                                        title="@lang('home.black')"></span><span
                                        class="nav-title">@lang('home.black')</span></a> <span
                                    class="count">12</span></li>
                        <li class="wc-layered-nav-term show-color @if(in_array('brown',$colors) ) chosen @endif  "
                            data-color="brown"><a
                                    data-filter="filter_colors"
                                    data-value="brown"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#a52a2a;color:rgba(165, 42, 42, 0.5);"
                                        title="@lang('home.brown')"></span><span
                                        class="nav-title">@lang('home.brown')</span></a> <span
                                    class="count">12</span></li>
                        <li class="wc-layered-nav-term show-color @if(in_array('gold',$colors) ) chosen @endif "
                            data-color="gold"><a
                                    data-filter="filter_colors"
                                    data-value="gold"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#efbe37;color:rgba(239,190,55,0.5);"
                                        title="@lang('home.gold')"></span><span
                                        class="nav-title">@lang('home.gold')</span></a> <span
                                    class="count">4</span></li>
                        <li class="wc-layered-nav-term show-color @if(in_array('red',$colors) ) chosen @endif "
                            data-color="red"><a
                                    data-filter="filter_colors"
                                    data-value="red"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#dd3333;color:rgba(221,51,51,0.5);"
                                        title="@lang('home.red')"></span><span
                                        class="nav-title">@lang('home.red')</span></a> <span
                                    class="count">4</span></li>
                        <li class="wc-layered-nav-term show-color @if(in_array('silver',$colors) ) chosen @endif "
                            data-color="silver"><a
                                    data-filter="filter_colors"
                                    data-value="silver"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#cccccc;color:rgba(204,204,204,0.5);"
                                        title="@lang('home.silver')"></span><span
                                        class="nav-title">@lang('home.silver')</span></a> <span
                                    class="count">1</span></li>
                        <li class="wc-layered-nav-term show-color @if(in_array('white',$colors) ) chosen @endif "
                            data-color="white"><a
                                    data-filter="filter_colors"
                                    data-value="white"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#ffffff;color:rgba(255,255,255,0.5);box-shadow: 0 0 0 1px #00000014;"
                                        title="@lang('home.white')"></span><span
                                        class="nav-title">@lang('home.white')</span></a> <span
                                    class="count">14</span></li>
                    </ul>
                </aside>
                <aside
                        class="widget woocommerce widget_layered_nav woocommerce-widget-layered-nav">
                    <h3 class="widget-title">@lang('home.mechanism')</h3>
                    <ul class="woocommerce-widget-layered-nav-list">
                        <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term ">
                            <a rel="nofollow"
                               data-filter="mech"
                               data-value="all"
                               href="">@lang('home.all')</a> <span
                                    class="count">({{$watches_count}})</span></li>
                        <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term ">
                            <a rel="nofollow"
                               data-filter="mech"
                               data-value="mechanical"
                               href="">@if(Request::get('mech')=='mechanical') <i
                                        class="fa fa-check"></i> @endif @lang('home.mechanical')</a> <span
                                    class="count">({{$mech_count}})</span></li>
                        <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term ">
                            <a
                                    rel="nofollow"
                                    href=""
                                    data-filter="mech"
                                    data-value="quartz"
                            >@if(Request::get('mech')=='quartz') <i class="fa fa-check"></i> @endif  @lang('home.automatic')
                            </a>
                            <span class="count">({{$watches_count - $mech_count}})</span></li>
                    </ul>
                </aside>
                <aside
                        class="widget woocommerce widget_layered_nav woocommerce-widget-layered-nav">
                    <h3 class="widget-title">@lang('home.on_sale')</h3>
                    <ul class="woocommerce-widget-layered-nav-list">
                        <li
                                class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term ">
                            <a rel="nofollow"
                               data-filter="on_sale"
                               data-value="on_sale"
                               href=""> @if(Request::get('on_sale')=='on_sale') <i
                                        class="fa fa-check"></i> @endif @lang('home.on_sale')</a> <span
                                    class="count">({{$on_sale_count}})</span></li>
                        <li
                                class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term ">
                            <a rel="nofollow"
                               data-filter="on_sale"
                               data-value="all"
                               href="">@lang('home.all')</a>
                            <span class="count">({{$watches_count}})</span></li>
                    </ul>
                </aside>
            </div>
        </div>

        <div class="xlarge-10 large-9 medium-9 columns content-pos">
            <!-- Shop Order Bar -->
            <div class="top_bar_shop">

                <div class="catalog-ordering">


                    <p class="woocommerce-result-count">
                        @lang('home.results',['from' => ($from+1),'to' => ($to<$total_count ? $to : $total_count), 'all' => $total_count])
                    </p>
                    <ul class="shop-ordering">


                        <li>
                            <div class="shop-layout-opts" data-display-type="grid">
                                <a href="#" class="layout-opt tooltip" data-layout="grid"
                                   title="Grid Layout"><i class="grid-icon active"></i></a>
                                <a href="#" class="layout-opt tooltip" data-layout="list"
                                   title="List Layout"><i class="list-icon "></i></a>
                            </div>
                        </li>

                        <li>


                            <form class="woocommerce-viewing" method="get">
                                <select name="count" class="count">
                                    <option value="16" selected='selected'>16</option>
                                    @if($count=='28')
                                        <option selected="selected" value="28">28</option>
                                    @else
                                        <option value="28">28</option>
                                    @endif
                                    @if($count=='40')
                                        <option selected="selected" value="40">40</option>
                                    @else
                                        <option value="40">40</option>
                                    @endif
                                </select>
                                <input type="hidden" name="page" value="1"/>
                                <input type="hidden" name="orderby" value="{{$orderby}}"/>
                            </form>
                        </li>
                        <li>
                            <form class="woocommerce-ordering" method="get">
                                <select name="orderby" class="orderby"
                                        aria-label="Shop order">
                                    <option selected="selected"
                                            value="menu_order">@lang('home.default_sorting')</option>
                                    @if($orderby=='price-asc')
                                        <option selected="selected"
                                                value="price-asc">@lang('home.sort_by_price_l_h')</option>
                                    @else
                                        <option value="price-asc">@lang('home.sort_by_price_l_h')</option>
                                    @endif
                                    @if($orderby=='price-desc')
                                        <option selected="selected"
                                                value="price-desc">@lang('home.sort_by_price_h_l')</option>
                                    @else
                                        <option value="price-desc">@lang('home.sort_by_price_h_l')</option>
                                    @endif
                                </select>
                                <input type="hidden" name="page" value="1"/>
                                <input type="hidden" name="count" value="{{$count}}"/>
                            </form>
                        </li>
                    </ul>
                </div>
                <!--catalog-ordering-->
                <div class="clearfix"></div>
            </div><!-- .top_bar_shop-->
            <div class="woocommerce-notices-wrapper"></div>
            <div class="active_filters_ontop"></div>

            <ul id="products"
                class="product-category-list products products-grid small-block-grid-2 medium-block-grid-3 large-block-grid-4 xlarge-block-grid-4 xxlarge-block-grid-4 columns-4 product-layout-grid">

                @foreach ($products as $product)
                    <li
                            class="product-item  spinner-circle palign-left  product_hover_enable product_hover_mob_disable">
                        <figure class="product-inner">
                            <div class="image-container standart">
                                <a href="{{route('product',['id' =>$product->id]) }}">
                                    <div class="product_thumbnail_wrapper">
                                        <div class="product_thumbnail with_second_image">
																	<span class="product_thumbnail_background"
                                                                          style="background-image:url('{{config('app.uploads_location')}}/{{$product->back_photo_small_url}}')"></span>
                                            <img width="350" height="380"
                                                 src="{{config('app.uploads_location')}}/{{$product->front_photo_small_url}}"
                                                 class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                 alt=""
                                                 sizes="(max-width: 350px) 100vw, 350px"/>
                                        </div>
                                    </div>
                                </a>
                                <div class="ev-attr-swatches"></div>
                            </div>
                            <div class="category-discription-grid-list">
                                <p class="product-category-listing">
                                    <a href="" class="product-category-link">
                                        @if($product->product_type == 'watches')
                                            {{__('home.watches')}}
                                        @else
                                            {{__('home.watch_boxes')}}
                                        @endif
                                    </a>
                                </p>
                                <h4><a class="product-title-link"
                                       href="{{route('product',['id' =>$product->id])}}"></a>{{$product->name}}</h4>
                                <div class="archive-product-rating">
                                </div>
                                <p class="description-list">{{$product->description}}</p>
                            </div>
                            <div class="category-price-grid-list">
                                <span class="price">
                                   @if($product->sale_price>0)
                                        <del>
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price)}}
                                            </span>
                                        </del>
                                    @endif
                                    <ins>
                                        <span class="woocommerce-Price-amount amount">
                                            <span class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price - (int)$product->sale_price)}}
                                        </span>
                                    </ins>
                                </span>

                                <div class="clearfix"></div>
                                <a href="{{route('product',['id' =>$product->id])}}"
                                   class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                </a>
                                @if(in_array($product->id,request()->shopping_cart['ids']))
                                    <a
                                            href="{{route('view_cart')}}"
                                            class="wc-forward button product_type_simple ajax_add_to_cart added_to_cart_button"
                                            title="@lang('home.view_cart')">@lang('home.view_cart')</a>
                                @else
                                    <a href="{{route('add_to_card',['id' => $product->id])}}" data-quantity="1"
                                       class="button product_type_simple add_to_cart_button ajax_add_to_cart"
                                       data-product_id="{{$product->id}}" data-product_sku="WS1040"
                                       aria-label="Add &ldquo;{{$product->name}}&rdquo; to your cart"
                                       rel="nofollow">@lang('home.add_to_card')</a>
                                @endif
                                <div class="clearfix"></div>

                                <div class="prod-plugins">
                                    <ul>
                                        <li>
                                            <div
                                                    class="yith-wcwl-add-to-wishlist add-to-wishlist-369  wishlist-fragment on-first-load"
                                                    data-fragment-ref="369"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/product-category\/cellphones?product_cat=cellphones&quot;,&quot;wishlist_url&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/wishlist\/&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:369,&quot;parent_product_id&quot;:369,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse Wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in the wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:false,&quot;item&quot;:&quot;add_to_wishlist&quot;}">

                                                <!-- ADD TO WISHLIST -->

                                                @if(in_array($product->id,request()->wish_list['ids']))
                                                    <div class="yith-wcwl-wishlistaddedbrowse">
                                                        <span class="feedback">@lang('home.product_added_to_wish_list')</span>
                                                        <a href="{{route('wishList')}}" rel="nofollow" data-title="Browse Wishlist">Browse Wishlist	</a
                                                    </div>
                                                @else
                                                    <div class="yith-wcwl-add-button">
                                                        <a href="{{route('add_to_wish_list',['id' => $product->id])}}"
                                                           rel="nofollow" data-product-id="{{$product->id}}"
                                                           data-product-type="simple"
                                                           data-original-product-id="{{$product->id}}"
                                                           class="add_to_wishlist single_add_to_wishlist"
                                                           data-title="Add to Wishlist">
                                                            <span>@lang('home.add_to_wish_list')</span>
                                                        </a>
                                                    </div>
                                                @endif
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </li>

                                    </ul>
                                </div>


                            </div>
                            <!--.category-price-grid-list-->


                            <div class="category-discription-grid">


                                <p class="product-category-listing"><a href="" class="product-category-link">
                                        @if($product->product_type == 'watches')
                                            {{__('home.watches')}}
                                        @else
                                            {{__('home.watch_boxes')}}
                                        @endif
                                    </a></p>
                                <h4><a class="product-title-link"
                                       href="{{route('product',['id' =>$product->id])}}">{{$product->name}}</a></h4>
                                <div class="archive-product-rating">
                                </div>


                                <div class="product_after_shop_loop">


                                    <div class="product_after_shop_loop_switcher">

                                        <div class="product_after_shop_loop_price">

																	<span class="price">
                                                                       @if($product->sale_price>0)
                                                                            <del>
                                                                                <span
                                                                                        class="woocommerce-Price-amount amount">
                                                                                    <span
                                                                                            class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price)}}
                                                                                </span>
                                                                            </del>
                                                                        @endif
                                                                        <ins>
                                                                            <span
                                                                                    class="woocommerce-Price-amount amount">
                                                                                <span
                                                                                        class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price - (int)$product->sale_price)}}
                                                                            </span>
                                                                        </ins>
                                                                    </span>
                                        </div>

                                        <div class="product_after_shop_loop_buttons">
                                            </a>
                                            @if(in_array($product->id,request()->shopping_cart['ids']))
                                                <a
                                                        href="{{route('view_cart')}}"
                                                        class="wc-forward button product_type_simple ajax_add_to_cart added_to_cart_button"
                                                        title="@lang('home.view_cart')">@lang('home.view_cart')</a>
                                            @else
                                                <a href="{{route('add_to_card',['id' => $product->id])}}"
                                                   data-quantity="1"
                                                   class="button product_type_simple add_to_cart_button ajax_add_to_cart"
                                                   data-product_id="{{$product->id}}" data-product_sku="WS1040"
                                                   aria-label="Add &ldquo;{{$product->name}}&rdquo; to your cart"
                                                   rel="nofollow">@lang('home.add_to_card')</a>
                                            @endif
                                        </div>


                                    </div>

                                </div>

                            </div>
                            <!--.category-discription-grid-->

                            <div class="inner-desc">
                                <p class="description-list">{{Str::limit($product->description, 60)}}</p>


                                <div class="prod-plugins">
                                    <ul>
                                        <li>
                                            <div
                                                    class="yith-wcwl-add-to-wishlist add-to-wishlist-369  wishlist-fragment on-first-load"
                                                    data-fragment-ref="369"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/product-category\/cellphones?product_cat=cellphones&quot;,&quot;wishlist_url&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/wishlist\/&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:369,&quot;parent_product_id&quot;:369,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse Wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in the wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:false,&quot;item&quot;:&quot;add_to_wishlist&quot;}">

                                                <!-- ADD TO WISHLIST -->

                                                @if(in_array($product->id,request()->wish_list['ids']))
                                                    <div class="yith-wcwl-wishlistaddedbrowse">
                                                        <span class="feedback">@lang('home.product_added_to_wish_list')</span>
                                                        <a href="{{route('wishList')}}" rel="nofollow" data-title="Browse Wishlist">Browse Wishlist	</a
                                                    </div>
                                                @else
                                                    <div class="yith-wcwl-add-button">
                                                        <a href="{{route('add_to_wish_list',['id' => $product->id])}}"
                                                           rel="nofollow" data-product-id="{{$product->id}}"
                                                           data-product-type="simple"
                                                           data-original-product-id="{{$product->id}}"
                                                           class="add_to_wishlist single_add_to_wishlist"
                                                           data-title="Add to Wishlist">
                                                            <span>@lang('home.add_to_wish_list')</span>
                                                        </a>
                                                    </div>
                                            @endif
                                                <!-- COUNT TEXT -->

                                            </div>
                                        </li>

                                    </ul>
                                </div>

                            </div>

                        </figure>
                        <!-- <div class="clearfix"></div> -->
                    </li>
                @endforeach
            </ul>

            <div class="woocommerce-after-shop-loop-wrapper">
                <nav class="woocommerce-pagination">
                    <ul class='page-numbers'>
                        @if($page!=1)
                            <li>
                                <a class="prev page-numbers" data-page="{{$page-1}}">←</a>
                            </li>
                        @endif
                        @if($page==1)
                            <li>
                                <span aria-current="page" class="page-numbers current">1</span>
                            </li>
                            @if($total_page_count>=2)
                                <li>
                                    <a class="page-numbers" data-page="2">2</a>
                                </li>
                            @endif
                            @if($total_page_count>=3)
                                <li>
                                    <a class="page-numbers" data-page="3">3</a>
                                </li>
                            @endif
                        @elseif($page!=$total_page_count)
                            <li>
                                <a class="page-numbers" data-page="{{$page-1}}">{{$page-1}}</a>
                            </li>
                            <li>
                                <span aria-current="page" class="page-numbers current">{{$page}}</span>
                            </li>
                            <li>
                                <a class="page-numbers" data-page="{{$page+1}}">{{$page+1}}</a>
                            </li>
                        @else
                            @if($page-2>0)
                                <li>
                                    <a class="page-numbers" data-page="{{$page-2}}">{{$page-2}}</a>
                                </li>
                            @endif
                            @if($page-1>0)
                                <li>
                                    <a class="page-numbers" data-page="{{$page-1}}">{{$page-1}}</a>
                                </li>
                            @endif
                            <li>
                                <span aria-current="page" class="page-numbers current">{{$page}}</span>
                            </li>
                        @endif
                        @if($page<$total_page_count)
                            <li>
                                <a class="next page-numbers" data-page="{{$page+1}}">→</a>
                            </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div><!-- .large-9 or .large-12 -->


    </div><!-- .row -->

    <script>

        $(document).ready(function () {
            document.querySelector('.single_product_related #products').setAttribute('id', 'best-sellers');
        });

        let full_url = "{!! $full_url !!}";

        document.querySelectorAll('a.page-numbers').forEach(el => {
            let page = el.getAttribute('data-page');
            el.setAttribute('href', updateQueryStringParameter(full_url, 'page', page));
        });

        const elements = document.querySelectorAll(".shop_sidebar [data-filter]");
        let filter_colors_chosen = document.querySelectorAll(".shop_sidebar .show-color.chosen");
        let selected_colors = '';
        for (const el of filter_colors_chosen) {
            selected_colors += el.getAttribute('data-color') + ',';
        }

        for (const el of elements) {
            el.addEventListener('click', function (event) {
                let filter = el.getAttribute('data-filter');
                let value = el.getAttribute('data-value');

                let max_value = document.querySelector("#max_price").value;
                let min_value = document.querySelector("#min_price").value;

                if (filter === 'product_type') {
                    full_url = updateQueryStringParameter(full_url, 'watch_type', '');
                }
                if (filter === 'watch_type') {
                    full_url = updateQueryStringParameter(full_url, 'product_type', 'watches');
                }
                if (filter === 'filter_colors') {
                    if (selected_colors.match(value)) {

                        selected_colors = selected_colors.replace(value, '');
                        value = '';
                    }
                    value = selected_colors + value;
                }
                full_url = updateQueryStringParameter(full_url, 'min_price', min_value);
                full_url = updateQueryStringParameter(full_url, 'max_price', max_value);

                el.setAttribute('href', updateQueryStringParameter(full_url, filter, value));
            })
        }


    </script>
@endsection


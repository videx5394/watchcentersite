@extends('layouts.app')

@section('content')

    <div id="content" class="site-content" role="main" style="">

        <div class="row woocommerce">
            <div class="large-12 columns wc-notice">
            </div>
        </div>

        <article id="post-36" class="post-36 page type-page status-publish hentry">

            <div class="row">
                <div class="large-12 columns">

                    <div class="entry-content">
                        <div class="woocommerce">
                            <style>
                                .site-content {
                                    margin-top: 80px;
                                    margin-bottom: 80px;
                                }
                            </style>


                            <div class="row">
                                <div class="medium-10 medium-centered large-6 large-centered columns">

                                    <div class="woocommerce-notices-wrapper"></div>
                                    <div class="login-register-container">

                                        <div class="row">


                                            <div class="large-12 columns">
                                                <div class="account-forms-container">
                                                    <ul class="account-tab-list">

                                                        <li class="account-tab-item"><a class="account-tab-link current"
                                                                                        href="#login">{{ __('Login') }}</a>
                                                        </li>

                                                        <li class="account-tab-item last"><a class="account-tab-link"
                                                                                             href="#register">{{ __('Register') }}</a>
                                                        </li>

                                                    </ul>

                                                    <div class="account-forms">
                                                        <form id="login" method="post"

                                                              action="{{ route('login') }}"
                                                              class="login-form woocommerce-form woocommerce-form-login login">
                                                            @csrf


                                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                                <label for="username">{{ __('E-Mail Address') }}
                                                                    &nbsp;<span class="required">*</span></label>
                                                                <input id="email" type="email"
                                                                       class="form-control @error('email') is-invalid @enderror woocommerce-Input woocommerce-Input--text input-text"
                                                                       name="email" value="{{ old('email') }}" required
                                                                       autocomplete="email" autofocus>
                                                            <div class="col-md-6">


                                                                @error('email')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                            </p>
                                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                                <label
                                                                    for="password">{{ __('Password') }}
                                                                    &nbsp;<span
                                                                        class="required woocommerce-Input woocommerce-Input--text input-text">*</span></label>
                                                                <input id="password" type="password"
                                                                       class="form-control @error('password') is-invalid @enderror woocommerce-Input woocommerce-Input--text input-text"
                                                                       name="password"
                                                                       required autocomplete="current-password">

                                                                @error('password')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </p>


                                                            <p class="form-row">
                                                                <input type="hidden" id="woocommerce-login-nonce"
                                                                       name="woocommerce-login-nonce"
                                                                       value="21d08c22e1"/><input type="hidden"
                                                                                                  name="_wp_http_referer"
                                                                                                  value="/electronics/my-account/"/>
                                                                <input type="submit"
                                                                       class="woocommerce-Button button woocommerce-form-login__submit"
                                                                       name="login" value="Log in"/>

{{--                                                                <label for="rememberme" class="inline">--}}
{{--                                                                    <input--}}
{{--                                                                        class="woocommerce-Input woocommerce-Input--checkbox"--}}
{{--                                                                        name="rememberme" type="checkbox"--}}
{{--                                                                        {{ old('remember') ? 'checked' : '' }}--}}
{{--                                                                        id="rememberme"--}}
{{--                                                                        value="forever"/> {{ __('Remember Me') }}--}}
{{--                                                                </label>--}}
                                                            </p>

                                                            @if (Route::has('password.request'))
{{--                                                                <p class="woocommerce-LostPassword lost_password">--}}
{{--                                                                    <a class="btn btn-link"--}}
{{--                                                                       href="{{ route('password.request') }}">--}}
{{--                                                                        {{ __('Forgot Your Password?') }}--}}
{{--                                                                    </a>--}}
{{--                                                                </p>--}}
                                                            @endif
                                                        </form>


                                                        <form id="register" method="post"
                                                              action="{{ route('register') }}" class="register">
                                                            @csrf
                                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                                <label for="name">{{ __('Name') }}&nbsp;<span
                                                                        class="required">*</span></label>

                                                                <input id="name" type="text"
                                                                       class="form-control woocommerce-Input woocommerce-Input--text input-text @error('name') is-invalid @enderror"
                                                                       name="name" value="{{ old('name') }}"
                                                                       required autocomplete="name" autofocus>
                                                                @error('name')
                                                                <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </p>

                                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                                <label for="email">{{ __('E-Mail Address') }}&nbsp;<span
                                                                        class="required">*</span></label>
                                                                <input type="email"
                                                                       class="woocommerce-Input woocommerce-Input--text input-text @error('email') is-invalid @enderror"
                                                                       name="email" id="email" autocomplete="email"
                                                                       value="{{ old('email') }}"/>
                                                                @error('email')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </p>

                                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                                <label
                                                                    for="password">{{ __('Password') }}
                                                                    &nbsp;<span
                                                                        class="required woocommerce-Input woocommerce-Input--text input-text">*</span></label>
                                                                <input id="password" type="password"
                                                                       class="form-control @error('password') is-invalid @enderror woocommerce-Input woocommerce-Input--text input-text"
                                                                       name="password"
                                                                       required autocomplete="new-password">

                                                                @error('password')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </p>

                                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                                <label
                                                                    for="password-confirm">{{ __('Confirm Password') }}
                                                                    &nbsp;<span
                                                                        class="required woocommerce-Input woocommerce-Input--text input-text">*</span></label>
                                                                <input id="password-confirm" type="password"
                                                                       class="woocommerce-Input woocommerce-Input--text input-text"
                                                                       name="password_confirmation"
                                                                       required autocomplete="new-password">
                                                            </p>

                                                            <div class="woocommerce-privacy-policy-text"></div>

                                                            <p class="woocommerce-FormRow form-row">
                                                                <input type="hidden" id="woocommerce-register-nonce"
                                                                       name="woocommerce-register-nonce"
                                                                       value="b08aa5e6b9"/><input type="hidden"
                                                                                                  name="_wp_http_referer"
                                                                                                  value="/electronics/my-account/"/>
                                                                <input type="submit" class="woocommerce-Button button"
                                                                       name="register" value="Register"/>
                                                            </p>


                                                        </form>


                                                    </div><!-- .account-forms-->
                                                </div><!-- .account-forms-container-->
                                            </div><!-- .medium-8-->
                                        </div><!-- .row-->
                                    </div><!-- .login-register-container-->


                                </div><!-- .large-6-->
                            </div><!-- .rows-->
                        </div>
                    </div><!-- .entry-content -->

                </div><!-- .columns -->
            </div><!-- .row -->

        </article><!-- #post -->
        <div class="clearfix"></div>
    </div><!-- #content -->



@endsection

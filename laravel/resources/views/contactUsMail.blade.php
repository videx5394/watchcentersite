<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Order</title>
</head>
<body>
<h2>Concat Us</h2>
<table style="text-align: center">
    <thead>
    <tr>
        <th>Field</th>
        <th>Value</th>
    </tr>
    </thead>
   <tbody>
   <tr>
       <td>Name</td>
       <td>{{$request['name']}}</td>
   </tr>
   <tr>
       <td>Email</td>
       <td>{{$request['email']}}</td>
   </tr>
   <tr>
       <td>Subject</td>
       <td>{{$request['subject']}}</td>
   </tr>
   <tr>
       <td>Message</td>
       <td>{{$request['message']}}</td>
   </tr>
   </tbody>
</table>
</body>
</html>

@extends('layouts.app')

@section('content')

    <div class="row woocommerce">
        <div class="large-12 columns wc-notice">
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    @if(count($products)==0)
        <article id="post-34" class="post-34 page type-page status-publish hentry">

            <div class="row">
                <div class="large-12 columns">

                    <div class="entry-content">
                        <div class="woocommerce">
                            <style>
                                .site_header { display: none; }
                            </style>

                            <div class="row cart-empty">
                                <div class="large-10 text-center large-centered columns">

                                    <div class="cart-empty-icon"></div>

                                    <p class="cart-empty-text"> @lang('checkout.cart_is_empty')	</p>

                                    <p class="return-to-shop">
                                        <a class="button wc-backward" href="{{asset('home')}}/">
                                            @lang('checkout.return_to_shop') 			</a>
                                    </p>


                                </div><!-- .large-10-->
                            </div><!-- .row--></div>
                    </div><!-- .entry-content -->

                </div><!-- .columns -->
            </div><!-- .row -->

        </article><!-- #post -->
    @else
        <article id="post-34" class="post-34 page type-page status-publish hentry">

            <div class="row">
                <div class="large-12 columns">

                    <div class="entry-content">
                        <div class="woocommerce">
                            <style>
                                .site_header.with_featured_img,
                                .site_header.without_featured_img {
                                    margin-bottom: 70px;
                                }
                            </style>

                            <div class="woocommerce-notices-wrapper"></div>
                            <form class="woocommerce-cart-form"
                                  action="{{route('view_cart')}}" method="post">

                                <div class="cart_container">


                                    <div class="row">
                                        <div class="large-12 large-centered columns">


                                            <div class="row">
                                                <div class="large-7 columns">

                                                    <div class="cart_left_wrapper">

                                                        <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"
                                                               cellspacing="0">

                                                            <thead>
                                                            <tr>
                                                                <th class="product-thumbnail-thead">@lang('viewCart.product')</th>
                                                                <th class="product-name-thead">@lang('viewCart.name')</th>
                                                                <th class="product-price-thead">@lang('viewCart.price')</th>
                                                                <th class="product-subtotal-thead">&nbsp;</th>
                                                                <th class="product-remove-thead">&nbsp;</th>
                                                            </tr>
                                                            </thead>

                                                            <tbody>


                                                            @foreach($products as $product)
                                                                <tr class="woocommerce-cart-form__cart-item cart_item" data-price="{{sprintf("%.2f",$product->price - (int)$product->sale_price)}}">
                                                                    <td class="product-thumbnail">
                                                                        <a href="{{route('product',['id'=>$product->id])}}">
                                                                            <img
                                                                                width="350" height="380"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                src="{{config('app.uploads_location')}}/{{$product->front_photo_small_url}}"
                                                                                alt="">
                                                                        </a>
                                                                    </td>

                                                                    <td class="product-name">
                                                                        <a href="{{route('product',['id'=>$product->id])}}">{{$product->name}}</a>
                                                                    </td>



                                                                    <td class="product-subtotal" data-title="Subtotal">
                                                                <span class="woocommerce-Price-amount amount"><span
                                                                        class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price - (int)$product->sale_price)}}</span>
                                                                    </td>

                                                                    <td class="product-remove">
                                                                        <a
                                                                            onclick="removeProductFromCart($(this),{{$product->id}},'{{route('remove_from_card_ajax')}}')"
                                                                            class="remove" aria-label="Remove this item"
                                                                            data-product_id="{{$product->id}}"
                                                                            data-product_sku="WS1025">
                                                                            <i class="icon-close-regular"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach


                                                            </tbody>
                                                        </table>

                                                        <div class="coupon_code_wrapper">

                                                            <h4 class="coupon_code_text">@lang('viewCart.coupon')</h4>

                                                            <div class="coupon_code_wrapper_inner">

                                                                <input name="coupon_code" class="input-text"
                                                                       id="coupon_code" value=""
                                                                       placeholder="@lang('viewCart.coupon_code')">
                                                                <button onclick="applyCoupon()" type="button" class="button" name="apply_coupon" value="Apply coupon">@lang('viewCart.apply_coupon')</button>
                                                            </div>
                                                        </div>


                                                    </div><!--.cart_left_wrapper-->

                                                </div><!-- .large-7 -->

                                                <div class="large-5 columns">

                                                    <div class="cart_right_wrapper bordered">

                                                        <div class="cart-collaterals">

                                                            <div class="cart-totals-wrapper">

                                                                <div class="cart_totals ">


                                                                    <h2>@lang('viewCart.cart_totals')</h2>

                                                                    <table cellspacing="0">

                                                                        <tbody>
                                                                        <tr class="cart-subtotal">
                                                                            <th>@lang('viewCart.subtotal')</th>
                                                                            <td data-title="@lang('viewCart.subtotal')"><span
                                                                                    class="woocommerce-Price-amount amount"><span
                                                                                        class="woocommerce-Price-currencySymbol">₼</span><span
                                                                                        class="subtotal">{{sprintf("%.2f",$price)}}</span></span>
                                                                            </td>
                                                                        </tr>

                                                                        <tr class="cart-subtotal">
                                                                            <th>@lang('viewCart.coupon')</th>
                                                                            <td data-title="@lang('viewCart.coupon')"><span
                                                                                    class="woocommerce-Price-amount amount"><span
                                                                                        class="woocommerce-Price-currencySymbol coupon"></span>%</span>
                                                                            </td>
                                                                        </tr>


                                                                        <tr class="woocommerce-shipping-totals shipping">
                                                                            <th>@lang('viewCart.shipping')</th>
                                                                            <td data-title="@lang('viewCart.shipping')">
                                                                                <ul id="shipping_method"
                                                                                    class="woocommerce-shipping-methods">
                                                                                    <li>
                                                                                        <input type="radio"
                                                                                               name="shipping_method[0]"
                                                                                               data-index="0"
                                                                                               id="shipping_method_0_legacy_free_shipping"
                                                                                               value="baku"
                                                                                               checked="checked"
                                                                                               class="shipping_method"><label
                                                                                            for="shipping_method_0_legacy_free_shipping">@lang('viewCart.baku')</label>
                                                                                    </li>
                                                                                    <li>
                                                                                        <input type="radio"
                                                                                               name="shipping_method[0]"
                                                                                               data-index="0"
                                                                                               id="shipping_method_0_legacy_flat_rate"
                                                                                               value="cities"
                                                                                               class="shipping_method"
                                                                                        ><label
                                                                                            for="shipping_method_0_legacy_flat_rate">@lang('viewCart.other_cities')
                                                                                            : <span
                                                                                                class="woocommerce-Price-amount amount"><span
                                                                                                    class="woocommerce-Price-currencySymbol">₼</span>3.00</span></label>
                                                                                    </li>

                                                                                </ul>
                                                                            </td>
                                                                        </tr>


                                                                        <tr class="order-total">
                                                                            <th>@lang('viewCart.total_price')</th>
                                                                            <td data-title="Total"><strong><span
                                                                                        class="woocommerce-Price-amount amount"><span
                                                                                            class="woocommerce-Price-currencySymbol">₼</span><span class="total_price">{{sprintf("%.2f",$price)}}</span></span></strong>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>


                                                                </div>
                                                            </div>

                                                            <div class="cart-buttons">

                                                                <a href=""
                                                                   onclick="proceedToCheckout($(this))"
                                                                   class="checkout-button button alt wc-forward">
                                                                    @lang('viewCart.proceed_to_checkout')</a>

                                                                <input type="hidden" id="woocommerce-cart-nonce"
                                                                       name="woocommerce-cart-nonce"
                                                                       value="851852e924"><input type="hidden"
                                                                                                 name="_wp_http_referer"
                                                                                                 value="/electronics/cart/">
                                                            </div><!--.cart-buttons-->

                                                        </div><!-- .cart-collaterals -->

                                                    </div><!--.cart_right_wrapper-->

                                                </div><!-- .large-5 -->
                                            </div><!-- .row -->

                                        </div><!-- .large-9 -->
                                    </div><!-- .row -->


                                    <div class="row">
                                        <div class="large-12 columns">


                                        </div><!-- .large-10 -->
                                    </div><!-- .row -->


                                </div><!-- .cart_container-->
                            </form>
                        </div>
                    </div><!-- .entry-content -->

                </div><!-- .columns -->
            </div><!-- .row -->

        </article><!-- #post -->

    @endif


    <script>

        var price = parseFloat("{{sprintf("%.2f",$price)}}");
        var shipping_method = 'baku';
        var sale = 0;

        let proceedToCheckout = (element) => {

          let url = "{{route('checkout')}}";

          url = updateQueryStringParameter(url, 'shipping', shipping_method);

          element.attr('href',url);
        };

        $('.shipping_method').change(function () {
            shipping_method = $(this).val();
            updateTotalprice();
        });

        async function applyCoupon() {
            let value = $('#coupon_code').val();

            const data = {
                coupon: value
            };

            addLoading($('.coupon_code_wrapper'));


            const response = await fetch("{{route('applyCoupon')}}", {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client
                body: JSON.stringify(data) // body data type must match "Content-Type" header
            });

            if(response.status === 200)
            {
                $('.cart-subtotal .coupon').text('');

                const result = await response.json();
                sale = parseInt(result.sale);

                updateTotalprice();

                if(sale > 0)
                {
                    Swal.fire({
                        title: "@lang('checkout.coupon_added_title')",
                        text: "@lang('checkout.coupon_added_message')",
                        icon: 'success',
                        confirmButtonText: 'OK'
                    });
                    $('.cart-subtotal .coupon').text(sale);
                }
                else {
                    $('#coupon_code').val('');
                }

                removeLoading($('.coupon_code_wrapper'));
            }

        }

        function updateTotalprice() {

            let total_price = price;

            total_price = roundTo(total_price - total_price*sale/100,2);

            let formatted_total_price = total_price.toString().split(/\./);



            if(shipping_method == 'cities')
            {
                total_price += 3;
            }
            else
            {
                //total_price += -3;
            }

            if(formatted_total_price[1] && formatted_total_price[1].length === 1)
            {
                total_price = total_price.toString() + '0';
            }

            $('.order-total .total_price').text(total_price);
        }

        async function removeProductFromCart(element,product_id,route)
        {
            addLoading($('.shop_table'));

            const product_price = parseInt(element.closest('tr').attr('data-price'));

            price = roundTo(price - product_price,2);

            $('.cart-subtotal .subtotal').text(price);

            const data = {
                product_id: product_id
            };
            const response = await fetch(route, {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client
                body: JSON.stringify(data) // body data type must match "Content-Type" header
            });

            if(response.status === 200)
            {
                element.closest('tr').remove();

                updateTotalprice();

                const result = await response.json();

                if(result.fragments)
                {
                    $('.widget_shopping_cart_content').html(result.fragments['div.widget_shopping_cart_content']);

                    let product_count = $('.widget_shopping_cart_content .product_list_widget tbody tr').length;
                    $('.shop-bag .shopbag_items_number').text(product_count);

                    $('.shop-bag .overview .amount').html(result.fragments['.shop-bag .overview .amount']);
                    $('.shop-bag .bag-items-number').text($('.shop-bag .bag-items-number')[0].innerText.replace(/[0-9]+/g,product_count));
                }
            }

            removeLoading($('.shop_table'));
        }

    </script>



@endsection

@extends('layouts.app')

@section('content')
    <style>
        .wrapper {
            margin: 10px auto;
            width: 80%;
            text-align: center;
        }
    </style>
    <div class="wrapper">
        <h2>@lang('paymentDeclined.invalid_payment')</h2>
        <p>@lang('paymentDeclined.error_happened')</p>
        <p>@lang('paymentDeclined.error_reasons')</p>
        <h2>@lang('paymentDeclined.possible_reasons')</h2>
        <p>@lang('paymentDeclined.insufficient_fund')</p>
        <p>@lang('paymentDeclined.confirmation_error')</p>
        <br>
        <em>@lang('paymentDeclined.contact_us')</em>
    </div>
@endsection

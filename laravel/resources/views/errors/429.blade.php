@extends('errors::minimal')

@section('title', __('Too Many Requests'))
@section('code', '429')
@section('message', "Sorğunuz yoxlanılır zəhmət olmasa 1 dəqiqədən sonra səhifəni yeniləyin")

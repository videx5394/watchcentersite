@extends('layouts.app')

@section('content')
    <style>
        #payment .cash {
            font-size: 44px;
            color: green;
            position: relative;
            top: 10px;
            right: 5px;
        }

        #payment img, i {
            margin-left: 10px;
        }

        .form_errors li {
            color: red;
            font-size: 20px;
        }

        .bircartTable
        {
            border: 1px solid #cf33421a;
            margin-top: 20px;
            margin-bottom: 40px;
            margin-right: 0;
            margin-left: 0;
            display: none;
        }

        .bircartTable table
        {
            margin-bottom: 0px;
        }

        .bircartTable tr td,th
        {
            text-align: center !important;
            padding: 10px !important;
        }

        .bircartTable tr td
        {
            color: rgb(207,51,66)
        }

        .bircartTable button
        {
            width: 100px;
            height: 40px;
            font-size: 13px;
            padding: 0;
        }

        .bircartTable .back
        {
            background-color: #ff0000a1;
        }

        .bircartTable .confirm
        {
            background-color: #79a6e9;
        }

        .bircartTable thead
        {
            background-color: rgb(207,51,66);
            color: white;
            line-height: 3;
            padding: 10px;
        }
    </style>
    <script src="{{asset('js/coupon.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        const coupon = new Coupon("{{sprintf("%.2f",$price)}}", "{{$totals->sale}}", "{{$totals->shipping}}", "{{route('applyCoupon')}}", () => {
            Swal.fire({
                title: "@lang('checkout.coupon_added_title')",
                text: "@lang('checkout.coupon_added_message')",
                icon: 'success',
                confirmButtonText: 'OK'
            });
        });
    </script>
    <div class="row woocommerce">
        <div class="large-12 columns wc-notice">
        </div>
    </div>

    @if(count($products)==0)
        <div class="row woocommerce">
            <div class="large-12 columns wc-notice">

                <div class="woocommerce-info">
                    @lang('checkout.checkout_is_not_available')    </div>
            </div>
        </div>
        <article id="post-34" class="post-34 page type-page status-publish hentry">

            <div class="row">
                <div class="large-12 columns">

                    <div class="entry-content">
                        <div class="woocommerce">
                            <style>
                                .site_header {
                                    display: none;
                                }
                            </style>

                            <div class="row cart-empty">
                                <div class="large-10 text-center large-centered columns">

                                    <div class="cart-empty-icon"></div>

                                    <p class="cart-empty-text"> @lang('checkout.cart_is_empty')    </p>

                                    <p class="return-to-shop">
                                        <a class="button wc-backward" href="{{asset('home')}}/">
                                            @lang('checkout.return_to_shop')            </a>
                                    </p>


                                </div><!-- .large-10-->
                            </div><!-- .row--></div>
                    </div><!-- .entry-content -->

                </div><!-- .columns -->
            </div><!-- .row -->

        </article><!-- #post -->

    @else
        <article id="post-35" class="post-35 page type-page status-publish hentry">
            <div class="row woocommerce-checkout">
                <div class="large-12 columns">

                    <div class="entry-content">
                        <div class="woocommerce">
                            <div class="woocommerce-notices-wrapper"></div>
                            <style>
                                .site_header.with_featured_img,
                                .site_header.without_featured_img {
                                    margin-bottom: 50px;
                                }
                            </style>
                            <div class="checkout_coupon_box">


                                <div class="woocommerce-info">
                                    @lang('checkout.have_a_coupon') <a onclick="toggleCouponBlock()"
                                                                       class="showcoupon">@lang('checkout.click_here')</a>
                                </div>

                                <div class="row">
                                    <div class="large-7 large-centered columns">

                                        <form class="checkout_coupon woocommerce-form-coupon" method="post"
                                              style="display: @if(!is_null(request()->get('coupon'))) block @else none @endif">
                                            <div class="checkout_coupon_inner">
                                                <input type="text" name="coupon_code" class="input-text"
                                                       placeholder="@lang('checkout.coupon_code')" id="coupon_code"
                                                       value="{{$totals->coupon}}">
                                                <button type="button" onclick="coupon.applyCoupon()" class="button"
                                                        name="apply_coupon"
                                                        value="Apply coupon">@lang('checkout.apply_coupon')</button>
                                            </div>
                                        </form>
                                    </div><!-- .large-8-->
                                </div><!-- .row-->

                            </div>
                            <!-- .checkout_coupon_box-->
                            <div class="woocommerce-notices-wrapper"></div>
                            <div class="row">
                                <div class="large-12 columns">

                                    <form name="checkout" method="post" class="checkout woocommerce-checkout"
                                          action="{{route('checkout')}}" novalidate="novalidate">


                                        <div class="row">

                                            <div class="large-7 columns">
                                                <div class="checkout_left_wrapper">

                                                    <div class="col2-set" id="customer_details">

                                                        <div class="col-1">

                                                            <div class="woocommerce-billing-fields">

                                                                <h3>@lang('checkout.billing_details')</h3>

                                                                <div class="alert alert-danger ">
                                                                    <ul class="form_errors">

                                                                    </ul>
                                                                </div>


                                                                <div class="woocommerce-billing-fields__field-wrapper">
                                                                    <p class="form-row form-row-first validate-required"
                                                                       id="firstName_field" data-priority="10">
                                                                        <label for="firstName"
                                                                               class="">@lang('checkout.firstName_placeholder')
                                                                            &nbsp;<abbr
                                                                                    class="required"
                                                                                    title="required">*</abbr></label><span
                                                                                class="woocommerce-input-wrapper"><input
                                                                                    type="text" class="input-text "
                                                                                    name="firstName"
                                                                                    id="firstName"
                                                                                    placeholder="@lang('checkout.firstName_placeholder')"
                                                                                    value=""
                                                                                    autocomplete="given-name"></span>
                                                                    </p>
                                                                    <p class="form-row form-row-last validate-required"
                                                                       id="surname_field" data-priority="20">
                                                                        <label for="surname"
                                                                               class="">@lang('checkout.surname')
                                                                            &nbsp;<abbr
                                                                                    class="required"
                                                                                    title="required">*</abbr></label><span
                                                                                class="woocommerce-input-wrapper"><input
                                                                                    type="text" class="input-text "
                                                                                    name="surname"
                                                                                    id="surname"
                                                                                    placeholder="@lang('checkout.surname_placeholder')"
                                                                                    value=""
                                                                                    autocomplete="family-name"></span>
                                                                    </p>

                                                                    <p class="form-row form-row-wide address-field validate-required"
                                                                       id="address_field" data-priority="50">
                                                                        <label for="address"
                                                                               class="">@lang('checkout.address')
                                                                            &nbsp;<abbr
                                                                                    class="required"
                                                                                    title="required">*</abbr></label><span
                                                                                class="woocommerce-input-wrapper"><input
                                                                                    type="text" class="input-text "
                                                                                    name="address"
                                                                                    id="address"
                                                                                    placeholder="@lang('checkout.address_placeholder')"
                                                                                    value=""
                                                                                    autocomplete="address-line1"
                                                                                    data-placeholder="@lang('checkout.address_placeholder')"></span>
                                                                    </p>

                                                                    <p class="form-row form-row-wide validate-required validate-phone"
                                                                       id="phone_field" data-priority="100">
                                                                        <label
                                                                                for="phone"
                                                                                class="">@lang('checkout.phone')
                                                                            &nbsp;<abbr
                                                                                    class="required"
                                                                                    title="required">*</abbr></label><span
                                                                                class="woocommerce-input-wrapper"><input
                                                                                    type="tel" class="input-text "
                                                                                    name="phone"
                                                                                    id="phone"
                                                                                    placeholder="@lang('checkout.phone_placeholder')"
                                                                                    value="" autocomplete="tel"></span>
                                                                    </p>
                                                                    <p class="form-row form-row-wide validate-required validate-email"
                                                                       id="email_field" data-priority="110">
                                                                        <label
                                                                                for="email"
                                                                                class="">@lang('checkout.email')
                                                                            &nbsp;<abbr
                                                                                    class="required"
                                                                                    title="required">*</abbr></label><span
                                                                                class="woocommerce-input-wrapper"><input
                                                                                    type="email" class="input-text "
                                                                                    name="email"
                                                                                    id="email"
                                                                                    placeholder="@lang('checkout.email_placeholder')"
                                                                                    value="{{$email}}"
                                                                                    autocomplete="email username"></span>
                                                                    </p>
                                                                    <div
                                                                            class="woocommerce-additional-fields__field-wrapper">
                                                                        <p class="form-row notes"
                                                                           id="order_comments_field"
                                                                           data-priority=""><label for="order_comments"
                                                                                                   class="">@lang('checkout.order_notes')</label><span
                                                                                    class="woocommerce-input-wrapper"><textarea
                                                                                        name="order_comments"
                                                                                        class="input-text "
                                                                                        id="order_comments"
                                                                                        placeholder="@lang('checkout.order_notes_placeholder')"
                                                                                        rows="2"
                                                                                        cols="5"></textarea></span>
                                                                        </p>
                                                                    </div>

                                                                    @if($not_in_stock == 1)
                                                                        <div>
                                                                            <input id="order_conditions" type="checkbox"
                                                                                   class="order_conditions">
                                                                            <label style="text-transform: inherit" for="order_conditions">@lang('ordersNotInStock.order_conditions')</label>
                                                                            <a href="#order_conditions"></a>
                                                                        </div>
                                                                        <div>
                                                                            <em>@lang('ordersNotInStock.order_conditions_text')</em>
                                                                        </div>
                                                                    @endif
                                                                </div>

                                                            </div>


                                                        </div>

                                                    </div>


                                                </div><!--.checkout_left_wrapper-->
                                                <div class="bircartTable">
                                                    <table>
                                                        <thead class="thead-dark">
                                                        <tr>
                                                            <th scope="col">@lang('checkout.month_count')</th>
                                                            <th scope="col">@lang('checkout.monthly')</th>
                                                            <th style="text-align: center;" scope="col">@lang('checkout.total')</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>2 @lang('checkout.month') <input type="radio"
                                                                        name="month"
                                                                        value="2"
                                                                        checked="checked"></td>
                                                            <td></td>
                                                            <td rowspan="3" style="text-align: center; center;font-size: 28px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3 @lang('checkout.month') <input type="radio"
                                                                                                name="month"
                                                                                                value="3"
                                                                                                ></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6 @lang('checkout.month') <input type="radio"
                                                                                                name="month"
                                                                                                value="6"
                                                                                                ></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><button type="button" onclick="unFreezePayment()" class="back">@lang('checkout.back')</button></td>
                                                            <td></td>
                                                            <td><button onclick="makeApiOrder()" type="button" class="confirm">@lang('checkout.confirm_order')</button></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div><!--.large-7-->
                                            <div class="large-5 columns">
                                                <div class="checkout_right_wrapper bordered">
                                                    <div class="order_review_wrapper">

                                                        <h3 id="order_review_heading">@lang('checkout.your_order')</h3>

                                                        <div id="order_review"
                                                             class="woocommerce-checkout-review-order">
                                                            <table
                                                                    class="shop_table woocommerce-checkout-review-order-table">
                                                                <thead>
                                                                <tr>
                                                                    <th class="product-name">@lang('checkout.product')</th>
                                                                    <th class="product-total">@lang('checkout.price')</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>

                                                                <tr class="cart-subtotal">
                                                                    <td class="product-name">@lang('viewCart.coupon')</td>
                                                                    <td data-title="@lang('viewCart.coupon')"><span
                                                                                class="woocommerce-Price-amount amount"><span
                                                                                    class="woocommerce-Price-currencySymbol coupon">@if($totals->sale > 0){{$totals->sale}}@endif</span>%</span>
                                                                    </td>
                                                                </tr>

                                                                @foreach($products as $product)
                                                                    <tr class="cart_item">
                                                                        <td class="product-name">
                                                                            {{$product->name}}&nbsp;
                                                                        </td>
                                                                        <td class="product-total">
                                                                    <span class="woocommerce-Price-amount amount"><span
                                                                                class="woocommerce-Price-currencySymbol">₼</span>{{sprintf("%.2f",$product->price - (int)$product->sale_price)}}</span>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach

                                                                </tbody>
                                                                <tfoot>

                                                                <tr class="woocommerce-shipping-totals shipping">
                                                                    <th>@lang('checkout.shipping')</th>
                                                                    <td data-title="@lang('checkout.shipping')">
                                                                        <ul id="shipping_method"
                                                                            class="woocommerce-shipping-methods">
                                                                            <li>
                                                                                <input type="radio"
                                                                                       name="shipping_method[0]"
                                                                                       data-index="0"
                                                                                       id="shipping_method_0_legacy_flat_rate"
                                                                                       value="baku"
                                                                                       checked="checked"
                                                                                       onchange="coupon.shippingMethidChanged($(this))"
                                                                                       class="shipping_method"><label
                                                                                        for="shipping_method_0_legacy_flat_rate">@lang('viewCart.baku')</label>
                                                                            </li>
                                                                            <li>
                                                                                <input type="radio"
                                                                                       name="shipping_method[0]"
                                                                                       data-index="0"
                                                                                       id="shipping_method_0_legacy_free_shipping"
                                                                                       value="cities"
                                                                                       @if($totals->shipping == 'cities') checked="checked"
                                                                                       @endif
                                                                                       class="shipping_method"
                                                                                       onchange="coupon.shippingMethidChanged($(this))"
                                                                                >
                                                                                <label
                                                                                        for="shipping_method_0_legacy_free_shipping">@lang('viewCart.other_cities')
                                                                                    : <span
                                                                                            class="woocommerce-Price-amount amount"><span
                                                                                                class="woocommerce-Price-currencySymbol">₼</span>3.00</span></label>
                                                                            </li>
                                                                        </ul>
                                                                    </td>
                                                                </tr>


                                                                <tr class="order-total">
                                                                    <th>@lang('home.total_price')</th>
                                                                    <td><strong><span
                                                                                    class="woocommerce-Price-amount amount"><span
                                                                                        class="woocommerce-Price-currencySymbol">₼</span><span
                                                                                        class="total_price">{{sprintf("%.2f",$price)}}</span></span></strong>
                                                                    </td>
                                                                </tr>
                                                                </tfoot>
                                                            </table>


                                                            <div id="payment" class="woocommerce-checkout-payment">
                                                                <ul class="wc_payment_methods payment_methods methods">
                                                                    <li>
                                                                        <input type="radio"
                                                                               name="payment[0]"
                                                                               data-index="0"
                                                                               id="payment_method_visa_master"
                                                                               value="visa_master"
                                                                               onchange=""
                                                                               class="payment_method">
                                                                        <label
                                                                                for="payment_method_visa_master">@lang('checkout.visa_master')
                                                                            <img
                                                                                    src="{{asset('images')}}/visa.png"
                                                                                    alt="Visa">
                                                                            <img
                                                                                    src="{{asset('images')}}/mastercard.png"
                                                                                    alt="Master">
                                                                        </label>
                                                                    </li>
                                                                    <li style="margin-top: 15px;">
                                                                        <input type="radio"
                                                                               name="payment[0]"
                                                                               data-index="0"
                                                                               id="payment_method_bircart"
                                                                               value="bircart"
                                                                               onchange=""
                                                                               class="payment_method">
                                                                        <label
                                                                                for="payment_method_bircart">@lang('checkout.bircart')
                                                                            <img
                                                                                    src="{{asset('images')}}/birkart.png"
                                                                                    alt="Visa">
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <input type="radio"
                                                                               name="payment[0]"
                                                                               data-index="0"
                                                                               id="payment_method_cash"
                                                                               value="cash"
                                                                               checked="checked"
                                                                               onchange=""
                                                                               class="payment_method">
                                                                        <label
                                                                                for="payment_method_cash">@lang('checkout.cash')
                                                                            <i title="cash" class="fa fa-money cash"
                                                                               aria-hidden="true"></i>
                                                                        </label>
                                                                    </li>

                                                                </ul>
                                                                <div class="form-row place-order">
                                                                    <noscript>
                                                                        Since your browser does not support JavaScript,
                                                                        or
                                                                        it is disabled, please ensure you click the <em>Update
                                                                            Totals</em> button before placing your
                                                                        order.
                                                                        You may be charged more than the amount stated
                                                                        above
                                                                        if you fail to do so. <br/>
                                                                        <button type="submit" class="button alt"
                                                                                name="woocommerce_checkout_update_totals"
                                                                                value="Update totals">Update totals
                                                                        </button>
                                                                    </noscript>

                                                                    <div class="woocommerce-terms-and-conditions-wrapper">
                                                                        <div class="woocommerce-privacy-policy-text"></div>
                                                                    </div>


                                                                    <button type="button" class="button alt"
                                                                            name="woocommerce_checkout_place_order"
                                                                            id="place_order"
                                                                            value="@lang('checkout.place_order')"
                                                                            onclick="placeOrder()"
                                                                            data-value="@lang('checkout.place_order')"> @lang('checkout.place_order')
                                                                    </button>

                                                                    <input type="hidden"
                                                                           id="woocommerce-process-checkout-nonce"
                                                                           name="woocommerce-process-checkout-nonce"
                                                                           value="ef3b467f1b"><input type="hidden"
                                                                                                     name="_wp_http_referer"
                                                                                                     value="/electronics/?wc-ajax=update_order_review">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!--.order_review_wrapper-->
                                                </div><!--.checkout_right_wrapper-->
                                            </div><!--.large-5-->
                                        </div><!--.row-->
                                    </form>
                                </div><!-- .columns -->
                            </div><!-- .row -->
                        </div>
                    </div><!-- .entry-content -->
                </div><!-- .columns -->
            </div><!-- .row -->
        </article><!-- #post -->
    @endif




    <div class="clearfix"></div>

    <script>

        const not_in_stock = "{{$not_in_stock}}";

        function toggleCouponBlock() {
            $('.checkout_coupon').toggle("slow");
        }

        freezePayment = () =>
        {
            $('#place_order').attr('disabled','disabled');
            $('#order_review input').attr('disabled','disabled');
            $('#customer_details input,textarea').attr('disabled','disabled');
        };

        unFreezePayment = () =>
        {
            $('.bircartTable').hide();
            $('#place_order').removeAttr('disabled');
            $('#order_review input').removeAttr('disabled');
            $('#customer_details input,textarea').removeAttr('disabled');
        };



        fillFormErrors = (errors) => {
            $('.form_errors').html('');
            for (fieldName in errors) {
                let field = errors[fieldName];
                for (idx in field) {
                    let replaceTo = $("[for='"+fieldName+"']").text();
                    if(fieldName === 'firstName')
                    {
                        fieldName = 'first name';
                    }
                    let error = field[idx].replace(fieldName,"'"+replaceTo+"'");
                    $('.form_errors').append(`<li>${error}</li>`);
                }
            }
        };

        alertServerError = () =>
        {
            Swal.fire({
                title: "@lang('checkout.form_error_title')",
                text: "@lang('checkout.form_error_message')",
                icon: 'error',
                confirmButtonText: 'OK'
            });
        };

        handleInvalidPlaceOrder = (result) =>
        {
            if(result.message === 'no products in shopping cart')
            {
                Swal.fire({
                    title: "@lang('checkout.cart_is_empty')",
                    text: "@lang('checkout.checkout_is_not_available')",
                    icon: 'error',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                }).then((result) => {
                    window.location.reload();
                });
            }
            else
            {
                alertServerError();
            }
        };

        makeApiOrder = async () => {
            var data = getPaymentData();
            var divisionCount = 1;
            if(data.payment === 'bircart')
            {
                divisionCount = $('.bircartTable [name="month"]:checked').val();
            }
            data.divisionCount = divisionCount;
            const response = await fetch("{{route('confirmApiOrder')}}", {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client
                body: JSON.stringify(data) // body data type must match "Content-Type" header
            });

            if (response.status === 200) {
                const result = await response.json();
                window.location.href = result.url;
            }
            else if (response.status === 422) {
                const result = await response.json();
                const errors = result.errors;
                fillFormErrors(errors);
            }
            else if (response.status === 503) {
                alertServerError();
            }
            else {
                alertServerError();
            }
        };

        placeCashOrder = async (data) => {
            const response = await fetch("{{route('placeOrder')}}", {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client
                body: JSON.stringify(data) // body data type must match "Content-Type" header
            });

            if (response.status === 200) {
                Swal.fire({
                    title: "@lang('checkout.form_success_title')",
                    text: "@lang('checkout.form_success_message')",
                    icon: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                }).then((result) => {
                    window.location.href = '/';
                })
            } else if (response.status === 422) {
                const result = await response.json();
                const errors = result.errors;
                fillFormErrors(errors);
            }
            else if(response.status === 400) {//no products in shopping cart
                const result = await response.json();
                handleInvalidPlaceOrder(result);
            }
            else {
                alertServerError();
            }
            removeLoading($('.order_review_wrapper'));
        };

        getPaymentData = () => {
            const payment_method = $('#payment .payment_method:checked').val();

            return {
                'firstName': $('#firstName').val(),
                'surname': $('#surname').val(),
                'address': $('#address').val(),
                'phone': $('#phone').val(),
                'email': $('#email').val(),
                'note': $('#order_comments').val(),
                'shipping': coupon.shipping,
                'coupon': $('#coupon_code').val(),
                'payment': payment_method,
                'not_in_stock' : not_in_stock
            };
        }


        getBircartTable = async (data) => {
            const response = await fetch("{{route('getBircartTable')}}", {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client
                body: JSON.stringify(data) // body data type must match "Content-Type" header
            });

            if (response.status === 200) {
                $('.bircartTable').show();
                freezePayment();
                const result = await response.json();
                $('.bircartTable tbody tr:eq(0) td:eq(2)').text('₼ '+result.totalPrice);
                $('.bircartTable tbody tr:eq(0) td:eq(1)').text('₼ '+result.departed['2']);
                $('.bircartTable tbody tr:eq(1) td:eq(1)').text('₼ '+result.departed['3']);
                $('.bircartTable tbody tr:eq(2) td:eq(1)').text('₼ '+result.departed['6']);
            } else if (response.status === 422) {
                const result = await response.json();
                const errors = result.errors;
                fillFormErrors(errors);
            }
            else if(response.status === 400) {//no products in shopping cart
                const result = await response.json();
                handleInvalidPlaceOrder(result);
            }
            else {
                alertServerError();
            }
            removeLoading($('.order_review_wrapper'));
        };

        placeOrder = async () => {
            // return if oreder conditions checkbox is not check
            if(!$('#order_conditions').is(':checked') && not_in_stock === "1")
            {
                $('[href="#order_conditions"]').click();
                $('[for="order_conditions"]').css('color','red');
                return;
            }
            else
            {
                $('[for="order_conditions"]').css('color','');
            }

            addLoading($('.order_review_wrapper'));

            const payment_method = $('#payment .payment_method:checked').val();

            data = getPaymentData();

            if(payment_method === "cash")
            {
                placeCashOrder(data);
            }
            else if(payment_method === "bircart")
            {
                getBircartTable(data);
            }
            else if(payment_method === "visa_master")
            {
                makeApiOrder();
            }
            removeLoading($('.order_review_wrapper'));
        };

        coupon.updateTotalprice();
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <style>
        .wrapper {
            margin: 10px auto;
            width: 60%;
        }

        @if(app()->getLocale()=='az')
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: "";
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 10.0pt;
            margin-left: 0in;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-ansi-language: RU;
            mso-fareast-language: RU;
        }

        span.maddechar {
            mso-style-name: maddechar;
            mso-style-unhide: no;
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-ansi-language: RU;
            mso-fareast-language: RU;
        }

        .MsoPapDefault {
            mso-style-type: export-only;
            margin-bottom: 10.0pt;
            line-height: 115%;
        }

        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 56.7pt 42.5pt 56.7pt 85.05pt;
            mso-header-margin: 35.4pt;
            mso-footer-margin: 35.4pt;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        @else
p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: "";
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: .25pt;
            margin-left: .5pt;
            text-indent: -.5pt;
            line-height: 111%;
            mso-pagination: widow-orphan;
            font-size: 12.0pt;
            mso-bidi-font-size: 11.0pt;
            font-family: "Times New Roman", serif;
            mso-fareast-font-family: "Times New Roman";
            color: black;
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
        }

        .MsoPapDefault {
            mso-style-type: export-only;
            margin-bottom: 8.0pt;
            line-height: 107%;
        }

        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 55.35pt 42.4pt 58.4pt 85.1pt;
            mso-header-margin: .5in;
            mso-footer-margin: .5in;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        /* List Definitions */
        @list l0 {
            mso-list-id: 47389195;
            mso-list-type: hybrid;
            mso-list-template-ids: -1350539710 1974114800 913984354 1167997608 -989546732 -384546624 -2078645068 -564001278 -1158370920 -1481894346;
        }
        @list l0:level1 {
            mso-level-start-at: 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l0:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l0:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l0:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l0:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l0:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l0:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l0:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l0:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l1 {
            mso-list-id: 167260328;
            mso-list-type: hybrid;
            mso-list-template-ids: -1376454962 56133240 718025852 -862579042 643171528 -977600780 1593359938 -1664206600 -532394358 2012410118;
        }
        @list l1:level1 {
            mso-level-start-at: 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l1:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l1:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l1:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l1:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l1:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l1:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l1:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l1:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l2 {
            mso-list-id: 440339091;
            mso-list-type: hybrid;
            mso-list-template-ids: -1591448474 -600556838 303214138 -1776143346 1433797324 -1076822428 -870134810 -1208311542 -1909132536 -101169120;
        }
        @list l2:level1 {
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 12.0pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l2:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l2:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l2:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l2:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l2:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l2:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l2:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l2:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l3 {
            mso-list-id: 786003184;
            mso-list-type: hybrid;
            mso-list-template-ids: -830974956 -399583706 392484260 -1737597284 -376527448 -1099013692 694598346 1214939602 -395029456 877299144;
        }
        @list l3:level1 {
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l3:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l3:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l3:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l3:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l3:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l3:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l3:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l3:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l4 {
            mso-list-id: 858474620;
            mso-list-type: hybrid;
            mso-list-template-ids: 214093978 849773664 -68934100 -514680792 -1134920996 -1161528188 1053197436 786335986 971650456 -1774058902;
        }
        @list l4:level1 {
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l4:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l4:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l4:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l4:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l4:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l4:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l4:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l4:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l5 {
            mso-list-id: 990596888;
            mso-list-type: hybrid;
            mso-list-template-ids: 1152815446 442813116 600610100 -1807979960 -745095172 -612876758 713474918 -1331809050 -1372443544 428088488;
        }
        @list l5:level1 {
            mso-level-number-format: bullet;
            mso-level-text: *;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 9.0pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l5:level2 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l5:level3 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l5:level4 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l5:level5 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l5:level6 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l5:level7 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l5:level8 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l5:level9 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l6 {
            mso-list-id: 1009602121;
            mso-list-type: hybrid;
            mso-list-template-ids: 744767942 -627441302 -1554055336 -1997636094 -1275458160 -1195055242 375586760 782784048 -803840260 1480354980;
        }
        @list l6:level1 {
            mso-level-start-at: 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l6:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l6:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l6:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l6:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l6:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l6:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l6:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l6:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l7 {
            mso-list-id: 1065444941;
            mso-list-type: hybrid;
            mso-list-template-ids: -304689262 -1477826176 1871892462 1596986430 20751602 2024302902 -1604167900 1826943446 1441576302 -897187226;
        }
        @list l7:level1 {
            mso-level-number-format: bullet;
            mso-level-text: *;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l7:level2 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l7:level3 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l7:level4 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l7:level5 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l7:level6 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l7:level7 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l7:level8 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l7:level9 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l8 {
            mso-list-id: 1093480454;
            mso-list-type: hybrid;
            mso-list-template-ids: 571931798 -1185652010 1019131682 928408234 2013799718 -1783465478 170849534 1009950252 -1534174130 -1456693040;
        }
        @list l8:level1 {
            mso-level-number-format: bullet;
            mso-level-text: *;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 9.0pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l8:level2 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l8:level3 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l8:level4 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l8:level5 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l8:level6 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l8:level7 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l8:level8 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l8:level9 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l9 {
            mso-list-id: 1164125503;
            mso-list-type: hybrid;
            mso-list-template-ids: 833502000 589834818 -1311373722 -993784922 890160876 -830280656 -1380918564 -704852594 938113106 1763106578;
        }
        @list l9:level1 {
            mso-level-number-format: bullet;
            mso-level-text: *;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 9.0pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l9:level2 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l9:level3 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l9:level4 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l9:level5 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l9:level6 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l9:level7 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l9:level8 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l9:level9 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l10 {
            mso-list-id: 1175025725;
            mso-list-type: hybrid;
            mso-list-template-ids: 855777898 -997326036 114727446 -1223903306 857247476 -905671250 -926019436 1771892090 495230758 133312316;
        }
        @list l10:level1 {
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l10:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l10:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l10:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l10:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l10:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l10:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l10:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l10:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l11 {
            mso-list-id: 1343361444;
            mso-list-type: hybrid;
            mso-list-template-ids: -861109870 506343462 -1575342056 1399254124 -2113344332 1665300738 -815244030 -1461708156 2131674580 -84134860;
        }
        @list l11:level1 {
            mso-level-start-at: 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l11:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l11:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l11:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l11:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l11:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l11:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l11:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l11:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l12 {
            mso-list-id: 1545872939;
            mso-list-type: hybrid;
            mso-list-template-ids: -383235210 -2081111150 1301050436 -246105922 -436667518 933553578 954382186 -200387106 -1976033806 629055276;
        }
        @list l12:level1 {
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l12:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l12:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l12:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l12:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l12:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l12:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l12:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l12:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l13 {
            mso-list-id: 1874492096;
            mso-list-type: hybrid;
            mso-list-template-ids: -600543562 -628222344 1615256704 -1356416666 609252722 -145730042 2028372598 1574096842 552662438 1838737258;
        }
        @list l13:level1 {
            mso-level-number-format: bullet;
            mso-level-text: *;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 9.0pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l13:level2 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l13:level3 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l13:level4 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l13:level5 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l13:level6 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l13:level7 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l13:level8 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l13:level9 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l14 {
            mso-list-id: 2049715185;
            mso-list-type: hybrid;
            mso-list-template-ids: -272073174 -1505731356 686879270 445441070 1438264196 -999399322 -1450774684 1773837980 798364466 -308377098;
        }
        @list l14:level1 {
            mso-level-start-at: 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l14:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l14:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l14:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l14:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l14:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l14:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l14:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l14:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l15 {
            mso-list-id: 2060089712;
            mso-list-type: hybrid;
            mso-list-template-ids: 801130210 -1350638378 -2104322658 1793780836 2113948608 -1917447342 2002409364 1185865538 -1889086434 1118590794;
        }
        @list l15:level1 {
            mso-level-number-format: bullet;
            mso-level-text: *;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 9.0pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l15:level2 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l15:level3 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l15:level4 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l15:level5 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l15:level6 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l15:level7 {
            mso-level-number-format: bullet;
            mso-level-text: •;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l15:level8 {
            mso-level-number-format: bullet;
            mso-level-text: o;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l15:level9 {
            mso-level-number-format: bullet;
            mso-level-text: \25AA;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l16 {
            mso-list-id: 2144539484;
            mso-list-type: hybrid;
            mso-list-template-ids: -1085748934 1282847068 -910754696 485288328 -1969331458 285494214 1724025962 798117088 2144862314 747780154;
        }
        @list l16:level1 {
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .5pt;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l16:level2 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 2;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: .75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l16:level3 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 3;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l16:level4 {
            mso-level-text:% 4;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 1.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l16:level5 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 5;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l16:level6 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 6;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 2.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l16:level7 {
            mso-level-text:% 7;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l16:level8 {
            mso-level-number-format: alpha-lower;
            mso-level-text:% 8;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 3.75in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        @list l16:level9 {
            mso-level-number-format: roman-lower;
            mso-level-text:% 9;
            mso-level-tab-stop: none;
            mso-level-number-position: left;
            margin-left: 4.25in;
            text-indent: 0in;
            mso-ansi-font-size: 12.0pt;
            mso-bidi-font-size: 12.0pt;
            color: black;
            border: none;
            mso-ansi-font-weight: normal;
            mso-ansi-font-style: normal;
            text-underline: black;
            text-decoration: none;
            text-underline: none;
            text-decoration: none;
            text-line-through: none;
            vertical-align: baseline;
        }
        ol {
            margin-bottom: 0in;
        }

        ul {
            margin-bottom: 0in;
        }

        -->
        @endif


        -->
    </style>
    <div class="wrapper">
        <hr>

        @if(app()->getLocale()=='az')
            <div class=WordSection1>


                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><b><span lang=AZ-LATIN style='font-size:
12.0pt;
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&#304;stehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n müdafi&#601;si haqq&#305;nda</span></b><span lang=RU
                                                                    style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><b><span lang=AZ-LATIN style='font-size:
12.0pt;
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;</span></b><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span lang=AZ-LATIN style='font-size:
12.0pt;
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>AZ&#399;RBAYCAN
RESPUBL&#304;KASININ QANUNU</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span lang=AZ-LATIN style='font-size:
12.0pt;
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Bu Qanun Az&#601;rbaycan Respublikas&#305;n&#305;n
&#601;razisind&#601; istehlakç&#305;lar üçün b&#601;rab&#601;r &#351;&#601;rait
yarad&#305;lmas&#305; m&#601;qs&#601;di il&#601; alq&#305;-satq&#305;
prosesind&#601;, i&#351; görülm&#601;sind&#601; v&#601; xidm&#601;t
göst&#601;rilm&#601;sind&#601; istehlakç&#305; il&#601; istehsalç&#305;,
sat&#305;c&#305; v&#601; icraç&#305; aras&#305;nda münasib&#601;tl&#601;rin
eyni cür t&#601;nziml&#601;nm&#601;sinin, habel&#601; istehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n müdafi&#601;sinin ümumi hüquqi, iqtisadi v&#601; sosial
&#601;saslar&#305;n&#305; v&#601; mexanizmini
mü&#601;yy&#601;nl&#601;&#351;dirir.</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Qanun BMT-nin Ba&#351; Assambleyas&#305; t&#601;r&#601;find&#601;n
q&#601;bul edilmi&#351; “&#304;stehlakç&#305;lar&#305;n maraqlar&#305;n&#305;
müdafi&#601; etm&#601;k üçün r&#601;hb&#601;r prinsipl&#601;r”
&#601;sas&#305;nda i&#351;l&#601;nmi&#351; v&#601; Az&#601;rbaycan
Respublikas&#305;nda bel&#601; münasib&#601;tl&#601;rin dünya
t&#601;crüb&#601;sin&#601; uy&#287;unla&#351;d&#305;r&#305;lmas&#305;na
yön&#601;ldilmi&#351;dir.</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;text-indent:17.85pt;line-height:normal'><span lang=AZ-LATIN
                                                                style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;text-indent:17.85pt;line-height:normal'><span lang=AZ-LATIN
                                                                style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>I f&#601;sil</span><span
                        style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>ÜMUM&#304; MÜDD&#399;ALAR</span></b><span style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;mso-ansi-language:AZ-LATIN'>Madd&#601; 1.&nbsp;<b>&#399;sas
anlay&#305;&#351;lar</b></span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Bu Qanunda istifad&#601; edil&#601;n anlay&#305;&#351;lar:</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlakç&#305;</span></b><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;— &#351;&#601;xsi
t&#601;l&#601;bat&#305;n&#305; öd&#601;m&#601;k m&#601;qs&#601;di il&#601; mal,
i&#351; v&#601; xidm&#601;tl&#601;rd&#601;n istifad&#601; ed&#601;n,
onlar&#305; alan, sifari&#351; ver&#601;n, yaxud almaq v&#601; ya sifari&#351;
verm&#601;k niyy&#601;ti olan &#351;&#601;xs;</span><span lang=AZ-LATIN
                                                          style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehsalç&#305;&nbsp;</span></b><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;mso-ansi-language:AZ-LATIN'>— mülkiyy&#601;t formas&#305;ndan
v&#601; t&#601;&#351;kilati-hüquqi formas&#305;ndan as&#305;l&#305; olmayaraq
sat&#305;&#351; üçün mal istehsal ed&#601;n mü&#601;ssis&#601;, idar&#601;,
t&#601;&#351;kilat v&#601; ya sahibkar;</span><span lang=AZ-LATIN
                                                    style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>icraç&#305;&nbsp;</span></b><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;mso-ansi-language:AZ-LATIN'>— i&#351; gör&#601;n, yaxud
xidm&#601;t göst&#601;r&#601;n mü&#601;ssis&#601;, idar&#601;,
t&#601;&#351;kilat v&#601; ya sahibkar;</span><span lang=AZ-LATIN
                                                    style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>sat&#305;c&#305;</span></b><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;— al&#287;&#305;-satq&#305;
&#601;m&#601;liyyat&#305; aparan, mal satan mü&#601;ssis&#601;, idar&#601;,
t&#601;&#351;kilat v&#601; ya sahibkar;</span><span lang=AZ-LATIN
                                                    style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>normativ s&#601;n&#601;d</span></b><span lang=AZ-LATIN
                                                   style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;—
mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin) keyfiyy&#601;tin&#601;
v&#601; t&#601;hlük&#601;sizliyin&#601; Az&#601;rbaycan Respublikas&#305;
qanunvericiliyin&#601; uy&#287;un m&#601;cburi t&#601;l&#601;bl&#601;ri
mü&#601;yy&#601;nl&#601;&#351;dir&#601;n dövl&#601;t standartlar&#305;,
farmakoloji, sanitariya v&#601; tikinti normalar&#305;, qaydalar&#305; v&#601;
dig&#601;r s&#601;n&#601;dl&#601;r;</span><span lang=AZ-LATIN style='font-size:
10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>qüsur&nbsp;</span></b><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>— mal&#305;n (i&#351;in, xidm&#601;tin)
normativ s&#601;n&#601;dl&#601;rin t&#601;l&#601;bl&#601;rin&#601;,
müqavil&#601; &#351;&#601;rtl&#601;rin&#601;, yaxud ir&#601;li sürül&#601;n
ba&#351;qa t&#601;l&#601;bl&#601;r&#601;, habel&#601; icraç&#305; v&#601; ya
sat&#305;c&#305;n&#305;n mal (i&#351;, xidm&#601;t) haqq&#305;nda verdiyi
m&#601;lumata uy&#287;unsuzlu&#287;u;</span><span lang=AZ-LATIN
                                                  style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mühüm qüsur</span></b><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>&nbsp;— maldan (i&#351;d&#601;n,
xidm&#601;td&#601;n) onun m&#601;qs&#601;dli t&#601;yinat&#305;na müvafiq
sur&#601;td&#601; istifad&#601; olunmas&#305;n&#305; qeyri-mümkün v&#601; ya
yolverilm&#601;z ed&#601;n, istehlakç&#305; üçün aradan qald&#305;r&#305;la
bilm&#601;y&#601;n, aradan qald&#305;r&#305;lmas&#305; üçün çoxlu
&#601;m&#601;k v&#601; vaxt m&#601;sr&#601;fi t&#601;l&#601;b ed&#601;n,
mal&#305; (i&#351;i, xidm&#601;ti) müqavil&#601;d&#601; n&#601;z&#601;rd&#601;
tutuldu&#287;undan ba&#351;qa &#351;&#601;kl&#601; salan, ya da aradan
qald&#305;r&#305;ld&#305;qdan sonra yenid&#601;n üz&#601; ç&#305;xan qüsur;</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>uy&#287;unluq sertifikat&#305; (sertifikat)</span></b><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;— sertifikatla&#351;d&#305;rma sisteminin qaydalar&#305;
üzr&#601; sertifikatla&#351;d&#305;r&#305;lm&#305;&#351;&nbsp;</span><i><span
                            lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>mal&#305;n (xidm&#601;tin, i&#351;in),
prosesin, idar&#601;etm&#601; sisteminin v&#601; i&#351;çi hey&#601;tinin
müvafiq standartda, texniki reqlamentd&#601; v&#601; dig&#601;r normativ hüquqi
aktda mü&#601;yy&#601;n olunmu&#351; t&#601;l&#601;bl&#601;r&#601;</span></i><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;uy&#287;unlu&#287;unu t&#601;sdiq etm&#601;k üçün verilmi&#351;
s&#601;n&#601;d;</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black;
mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>uy&#287;unluq ni&#351;an&#305;</span></b><span lang=AZ-LATIN
                                                         style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;—&nbsp;</span><i><span
                            lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>mal&#305;n (xidm&#601;tin, i&#351;in),
prosesin, idar&#601;etm&#601; sisteminin v&#601; i&#351;çi hey&#601;tinin
müvafiq standartda, texniki reqlamentd&#601; v&#601; dig&#601;r normativ hüquqi
aktda mü&#601;yy&#601;n olunmu&#351; t&#601;l&#601;bl&#601;r&#601;</span></i><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;uy&#287;unlu&#287;unu göst&#601;r&#601;n,
sertifikatla&#351;d&#305;rma sisteminin qaydalar&#305;na uy&#287;un
veril&#601;n v&#601; t&#601;tbiq edil&#601;n, mü&#601;yy&#601;n olunmu&#351;
qaydada qeydiyyata al&#305;nan ni&#351;and&#305;r;</span><a name="_ednref1"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn1"><span
                            style='mso-bookmark:_ednref1'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[1]</span></sup></b></span><span
                            style='mso-bookmark:_ednref1'></span></a><span style='mso-bookmark:_ednref1'></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mal&#305;n (i&#351;in, xidm&#601;tin) t&#601;hlük&#601;sizliyi</span></b><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;— maldan (i&#351;in, xidm&#601;tin
n&#601;tic&#601;l&#601;rind&#601;n) istifad&#601; edilm&#601;sinin, onun
saxlanmas&#305;n&#305;n, da&#351;&#305;nmas&#305;n&#305;n,
i&#351;l&#601;dilm&#601;sinin adi &#351;&#601;raitind&#601; v&#601; ya
i&#351;in görülm&#601;si (xidm&#601;t göst&#601;rilm&#601;si) prosesind&#601;
istehlakç&#305;n&#305;n h&#601;yat&#305;na, sa&#287;laml&#305;&#287;&#305;na,
&#601;mlak&#305;na, habel&#601; &#601;traf mühit&#601; z&#601;r&#601;r vurulmas&#305;n&#305;n
istisna olunmas&#305;;</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>z&#601;man&#601;t müdd&#601;ti</span></b><span lang=AZ-LATIN
                                                         style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;—
müvafiq normativ s&#601;n&#601;dl&#601;rd&#601; n&#601;z&#601;rd&#601;
tutulmu&#351; z&#601;man&#601;t öhd&#601;likl&#601;rinin qüvv&#601;d&#601; oldu&#287;u
müdd&#601;t.</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black;
mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>n&#601;zar&#601;t markas&#305;</span></b><span lang=AZ-LATIN
                                                         style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;—
audiovizual &#601;s&#601;rin, fonoqram&#305;n, kompyuter proqram&#305;n&#305;n,
m&#601;lumat toplusunun, kitab&#305;n nüsx&#601;l&#601;rinin v&#601; dig&#601;r
mü&#601;lliflik v&#601; &#601;laq&#601;li hüquq obyektl&#601;rinin
mü&#601;lliflik hüququ v&#601; &#601;laq&#601;li hüquqlara &#601;m&#601;l
edilm&#601;kl&#601; haz&#305;rland&#305;&#287;&#305;n&#305; (istehsal
olundu&#287;unu), h&#601;mçinin sat&#305;&#351;&#305; üçün müvafiq dövl&#601;t
orqan&#305;ndan raz&#305;l&#305;&#287;&#305;n al&#305;nmas&#305;
t&#601;l&#601;b olunan mallar&#305;n, m&#601;mulatlar&#305;n v&#601;
m&#601;lumat materiallar&#305;n&#305;n sat&#305;&#351;&#305;na bel&#601;
raz&#305;l&#305;&#287;&#305;n verilm&#601;sini t&#601;sdiq ed&#601;n v&#601;
h&#601;min nüsx&#601;l&#601;ri yaymaq hüququ ver&#601;n,
nüsx&#601;l&#601;r&#601; yap&#305;&#351;d&#305;r&#305;lan
h&#601;rf-r&#601;q&#601;m kodu v&#601; holoqrafik müdafi&#601; vasit&#601;si
olan v&#601; bird&#601;f&#601;lik istifad&#601; üçün n&#601;z&#601;rd&#601;
tutulan vahid nümun&#601;li xüsusi ni&#351;an.</span><a name="_ednref2"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn2"><span
                            style='mso-bookmark:_ednref2'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[2]</span></sup></b></span><span
                            style='mso-bookmark:_ednref2'></span></a><span style='mso-bookmark:_ednref2'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;2.&nbsp;<b>&#304;stehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n
müdafi&#601;si haqq&#305;nda Az&#601;rbaycan Respublikas&#305;n&#305;n
qanunvericiliyi</b></span><span lang=RU style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n müdafi&#601;si
haqq&#305;nda Az&#601;rbaycan Respublikas&#305;n&#305;n
qanunvericiliyi&nbsp;Az&#601;rbaycan Respublikas&#305;n&#305;n Mülki
M&#601;c&#601;ll&#601;sind&#601;n,&nbsp;bu Qanundan v&#601; bu Qanuna müvafiq
q&#601;bul edilmi&#351; dig&#601;r normativ-hüquqi aktlardan ibar&#601;tdir.</span><a
                        name="_ednref3"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn3"><span
                            style='mso-bookmark:_ednref3'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[3]</span></sup></b></span><span
                            style='mso-bookmark:_ednref3'></span></a><span style='mso-bookmark:_ednref3'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Az&#601;rbaycan Respublikas&#305;n&#305;n beyn&#601;lxalq
müqavil&#601;l&#601;rind&#601; mü&#601;yy&#601;nl&#601;&#351;dirilmi&#351;
qaydalar bu Qanunda n&#601;z&#601;rd&#601; tutulanlardan f&#601;rqli olarsa,
beyn&#601;lxalq müqavil&#601;l&#601;rin qaydalar&#305; t&#601;tbiq edilir.</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><i><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#399;l&#601;t azad iqtisadi zonas&#305;nda istehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n müdafi&#601;si sah&#601;sind&#601;
münasib&#601;tl&#601;r “&#399;l&#601;t azad iqtisadi zonas&#305; haqq&#305;nda”
Az&#601;rbaycan Respublikas&#305; Qanununun t&#601;l&#601;bl&#601;rin&#601;
uy&#287;un olaraq t&#601;nziml&#601;nir.</span></i><a name="_ednref4"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn4"><span
                            style='mso-bookmark:_ednref4'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[4]</span></sup></b></span><span
                            style='mso-bookmark:_ednref4'></span></a><span style='mso-bookmark:_ednref4'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;text-indent:17.85pt;line-height:normal'><span lang=AZ-LATIN
                                                                style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>II
f&#601;sil</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;STEHLAKÇILARIN HÜQUQLARI</span></b><span lang=RU
                                                         style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;3<b>. &#304;stehlakç&#305;lar&#305;n hüquqlar&#305;</b></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Az&#601;rbaycan Respublikas&#305; &#601;razisind&#601;
istehlakç&#305;lar a&#351;a&#287;&#305;dak&#305; hüquqlara malikdir:</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin) v&#601;
onlar&#305;n istehsalç&#305;s&#305;n&#305;n, icraç&#305;s&#305;n&#305;n v&#601;
sat&#305;c&#305;s&#305;n&#305;n s&#601;rb&#601;st seçilm&#601;sin&#601;;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlak etdikl&#601;ri mallar&#305;n (i&#351;l&#601;rin,
xidm&#601;tl&#601;rin) laz&#305;mi keyfiyy&#601;td&#601; olmas&#305;na;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin)
t&#601;hlük&#601;sizliyin&#601;;</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin)
miqdar&#305;, çe&#351;idi v&#601; keyfiyy&#601;ti haqq&#305;nda dol&#287;un
v&#601; düzgün m&#601;lumat &#601;ld&#601; etm&#601;y&#601;;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>qanunvericilikd&#601; n&#601;z&#601;rd&#601; tutulan hallarda,
laz&#305;mi keyfiyy&#601;ti olmayan, habel&#601; insanlar&#305;n
sa&#287;laml&#305;&#287;&#305;na, h&#601;yat&#305;na t&#601;hlük&#601;li olan
mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin) vurdu&#287;u
z&#601;r&#601;rin öd&#601;nilm&#601;sin&#601;;</span><span lang=RU
                                                           style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>öz hüquqlar&#305;n&#305;n v&#601; qanuni m&#601;nafel&#601;rinin
müdafi&#601;si üçün s&#601;lahiyy&#601;tli dövl&#601;t orqanlar&#305;na v&#601;
m&#601;hk&#601;m&#601;y&#601; müraci&#601;t etm&#601;y&#601;;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>ictimai t&#601;&#351;kilatlarda (istehlakç&#305;lar birliyind&#601;)
birl&#601;&#351;m&#601;y&#601;.</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;4.&nbsp;<b>Z&#601;man&#601;tli istehlak s&#601;viyy&#601;si</b></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Z&#601;man&#601;tli istehlak s&#601;viyy&#601;si
a&#351;a&#287;&#305;dak&#305; formalarda t&#601;min edilir:</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>h&#601;r bir istehlakç&#305;n&#305;n azad &#351;&#601;kild&#601; mal
almas&#305;na z&#601;man&#601;t olmad&#305;&#287;&#305; hallarda, mallar&#305;n
normala&#351;d&#305;r&#305;lm&#305;&#351; formada bölünm&#601;sinin
t&#601;tbiqi il&#601;;</span><span lang=RU style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>v&#601;t&#601;nda&#351;lara kompensasiya öd&#601;ni&#351;l&#601;ri,
müxt&#601;lif müavin&#601;t v&#601; güz&#601;&#351;tl&#601;r
verm&#601;kl&#601;.</span><span lang=RU style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;5.&nbsp;<b>&#304;stehlakç&#305;n&#305;n mallar&#305;n
(i&#351;l&#601;rin, xidm&#601;tl&#601;rin) laz&#305;mi keyfiyy&#601;td&#601;
olmas&#305;na hüququ</b></span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>1.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305; sat&#305;c&#305;dan
(istehsalç&#305;dan, icraç&#305;dan) al&#305;nm&#305;&#351; mal&#305;n
(görülmü&#351; i&#351;in, göst&#601;rilmi&#351; xidm&#601;tin)
keyfiyy&#601;tinin normativ s&#601;n&#601;dl&#601;r&#601;, müqavil&#601;
&#351;&#601;rtl&#601;rin&#601; uy&#287;unlu&#287;u bar&#601;d&#601; v&#601;
h&#601;mçinin mal (i&#351;, xidm&#601;t) haqq&#305;nda sat&#305;c&#305;n&#305;n
(istehsalç&#305;n&#305;n, icraç&#305;n&#305;n) t&#601;qdim etdiyi m&#601;lumata
cavab verm&#601;sini t&#601;l&#601;b etm&#601;k hüququna malikdir.</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>2.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305;lar&#305;n
h&#601;yat&#305;n&#305;n, sa&#287;laml&#305;&#287;&#305;n&#305;n v&#601;
&#601;mlak&#305;n&#305;n, habel&#601; &#601;traf mühitin t&#601;hlük&#601;sizliyin&#601;
dair mala (i&#351;&#601;, xidm&#601;t&#601;) aid t&#601;l&#601;bl&#601;r
normativ s&#601;n&#601;dl&#601;rl&#601; mü&#601;yy&#601;n edilir.</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Ayr&#305;-ayr&#305; mal qruplar&#305; (i&#351;l&#601;r,
xidm&#601;tl&#601;r) üçün yuxar&#305;da göst&#601;ril&#601;n
t&#601;l&#601;bl&#601;r Az&#601;rbaycan Respublikas&#305;n&#305;n
qanunvericilik aktlar&#305; il&#601; mü&#601;yy&#601;n edilir.</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>3.&nbsp;&nbsp;&nbsp;Sat&#305;c&#305; (istehsalç&#305;, icraç&#305;)
istehlakç&#305;ya keyfiyy&#601;ti normativ s&#601;n&#601;dl&#601;r&#601;,
müqavil&#601; &#351;&#601;rtl&#601;rin&#601; v&#601; h&#601;mçinin mal
(i&#351;, xidm&#601;t) haqq&#305;nda istehsalç&#305;n&#305;n
(icraç&#305;n&#305;n) t&#601;qdim etdiyi m&#601;lumata uy&#287;un olan mal
verm&#601;lidir.</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Az&#601;rbaycan Respublikas&#305; &#601;razisin&#601; idxal
olunmu&#351; mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin)
laz&#305;mi keyfiyy&#601;tini t&#601;sdiq ed&#601;n, qanunvericilikl&#601;
n&#601;z&#601;rd&#601; tutulmu&#351; s&#601;n&#601;d olmal&#305;d&#305;r.</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>4.&nbsp;&nbsp;&nbsp;&#304;stehsalç&#305; (icraç&#305;) normativ
s&#601;n&#601;dd&#601; n&#601;z&#601;rd&#601; tutulan v&#601; ya
istehlakç&#305; il&#601; müqavil&#601; &#601;sas&#305;nda mal&#305;n
(görül&#601;n i&#351;in, xidm&#601;tin n&#601;tic&#601;l&#601;rinin)
xidm&#601;t müdd&#601;ti &#601;rzind&#601;, bel&#601; müdd&#601;tin
olmad&#305;&#287;&#305; halda is&#601; 10 il müdd&#601;tind&#601;
t&#601;yinat&#305; üzr&#601; istifad&#601; edilm&#601;sin&#601; t&#601;minat
verm&#601;lidir.</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehsalç&#305;, mallara texniki xidm&#601;ti v&#601;
onlar&#305;n z&#601;man&#601;tli t&#601;mirini, habel&#601; texniki t&#601;mir
v&#601; xidm&#601;t göst&#601;r&#601;n t&#601;&#351;kilatlar üçün laz&#305;m
olan h&#601;cmd&#601; v&#601; çe&#351;idd&#601; ehtiyat hiss&#601;l&#601;ri
il&#601; t&#601;chizini bütün istehsal müdd&#601;ti &#601;rzind&#601;, mal
(i&#351;, xidm&#601;t) istehsaldan ç&#305;xar&#305;ld&#305;qdan sonra is&#601;
xidm&#601;t müdd&#601;ti &#601;rzind&#601;, bu müdd&#601;t
n&#601;z&#601;rd&#601; tutulmayan hallarda is&#601; 10 il &#601;rzind&#601;
t&#601;min etm&#601;lidir.</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;6.&nbsp;&nbsp;&nbsp;<b>Z&#601;man&#601;t öhd&#601;likl&#601;ri</b></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>1.&nbsp;&nbsp;&nbsp;&#304;stehsalç&#305; (icraç&#305;) mal&#305;n
(i&#351;in, xidm&#601;tin), h&#601;mçinin komplektl&#601;&#351;dirici
m&#601;mulatlar&#305;n qanunvericilikd&#601; n&#601;z&#601;rd&#601; tutulan
z&#601;man&#601;t müdd&#601;ti &#601;rzind&#601;, bu müdd&#601;tl&#601;rin
olmad&#305;&#287;&#305; hallarda is&#601; müqavil&#601; il&#601;
mü&#601;yy&#601;nl&#601;&#351;dirilmi&#351; qaydada normal i&#351;ini
(t&#601;tbiqini v&#601; istifad&#601;sini) t&#601;min edir.</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Komplektl&#601;&#351;dirici m&#601;mulatlar&#305;n z&#601;man&#601;t
müdd&#601;ti qanunvericilikd&#601; v&#601; ya müqavil&#601;d&#601; ayr&#305;
cür göst&#601;rilm&#601;yibs&#601;, mal&#305;n (i&#351;in, xidm&#601;tin)
özünün z&#601;man&#601;t müdd&#601;tind&#601;n az olmamal&#305;d&#305;r.</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>2.&nbsp;&nbsp;&nbsp;Z&#601;man&#601;t müdd&#601;ti mal&#305;n
(i&#351;in, xidm&#601;tin) pasportunda v&#601; ya ni&#351;anlama
ka&#287;&#305;z&#305;nda (yarl&#305;qda), yaxud mala (i&#351;&#601;
xidm&#601;t&#601;) &#601;lav&#601; olunan dig&#601;r s&#601;n&#601;dd&#601;
göst&#601;rilir.</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Tez xarab olan v&#601; insanlar&#305;n
sa&#287;laml&#305;&#287;&#305;na, h&#601;yat&#305;na v&#601;
&#601;mlak&#305;na, habel&#601; &#601;traf mühit&#601; t&#601;hlük&#601;
k&#601;sb ed&#601;n &#601;rzaq m&#601;hsullar&#305;n&#305;n,
d&#601;rmanlar&#305;n, &#601;triyyat-kosmetika vasit&#601;l&#601;rinin, kimya
m&#601;hsullar&#305;n&#305;n v&#601; ba&#351;qa mallar&#305;n
(i&#351;l&#601;rin, xidm&#601;tl&#601;rin) üz&#601;rind&#601; (qablar&#305;nda)
v&#601; ya onlara &#601;lav&#601; edil&#601;n müvafiq
s&#601;n&#601;dl&#601;rd&#601; yararl&#305;l&#305;q müdd&#601;ti
göst&#601;rilm&#601;lidir.</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Yararl&#305;l&#305;q müdd&#601;ti ötmü&#351; mallar&#305;n
sat&#305;&#351;&#305; qada&#287;an edilir.</span><span lang=RU
                                                       style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Z&#601;man&#601;t müdd&#601;tl&#601;ri &#601;g&#601;r alq&#305;-satq&#305;
müqavil&#601;sind&#601; ayr&#305; müdd&#601;t n&#601;z&#601;rd&#601;
tutulmay&#305;bsa, mal&#305;n istehlakç&#305;ya verildiyi m&#601;qamdan,
yararl&#305;l&#305;q müdd&#601;tl&#601;ri is&#601;
haz&#305;rland&#305;&#287;&#305; günd&#601;n hesablan&#305;r.&nbsp;</span><a
                        name="_ednref5"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn5"><span
                            style='mso-bookmark:_ednref5'><sup><span lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:blue;mso-ansi-language:AZ-LATIN'>[5]</span></sup></span><span
                            style='mso-bookmark:_ednref5'></span></a><span style='mso-bookmark:_ednref5'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>3.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305; sat&#305;c&#305;
(istehsalç&#305;, icraç&#305;) qar&#351;&#305;s&#305;nda z&#601;man&#601;t
müdd&#601;ti mü&#601;yy&#601;n edilm&#601;mi&#351; mallarda a&#351;kar
olunmu&#351; qüsurlar bar&#601;d&#601;, &#601;g&#601;r müqavil&#601;d&#601;
ba&#351;qa hal n&#601;z&#601;rd&#601;
tutulmay&#305;bsa,&nbsp;&nbsp;Az&#601;rbaycan Respublikas&#305;n&#305;n Mülki
M&#601;c&#601;ll&#601;si il&#601; mü&#601;yy&#601;n edilmi&#351;
müdd&#601;td&#601;n gec olmamaq &#351;&#601;rti il&#601; öz
t&#601;l&#601;bl&#601;rini ir&#601;li sürm&#601;k hüququna malikdir.&nbsp;</span><a
                        name="_ednref6"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn6"><span
                            style='mso-bookmark:_ednref6'><sup><span lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:blue;mso-ansi-language:AZ-LATIN'>[6]</span></sup></span><span
                            style='mso-bookmark:_ednref6'></span></a><span style='mso-bookmark:_ednref6'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>4.&nbsp;&nbsp;&nbsp;Z&#601;man&#601;tli t&#601;mir
apar&#305;lark&#601;n mal&#305;n (i&#351;in, xidm&#601;tin) z&#601;man&#601;t
müdd&#601;ti onun t&#601;mird&#601; oldu&#287;u müdd&#601;t q&#601;d&#601;r
uzad&#305;l&#305;r. Göst&#601;ril&#601;n müdd&#601;t istehlakç&#305;n&#305;n
qüsurlar&#305; aradan qald&#305;rmaq t&#601;l&#601;bini ir&#601;li sürdüyü
günd&#601;n hesablan&#305;r. Mal d&#601;yi&#351;dirildikd&#601; z&#601;man&#601;t
müdd&#601;ti d&#601;yi&#351;dirilm&#601; gününd&#601;n etibar&#601;n
yenid&#601;n hesablan&#305;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;7.&nbsp;<b>Qüsuru olan mal sat&#305;lark&#601;n
istehlakç&#305;n&#305;n hüquqlar&#305;</b></span><span lang=AZ-LATIN
                                                       style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>1.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305; müqavil&#601; v&#601; ya
dig&#601;r qaydalarla mü&#601;yy&#601;n olunmu&#351; z&#601;man&#601;t
müdd&#601;ti &#601;rzind&#601; ald&#305;&#287;&#305; malda qüsur v&#601; ya
saxtala&#351;d&#305;rma a&#351;kar ed&#601;rs&#601;, öz ist&#601;yin&#601;
gör&#601; sat&#305;c&#305;dan v&#601; ya istehsalç&#305;dan
a&#351;a&#287;&#305;dak&#305;lar&#305; t&#601;l&#601;b etm&#601;k hüququna
malikdir:</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>laz&#305;mi keyfiyy&#601;tli mala d&#601;yi&#351;dirm&#601;yi;</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>sat&#305;&#351; qiym&#601;tini uy&#287;un
m&#601;bl&#601;&#287;d&#601; azaltma&#287;&#305;;</span><span lang=AZ-LATIN
                                                              style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mal&#305;n qüsurlar&#305;n&#305;n icraç&#305;n&#305;n
(sat&#305;c&#305;n&#305;n, istehlakç&#305;n&#305;n) hesab&#305;na aradan
qald&#305;r&#305;lmas&#305;n&#305; v&#601; ya qüsurlar&#305;n aradan
qald&#305;r&#305;lmas&#305; üçün istehlakç&#305;n&#305;n v&#601; ya üçüncü
&#351;&#601;xsl&#601;rin ç&#601;kdiyi x&#601;rcl&#601;rin &#601;v&#601;zinin
öd&#601;nilm&#601;sini;</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mal&#305;n, h&#601;min mala uy&#287;un dig&#601;r modelli
(markal&#305;, tipli v&#601; i. a.) mal il&#601;, d&#601;y&#601;ri yenid&#601;n
hesablanma &#351;&#601;rti il&#601; &#601;v&#601;z edilm&#601;sini;</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>müqavil&#601;nin l&#601;&#287;v edilm&#601;sini v&#601; ç&#601;kdiyi
z&#601;r&#601;rin öd&#601;nilm&#601;sini.</span><span lang=AZ-LATIN
                                                      style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>2.&nbsp;&nbsp;&nbsp;Sat&#305;c&#305; (icraç&#305;) istehlakç&#305;dan
laz&#305;mi keyfiyy&#601;ti olmayan, mal&#305; geri götürm&#601;y&#601; v&#601;
bu madd&#601;nin birinci b&#601;ndind&#601; göst&#601;ril&#601;n
istehlakç&#305;n&#305;n t&#601;l&#601;bl&#601;rind&#601;n birini yerin&#601;
yetirm&#601;y&#601; borcludur.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;ri qabaritli v&#601; a&#287;&#305;rç&#601;kili mallar&#305;n
istehlakç&#305;dan sat&#305;c&#305;ya (icraç&#305;ya) qaytar&#305;lmas&#305;
v&#601; d&#601;yi&#351;diril&#601;r&#601;k yenid&#601;n istehlakç&#305;ya
çatd&#305;r&#305;lmas&#305; sat&#305;c&#305;n&#305;n (icraç&#305;n&#305;n)
hesab&#305;na h&#601;yata keçirilir.</span><span lang=AZ-LATIN
                                                 style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>3.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305;n&#305;n mal&#305;
d&#601;yi&#351;m&#601;k t&#601;l&#601;bi mal olduqda d&#601;rhal yerin&#601;
yetirilm&#601;li, z&#601;rur&#601;t olduqda onun keyfiyy&#601;ti müvafiq
t&#601;l&#601;b veril&#601;n andan 14 gün müdd&#601;tind&#601;
yoxlan&#305;lmal&#305; v&#601; ya t&#601;r&#601;fl&#601;r aras&#305;nda
raz&#305;la&#351;d&#305;r&#305;lm&#305;&#351; müdd&#601;t &#601;rzind&#601;
d&#601;yi&#351;dirilm&#601;lidir.</span><span lang=AZ-LATIN style='font-size:
10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;n&#305;n mal&#305; d&#601;yi&#351;dirm&#601;k
t&#601;l&#601;bi, mal olmad&#305;qda müvafiq &#601;riz&#601;nin verildiyi andan
iki ay müdd&#601;tind&#601; öd&#601;nilm&#601;lidir.</span><span lang=AZ-LATIN
                                                                 style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Göst&#601;ril&#601;n müdd&#601;t &#601;rzind&#601; mal&#305;
d&#601;yi&#351;dirm&#601;k mümkün olmad&#305;qda, istehlakç&#305;
sat&#305;c&#305; (istehsalç&#305;) qar&#351;&#305;s&#305;nda bu madd&#601;nin
birinci b&#601;ndinin ikinci, üçüncü, dördüncü v&#601; be&#351;inci
abzaslar&#305;nda n&#601;z&#601;rd&#601; tutulmu&#351; dig&#601;r
t&#601;l&#601;bl&#601;rin yerin&#601; yetirilm&#601;sini t&#601;l&#601;b
ed&#601; bil&#601;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>4.&nbsp;&nbsp;&nbsp;Qüsurlu mal laz&#305;mi keyfiyy&#601;tli, eyni
modelli (markal&#305;, tipli v&#601; i. a.) mala d&#601;yi&#351;diril&#601;rk&#601;n
qiym&#601;t d&#601;yi&#351;&#601;rs&#601;, istehlakç&#305; qiym&#601;t
f&#601;rqini öd&#601;mir.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Qüsurlu mal laz&#305;mi keyfiyy&#601;tli, eyni
&#601;lam&#601;tl&#601;ri olan, lakin ba&#351;qa modelli (markal&#305;, tipli
v&#601; i. a.) mala d&#601;yi&#351;diril&#601;rk&#601;n qiym&#601;tl&#601;r
d&#601;yi&#351;dikd&#601;, mal&#305;n d&#601;y&#601;ri göst&#601;ril&#601;n
mal&#305;n al&#305;nd&#305;&#287;&#305; vaxt qüvv&#601;d&#601; olmu&#351;
qiym&#601;tl&#601;r &#601;sas götürülm&#601;kl&#601; yenid&#601;n
hesablan&#305;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black;
mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Müqavil&#601; l&#601;&#287;v edildikd&#601;, istehlakç&#305; il&#601;
hesablamalar mala qiym&#601;tl&#601;r artan hallarda eyni
d&#601;r&#601;c&#601;li mallar&#305;n qiym&#601;tl&#601;rinin artmas&#305;
n&#601;z&#601;r&#601; al&#305;nmaqla, qiym&#601;tl&#601;r a&#351;a&#287;&#305;
dü&#351;dükd&#601; is&#601; mal&#305;n al&#305;nd&#305;&#287;&#305; vaxt d&#601;y&#601;ri
&#601;sas götürülm&#601;kl&#601; apar&#305;l&#305;r.</span><span lang=AZ-LATIN
                                                                 style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>5.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305;ya keyfiyy&#601;tsiz
&#601;rzaq mal&#305; sat&#305;lan halda, keyfiyy&#601;tsizlik mal&#305;n
yararl&#305;l&#305;q müdd&#601;ti &#601;rzind&#601; a&#351;kar olunubsa,
sat&#305;c&#305; h&#601;min mal&#305; keyfiyy&#601;tli mala
d&#601;yi&#351;dirm&#601;li v&#601; ya mal&#305;n d&#601;y&#601;rinin
m&#601;bl&#601;&#287;ini qaytarmal&#305;d&#305;r.</span><span lang=AZ-LATIN
                                                              style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Bel&#601; hallarda istehlakç&#305; il&#601; hesabla&#351;ma bu
madd&#601;nik 4-cü b&#601;ndind&#601; göst&#601;ril&#601;n qaydada
apar&#305;l&#305;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>6.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305; mal&#305;n qüsurunun
&#601;v&#601;zsiz olaraq aradan qald&#305;r&#305;lmas&#305;n&#305;
t&#601;l&#601;b ed&#601;rs&#601;, qüsur 14 gün &#601;rzind&#601; v&#601; ya
t&#601;r&#601;fl&#601;rin raz&#305;l&#305;&#287;&#305; il&#601; ba&#351;qa
müdd&#601;td&#601; aradan qald&#305;r&#305;lmal&#305;d&#305;r.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;n&#305;n t&#601;l&#601;bi il&#601;
sat&#305;c&#305; (icraç&#305;) m&#601;i&#351;&#601;t texnikas&#305;n&#305;n
v&#601; n&#601;qliyyat vasit&#601;l&#601;rinin t&#601;mir edildiyi v&#601; ya
d&#601;yi&#351;dirildiyi müdd&#601;t üçün ona &#601;v&#601;zsiz olaraq
(evin&#601; çatd&#305;rmaqla) ox&#351;ar mal verm&#601;lidir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>T&#601;mir (d&#601;yi&#351;dirm&#601;), habel&#601; mal&#305;n
qüsurlar&#305;n&#305;n aradan qald&#305;r&#305;ld&#305;&#287;&#305; v&#601; ya
d&#601;yi&#351;dirildiyi müdd&#601;t üçün ox&#351;ar mal&#305; verm&#601;k
t&#601;l&#601;binin yerin&#601; yetirilm&#601;si (14 gün)
l&#601;ngidildikd&#601; sat&#305;c&#305; (icraç&#305;) istehlakç&#305;ya mal
verm&#601;kl&#601; bir vaxtda, mü&#601;yy&#601;n edilmi&#351; müdd&#601;tin
ötürüldüyü h&#601;r gün üçün ona mal&#305;n sat&#305;&#351; qiym&#601;tinin bir
faizi m&#601;bl&#601;&#287;ind&#601; d&#601;bb&#601; pulu öd&#601;yir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>7.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305;n&#305;n
t&#601;l&#601;bin&#601;, q&#601;bz, mal v&#601; ya kassa çeki,
z&#601;man&#601;t müdd&#601;ti olan mallar üzr&#601; is&#601; texniki pasport
v&#601; ya onu &#601;v&#601;z ed&#601;n dig&#601;r s&#601;n&#601;d t&#601;qdim
edildikd&#601; bax&#305;l&#305;r.</span><span lang=AZ-LATIN style='font-size:
10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Sat&#305;c&#305; istehlakç&#305;ya mal satark&#601;n ona q&#601;bz,
mal, kassa çeki v&#601; ya dig&#601;r yaz&#305;l&#305; s&#601;n&#601;d
verm&#601;lidir.</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black;
mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305; ona veril&#601;n texniki pasportu v&#601; ya onu
&#601;v&#601;z ed&#601;n dig&#601;r s&#601;n&#601;di itir&#601;rs&#601;,
onlar&#305;n b&#601;rpas&#305; qanunvericilikd&#601; n&#601;z&#601;rd&#601;
tutulmu&#351; qaydada h&#601;ll edilir.</span><span lang=AZ-LATIN
                                                    style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>8.&nbsp;&nbsp;&nbsp;&#304;stehsalç&#305; istehlakç&#305;n&#305;n mala
olan iddias&#305;n&#305; aradan qald&#305;rmaq üçün sat&#305;c&#305;n&#305;n
s&#601;rf etdiyi x&#601;rcl&#601;ri öd&#601;m&#601;lidir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>9.&nbsp;&nbsp;&nbsp;Az&#601;rbaycan Respublikas&#305;ndan
k&#601;narda istehsal olunmu&#351; v&#601; ya vasit&#601;çil&#601;rd&#601;n
al&#305;nm&#305;&#351; mallar bar&#601;sind&#601; bu madd&#601;nin birinci
b&#601;ndind&#601; mü&#601;yy&#601;n edilmi&#351; t&#601;l&#601;bl&#601;r
sat&#305;c&#305;n&#305;n hesab&#305;na öd&#601;nilir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>10.&nbsp;&#399;g&#601;r sat&#305;c&#305;, istehsalç&#305;
(onlar&#305;n funksiyalar&#305;n&#305; yerin&#601; yetir&#601;n
mü&#601;ssis&#601;l&#601;r) mal&#305;n qüsurlar&#305;n&#305;n istehlakç&#305;
t&#601;r&#601;find&#601;n istifad&#601; v&#601; ya saxlanma
qaydalar&#305;n&#305;n pozulmas&#305;, üçüncü &#351;&#601;xsl&#601;rin
h&#601;r&#601;k&#601;tl&#601;ri v&#601; ya t&#601;bii f&#601;lak&#601;t
n&#601;tic&#601;sind&#601; &#601;m&#601;l&#601; g&#601;ldiyini sübuta
yetir&#601;rs&#601;, istehlakç&#305;n&#305;n bu madd&#601;d&#601;
n&#601;z&#601;rd&#601; tutulan t&#601;l&#601;bl&#601;ri t&#601;min
olunmamal&#305;d&#305;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305; mal&#305;n keyfiyy&#601;tinin yoxlanmas&#305;nda
&#351;&#601;xs&#601;n, yaxud öz nümay&#601;nd&#601;si vasit&#601;sil&#601;
i&#351;tirak etm&#601;k hüququna malikdir.</span><span lang=AZ-LATIN
                                                       style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;8.&nbsp;<b>&#304;&#351; görülm&#601;si v&#601; xidm&#601;t
göst&#601;rilm&#601;si üçün müqavil&#601; ba&#287;lanmas&#305;nda icraç&#305;n&#305;n
v&#601;zif&#601;l&#601;ri</b></span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Bazarda hökmran mövqe tutan istehsalç&#305; v&#601; ya icraç&#305;
(onun nizamnam&#601; f&#601;aliyy&#601;tind&#601;n, yaxud istehsal
imkanlar&#305;ndan k&#601;nara ç&#305;xd&#305;&#287;&#305;n&#305; sübut etdiyi
hallar istisna olmaqla) istehlakç&#305; il&#601; i&#351;l&#601;rin icra
edilm&#601;si v&#601; xidm&#601;t göst&#601;rilm&#601;si üçün&nbsp;qeyri-b&#601;rab&#601;r
&#351;&#601;rt t&#601;klif etm&#601;d&#601;n&nbsp;müqavil&#601;
ba&#287;lama&#287;a borcludur. Bu halda o, öz istehsalat, yaxud dig&#601;r
t&#601;s&#601;rrüfat f&#601;aliyy&#601;tini el&#601; t&#601;&#351;kil
etm&#601;lidir ki, &#601;halinin ehtiyaclar&#305; laz&#305;mi
s&#601;viyy&#601;d&#601; v&#601; fasil&#601;siz t&#601;min edilsin.
&#304;craç&#305;n&#305;n &#601;sass&#305;z olaraq, müqavil&#601; ba&#287;lamaqdan
imtina etm&#601;si n&#601;tic&#601;sind&#601; o, i&#351;l&#601;rin yerin&#601;
yetirilm&#601;sind&#601;n v&#601; xidm&#601;tl&#601;rin
göst&#601;rilm&#601;m&#601;sind&#601;n istehlakç&#305;ya d&#601;y&#601;n
ziyan&#305;n &#601;v&#601;zini öd&#601;m&#601;y&#601; borcludur.</span><b><span
                            lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>&nbsp;<a name="_ednref7"></a><a
                                href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn7"><span
                                    style='mso-bookmark:_ednref7'><sup><span style='mso-bidi-font-size:11.0pt;
color:blue'>[7]</span></sup></span><span style='mso-bookmark:_ednref7'></span></a></span></b><span
                        style='mso-bookmark:_ednref7'></span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;9.&nbsp;<b>&#304;&#351; görülm&#601;sin&#601; v&#601;
xidm&#601;t göst&#601;rilm&#601;sin&#601; dair müqavil&#601;l&#601;rin
&#351;&#601;rtl&#601;ri pozulark&#601;n istehlakç&#305;lar&#305;n
hüquqlar&#305;</b></span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;1.&nbsp;&#304;craç&#305; i&#351; görülm&#601;sin&#601; v&#601;
xidm&#601;t göst&#601;rilm&#601;sin&#601; dair müqavil&#601;nin icras&#305;na
vaxt&#305;nda ba&#351;lam&#305;rsa v&#601; ya çox l&#601;ng
i&#351;l&#601;m&#601;si i&#351;in vaxt&#305;nda ba&#351;a çatmamas&#305;na
d&#601;lal&#601;t edirs&#601;, istehlakç&#305;n&#305;n müqavil&#601;d&#601;n
imtina etm&#601;k v&#601; itkil&#601;rin öd&#601;nilm&#601;sini t&#601;l&#601;b
etm&#601;k hüququ vard&#305;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>2.&nbsp;&nbsp;&nbsp;&#304;&#351;l&#601;rd&#601;
(xidm&#601;tl&#601;rd&#601;) müqavil&#601; &#351;&#601;rtl&#601;rind&#601;n
&#601;h&#601;miyy&#601;tli d&#601;r&#601;c&#601;d&#601; k&#601;nara
ç&#305;x&#305;ld&#305;qda, yaxud ba&#351;qa mühüm qüsurlar olduqda
istehlakç&#305; h&#601;min qüsurlar&#305; aradan qald&#305;rmaq üçün
icraç&#305;ya &#601;lav&#601; müdd&#601;t ver&#601;rs&#601;, bu
müdd&#601;td&#601; d&#601; i&#351; (xidm&#601;t) yerin&#601;
yetirilm&#601;zs&#601;, müqavil&#601;nin l&#601;&#287;v olunmas&#305;n&#305;
v&#601; itkil&#601;rin öd&#601;nilm&#601;sini t&#601;l&#601;b etm&#601;k
v&#601; ya icraç&#305;n&#305;n hesab&#305;na qüsurlar&#305;n aradan
qald&#305;r&#305;lmas&#305;n&#305; üçüncü &#351;&#601;xsl&#601;r&#601;
tap&#351;&#305;rmaq hüququ var.</span><span lang=AZ-LATIN style='font-size:
10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>3.&nbsp;&nbsp;&nbsp;&#304;craç&#305; müqavil&#601;nin
&#351;&#601;rtl&#601;rind&#601;n k&#601;nara ç&#305;xaraq, i&#351;i
(xidm&#601;ti) pisl&#601;&#351;dirmi&#351;s&#601;, yaxud i&#351;d&#601;
(xidm&#601;td&#601;) ba&#351;qa qüsurlara yol vermi&#351;dirs&#601;,
istehlakç&#305;n&#305;n ist&#601;yin&#601; uy&#287;un olaraq,
göst&#601;ril&#601;n qüsurlar&#305;n icraç&#305;n&#305;n hesab&#305;na
&#601;v&#601;zsiz olaraq müvafiq müdd&#601;td&#601; aradan
qald&#305;r&#305;lmas&#305;n&#305; v&#601; ya i&#351;in (xidm&#601;tin)
qüsurlar&#305;n&#305; öz v&#601;saitl&#601;ri il&#601; aradan
qald&#305;rark&#601;n ç&#601;kdiyi z&#601;ruri x&#601;rcl&#601;rin
öd&#601;nilm&#601;sini, yaxud i&#351;in (xidm&#601;tin) haqq&#305;n&#305;n
müvafiq sur&#601;td&#601; azald&#305;lmas&#305;n&#305; t&#601;l&#601;b
etm&#601;k hüququ var.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>4.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305;n&#305;n materiallar&#305;ndan
görülmü&#351; i&#351;l&#601;rd&#601; (xidm&#601;tl&#601;rd&#601;) müqavil&#601;
&#351;&#601;rtl&#601;rind&#601;n &#601;h&#601;miyy&#601;tli
k&#601;naraç&#305;xma v&#601; ya dig&#601;r mühüm qüsurlar a&#351;kar
edil&#601;rs&#601;, istehlakç&#305;n&#305;n ist&#601;yin&#601; uy&#287;un
olaraq, bu i&#351;l&#601;rin icraç&#305;n&#305;n eyni cinsli v&#601;
keyfiyy&#601;tli material&#305;ndan haz&#305;rlanmas&#305;n&#305;, yaxud
müqavil&#601;nin l&#601;&#287;v olunmas&#305;n&#305; v&#601; itkil&#601;rin
öd&#601;nilm&#601;sini t&#601;l&#601;b etm&#601;k hüququ vard&#305;r.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>5.&nbsp;&nbsp;&nbsp;Bu madd&#601;nin 2-ci b&#601;ndind&#601;
göst&#601;rilmi&#351; qüsurlar, mü&#601;yy&#601;n edilmi&#351;
müdd&#601;td&#601; aradan qald&#305;r&#305;lmad&#305;qda, habel&#601;
i&#351;l&#601;rin (xidm&#601;tl&#601;rin) yerin&#601; yetirilm&#601;si
l&#601;ngidildikd&#601;, icraç&#305; i&#351;in (xidm&#601;tin) q&#601;bul
edilm&#601;si zaman&#305; istehlakç&#305;ya gecikdirilmi&#351; h&#601;r gün
üçün i&#351;in v&#601; ya xidm&#601;tin d&#601;y&#601;rinin (&#601;g&#601;r
d&#601;y&#601;r ayr&#305;ca mü&#601;yy&#601;n edilm&#601;mi&#351;dirs&#601;,
sifari&#351;in d&#601;y&#601;rinin) bir faizi m&#601;bl&#601;&#287;ind&#601;
d&#601;bb&#601; pulu öd&#601;yir, bu &#351;&#601;rtl&#601; ki,
müqavil&#601;d&#601; d&#601;bb&#601; pulunun ba&#351;qa m&#601;bl&#601;&#287;i
n&#601;z&#601;rd&#601; tutulmu&#351; olmas&#305;n.</span><span lang=AZ-LATIN
                                                               style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Öhd&#601;likl&#601;rin icra edilm&#601;diyi v&#601; ya
laz&#305;m&#305;nca icra edilm&#601;diyi hallar üçün mü&#601;yy&#601;n
edilmi&#351; d&#601;bb&#601; pulunun (c&#601;rim&#601;nin, penyan&#305;n)
icraç&#305; t&#601;r&#601;find&#601;n öd&#601;nilm&#601;si v&#601;
itkil&#601;rin &#601;v&#601;zinin verilm&#601;si onu öhd&#601;likl&#601;ri
yerin&#601; yetirm&#601;k v&#601;zif&#601;sind&#601;n azad etmir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>6.&nbsp;&nbsp;&nbsp;&#304;craç&#305; görülmü&#351;
i&#351;l&#601;rd&#601; v&#601; göst&#601;rilmi&#351; xidm&#601;tl&#601;rd&#601;
qüsurlar&#305;n istehlakç&#305;n&#305;n t&#601;qsiri ucbat&#305;ndan
&#601;m&#601;l&#601; g&#601;ldiyini sübut ed&#601;rs&#601;, buna gör&#601;
m&#601;suliyy&#601;t da&#351;&#305;m&#305;r.</span><span lang=AZ-LATIN
                                                         style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>7.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305;n&#305;n bu madd&#601;nin
2-ci v&#601; 5-ci b&#601;ndl&#601;rind&#601; n&#601;z&#601;rd&#601;
tutulmu&#351; t&#601;l&#601;bl&#601;ri i&#351;in (xidm&#601;tin) yerin&#601;
yetirilm&#601;si gedi&#351;ind&#601; istehlakç&#305; t&#601;r&#601;find&#601;n
q&#601;bul edilm&#601;si zaman&#305;, habel&#601; z&#601;man&#601;t
müdd&#601;ti &#601;rzind&#601;, bel&#601; müdd&#601;t olmad&#305;qda is&#601; bir
il &#601;rzind&#601;, qüsurlar a&#351;kar edil&#601;rk&#601;n ir&#601;li
sürül&#601; bil&#601;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Z&#601;man&#601;t müdd&#601;ti i&#351;in (xidm&#601;tin)
istehlakç&#305; t&#601;r&#601;find&#601;n q&#601;bul olundu&#287;u günd&#601;n,
istehlakç&#305;n&#305;n t&#601;qsiri üzünd&#601;n vaxt&#305;nda q&#601;bul
olunmad&#305;qda is&#601; i&#351;in (xidm&#601;tin) icras&#305; üçün
müqavil&#601;d&#601; mü&#601;yy&#601;n edilmi&#351; müdd&#601;td&#601;n
hesablan&#305;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black;
mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>8.&nbsp;&nbsp;&nbsp;&#304;craç&#305; istehlakç&#305;dan q&#601;bul
etdiyi &#601;&#351;yan&#305; (material&#305;) itirdikd&#601;, korlad&#305;qda,
z&#601;d&#601;l&#601;dikd&#601; istehlakç&#305;ya özünün keyfiyy&#601;tc&#601;
v&#601; qiym&#601;tc&#601; ox&#351;ar &#601;&#351;yas&#305;n&#305;
qaytarmal&#305; (i&#351; görm&#601;li v&#601; xidm&#601;t
göst&#601;rm&#601;li), bu mümkün olmad&#305;qda istehlakç&#305;n&#305;n
raz&#305;l&#305;&#287;&#305; il&#601; &#601;&#351;yan&#305;n (material&#305;n)
d&#601;y&#601;rini v&#601; d&#601;ymi&#351; itkil&#601;ri ona bir aydan gec
olmamaq &#351;&#601;rti il&#601; öd&#601;m&#601;lidir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>9.&nbsp;&nbsp;&nbsp;&#304;craç&#305; &#601;&#351;yan&#305;n
(material&#305;n) korlanmas&#305;na, g&#601;tirib ç&#305;xara bil&#601;n xüsusi
xass&#601;l&#601;ri bar&#601;d&#601; istehlakç&#305;n&#305; x&#601;b&#601;rdar
etm&#601;lidir. Elmi v&#601; texniki bilikl&#601;rin s&#601;viyy&#601;si
&#601;&#351;yan&#305;n (material&#305;n) xüsusi xass&#601;l&#601;rini üz&#601;
ç&#305;xarma&#287;a imkan verm&#601;dikd&#601; bel&#601;, icraç&#305;
m&#601;suliyy&#601;td&#601;n azad edilmir.</span><span lang=AZ-LATIN
                                                       style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;craç&#305;n&#305;n i&#351; görm&#601;k v&#601; xidm&#601;t
göst&#601;rm&#601;k üçün q&#601;bul etdiyi &#601;&#351;yan&#305;n
(material&#305;n) d&#601;y&#601;rin istehlakç&#305; müqavil&#601;
ba&#287;layark&#601;n, mü&#601;yy&#601;nl&#601;&#351;dirir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>10.&nbsp;&#304;&#351; görülm&#601;sind&#601; v&#601; xidm&#601;t
göst&#601;rilm&#601;sind&#601;, istehlakç&#305;n&#305;n
h&#601;yat&#305;n&#305;, sa&#287;laml&#305;&#287;&#305;n&#305; v&#601; ya
&#601;mlak&#305;n&#305;n t&#601;hlük&#601;sizliyini t&#601;min
etm&#601;y&#601;n materiallar&#305;n, avadanl&#305;qlar&#305;n cihazlar&#305;n,
al&#601;tl&#601;rin, qur&#287;ular&#305;n v&#601; ba&#351;qa
vasit&#601;l&#601;rin t&#601;tbiqi n&#601;tic&#601;sind&#601;
v&#601;t&#601;nda&#351;&#305;n h&#601;yat&#305;na,
sa&#287;laml&#305;&#287;&#305;na v&#601; ya &#601;mlak&#305;na z&#601;r&#601;r
d&#601;y&#601;rs&#601;, icraç&#305; onlar xass&#601;l&#601;rini
bilib-bilm&#601;diyind&#601;n as&#305;l&#305; olmayaraq m&#601;suliyy&#601;t
da&#351;&#305;y&#305;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;10.&nbsp;<b>&#304;stehlakç&#305;lar&#305;n mallar&#305;n
(i&#351;l&#601;rin, xidm&#601;tl&#601;rin) t&#601;hlük&#601;siz olmas&#305;na
hüquqlar&#305;</b></span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:3.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:20.0pt;line-height:normal'><span lang=AZ-LATIN
                                                             style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>1.
&#304;stehlakç&#305; qanunla n&#601;z&#601;rd&#601; tutulmu&#351; hallarda
mallar&#305;n, m&#601;mulatlar&#305;n v&#601; m&#601;lumat
materiallar&#305;n&#305;n n&#601;zar&#601;t markas&#305; il&#601;
markalanmas&#305;n&#305;n t&#601;min olunmas&#305; hüququna malikidir.</span><a
                        name="_ednref8"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn8"><span
                            style='mso-bookmark:_ednref8'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[8]</span></sup></b></span><span
                            style='mso-bookmark:_ednref8'></span></a><span style='mso-bookmark:_ednref8'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:3.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:20.0pt;line-height:normal'><span lang=AZ-LATIN
                                                             style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>Mallar&#305;n,
m&#601;mulatlar&#305;n v&#601; m&#601;lumat materiallar&#305;n&#305;n
n&#601;zar&#601;t markas&#305; il&#601; markalanmas&#305; h&#601;min
obyektl&#601;rin mü&#601;lliflik hüququ v&#601; &#601;laq&#601;li hüquqlara &#601;m&#601;l
edilm&#601;kl&#601; haz&#305;rland&#305;&#287;&#305;n&#305;,
sat&#305;&#351;&#305; üçün müvafiq dövl&#601;t orqan&#305;ndan
raz&#305;l&#305;q al&#305;nmas&#305; t&#601;l&#601;b olunduqda bel&#601;
raz&#305;l&#305;&#287;&#305;n al&#305;nmas&#305;n&#305;, h&#601;min
nüsx&#601;l&#601;ri yaymaq hüququnun verildiyini t&#601;sdiq edir.</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:3.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:20.0pt;line-height:normal'><span lang=AZ-LATIN
                                                             style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>N&#601;zar&#601;t
markas&#305; il&#601; markalanma t&#601;l&#601;bi istehlakç&#305;lar&#305;n
t&#601;hrif edilm&#601;mi&#351; v&#601; d&#601;yi&#351;ikliy&#601; m&#601;ruz
qalmam&#305;&#351;, mü&#601;yy&#601;n edilmi&#351; &#601;xlaqi v&#601;
m&#601;n&#601;vi d&#601;y&#601;rl&#601;r&#601; uy&#287;un
haz&#305;rlanm&#305;&#351; mallardan, m&#601;mulatlardan v&#601; m&#601;lumat
materiallar&#305;ndan istifad&#601; hüququnun t&#601;min edilm&#601;sin&#601;
xidm&#601;t edir. N&#601;zar&#601;t markas&#305;n&#305;n formas&#305;, uçotu,
istifad&#601;si v&#601; verilm&#601;si qaydalar&#305; müvafiq icra
hakimiyy&#601;ti orqan&#305; t&#601;r&#601;find&#601;n mü&#601;yy&#601;n
edilir.</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:3.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:20.0pt;line-height:normal'><span lang=AZ-LATIN
                                                             style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>A&#351;a&#287;&#305;dak&#305;lar&#305;n
n&#601;zar&#601;t markas&#305; il&#601; markalanmas&#305; t&#601;l&#601;b
olunur:</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:3.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:20.0pt;line-height:normal'><span lang=AZ-LATIN
                                                             style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>d&#601;rman
vasit&#601;l&#601;ri;</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:3.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:20.0pt;line-height:normal'><span lang=AZ-LATIN
                                                             style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>audiovizual
&#601;s&#601;rin, fonoqram&#305;n, kompyuter proqram&#305;n&#305;n,
m&#601;lumat toplusunun, kitab&#305;n nüsx&#601;l&#601;ri v&#601; dig&#601;r
mü&#601;lliflik v&#601; &#601;laq&#601;li hüquq obyektl&#601;ri;</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:3.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:20.0pt;line-height:normal'><span lang=AZ-LATIN
                                                             style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>dini
t&#601;yinatl&#305; &#601;d&#601;biyyat (ka&#287;&#305;z v&#601; elektron
da&#351;&#305;y&#305;c&#305;lar&#305;nda), audio v&#601; video materiallar, mal
v&#601; m&#601;mulatlar v&#601; dini m&#601;zmunlu ba&#351;qa m&#601;lumat
materiallar&#305;.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Sat&#305;c&#305; (istehsalç&#305;, icraç&#305;) istehlakç&#305;ya
qanunla n&#601;z&#601;rd&#601; tutulmu&#351; hallarda n&#601;zar&#601;t
markas&#305; il&#601; markalanm&#305;&#351; mallar&#305;, m&#601;mulatlar&#305;
v&#601; m&#601;lumat materiallar&#305;n&#305; verm&#601;lidir.
N&#601;zar&#601;t markas&#305; il&#601; markalanmam&#305;&#351; mallar&#305;n,
m&#601;mulatlar&#305;n v&#601; m&#601;lumat materiallar&#305;n&#305;n
yay&#305;lmas&#305;n&#305; h&#601;yata keçir&#601;n &#351;&#601;xsl&#601;r
qanunla n&#601;z&#601;rd&#601; tutulmu&#351; qaydada m&#601;suliyy&#601;t
da&#351;&#305;y&#305;rlar.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>2.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305; mallar&#305;n
(i&#351;l&#601;rin, xidm&#601;tl&#601;rin) adi &#351;&#601;raitd&#601;
istifad&#601; olunmas&#305;n&#305;n, saxlan&#305;lmas&#305;n&#305;n v&#601;
da&#351;&#305;nmas&#305;n&#305;n, onun h&#601;yat&#305;,
sa&#287;laml&#305;&#287;&#305;, el&#601;c&#601; d&#601; &#601;traf mühit üçün
t&#601;hlük&#601;siz olmas&#305;n&#305;n, h&#601;mçinin onun &#601;mlak&#305;na
ziyan vurulmamas&#305;n&#305;n t&#601;min olunmas&#305; hüququna malikdir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#399;g&#601;r istifad&#601; edilm&#601;si
v&#601;t&#601;nda&#351;lar&#305;n h&#601;yat&#305;na
sa&#287;laml&#305;&#287;&#305;na v&#601; &#601;mlak&#305;na, habel&#601;
&#601;traf mühit&#601; z&#601;r&#601;r vura bil&#601;n mala (i&#351;&#601;,
xidm&#601;t&#601;) aid t&#601;hlük&#601;sizlik t&#601;l&#601;bl&#601;rini
mü&#601;yy&#601;nl&#601;&#351;dir&#601;n normativ s&#601;n&#601;d yoxdursa,
onda müvafiq icra hakimiyy&#601;ti orqanlar&#305; bel&#601; normativ
s&#601;n&#601;dl&#601;rin t&#601;xir&#601; sal&#305;nmadan
haz&#305;rlanmas&#305;n&#305; v&#601; q&#601;bul edilm&#601;sini t&#601;min
etm&#601;li, icraç&#305; t&#601;r&#601;find&#601;n mal&#305;n
istehsal&#305;n&#305; v&#601; sat&#305;&#351;&#305;n&#305;, i&#351;in
görülm&#601;sini v&#601; xidm&#601;t göst&#601;rilm&#601;sini d&#601;rhal
dayand&#305;rmal&#305;d&#305;r.</span><span lang=AZ-LATIN style='font-size:
10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>3.&nbsp;&nbsp;&nbsp;Mü&#601;yy&#601;n edilmi&#351; vaxtdan art&#305;q
istifad&#601; edilm&#601;si istehlakç&#305;lar&#305;n h&#601;yat&#305;na,
sa&#287;laml&#305;&#287;&#305;na, onlar&#305;n &#601;mlak&#305;na v&#601; ya
&#601;traf mühit&#601; t&#601;hlük&#601; k&#601;sb ed&#601;n v&#601; ya
z&#601;r&#601;r vura bil&#601;n mallara (i&#351;l&#601;r&#601;,
xidm&#601;tl&#601;r&#601;) xidm&#601;t müdd&#601;ti (yararl&#305;l&#305;q
müdd&#601;ti) mü&#601;yy&#601;n edilm&#601;lidir.</span><span lang=AZ-LATIN
                                                              style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehsalç&#305; (icraç&#305;) bu kimi mallar&#305;n t&#601;limatlar&#305;nda
xidm&#601;t (yararl&#305;l&#305;q) müdd&#601;tl&#601;rini
göst&#601;rm&#601;y&#601; borcludur.</span><span lang=AZ-LATIN
                                                 style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>4.&nbsp;&nbsp;&nbsp;Mal&#305;n (i&#351;in, xidm&#601;tin)
t&#601;hlük&#601;siz istifad&#601; edilm&#601;si v&#601; ya
da&#351;&#305;nmas&#305;, saxlan&#305;lmas&#305; üçün xüsusi qaydalara
&#601;m&#601;l olunmas&#305; z&#601;ruridirs&#601;, onda istehsalç&#305;
(icraç&#305;) bel&#601; qaydalar&#305; mü&#601;yy&#601;nl&#601;&#351;dirm&#601;li,
sat&#305;c&#305; (icraç&#305;) is&#601; bunlar&#305; istehlakç&#305;n&#305;n
n&#601;z&#601;rin&#601; çatd&#305;rmal&#305;d&#305;r.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>5.&nbsp;&nbsp;&nbsp;Qanunvericilik aktlar&#305; il&#601; v&#601; ya
dig&#601;r normativ s&#601;n&#601;dl&#601;rd&#601;
v&#601;t&#601;nda&#351;lar&#305;n h&#601;yat&#305;,
sa&#287;laml&#305;&#287;&#305; v&#601; &#601;mlak&#305;, &#601;traf mühitin
mühafiz&#601;si üçün t&#601;hlük&#601;sizlik t&#601;l&#601;bl&#601;rinin
mü&#601;yy&#601;n edildiyi mallar (i&#351;l&#601;r, xidm&#601;tl&#601;r)
qüvv&#601;d&#601; olan qanunvericiliy&#601; &#601;sas&#601;n m&#601;cburi
sertifikasiyadan keçirilm&#601;lidir. Göst&#601;ril&#601;n
t&#601;l&#601;bl&#601;r&#601; uy&#287;unlu&#287;unu t&#601;sdiq ed&#601;n
sertifikat olmad&#305;qda bel&#601; mallar&#305; (o cüml&#601;d&#601;n xarici
mallar&#305;) Az&#601;rbaycan Respublikas&#305;nda satmaq v&#601; istifad&#601;
etm&#601;k qada&#287;and&#305;r.</span><span lang=AZ-LATIN style='font-size:
10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Bel&#601; mallar&#305;n Az&#601;rbaycan Respublikas&#305;n&#305;n
&#601;razisin&#601; idxal edilm&#601;sin&#601; raz&#305;l&#305;q üçün gömrük
orqanlar&#305;na t&#601;qdim edil&#601;n, müvafiq s&#601;lahiyy&#601;tli orqan
t&#601;r&#601;find&#601;n verilmi&#351; v&#601; ya da tan&#305;nm&#305;&#351;
uy&#287;unluq sertifikat&#305; &#601;sas verir.</span><span lang=AZ-LATIN
                                                            style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Bu b&#601;ndl&#601; n&#601;z&#601;rd&#601; tutulmu&#351;
mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin) t&#601;hlük&#601;siz
olmas&#305; bar&#601;d&#601; t&#601;l&#601;bl&#601;rin pozulmas&#305;na
gör&#601; m&#601;suliyy&#601;t bu Qanunla v&#601; dig&#601;r qanunvericilik
aktlar&#305; il&#601; mü&#601;yy&#601;n edilir.</span><span lang=AZ-LATIN
                                                            style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>6. &#304;stehlakç&#305; mal&#305;n (i&#351;in, xidm&#601;tin
n&#601;tic&#601;l&#601;rinin) istifad&#601;si, da&#351;&#305;nmas&#305; v&#601;
saxlan&#305;lmas&#305; qaydalar&#305;na düzgün &#601;m&#601;l etdikd&#601;,
lakin h&#601;min mal (i&#351;, xidm&#601;t) onun h&#601;yat&#305;na,
sa&#287;laml&#305;&#287;&#305;na, &#601;mlak&#305;na v&#601; ya &#601;traf
mühit&#601; z&#601;r&#601;r vurduqda v&#601; ya z&#601;r&#601;r vura
bil&#601;rs&#601;, istehsalç&#305; (icraç&#305;, sat&#305;c&#305;) onun
istehsal&#305;n&#305; (sat&#305;&#351;&#305;n&#305;) z&#601;r&#601;r vura
bil&#601;n s&#601;b&#601;bl&#601;r aradan qald&#305;r&#305;lanad&#601;k
dayand&#305;rmal&#305;, z&#601;ruri hallarda is&#601; onun
dövriyy&#601;d&#601;n ç&#305;xar&#305;lmas&#305; v&#601; istehlakç&#305;dan
geri qaytar&#305;lmas&#305; üçün t&#601;dbirl&#601;r görm&#601;lidir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Z&#601;r&#601;rin s&#601;b&#601;bl&#601;rini aradan qald&#305;rmaq
mümkün deyildirs&#601;, istehsalç&#305; (icraç&#305;) bel&#601; mallar&#305;
(i&#351;l&#601;ri, xidm&#601;tl&#601;ri) istehsaldan ç&#305;xarmaqla,
dövriyy&#601;d&#601;n y&#305;&#287;ma&#287;a v&#601; istehlakç&#305;dan geri
alma&#287;a borcludur. &#304;stehsalç&#305; (icraç&#305;) bu
v&#601;zif&#601;l&#601;rini yerin&#601; yetirm&#601;dikd&#601; mallar&#305;
(i&#351;l&#601;ri, xidm&#601;tl&#601;ri) istehsaldan ç&#305;xarmaq,
dövriyy&#601;d&#601;n y&#305;&#287;maq v&#601; istehlakç&#305;lardan geri almaq
haqq&#305;nda q&#601;rar&#305; müvafiq dövl&#601;t n&#601;zar&#601;t
orqanlar&#305; öz s&#601;lahiyy&#601;tl&#601;ri ç&#601;rçiv&#601;sind&#601;
q&#601;bul etm&#601;lidirl&#601;r. Bu h&#601;r&#601;k&#601;t
n&#601;tic&#601;sind&#601; &#601;rzaq xammal&#305; v&#601; m&#601;hsullar&#305;
qidalanma üçün yarars&#305;z say&#305;larsa, onlar m&#601;hv v&#601; ya
t&#601;krar emal edilm&#601;lidirl&#601;r.</span><span lang=AZ-LATIN
                                                       style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin) geri
qaytar&#305;lmas&#305; il&#601; &#601;laq&#601; dar istehlakç&#305;ya
d&#601;y&#601;n z&#601;r&#601;ri istehsalç&#305; (icraç&#305;) tam
h&#601;cmd&#601; öd&#601;m&#601;lidir.</span><span lang=AZ-LATIN
                                                   style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>7.&nbsp;&nbsp;&nbsp;Bu madd&#601;nin 4-cü v&#601; 5-ci
b&#601;ndl&#601;rind&#601; mü&#601;yy&#601;n edilmi&#351;
t&#601;l&#601;bl&#601;r pozulduqda, mallar&#305;n keyfiyy&#601;tin&#601;
dövl&#601;t n&#601;zar&#601;tini h&#601;yata keçir&#601;n orqanlar&#305;n
q&#601;rar&#305; il&#601; istehsalç&#305; (icraç&#305;) h&#601;min qaydalar
pozulmaqla sat&#305;lm&#305;&#351; mallardan &#601;ld&#601; etdiyi g&#601;liri
dövl&#601;t büdc&#601;sin&#601; köçürür.</span><span lang=AZ-LATIN
                                                     style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>8.&nbsp;&nbsp;&nbsp;Yeni (modernl&#601;&#351;dirilmi&#351;) mal
(i&#351;, xidm&#601;t) haz&#305;rlanark&#601;n istehsalç&#305;
v&#601;t&#601;nda&#351;lar&#305;n h&#601;yat&#305;n&#305;n,
sa&#287;laml&#305;&#287;&#305;n&#305;n, &#601;mlak&#305;n&#305;n, h&#601;mçinin
&#601;traf mühitin t&#601;hlük&#601;sizliyi t&#601;l&#601;bl&#601;rin&#601;
cavab verm&#601;sini, t&#601;sdiq etm&#601;k v&#601; dövl&#601;t
ekspertizas&#305;ndan keçirm&#601;k üçün h&#601;min mal&#305;n normativ
s&#601;n&#601;dl&#601;rini müvafiq orqana t&#601;qdim etm&#601;lidir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>9.&nbsp;&nbsp;&nbsp;&#304;stehsalç&#305; (icraç&#305;) malda
(i&#351;d&#601;, xidm&#601;td&#601;) ehtimal do&#287;uracaq x&#601;t&#601;r
(risk) v&#601; t&#601;hlük&#601;siz istifad&#601;si bar&#601;d&#601;
beyn&#601;lxalq t&#601;crüb&#601;l&#601;rd&#601; q&#601;bul edilmi&#351;
i&#351;ar&#601;l&#601;r vasit&#601;si il&#601; istehlakç&#305;ya m&#601;lumat
verm&#601;y&#601; borcludur.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;11.&nbsp;<b>Laz&#305;mi keyfiyy&#601;ti olmayan mal&#305;n
(i&#351;in, xidm&#601;tin) vurdu&#287;u z&#601;r&#601;r&#601; gör&#601;
&#601;mlak m&#601;suliyy&#601;ti&nbsp;</b></span><a name="_ednref9"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn9"><span
                            style='mso-bookmark:_ednref9'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[9]</span></sup></b></span><span
                            style='mso-bookmark:_ednref9'></span></a><span style='mso-bookmark:_ednref9'></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Mal&#305;n (i&#351;in, xidm&#601;tin) konstruksiya, istehsal, resept
v&#601; ba&#351;qa qüsurlar&#305; n&#601;tic&#601;sind&#601;
istehlakç&#305;n&#305;n h&#601;yat&#305;na, sa&#287;laml&#305;&#287;&#305;na
v&#601; ya &#601;mlak&#305;na d&#601;ymi&#351; z&#601;r&#601;r,
qanunvericilikd&#601; daha yüks&#601;k m&#601;suliyy&#601;t
n&#601;z&#601;rd&#601; tutulmay&#305;bsa, günahkar t&#601;r&#601;find&#601;n
istehlakç&#305;ya tam öd&#601;nilm&#601;lidir.</span><span lang=AZ-LATIN
                                                           style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Mal&#305;n, i&#351;in v&#601; ya xidm&#601;tin qüsurlar&#305;
n&#601;tic&#601;sind&#601; &#601;mlaka z&#601;r&#601;r vurulan</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>&nbsp;</span><span lang=AZ-LATIN
                                                           style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>hallarda bu
qayda yaln&#305;z o &#351;&#601;rtl&#601; t&#601;tbiq edilir ki,
keyfiyy&#601;tsiz m&#601;hsul dig&#601;r &#601;mlaka z&#601;r&#601;r
vurmu&#351; v&#601; h&#601;min dig&#601;r &#601;mlak öz t&#601;yinat&#305;na gör&#601;
&#601;sas&#601;n istehlak m&#601;qs&#601;di üçün istifad&#601; edilmi&#351;
olsun.</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;12.&nbsp;<b>M&#601;n&#601;vi ziyan&#305;n öd&#601;nilm&#601;si</b></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;lar&#305;n bu Qanunda n&#601;z&#601;rd&#601;
tutulan hüquqlar&#305; istehsalç&#305; (icraç&#305;, sat&#305;c&#305;)
t&#601;r&#601;find&#601;n pozularsa, istehlakç&#305;ya d&#601;y&#601;n
m&#601;n&#601;vi ziyan günahkar t&#601;r&#601;find&#601;n öd&#601;nilm&#601;lidir.
Öd&#601;nil&#601;n ziyan&#305;n d&#601;y&#601;ri, qanunla ba&#351;qa hal
n&#601;z&#601;rd&#601; tutulmay&#305;bsa, m&#601;hk&#601;m&#601;
t&#601;r&#601;find&#601;n mü&#601;yy&#601;n edilir.</span><span lang=AZ-LATIN
                                                                style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#601; 13.&nbsp;<b>&#304;stehlakç&#305;lar&#305;n mal (i&#351;,
xidm&#601;t) haqq&#305;nda m&#601;lumat almaq hüququ</b></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>1.&nbsp;&nbsp;&nbsp;Sat&#305;c&#305; (icraç&#305;)
istehlakç&#305;n&#305; maraqland&#305;ran mal&#305;n (i&#351;in, xidm&#601;tin)
qiym&#601;ti, istehlak xass&#601;l&#601;ri (&#601;rzaq mallar&#305;
bar&#601;sind&#601; is&#601;; h&#601;m d&#601; t&#601;rkibi,
yararl&#305;l&#305;q müdd&#601;ti, kaloriliyi, sa&#287;laml&#305;q üçün
z&#601;r&#601;rli madd&#601;l&#601;rin normativ s&#601;n&#601;dl&#601;rin
t&#601;l&#601;bl&#601;ri il&#601; müqayis&#601;li miqdar&#305;), &#601;ld&#601;
edilm&#601;si &#351;&#601;rtl&#601;ri, t&#601;minat öhd&#601;likl&#601;ri
v&#601; iddialar&#305;n ir&#601;li sürülm&#601;si, mal&#305;n
i&#351;l&#601;dilm&#601;si, saxlan&#305;lmas&#305; v&#601; t&#601;hlük&#601;siz
istifad&#601;si üsullar&#305; v&#601; qaydalar&#305; bar&#601;sind&#601;
istehlakç&#305;ya z&#601;ruri v&#601; düzgün m&#601;lumat verm&#601;lidir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Sat&#305;c&#305; (icraç&#305;) istehlakç&#305;ya
mü&#601;ssis&#601;nin satd&#305;&#287;&#305; mallar&#305;n ticar&#601;t
qaydalar&#305; v&#601; xidm&#601;t növl&#601;ri bar&#601;sind&#601; d&#601; dol&#287;un
v&#601; düzgün m&#601;lumat verm&#601;lidir.</span><span lang=AZ-LATIN
                                                         style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Az&#601;rbaycan Respublikas&#305;n&#305;n &#601;razisind&#601;
istehsal edil&#601;n, habel&#601; ixrac edil&#601;n mallar&#305;n
üz&#601;rind&#601;ki etiketl&#601;r v&#601; dig&#601;r yaz&#305;lar müvafiq
xarici dill&#601;rl&#601; yana&#351;&#305;, dövl&#601;t dilind&#601; d&#601;
olmal&#305;d&#305;r. Az&#601;rbaycan Respublikas&#305;na idxal edil&#601;n mal
v&#601; m&#601;hsullar&#305;n üz&#601;rind&#601;ki etiketl&#601;r v&#601;
adlar, onlardan istifad&#601; qaydalar&#305; bar&#601;d&#601; izahat
v&#601;r&#601;q&#601;l&#601;ri ba&#351;qa dill&#601;rl&#601; yana&#351;&#305;,
Az&#601;rbaycan dilin&#601; t&#601;rcüm&#601;si il&#601; mü&#351;ayi&#601;t
olunmal&#305;d&#305;r.&nbsp;</span><a name="_ednref10"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn10"><span
                            style='mso-bookmark:_ednref10'><sup><span lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:blue;mso-ansi-language:AZ-LATIN'>[10]</span></sup></span><span
                            style='mso-bookmark:_ednref10'></span></a><span style='mso-bookmark:_ednref10'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Sat&#305;c&#305; (icraç&#305;) Az&#601;rbaycan
Respublikas&#305;n&#305;n &#601;razisind&#601; satd&#305;&#287;&#305;
mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin) qiym&#601;tl&#601;rini
yaln&#305;z manatla göst&#601;rm&#601;lidir.&nbsp;</span><a name="_ednref11"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn11"><span
                            style='mso-bookmark:_ednref11'><sup><span lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:blue;mso-ansi-language:AZ-LATIN'>[11]</span></sup></span><span
                            style='mso-bookmark:_ednref11'></span></a><span style='mso-bookmark:_ednref11'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>2.&nbsp;&nbsp;&nbsp;Bu madd&#601;nin 1-ci b&#601;ndind&#601;
n&#601;z&#601;rd&#601; tutulmu&#351; m&#601;lumat, mala &#601;lav&#601; olunan
texniki s&#601;n&#601;dl&#601;r, habel&#601; ni&#351;anlanma, haz&#305;rlanma
v&#601; sat&#305;&#351; tarixinin göst&#601;rilm&#601;si vasit&#601;sil&#601;
v&#601; ya ayr&#305;-ayr&#305; xidm&#601;t sah&#601;l&#601;rind&#601;
q&#601;bul olunmu&#351; ba&#351;qa üsulla istehlakç&#305;n&#305;n n&#601;z&#601;rin&#601;
çatd&#305;r&#305;l&#305;r.</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>3.&nbsp;&nbsp;&nbsp;&#304;stehsal edilmi&#351; mal&#305;n
üz&#601;rind&#601; istehsal v&#601; ya ticar&#601;t markas&#305;,
&#601;mt&#601;&#601; ni&#351;an&#305; v&#601; ya co&#287;rafi göst&#601;rici
olmal&#305;d&#305;r. &#304;stehsal markas&#305;nda istehsalç&#305;n&#305;n
(icraç&#305;n&#305;n) ad&#305;, m&#601;nsubiyy&#601;ti, yeri v&#601;
standartlar&#305;n (normativ s&#601;n&#601;dl&#601;rin) i&#351;ar&#601;si göst&#601;rilir
haz&#305;rlanm&#305;&#351; mal istehsal markas&#305;na uy&#287;un
g&#601;lm&#601;lidir. Sahibkarl&#305;q f&#601;aliyy&#601;ti il&#601;
m&#601;&#351;&#287;ul olan &#351;&#601;xsin haz&#305;rlad&#305;&#287;&#305;
mal&#305;n (i&#351;in, xidm&#601;tin) etiketi (yarl&#305;&#287;&#305;)
olmal&#305;d&#305;r. Etiketd&#601; sahibkarl&#305;q f&#601;aliyy&#601;ti
il&#601; m&#601;&#351;&#287;ul olmaq hüququ ver&#601;n s&#601;n&#601;din
nömr&#601;si, onu t&#601;sdiq ed&#601;n orqan&#305;n ad&#305;, z&#601;ruri
hallarda onun sertifikatla&#351;d&#305;r&#305;lmas&#305;, xüsusi
t&#601;l&#601;bl&#601;r&#601; cavab ver&#601;n mallar (i&#351;l&#601;r,
xidm&#601;tl&#601;r) üçün is&#601; dövl&#601;t standartlar&#305;n&#305;n
nömr&#601;si haqq&#305;nda m&#601;lumat verilm&#601;lidir.&nbsp;</span><a
                        name="_ednref12"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn12"><span
                            style='mso-bookmark:_ednref12'><sup><span lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:blue;mso-ansi-language:AZ-LATIN'>[12]</span></sup></span><span
                            style='mso-bookmark:_ednref12'></span></a><span style='mso-bookmark:_ednref12'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>4.&nbsp;&nbsp;&nbsp;Sat&#305;lan mal bar&#601;sind&#601;
yanl&#305;&#351; m&#601;lumat&#305;n v&#601; ya kifay&#601;t q&#601;d&#601;r
dol&#287;un olmayan m&#601;lumat&#305;n verilm&#601;si, istehlakç&#305;n&#305;n
laz&#305;mi xass&#601;l&#601;r&#601; malik olmayan mal&#305; (i&#351;i,
xidm&#601;ti) almas&#305;na s&#601;b&#601;b olmu&#351;dursa, onun
müqavil&#601;ni l&#601;&#287;v etm&#601;k v&#601; itkil&#601;rin
öd&#601;nilm&#601;sini t&#601;l&#601;b etm&#601;k hüququ vard&#305;r.</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal'><i><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>5. &#304;stehsal&#305; (sat&#305;&#351;&#305;)
lisenziyala&#351;d&#305;r&#305;lan, sertifikatla&#351;d&#305;r&#305;lan
&#601;mt&#601;&#601;l&#601;r yaln&#305;z müvafiq t&#601;sdiql&#601;yici
s&#601;n&#601;dl&#601;r olduqda reklam edil&#601; bil&#601;r. Bu halda reklamda
lisenziyan&#305;n v&#601; sertifikat&#305;n nömr&#601;si, verilm&#601; tarixi
v&#601; onu ver&#601;n orqan&#305;n ad&#305; göst&#601;rilm&#601;lidir.</span></i><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal'><i><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>6. Haqs&#305;z, qeyri-d&#601;qiq v&#601; gizli reklam
n&#601;tic&#601;sind&#601; reklam istehlakç&#305;s&#305;na d&#601;y&#601;n
z&#601;r&#601;r m&#601;hk&#601;m&#601;nin q&#601;rar&#305; il&#601;
t&#601;qsirkar bilinmi&#351; reklam f&#601;aliyy&#601;tinin subyekti
t&#601;r&#601;find&#601;n öd&#601;nilir.</span></i><a name="_ednref13"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn13"><span
                            style='mso-bookmark:_ednref13'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[13]</span></sup></b></span><span
                            style='mso-bookmark:_ednref13'></span></a><span style='mso-bookmark:_ednref13'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>7. Mal (i&#351;, xidm&#601;t) haqq&#305;nda yanl&#305;&#351; v&#601;
ya yar&#305;mç&#305;q m&#601;lumat, yaxud haqs&#305;z<i>, qeyri-d&#601;qiq
v&#601; gizli</i>&nbsp;reklam n&#601;tic&#601;sind&#601; d&#601;y&#601;n
z&#601;r&#601;ri öd&#601;m&#601;k bar&#601;sind&#601; istehlakç&#305;n&#305;n
t&#601;l&#601;bl&#601;rin&#601; bax&#305;lark&#601;n onun &#601;ld&#601; etdiyi
mal&#305;n (i&#351;in, xidm&#601;tin) xass&#601;l&#601;rin&#601; v&#601;
xarakteristikas&#305;na dair xüsusi biliyinin olmamas&#305; ehtimal&#305;
&#601;sas götürülm&#601;lidir.</span><a name="_ednref14"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn14"><span
                            style='mso-bookmark:_ednref14'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[14]</span></sup></b></span><span
                            style='mso-bookmark:_ednref14'></span></a><span style='mso-bookmark:_ednref14'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>8. Dövl&#601;t istehlakç&#305;lara öz hüquqlar&#305; v&#601;
onlar&#305;n müdafi&#601;si haqq&#305;nda z&#601;ruri m&#601;lumat almaq üçün
&#351;&#601;rait yarad&#305;r.</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;14.&nbsp;<b>Ticar&#601;t v&#601; ba&#351;qa xidm&#601;t
növl&#601;ri sah&#601;l&#601;rind&#601; istehlakç&#305;lar&#305;n
hüquqlar&#305;</b></span><span lang=RU style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>1.&nbsp;&nbsp;&nbsp;Bütün v&#601;t&#601;nda&#351;lar ticar&#601;t
v&#601; ba&#351;qa xidm&#601;t növl&#601;ri sah&#601;sind&#601;
t&#601;l&#601;batlar&#305;n&#305;n öd&#601;nilm&#601;sind&#601; b&#601;rab&#601;r
hüquqa malikdirl&#601;r.</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Qanunvericilik aktlar&#305;nda n&#601;z&#601;rd&#601; tutulmu&#351;
hallardan ba&#351;qa, istehlakç&#305;lar&#305;n hüquqlar&#305;na üstünlük
verilm&#601;sin&#601;, birba&#351;a v&#601; ya dolay&#305;s&#305; il&#601;
h&#601;r hans&#305; &#351;&#601;kild&#601;
m&#601;hdudla&#351;d&#305;r&#305;lmas&#305;na yol verilmir. Sosial
müdafi&#601;y&#601; ehtiyac&#305; olan v&#601;t&#601;nda&#351;lar&#305;n
ayr&#305;-ayr&#305; qruplar&#305;na ticar&#601;t v&#601; dig&#601;r xidm&#601;t
növl&#601;ri sah&#601;sind&#601; Az&#601;rbaycan Respublikas&#305;n&#305;n
qanunvericilik aktlar&#305; il&#601; mü&#601;yy&#601;n edilmi&#351; qaydada güz&#601;&#351;tl&#601;r
v&#601; üstünlükl&#601;r veril&#601; bil&#601;r.</span><span lang=AZ-LATIN
                                                             style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>2.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305; sat&#305;c&#305;n&#305;n
(icraç&#305;n&#305;n) i&#351; rejimini n&#601;z&#601;r&#601; almaqla, özü üçün
&#601;lveri&#351;li olan vaxtda s&#601;rb&#601;st mal v&#601; xidm&#601;t
seçm&#601;k hüququna malikdir.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;ya s&#601;rb&#601;st mal v&#601; xidm&#601;t
seçm&#601;kd&#601; h&#601;r cür köm&#601;klik göst&#601;rm&#601;k
sat&#305;c&#305;n&#305;n (icraç&#305;n&#305;n) borcudur.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;n&#305; keyfiyy&#601;tsiz v&#601; ya ona
laz&#305;m olmayan çe&#351;idli mal alma&#287;a v&#601; xidm&#601;t
göst&#601;rilm&#601;sin&#601; m&#601;cbur etm&#601;k qada&#287;an edilir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>3.&nbsp;&nbsp;&nbsp;Sat&#305;c&#305; (icraç&#305;) istehlakç&#305;ya
öz mü&#601;ssis&#601;sinin ad&#305;, m&#601;nsubiyy&#601;ti v&#601; i&#351;
rejimi bar&#601;d&#601; düzgün v&#601; ba&#351;a dü&#351;ül&#601;n m&#601;lumat
verm&#601;lidir.</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black;
mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>4.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305; ald&#305;&#287;&#305;
mallar&#305;n keyfiyy&#601;tini, komplektliyini, ölçüsünü, ç&#601;kisini
v&#601; qiym&#601;tini yoxlamaq, mallar&#305;n düzgün v&#601;
t&#601;hlük&#601;siz istifad&#601;sini nümayi&#351; etdirm&#601;k hüququna
malikdir. Bel&#601; hallarda sat&#305;c&#305; istehlakç&#305;n&#305;n
t&#601;l&#601;bi il&#601; ona n&#601;zar&#601;t-ölçü cihazlar&#305;n&#305;,
mal&#305;n qiym&#601;ti haqq&#305;nda s&#601;n&#601;dl&#601;ri t&#601;qdim
etm&#601;lidir. Z&#601;man&#601;t Müdd&#601;tind&#601; mal&#305;n
keyfiyy&#601;tinin itm&#601;si s&#601;b&#601;bl&#601;rini mü&#601;yy&#601;n
etm&#601;k laz&#305;m g&#601;ldiyi halda sat&#305;c&#305;
istehlakç&#305;n&#305;n yaz&#305;l&#305; &#601;riz&#601;sini ald&#305;qdan
sonra 3 gün &#601;rzind&#601; bu mal&#305; ekspertizaya
gönd&#601;rm&#601;lidir. Ekspertiza sat&#305;c&#305;n&#305;n hesab&#305;na
apar&#305;l&#305;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>5.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305; sat&#305;c&#305;n&#305;n
süni sur&#601;td&#601; yaratd&#305;&#287;&#305; d&#601;std&#601;n ona
laz&#305;m olan mal&#305; almaq hüququna malikdir.</span><span lang=AZ-LATIN
                                                               style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>6.&nbsp;&nbsp;&nbsp;Ticar&#601;t v&#601; dig&#601;r xidm&#601;t
növl&#601;ri mü&#601;ssis&#601;l&#601;rind&#601; istehlakç&#305;n&#305;n
hüquqlar&#305; pozularsa, sat&#305;c&#305; (icraç&#305;) v&#601; bu
mü&#601;ssis&#601;l&#601;rin i&#351;çil&#601;ri qanunvericilikl&#601;
mü&#601;yy&#601;n olunmu&#351; qaydada m&#601;suliyy&#601;t
da&#351;&#305;y&#305;rlar.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;15.&nbsp;<b>&#304;stehlakç&#305;n&#305;n
t&#601;l&#601;bl&#601;rini öd&#601;m&#601;y&#601;n mal&#305; laz&#305;mi
keyfiyy&#601;tli mala d&#601;yi&#351;dirm&#601;k hüququ</b></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>1.&nbsp;&nbsp;&nbsp;Laz&#305;mi keyfiyy&#601;tli qeyri-&#601;rzaq
mal&#305; öz formas&#305;na, ölçüsün&#601;, fasonuna, r&#601;ngin&#601;
gör&#601; istehlakç&#305;ya yaram&#305;rsa v&#601; ya dig&#601;r
s&#601;b&#601;bl&#601;r&#601; gör&#601; t&#601;yinat&#305; üzr&#601;
istifad&#601; oluna bilm&#601;zs&#601;, istehlakç&#305;n&#305;n onu
al&#305;nd&#305;&#287;&#305; yerd&#601; uy&#287;un mala
d&#601;yi&#351;dirm&#601;k hüququ vard&#305;r.</span><span lang=AZ-LATIN
                                                           style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305; mal&#305;n al&#305;nma günü say&#305;lmamaq
&#351;&#601;rti il&#601;, 14 gün &#601;rzind&#601; h&#601;min mal&#305;
laz&#305;mi keyfiyy&#601;tli mala d&#601;yi&#351;dirm&#601;k hüququna malikdir.
P&#601;rak&#601;nd&#601; sat&#305;lan mal&#305;n d&#601;yi&#351;dirilm&#601;si
üçün sat&#305;c&#305; t&#601;r&#601;find&#601;n daha uzun müdd&#601;t elan
edil&#601; bil&#601;r.&nbsp;</span><a name="_ednref15"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn15"><span
                            style='mso-bookmark:_ednref15'><sup><span lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:blue;mso-ansi-language:AZ-LATIN'>[15]</span></sup></span><span
                            style='mso-bookmark:_ednref15'></span></a><span style='mso-bookmark:_ednref15'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305; t&#601;r&#601;find&#601;n &#601;ld&#601;
edilmi&#351; laz&#305;mi keyfiyy&#601;tli mal istifad&#601; olunmay&#305;bsa
v&#601; onun &#601;mt&#601;&#601; görünü&#351;ü, istehlak xass&#601;l&#601;ri,
plombu, yarl&#305;&#287;&#305;, h&#601;mçinin mal v&#601; yaxud kassa
q&#601;bzi v&#601; ya ona mal il&#601; birlikd&#601; verilmi&#351; dig&#601;r
s&#601;n&#601;dl&#601;ri saxlan&#305;l&#305;bsa, bu hallarda o d&#601;yi&#351;diril&#601;
bil&#601;r.</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Bu madd&#601;d&#601; göst&#601;rilmi&#351; &#601;saslar üzr&#601;
d&#601;yi&#351;dirilm&#601;li olmayan mallar&#305;n siyah&#305;s&#305;
Az&#601;rbaycan Respublikas&#305;n&#305;n Nazirl&#601;r Kabineti
t&#601;r&#601;find&#601;n t&#601;sdiq edilir.</span><span lang=RU
                                                          style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>2.&nbsp;&nbsp;&nbsp;Mal&#305; d&#601;yi&#351;dirm&#601; an&#305;nda
sat&#305;&#351;da uy&#287;un mal yoxdursa, istehlakç&#305; d&#601;y&#601;ri
yenid&#601;n hesablamaqla ist&#601;nil&#601;n ba&#351;qa bir mal&#305; almaq
v&#601; ya qaytar&#305;lan mal&#305;n d&#601;y&#601;ri
m&#601;bl&#601;&#287;ind&#601; pulu geri götürm&#601;k, ya da sat&#305;&#351;a
uy&#287;un mal g&#601;l&#601;n kimi onu d&#601;yi&#351;dirm&#601;k hüququna
malikdir. Sat&#305;c&#305; mal&#305;n sat&#305;&#351;a daxil oldu&#287;u gün
mal&#305;n d&#601;yi&#351;dirilm&#601;sini t&#601;l&#601;b ed&#601;n istehlakç&#305;ya
m&#601;lumat verm&#601;lidir.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;16.&nbsp;<b>&#304;stehlakç&#305;n&#305;n hüquqlar&#305;n&#305;
m&#601;hdudla&#351;d&#305;ran müqavil&#601; &#351;&#601;rtl&#601;rinin
etibars&#305;zl&#305;&#287;&#305;</b></span><span lang=AZ-LATIN
                                                  style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Qanunvericilikd&#601; n&#601;z&#601;rd&#601; tutulan hüquqlarla
müqayis&#601;d&#601; istehlakç&#305;n&#305;n hüquqlar&#305;n&#305;
m&#601;hdudla&#351;d&#305;ran müqavil&#601; &#351;&#601;rtl&#601;ri
etibars&#305;z say&#305;l&#305;r. &#304;stehlakç&#305;n&#305;n
hüquqlar&#305;n&#305; m&#601;hdudla&#351;d&#305;ran müqavil&#601;
&#351;&#601;rtl&#601;rinin t&#601;tbiqi n&#601;tic&#601;sind&#601;
istehlakç&#305;ya z&#601;r&#601;r d&#601;yibs&#601;, onlar t&#601;qsirkar
&#351;&#601;xs t&#601;r&#601;find&#601;n tam h&#601;cmd&#601;
öd&#601;nilm&#601;lidir.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehsalç&#305; (icraç&#305;, sat&#305;c&#305;) istehsalat
v&#601; ya ticar&#601;t f&#601;aliyy&#601;tind&#601; tutdu&#287;u
v&#601;zif&#601; üstünlükl&#601;rind&#601;n istifad&#601; ed&#601;r&#601;k
istehlakç&#305;ya z&#601;r&#601;r vurduqda istehlakç&#305; ona vurulmu&#351;
z&#601;r&#601;rin öd&#601;nilm&#601;si hüququna malikdir.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black;
mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;text-indent:17.85pt;line-height:normal'><span lang=AZ-LATIN
                                                                style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>III f &#601;
s i l</span><span style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;STEHLAKÇILARIN HÜQUQLARININ MÜDAF&#304;&#399;S&#304;</span></b><span
                        style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;17.&nbsp;<b>Ticar&#601;t v&#601; ba&#351;qa xidm&#601;t
növl&#601;ri haqq&#305;nda qaydalar</b></span><span style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Ticar&#601;t, m&#601;i&#351;&#601;t v&#601; dig&#601;r növ
xidm&#601;t (i&#351; görülm&#601;si, xidm&#601;t göst&#601;rilm&#601;si)
qaydalar&#305; Az&#601;rbaycan Respublikas&#305; Nazirl&#601;r Kabineti
t&#601;r&#601;find&#601;n t&#601;sdiq edilir. Bu qaydalar
“&#304;stehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n müdafi&#601;si
haqq&#305;nda” Az&#601;rbaycan Respublikas&#305; Qanununa v&#601; dig&#601;r
qanunvericilik aktlar&#305;na zidd olmamal&#305;d&#305;r.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Ticar&#601;t, m&#601;i&#351;&#601;t v&#601; dig&#601;r növ
xidm&#601;tl&#601;rin göst&#601;rilm&#601;si zaman&#305;
mü&#351;t&#601;ril&#601;rl&#601; hesabla&#351;malar&#305;n
apar&#305;lmas&#305;, habel&#601; vergil&#601;rin v&#601; dövl&#601;t
orqanlar&#305; t&#601;r&#601;find&#601;n göst&#601;ril&#601;n
xidm&#601;tl&#601;r&#601; (i&#351;l&#601;r&#601;) gör&#601; dövl&#601;t
rüsumlar&#305;n&#305;n v&#601; haqlar&#305;n&#305;n öd&#601;nilm&#601;si üçün
müvafiq POS-terminallar qura&#351;d&#305;r&#305;lmal&#305;d&#305;r.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>POS-terminallar qura&#351;d&#305;r&#305;lacaq obyektl&#601;rin
mü&#601;yy&#601;nl&#601;&#351;dirilm&#601;si meyarlar&#305;, &#601;razil&#601;r
üzr&#601; POS-terminallar&#305;n m&#601;rh&#601;l&#601;l&#601;rl&#601;
t&#601;tbiqi qaydas&#305; v&#601; c&#601;dv&#601;li müvafiq icra
hakimiyy&#601;ti orqan&#305; t&#601;r&#601;find&#601;n mü&#601;yy&#601;n
edilir.</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Bu madd&#601;nin ikinci hiss&#601;sind&#601; n&#601;z&#601;rd&#601;
tutulan POS-terminallar&#305;n qura&#351;d&#305;r&#305;lmamas&#305; Qanunla
n&#601;z&#601;rd&#601; tutulmu&#351; m&#601;suliyy&#601;t&#601; s&#601;b&#601;b
olur.</span><a name="_ednref16"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn16"><span
                            style='mso-bookmark:_ednref16'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[16]</span></sup></b></span><span
                            style='mso-bookmark:_ednref16'></span></a><span style='mso-bookmark:_ednref16'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-indent:
27.0pt;line-height:normal;background:white'><span lang=AZ-LATIN
                                                  style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-indent:
20.0pt;line-height:normal;background:white'><span lang=AZ-LATIN
                                                  style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>Madd&#601;
18.&nbsp;<b>&#304;stehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n
müdafi&#601;si üzr&#601; dövl&#601;t siyas&#601;ti&nbsp;</b></span><a
                        name="_ednref17"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn17"><span
                            style='mso-bookmark:_ednref17'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[17]</span></sup></b></span><span
                            style='mso-bookmark:_ednref17'></span></a><span style='mso-bookmark:_ednref17'></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-indent:
20.0pt;line-height:normal;background:white'><span lang=AZ-LATIN
                                                  style='font-size:10.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black;
mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>18.0. &#304;stehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n müdafi&#601;si üzr&#601; dövl&#601;t siyas&#601;ti
mallar&#305;n (xidm&#601;tl&#601;rin, i&#351;l&#601;rin)
istehlakç&#305;lar&#305;n&#305;n (istifad&#601;çil&#601;rinin) qanuni
maraqlar&#305;n&#305;n qorunmas&#305; m&#601;qs&#601;dil&#601; özünd&#601;
a&#351;a&#287;&#305;dak&#305; t&#601;l&#601;bl&#601;ri ehtiva edir:</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>18.0.1. istehlak bazar&#305;nda sat&#305;lan
mallar&#305;n (xidm&#601;tl&#601;rin, i&#351;l&#601;rin), o
cüml&#601;d&#601;n&nbsp;<s>yeyinti m&#601;hsullar&#305;n&#305;n v&#601;</s>&nbsp;d&#601;rman
vasit&#601;l&#601;rinin qabla&#351;d&#305;r&#305;lmas&#305;n&#305;n,
da&#351;&#305;nmas&#305;n&#305;n, saxlanmas&#305;n&#305;n v&#601;
sat&#305;&#351; &#351;&#601;raitinin sanitar-gigiyenik, ekoloji, texniki,
t&#601;hlük&#601;sizlik standartlar&#305;na, ticar&#601;t norma v&#601; qaydalar&#305;na<i>,
yeyinti m&#601;hsullar&#305;n&#305;n is&#601; qida t&#601;hlük&#601;sizliyi
sah&#601;sind&#601; texniki normativ hüquqi aktlara</i>&nbsp;uy&#287;un
olmas&#305;na dövl&#601;t n&#601;zar&#601;tini;</span><a name="_ednref18"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn18"><span
                            style='mso-bookmark:_ednref18'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[18]</span></sup></b></span><span
                            style='mso-bookmark:_ednref18'></span></a><span style='mso-bookmark:_ednref18'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>18.0.2. mallar&#305;n
(xidm&#601;tl&#601;rin, i&#351;l&#601;rin) b&#601;zi növl&#601;rinin
istehsal&#305;na, idxal&#305;na v&#601; sat&#305;&#351;&#305;na xüsusi
raz&#305;l&#305;q (lisenziya) verilm&#601;sini;</span><span lang=RU
                                                            style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>18.0.3. m&#601;cburi
sertifikatla&#351;d&#305;r&#305;lmal&#305; olan mallar&#305;n
(xidm&#601;tl&#601;rin, i&#351;l&#601;rin)
mü&#601;yy&#601;nl&#601;&#351;dirilm&#601;si v&#601; onlara uy&#287;unluq
sertifikat&#305;n&#305;n v&#601; (v&#601; ya) uy&#287;unluq ni&#351;anlar&#305;n&#305;n
verilm&#601;sini;</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>18.0.4. d&#601;rman
vasit&#601;l&#601;rinin sat&#305;&#351;&#305;, tibbi xidm&#601;tl&#601;rin
göst&#601;rilm&#601;si il&#601; m&#601;&#351;&#287;ul olan
&#351;&#601;xsl&#601;rin müvafiq tibbi t&#601;hsilinin olmas&#305;na
n&#601;zar&#601;ti;</span><span lang=RU style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>18.0.5. mallar&#305;n
(xidm&#601;tl&#601;rin, i&#351;l&#601;rin) m&#601;n&#351;&#601;yi,
k&#601;miyy&#601;ti v&#601; keyfiyy&#601;ti, yararl&#305;l&#305;q müdd&#601;ti,
sat&#305;&#351; &#351;&#601;rtl&#601;ri, qiym&#601;ti bar&#601;d&#601; yalan
v&#601; (v&#601; ya) natamam m&#601;lumatlar verm&#601;kl&#601;, yaxud&nbsp;<i>haqs&#305;z,
qeyri-d&#601;qiq v&#601; gizli</i>&nbsp;reklam vasit&#601;sil&#601;
sat&#305;c&#305;lar v&#601; (v&#601; ya)&nbsp;<i>reklamverici, reklam
yarad&#305;c&#305;s&#305;, reklam istehsalç&#305;s&#305;, reklam
yay&#305;c&#305;s&#305; v&#601; reklam agenti</i>&nbsp;t&#601;r&#601;find&#601;n
istehlakç&#305;lar&#305;n (istifad&#601;çil&#601;rin)
aldad&#305;lmas&#305;n&#305;n qar&#351;&#305;s&#305;n&#305;n
al&#305;nmas&#305;n&#305;;</span><a name="_ednref19"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn19"><span
                            style='mso-bookmark:_ednref19'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[19]</span></sup></b></span><span
                            style='mso-bookmark:_ednref19'></span></a><span style='mso-bookmark:_ednref19'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.15pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>18.0.6. h&#601;cmi müvafiq qaydada
mü&#601;yy&#601;nl&#601;&#351;diril&#601;r&#601;k, t&#601;sdiq olunmu&#351;
maddi v&#601; m&#601;n&#601;vi z&#601;r&#601;rin sat&#305;c&#305;
t&#601;r&#601;find&#601;n (istehsalç&#305;, idxalç&#305;,&nbsp;<i>reklam
f&#601;aliyy&#601;tinin subyekti</i>) al&#305;c&#305;ya (istehlakç&#305;ya)
öd&#601;nilm&#601;sini;</span><a name="_ednref20"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn20"><span
                            style='mso-bookmark:_ednref20'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[20]</span></sup></b></span><span
                            style='mso-bookmark:_ednref20'></span></a><span style='mso-bookmark:_ednref20'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.15pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>18.0.7. insanlar&#305;n
sa&#287;laml&#305;&#287;&#305;, &#601;traf mühit üçün t&#601;hlük&#601;li olan
mallar&#305;n (xidm&#601;tl&#601;rin, i&#351;l&#601;rin)
istehsal&#305;n&#305;n, idxal&#305;n&#305;n v&#601;
sat&#305;&#351;&#305;n&#305;n qada&#287;an olunmas&#305;n&#305;;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.15pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>18.0.8. istehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n
müdafi&#601;si sah&#601;sind&#601; f&#601;aliyy&#601;t göst&#601;r&#601;n
beyn&#601;lxalq t&#601;&#351;kilatlarla
&#601;m&#601;kda&#351;l&#305;&#287;&#305;, ictimai birlikl&#601;rin
f&#601;aliyy&#601;ti üçün müvafiq &#351;&#601;raitin
yarad&#305;lmas&#305;n&#305;.</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-indent:
27.0pt;line-height:normal;background:white'><span lang=AZ-LATIN
                                                  style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&nbsp;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-indent:
20.0pt;line-height:normal;background:white'><span lang=AZ-LATIN
                                                  style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>Madd&#601;
19.&nbsp;<b>&#304;stehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n v&#601;
qanuni maraqlar&#305;n&#305;n dövl&#601;t müdafi&#601;si</b></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>&nbsp;<a name="_ednref21"></a><a
                            href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn21"><span
                                style='mso-bookmark:_ednref21'><b><sup><span style='mso-bidi-font-size:11.0pt;
color:blue'>[21]</span></sup></b></span><span style='mso-bookmark:_ednref21'></span></a></span><span
                        style='mso-bookmark:_ednref21'></span><span lang=AZ-LATIN style='font-size:
10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>&nbsp;</span><span lang=AZ-LATIN
                                                           style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.1.&nbsp;Mallar&#305;n
(xidm&#601;tl&#601;rin, i&#351;l&#601;rin) istehlakç&#305;lar&#305;n&#305;n
hüquqlar&#305;n&#305;n v&#601; qanuni maraqlar&#305;n&#305;n dövl&#601;t
müdafi&#601;si ticar&#601;t, ictimai ia&#351;&#601;, m&#601;i&#351;&#601;t
v&#601; dig&#601;r xidm&#601;t normalar&#305;na v&#601; qaydalar&#305;na riay&#601;t
olunmas&#305;na müvafiq icra hakimiyy&#601;ti orqan&#305;
t&#601;r&#601;find&#601;n n&#601;zar&#601;t formas&#305;nda h&#601;yata
keçirilir.</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2. &#304;stehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n v&#601; qanuni maraqlar&#305;n&#305;n qorunmas&#305;
funksiyalar&#305;n&#305; yerin&#601; yetir&#601;n müvafiq icra hakimiyy&#601;ti
orqan&#305;:</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.1. istehsal, idxal edil&#601;n
v&#601; (v&#601; ya) sat&#305;lan mallar&#305;n (xidm&#601;tl&#601;rin,
i&#351;l&#601;rin) müvafiq standartlar&#305;n v&#601; texniki
&#351;&#601;rtl&#601;rin t&#601;l&#601;bl&#601;rin&#601; uy&#287;unlu&#287;unu
mü&#601;yy&#601;nl&#601;&#351;dirir, onlara uy&#287;unluq sertifikatlar&#305;
v&#601; (v&#601; ya) uy&#287;unluq ni&#351;anlar&#305; verir;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.2. m&#601;cburi
sertifikatla&#351;d&#305;r&#305;lmas&#305; t&#601;l&#601;b olunan mallar&#305;n
(xidm&#601;tl&#601;rin,<br>
i&#351;l&#601;rin) siyah&#305;s&#305;n&#305;
mü&#601;yy&#601;nl&#601;&#351;dirir v&#601; onlar&#305;n sertifikat&#305;
olmadan sat&#305;&#351;&#305;n&#305;n (icras&#305;n&#305;n) qar&#351;&#305;s&#305;n&#305;
al&#305;r v&#601; ticar&#601;t dövriyy&#601;sind&#601;n ç&#305;xar&#305;r;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.3.&nbsp;<i>texniki normativ hüquqi
aktlar&#305;n</i>&nbsp;t&#601;l&#601;bl&#601;rin&#601; cavab
verm&#601;y&#601;n, insanlar&#305;n h&#601;yat v&#601;
sa&#287;laml&#305;&#287;&#305;, &#601;traf mühit üçün t&#601;hlük&#601; yaradan
mallar&#305;n (xidm&#601;tl&#601;rin, i&#351;l&#601;rin) istehsal&#305;n&#305;n,
idxal&#305;n&#305;n v&#601; sat&#305;&#351;&#305;n&#305;n qar&#351;&#305;s&#305;n&#305;
al&#305;r;</span><a name="_ednref22"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn22"><span
                            style='mso-bookmark:_ednref22'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[22]</span></sup></b></span><span
                            style='mso-bookmark:_ednref22'></span></a><span style='mso-bookmark:_ednref22'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.4. m&#601;n&#351;&#601;yi
m&#601;lum olmayan, yararl&#305;l&#305;q müdd&#601;ti ötmü&#351; mallar&#305;n
ticar&#601;t dövriyy&#601;sind&#601;n ç&#305;xar&#305;lmamas&#305;na,
onlar&#305;n sat&#305;lmas&#305;na yol vermi&#351; &#351;&#601;xsl&#601;rin
müvafiq qanunvericilikd&#601; n&#601;z&#601;rd&#601; tutulmu&#351;
m&#601;suliyy&#601;t&#601; c&#601;lb olunmas&#305; üzr&#601;
t&#601;dbirl&#601;r görür;</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.5. istehsal&#305;, idxal&#305;
v&#601; sat&#305;&#351;&#305; qada&#287;an olunmu&#351; mallar
(xidm&#601;tl&#601;r, i&#351;l&#601;r) bar&#601;d&#601; &#601;haliy&#601;
vaxt&#305;nda m&#601;lumat verm&#601;k m&#601;qs&#601;dil&#601; kütl&#601;vi
informasiya vasit&#601;l&#601;rind&#601; m&#601;lumatlar d&#601;rc edir v&#601;
yay&#305;r;</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.6. sat&#305;lan xammal&#305;n,
materiallar&#305;n, haz&#305;r m&#601;hsullar&#305;n müvafiq keyfiyy&#601;t
v&#601; t&#601;hlük&#601;sizlik t&#601;l&#601;bl&#601;rin&#601;
uy&#287;unlu&#287;unun götürülmü&#351; nümun&#601;l&#601;r &#601;sas&#305;nda ekspertizas&#305;n&#305;
(s&#305;na&#287;&#305;n&#305;) keçirir v&#601; onun
n&#601;tic&#601;l&#601;rind&#601;n as&#305;l&#305; olaraq müvafiq
t&#601;dbirl&#601;r görür.&nbsp;<s>H&#601;min nümun&#601;l&#601;rin
d&#601;y&#601;ri v&#601; ekspertizan&#305;n (s&#305;na&#287;&#305;n)
keçirilm&#601;si x&#601;rcl&#601;ri yoxlan&#305;lan t&#601;s&#601;rrüfat
subyekti t&#601;r&#601;find&#601;n öd&#601;nilir v&#601; onun istehsal-t&#601;s&#601;rrüfat
x&#601;rcl&#601;rin&#601; aid edilir;</s></span><a name="_ednref23"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn23"><span
                            style='mso-bookmark:_ednref23'><b><sup><span lang=AZ-LATIN style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;
mso-bidi-font-family:Arial;
color:blue;mso-ansi-language:AZ-LATIN'>[23]</span></sup></b></span><span
                            style='mso-bookmark:_ednref23'></span></a><span style='mso-bookmark:_ednref23'></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.7. &#601;rzaq
m&#601;hsullar&#305;n&#305;n v&#601; d&#601;rman vasit&#601;l&#601;rinin,
insanlar&#305;n sa&#287;laml&#305;&#287;&#305; v&#601; &#601;traf mühit üçün
t&#601;hlük&#601; yarada bil&#601;n mallar&#305;n qabla&#351;d&#305;r&#305;lark&#601;n
sanitar-gigiyena v&#601; toksikologiya, möhk&#601;mlik bax&#305;m&#305;ndan
t&#601;hlük&#601;siz materiallardan istifad&#601; olunmas&#305;na
n&#601;zar&#601;t edir;</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.8.
patentl&#601;&#351;dirilmi&#351; d&#601;rman vasit&#601;l&#601;rinin,
insanlar&#305;n sa&#287;laml&#305;&#287;&#305; v&#601; &#601;traf mühit üçün
t&#601;hlük&#601; yarada bil&#601;n mallar&#305;n t&#601;hlük&#601;siz
istifad&#601; qaydalar&#305; bar&#601;d&#601; x&#601;b&#601;rdarl&#305;q
olmadan sat&#305;&#351;&#305;n&#305;n qar&#351;&#305;s&#305;n&#305; al&#305;r;</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.9.
saxtala&#351;d&#305;r&#305;lm&#305;&#351; mallar&#305;n ticar&#601;t
dövriyy&#601;sind&#601;n ç&#305;xar&#305;lmas&#305;n&#305;, bu mallar&#305;n
istehsal&#305; v&#601; sat&#305;&#351;&#305; il&#601; m&#601;&#351;&#287;ul
olan &#351;&#601;xsl&#601;rin müvafiq qanunvericilikl&#601;
n&#601;z&#601;rd&#601; tutulmu&#351; m&#601;suliyy&#601;t&#601; c&#601;lb
edilm&#601;sini t&#601;min edir;</span><span lang=AZ-LATIN style='font-size:
10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.10.&nbsp;&nbsp;&nbsp;&nbsp;mallar&#305;n
(xidm&#601;tl&#601;rin, i&#351;l&#601;rin) müvafiq icra hakimiyy&#601;ti
orqan&#305;n&#305;n t&#601;sdiq etdiyi, yaxud istehsalç&#305;
t&#601;r&#601;find&#601;n tövsiy&#601; olunan qiym&#601;tl&#601;rl&#601;
sat&#305;lmas&#305;na n&#601;zar&#601;t edir;</span><span lang=AZ-LATIN
                                                          style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.11. qiym&#601;tli metallar&#305;n,
qiym&#601;tli da&#351;lar&#305;n v&#601; onlardan haz&#305;rlanm&#305;&#351;
z&#601;rg&#601;rlik m&#601;mulatlar&#305;n&#305;n müvafiq qanunvericilikl&#601;
mü&#601;yy&#601;n olunmu&#351; sat&#305;&#351; qaydalar&#305;na riay&#601;t
olunmas&#305;na n&#601;zar&#601;t edir;</span><span lang=AZ-LATIN
                                                    style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.12. sat&#305;c&#305;lar
t&#601;r&#601;find&#601;n ç&#601;kid&#601;, qiym&#601;td&#601;,
hesabla&#351;malarda aldad&#305;lmas&#305;, mallar bar&#601;d&#601; yalan
m&#601;lumatlar verilm&#601;si, sat&#305;&#351; &#351;&#601;rtl&#601;rinin
pozulmas&#305; bar&#601;d&#601; al&#305;c&#305;lar&#305;n
müraci&#601;tl&#601;rin&#601; bax&#305;r v&#601; &#351;ikay&#601;tl&#601;rini
ara&#351;d&#305;r&#305;r, onlar&#305;n n&#601;tic&#601;l&#601;ri üzr&#601;
müvafiq t&#601;dbirl&#601;r görür;</span><span lang=AZ-LATIN style='font-size:
10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal;
background:white'><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>19.2.13. mallar&#305;n
(xidm&#601;tl&#601;rin, i&#351;l&#601;rin) istehsal&#305;, ixrac&#305;,
idxal&#305; v&#601; sat&#305;&#351;&#305; il&#601; m&#601;&#351;&#287;ul olan
&#351;&#601;xsl&#601;rd&#601;n onlar&#305;n keyfiyy&#601;ti, qiym&#601;ti, sat&#305;&#351;
&#351;&#601;rtl&#601;ri bar&#601;d&#601; m&#601;lumatlar al&#305;r;</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:20.0pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>19.2.14. sat&#305;lan mallar&#305;n (xidm&#601;tl&#601;rin,
i&#351;l&#601;rin) keyfiyy&#601;tinin yüks&#601;ldilm&#601;si, nomenklaturunun
v&#601; çe&#351;idinin geni&#351;l&#601;ndirilm&#601;si bar&#601;d&#601;
t&#601;klifl&#601;r haz&#305;rlay&#305;r v&#601; müvafiq icra hakimiyy&#601;ti
orqanlar&#305;na t&#601;qdim edir.</span><span lang=AZ-LATIN style='font-size:
10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;20.&nbsp;<b>&#304;stehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n müdafi&#601;si üzr&#601; yerli icra hakimiyy&#601;ti
orqanlar&#305;n&#305;n v&#601;zif&#601;l&#601;ri</b></span><span lang=AZ-LATIN
                                                                 style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Yerli icra hakimiyy&#601;ti orqanlar&#305; istehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n müdafi&#601;si üzr&#601; müvafiq orqanlar&#305; yarada
bil&#601;rl&#601;r. &#304;cra hakimiyy&#601;ti orqanlar&#305;n&#305;n
istehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n müdafi&#601;si üzr&#601;
müvafiq orqanlar&#305;:</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlakç&#305;lar&#305;n &#351;ikay&#601;tl&#601;rin&#601;
bax&#305;r, onlar&#305;n qanunvericilikl&#601; n&#601;z&#601;rd&#601;
tutulmu&#351; hüquqlar&#305;n&#305;n müdafi&#601;si üzr&#601; müvafiq
m&#601;sl&#601;h&#601;t verir;</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;
m&#601;hdudla&#351;d&#305;ran &#351;&#601;rtl&#601;ri a&#351;karlamaq
m&#601;qs&#601;dil&#601; sat&#305;c&#305;larla (istehsalç&#305;larla,
icraç&#305;larla) istehlakç&#305;lar aras&#305;nda ba&#287;lanan
müqavil&#601;l&#601;ri t&#601;hlil edir;</span><span lang=AZ-LATIN
                                                     style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlakç&#305;lar&#305;n h&#601;yat&#305;na,
sa&#287;laml&#305;&#287;&#305;na, yaxud &#601;mlak&#305;na d&#601;y&#601;n
ziyan haqq&#305;nda m&#601;lumat toplay&#305;r v&#601; onu aidiyy&#601;ti
üzr&#601; s&#601;lahiyy&#601;tli orqanlara gönd&#601;rir;</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlakç&#305;lar&#305;n mallarda (i&#351;l&#601;rd&#601;,
xidm&#601;tl&#601;rd&#601;) nöqsanlar, yaxud t&#601;hlük&#601;li mallar
(i&#351;l&#601;r, xidm&#601;tl&#601;r) a&#351;kar etdikl&#601;ri hallarda
bunlar bar&#601;d&#601; d&#601; mallar&#305;n (i&#351;l&#601;rin,
xidm&#601;tl&#601;rin) t&#601;hlük&#601;sizliyin&#601; n&#601;zar&#601;ti
h&#601;yata keçir&#601;n müvafiq dövl&#601;t icra hakimiyy&#601;ti
orqanlar&#305;na x&#601;b&#601;r verir;</span><span lang=AZ-LATIN
                                                    style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n müdafi&#601;si
m&#601;qs&#601;dil&#601; öz t&#601;&#351;&#601;bbüsü v&#601; ya
istehlakç&#305;n&#305;n (istehlakç&#305;lar ittifaq&#305;n&#305;n)
müraci&#601;ti &#601;sas&#305;nda m&#601;hk&#601;m&#601;
qar&#351;&#305;s&#305;nda iddia qald&#305;ra bil&#601;r.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;
21.</span><b><span lang=AZ-LATIN style='
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>&nbsp;&#304;stehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n pozulmas&#305;na gör&#601; m&#601;suliyy&#601;t&nbsp;</span></b><a
                        name="_ednref24"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn24"><span
                            style='mso-bookmark:_ednref24'><sup><span lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:blue;mso-ansi-language:AZ-LATIN'>[24]</span></sup></span><span
                            style='mso-bookmark:_ednref24'></span></a><span style='mso-bookmark:_ednref24'></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Bu Qanunun pozulmas&#305;na gör&#601; t&#601;qsirli
&#351;&#601;xsl&#601;r Az&#601;rbaycan Respublikas&#305;n&#305;n
qanunvericiliyi il&#601; n&#601;z&#601;rd&#601; tutulmu&#351; qaydada mülki,
inzibati v&#601; cinay&#601;t m&#601;suliyy&#601;ti da&#351;&#305;y&#305;rlar.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;22.&nbsp;<b>&#304;stehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n müdafi&#601;sini h&#601;yata keçir&#601;n dövl&#601;t
icra hakimiyy&#601;ti orqanlar&#305;n&#305;n q&#601;rarlar&#305;na,
onlar&#305;n v&#601;zif&#601;li &#351;&#601;xsl&#601;rinin, h&#601;mçinin bu
&#351;&#601;xsl&#601;rin h&#601;r&#601;k&#601;tl&#601;rin&#601; dair
&#351;ikay&#601;tl&#601;r&#601; bax&#305;lmas&#305;</b></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n
müdafi&#601;sini h&#601;yata keçir&#601;n dövl&#601;t icra hakimiyy&#601;ti
orqanlar&#305;n&#305;n q&#601;rarlar&#305;na, onlar&#305;n v&#601;zif&#601;li
&#351;&#601;xsl&#601;rinin, h&#601;mçinin bu &#351;&#601;xsl&#601;rin
h&#601;r&#601;k&#601;tl&#601;rin&#601; qar&#351;&#305;
&#351;ikay&#601;tl&#601;r&#601; qanunvericilikl&#601; mü&#601;yy&#601;n
olunmu&#351; qaydada bax&#305;l&#305;r.</span><span lang=AZ-LATIN
                                                    style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n
müdafi&#601;sini h&#601;yata keçir&#601;n dövl&#601;t icra hakimiyy&#601;ti
orqanlar&#305;n&#305;n, onlar&#305;n v&#601;zif&#601;li
&#351;&#601;xsl&#601;rinin q&#601;rarlar&#305;n&#305;n yerin&#601;
yetirilm&#601;si, h&#601;mçinin bu &#351;&#601;xsl&#601;rin
h&#601;r&#601;k&#601;tl&#601;rinin icras&#305; &#351;ikay&#601;tin
verilm&#601;si il&#601; dayand&#305;r&#305;lm&#305;r.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;23.&nbsp;<b>&#304;stehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n müdafi&#601;sini h&#601;yata keçir&#601;n dövl&#601;t
icra hakimiyy&#601;ti orqanlar&#305;n&#305;n hüquq-mühafiz&#601; orqanlar&#305;
il&#601; &#601;laq&#601;si</b></span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Hüquq-mühafiz&#601; orqanlar&#305;n&#305;n i&#351;çil&#601;ri
istehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n müdafi&#601;sini h&#601;yata
keçir&#601;n dövl&#601;t icra hakimiyy&#601;ti orqanlar&#305;n&#305;n
v&#601;zif&#601;li &#351;&#601;xsl&#601;rin&#601; öz
v&#601;zif&#601;l&#601;rinin yerin&#601; yetirilm&#601;sind&#601; köm&#601;klik
göst&#601;rir v&#601; bu i&#351;l&#601;rin görülm&#601;sind&#601; onlara
maneçilik tör&#601;d&#601;n v&#601;t&#601;nda&#351;lar&#305;n qanunsuz
h&#601;r&#601;k&#601;tl&#601;rinin qar&#351;&#305;s&#305;n&#305; al&#305;rlar.</span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;24.&nbsp;<b>Az&#601;rbaycan Respublikas&#305;nda
istehlakç&#305;lar&#305;n hüquqlar&#305;n&#305; müdafi&#601; ed&#601;n
dövl&#601;t icra hakimiyy&#601;ti orqanlar&#305;n&#305;n v&#601;zif&#601;li
&#351;&#601;xsl&#601;rinin hüquqi müdafi&#601;si</b></span><span lang=AZ-LATIN
                                                                 style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;lar&#305;n hüquqlar&#305;n&#305; müdafi&#601;
ed&#601;n dövl&#601;t orqanlar&#305;n&#305;n v&#601;zif&#601;li
&#351;&#601;xsl&#601;rinin v&#601; müt&#601;x&#601;ssisl&#601;rinin
hüquqlar&#305; Az&#601;rbaycan Respublikas&#305;n&#305;n müvafiq qanunvericilik
aktlar&#305; &#601;sas&#305;nda t&#601;min edilir.</span><span lang=AZ-LATIN
                                                               style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Onlar&#305;n v&#601;zif&#601;l&#601;rinin yerin&#601;
yetirilm&#601;sin&#601; mane olmaq v&#601; ya h&#601;r hans&#305;
&#351;&#601;kild&#601; t&#601;sir göst&#601;rm&#601;k, habel&#601; i&#351;l&#601;rin&#601;
müdaxil&#601; etm&#601;k qada&#287;and&#305;r.</span><span lang=AZ-LATIN
                                                           style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>Bel&#601; h&#601;r&#601;k&#601;tl&#601;r&#601; yol ver&#601;n
orqanlar, v&#601;zif&#601;li &#351;&#601;xsl&#601;r v&#601;
v&#601;t&#601;nda&#351;lar Az&#601;rbaycan Respublikas&#305;n&#305;n
qanunvericiliyi il&#601; mü&#601;yy&#601;nl&#601;&#351;dirilmi&#351; qaydada
m&#601;suliyy&#601;t da&#351;&#305;y&#305;rlar.</span><span lang=AZ-LATIN
                                                            style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;25.&nbsp;<b>(Ç&#305;xar&#305;l&#305;b)&nbsp;</b></span><a
                        name="_ednref25"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn25"><span
                            style='mso-bookmark:_ednref25'><sup><span lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:blue;mso-ansi-language:AZ-LATIN'>[25]</span></sup></span><span
                            style='mso-bookmark:_ednref25'></span></a><span style='mso-bookmark:_ednref25'></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;26.&nbsp;<b>&#304;stehlakç&#305;lar&#305;n
hüquqlar&#305;n&#305;n m&#601;hk&#601;m&#601; müdafi&#601;si</b></span><span
                        lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;lar&#305;n qanunvericilikl&#601;
n&#601;z&#601;rd&#601; tutulmu&#351; hüquqlar&#305;n&#305;n müdafi&#601;sini
m&#601;hk&#601;m&#601; h&#601;yata keçirir.</span><span lang=AZ-LATIN
                                                        style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;n&#305;n t&#601;l&#601;bl&#601;rini yerin&#601;
yetirm&#601;kl&#601; yana&#351;&#305;, m&#601;hk&#601;m&#601; h&#601;mçinin ona
d&#601;ymi&#351; m&#601;n&#601;vi (qeyri-&#601;mlak) z&#601;r&#601;rin d&#601;
öd&#601;nilm&#601;sini h&#601;ll edir.</span><span lang=AZ-LATIN
                                                   style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;lar hüquqlar&#305;n&#305;n pozulmas&#305;
bar&#601;d&#601; qald&#305;rd&#305;qlar&#305; iddialara gör&#601; dövl&#601;t
rüsumu verm&#601;kd&#601;n azad edilirl&#601;r.</span><span lang=AZ-LATIN
                                                            style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black;
mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;text-indent:17.85pt;line-height:normal'><span lang=AZ-LATIN
                                                                style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>IV f &#601;
s i l</span><span lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;STEHLAKÇILARIN &#304;CT&#304;MA&#304; T&#399;&#350;K&#304;LATLARI
(&#304;STEHLAKÇILAR B&#304;RL&#304;Y&#304;)</span></b><span lang=AZ-LATIN
                                                            style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
6.0pt;margin-left:0in;text-align:justify;text-justify:inter-ideograph;
text-indent:.25in;line-height:normal'><span lang=AZ-LATIN style='mso-bidi-font-family:
Arial;color:black;letter-spacing:3.0pt;mso-ansi-language:AZ-LATIN'>Madd&#601;</span><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;27.&nbsp;<b>&#304;stehlakç&#305;lar&#305;n ictimai
t&#601;&#351;kilatlar&#305; (istehlakç&#305;lar birliyi) v&#601; onlar&#305;n
hüquqlar&#305;</b></span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;letter-spacing:3.0pt;
mso-ansi-language:AZ-LATIN'>1.&nbsp;</span><span lang=AZ-LATIN
                                                 style='
mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>Az&#601;rbaycan
Respublikas&#305;n&#305;n v&#601;t&#601;nda&#351;lar&#305; öz qanuni
hüquqlar&#305;n&#305;n müdafi&#601;si üçün könüllülük &#601;sas&#305;nda
birl&#601;&#351;ib istehlakç&#305;lar&#305;n ictimai
t&#601;&#351;kilatlar&#305;n&#305; (istehlakç&#305;lar birliyini) yaratmaq
hüququna malikdirl&#601;r.</span><span lang=AZ-LATIN style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&#304;stehlakç&#305;lar birliyi ictimai t&#601;&#351;kilat olub, öz
f&#601;aliyy&#601;tini&nbsp;müvafiq qanunvericilik&nbsp;&#601;sas&#305;nda
h&#601;yata keçirir.&nbsp;</span><a name="_ednref26"></a><a
                        href="http://www.e-qanun.az/alpidata/framework/data/9/c_f_9479.htm#_edn26"><span
                            style='mso-bookmark:_ednref26'><sup><span lang=AZ-LATIN style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:blue;mso-ansi-language:AZ-LATIN'>[26]</span></sup></span><span
                            style='mso-bookmark:_ednref26'></span></a><span style='mso-bookmark:_ednref26'></span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>2.&nbsp;&nbsp;&nbsp;&#304;stehlakç&#305;lar birliyi:</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mallar&#305;n istehlak xass&#601;l&#601;rini, ona olan
t&#601;l&#601;bat&#305;, istehsal olunan v&#601; sat&#305;lan mal&#305;n
keyfiyy&#601;tin&#601; v&#601; qiym&#601;tin&#601; dair ictimai fikri
öyr&#601;nm&#601;k;</span><span lang=RU style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin)
ekspertizas&#305;n&#305; v&#601; s&#305;na&#287;&#305;n&#305; aparmaq üçün
müvafiq dövl&#601;t n&#601;zar&#601;t orqanlar&#305;na müraci&#601;t
etm&#601;k;</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>müvafiq icra hakimiyy&#601;ti orqanlar&#305;ndan v&#601;
t&#601;s&#601;rrüfat subyektl&#601;rind&#601;n öz m&#601;qs&#601;d v&#601;
niyy&#601;tl&#601;rini h&#601;yata keçirm&#601;k üçün laz&#305;mi m&#601;lumat
almaq;</span><span lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mallar&#305;n (i&#351;l&#601;rin, xidm&#601;tl&#601;rin)
keyfiyy&#601;tin&#601;, ticar&#601;t v&#601; dig&#601;r növ xidm&#601;t
sah&#601;l&#601;rin&#601; n&#601;zar&#601;t etm&#601;kd&#601; müvafiq
dövl&#601;t orqanlar&#305;na köm&#601;k göst&#601;rm&#601;k;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlakç&#305;lara qanunvericiliy&#601; uy&#287;un hüquqi
m&#601;sl&#601;h&#601;tl&#601;r verilm&#601;sini t&#601;&#351;kil etm&#601;k;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mal&#305;n (i&#351;in, xidm&#601;tin) keyfiyy&#601;tin&#601; aid
t&#601;l&#601;bl&#601;ri mü&#601;yy&#601;nl&#601;&#351;dir&#601;n normativ s&#601;n&#601;dl&#601;rin
haz&#305;rlanmas&#305;na dair t&#601;klif v&#601; r&#601;y verm&#601;k;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>qanunvericiliy&#601; uy&#287;un olaraq, müvafiq icra hakimiyy&#601;ti
orqanlar&#305; qar&#351;&#305;s&#305;nda istehlakç&#305;lar&#305;n
maraqlar&#305; haqq&#305;nda m&#601;s&#601;l&#601; qald&#305;rmaq v&#601; onu
müdafi&#601; etm&#601;k;</span><span lang=RU style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>mal&#305;n (i&#351;in, xidm&#601;tin) keyfiyy&#601;tinin
yax&#351;&#305;la&#351;d&#305;r&#305;lmas&#305; v&#601; qiym&#601;tqoyma
qaydalar&#305;na. &#601;m&#601;l edilm&#601;si,
mü&#601;yy&#601;nl&#601;&#351;dirilmi&#351; keyfiyy&#601;t
t&#601;l&#601;bl&#601;rin&#601; uy&#287;un g&#601;lm&#601;y&#601;n mal&#305;n
(i&#351;in, xidm&#601;tin) sat&#305;&#351;&#305;n&#305;n müv&#601;qq&#601;ti
dayand&#305;r&#305;lmas&#305;, v&#601;t&#601;nda&#351;lar&#305;n
h&#601;yat&#305;, sa&#287;laml&#305;&#287;&#305; v&#601; &#601;mlak&#305;,
&#601;traf mühit üçün t&#601;hlük&#601;li olan mal&#305;n (i&#351;in, xidm&#601;tin)
istehsal&#305;n&#305;n dayand&#305;r&#305;lmas&#305;, sat&#305;&#351;dan
götürülm&#601;si, &#601;sass&#305;z art&#305;r&#305;lm&#305;&#351;
qiym&#601;tl&#601;rl&#601; m&#601;hsul sat&#305;&#351;&#305;n&#305;n
dayand&#305;r&#305;lmas&#305;, habel&#601; mövcud qanunvericiliyi pozmaqla
mü&#601;yy&#601;nl&#601;&#351;dirilmi&#351; qiym&#601;tl&#601;rin
l&#601;&#287;v edilm&#601;si t&#601;dbirl&#601;ri bar&#601;sind&#601; müvafiq
icra hakimiyy&#601;ti orqanlar&#305;na, mü&#601;ssis&#601;l&#601;r&#601;
v&#601; t&#601;&#351;kilatlara t&#601;klifl&#601;r verm&#601;k;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlakç&#305;lar&#305;n ictimai t&#601;&#351;kilatlar&#305;n&#305;n
(istehlakç&#305;lar birliyinin) üzvü olmayan v&#601;t&#601;nda&#351;lar&#305;n,
qanunvericiliy&#601; uy&#287;un olaraq, hüquqlar&#305;n&#305;
m&#601;hk&#601;m&#601;d&#601; müdafi&#601; etm&#601;k;</span><span lang=RU
                                                                   style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>keyfiyy&#601;tsiz mal&#305;n (i&#351;in, xidm&#601;tin) istehsal&#305;nda
v&#601; sat&#305;&#351;&#305;nda t&#601;qsiri olan &#351;&#601;xsl&#601;rin
m&#601;suliyy&#601;t&#601; c&#601;lb edilm&#601;si bar&#601;d&#601; müvafiq
icra hakimiyy&#601;ti orqanlar&#305;na müraci&#601;t etm&#601;k;</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlakç&#305;lar&#305;n hüquqlar&#305; haqq&#305;nda
ictimaiyy&#601;t&#601; m&#601;lumat verm&#601;k;</span><span lang=RU
                                                             style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>istehlakç&#305;lar&#305;n hüquqlar&#305;n&#305;n müdafi&#601;si
haqq&#305;nda beyn&#601;lxalq &#601;m&#601;kda&#351;l&#305;&#287;&#305;n
inki&#351;af&#305;na köm&#601;k göst&#601;rm&#601;k hüququna malikdir.</span><span
                        lang=RU style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black'><o:p></o:p></span></p>

                <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:right;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;</span></b><span lang=RU style='font-size:10.0pt;font-family:
"Arial",sans-serif;color:black'><o:p></o:p></span></p>

                <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:right;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='font-size:9.0pt;
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>Az&#601;rbaycan
Respublikas&#305;n&#305;n Prezidenti</span></b><span style='font-size:10.0pt;
font-family:"Arial",sans-serif;
color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:right;text-indent:17.85pt;line-height:normal'><b><span
                            lang=AZ-LATIN style='font-size:9.0pt;
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>Heyd&#601;r &#399;L&#304;YEV.</span></b><span
                        style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:right;text-indent:17.85pt;line-height:normal'><span lang=AZ-LATIN
                                                               style='font-size:9.0pt;mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial;color:black;mso-ansi-language:
AZ-LATIN'>&nbsp;</span><span style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:17.85pt;line-height:normal'><span
                        lang=AZ-LATIN style='font-size:9.0pt;
mso-bidi-font-family:Arial;
color:black;mso-ansi-language:AZ-LATIN'>Bak&#305; &#351;&#601;h&#601;ri, 19
sentyabr 1995-ci il</span><span style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:.5in;line-height:normal'><span lang=AZ-LATIN style='font-size:9.0pt;

mso-bidi-font-family:Arial;color:black;mso-ansi-language:AZ-LATIN'>&#8470; 1113</span><span
                        style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span lang=AZ-LATIN style='font-size:10.0pt;font-family:"Arial",sans-serif;
color:black;mso-ansi-language:AZ-LATIN'>&nbsp;</span><span
                        style='font-size:10.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:EN-US'><o:p></o:p></span></p>

                <p class=MsoNormal><span style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

            </div>

        @else
            <div class=WordSection1><p class=MsoNormal
                                       style='margin-top:0in;margin-right:0in;margin-bottom:13.35pt; margin-left:0in;text-indent:0in;line-height:107%'>
                    <b style='mso-bidi-font-weight: normal'><span
                            style='font-size:16.0pt;mso-bidi-font-size:11.0pt;line-height: 107%;font-family:"Calibri",sans-serif;mso-fareast-font-family:Calibri'><span
                                style='mso-spacerun:yes'>         </span></span></b><b
                        style='mso-bidi-font-weight: normal'><span
                            style='font-size:11.0pt;line-height:107%;font-family:"Calibri",sans-serif; mso-fareast-font-family:Calibri'><span
                                style='mso-spacerun:yes'> </span></span></b></p>
                <p class=MsoNormal
                   style='text-align: center; margin-top:0in;margin-right:0in;margin-bottom:14.35pt; margin-left:-.25pt;line-height:107%'><b
                        style='mso-bidi-font-weight:normal'><span
                            style='font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:107%'><span
                                style='mso-spacerun:yes'>                       </span>Закон Азербайджанской Республики<span
                                style='mso-spacerun:yes'>  </span></span></b></p>
                <p class=MsoNormal
                   style='text-align: center; margin-top:0in;margin-right:0in;margin-bottom:12.45pt; margin-left:-.25pt;line-height:107%'><b
                        style='mso-bidi-font-weight:normal'><span
                            style='font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:107%'><span
                                style='mso-spacerun:yes'>                             </span>О защите прав потребителей </span></b>
                </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:0in;margin-bottom:13.3pt; margin-left:0in;text-indent:0in;line-height:107%'>
                    <b style='mso-bidi-font-weight: normal'><span
                            style='font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height: 107%'><span
                                style='mso-spacerun:yes'> </span></span></b></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:0in;margin-bottom:14.9pt; margin-left:0in;text-indent:0in;line-height:107%'>
                    <b style='mso-bidi-font-weight: normal'>Закон Азербайджанской Республики </b></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>О
                    защите прав потребителей<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Настоящий Закон определяет общие правовые, экономические и социальные основы и механизм единого
                    регулирования отношений между потребителем и изготовителем, продавцом и исполнителем в процессе
                    купли-продажи, выполнения работ и оказания услуг, а также защиты прав потребителей в целях создания
                    равных условий для них на территории Азербайджанской Республики.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Закон разработан на основе "Руководящих принципов для защиты интересов потребителей", принятых
                    Генеральной Ассамблеей ООН и направлен на приведение аналогичных отношений в Азербайджанской
                    Республике в соответствие с мировой практикой.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Глава I. Общие положения<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 1. Основные понятия<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Основные понятия, применяемые в настоящем Законе: </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>потребитель - лицо,
                    использующее, приобретающее, заказывающее, либо имеющее намерение приобрести или заказать товары,
                    работы и услуги для удовлетворения личных нужд;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>изготовитель -
                    независимо от формы собственности и организационно-правовой формы, предприятие, учреждение,
                    организация или предприниматель, производящие товары для реализации; </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>исполнитель -
                    предприятие, учреждение, организация или предприниматель, </p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    выполняющие работы или оказывающие услуги; </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>продавец -
                    предприятие, учреждение, организация или предприниматель, производящие операции по купле-продаже,
                    реализующие товары; </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>нормативный
                    документ - государственные стандарты, фармакологические, санитарные и строительные нормы, правила и
                    другие документы, которые в соответствии с законодательством Азербайджанской Республики
                    устанавливают обязательные требования к качеству и безопасности товаров (работ, услуг); </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>недостаток -
                    несоответствие товара (работы, услуги) требованиям нормативных документов, условиям договоров либо
                    другим предъявленным требованиям, а также информации о товаре (работе, услуге), предоставленной
                    исполнителем или продавцом;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>существенный
                    недостаток - недостаток, который делает невозможным или недопустимым использование товара (работы,
                    услуги) в соответствии с его целевым назначением, либо не может быть устранен к отношении данного
                    потребителя, либо для его устранения требуются большие затраты труда и времени, либо делает товар
                    (работу, услугу) иным, чем предусмотрено договором, либо проявляется вновь после его
                    устранения;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>сертификат
                    соответствия (сертификат) - документ, выданный для подтверждения соответствия продукта,
                    сертифицированного по правилам сертификационной системы установленным требованиям;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>знак соответствия -
                    знак, показывающий соответствие продукта, процесса или услуги установленным требованиям,
                    присваиваемым и применяемым в соответствии с правилами сертификационной системы, регистрируемый в
                    установленном порядке;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>безопасность товара
                    (работ, услуг) - обычные условия использования, хранения, транспортировки, эксплуатации товара
                    (работ, услуг) или процесса выполнения работ (оказания услуг), исключающие причинение вреда жизни,
                    здоровью, имуществу потребителя, а также окружающей среде;<span style='mso-spacerun:yes'>  </span>
                </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>гарантийный срок -
                    срок, в течение которого гарантийные обязательства, предусмотренные в соответствующих нормативных
                    документах имеют силу.<span style='mso-spacerun:yes'>  </span>Статья 2. Законодательство
                    Азербайджанской Республики о защите прав потребителей<span style='mso-spacerun:yes'>  </span>Законодательство
                    Азербайджанской Республики о защите прав потребителей состоит из настоящего Закона и издаваемых в
                    соответствии с ним иных нормативных правовых актов.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:39.55pt;margin-bottom: .25pt;margin-left:-.25pt'>
                    Если международным договором Азербайджанской Республики установлены иные правила, чем те, которые
                    содержатся в настоящем Законе, то применяются правила международного договора.<span
                        style='mso-spacerun:yes'>  </span>Глава II. Права потребителей<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 3. Права потребителей<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Потребители, находящиеся на территории Азербайджанской Республики имеют права на: </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>свободный выбор
                    товаров (работ, услуг) и их изготовителей, исполнителей и продавцов;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>надлежащее качество
                    потребляемых ими товаров (работ, услуг); </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>безопасность
                    товаров (работ, услуг);<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>получение полной и
                    достоверной информации о количестве, ассортименте и качестве товаров (работ, услуг);<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>возмещение убытков,
                    причиненных товарами (работами, услугами) ненадлежащего качества, а также вреда, причиненного
                    опасными для жизни и здоровья людей товарами </p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    (работами, услугами), в случаях предусмотренных законодательством;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>обращение в суд и
                    другие уполномоченные государственные органы за защитой своих прав и законных интересов;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>объединение в
                    общественные организации (объединения потребителей).<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 4. Гарантированный уровень потребления </p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Гарантированный уровень потребления обеспечивается:<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>введением
                    нормированного распределения товаров, в случае отсутствия гарантий их свободного приобретения каждым
                    потребителем;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l15 level1 lfo1'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>введением
                    компенсационных выплат, различных видов пособий и льгот гражданам.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 5. Право потребителя на надлежащее качество товаров (работ, услуг)<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:19.4pt;margin-bottom: .25pt;margin-left:-.25pt'>1.
                    Потребитель имеет право требовать от продавца (изготовителя, исполнителя), чтобы качество
                    приобретенного им товара (выполненной работы, оказанной услуги) отвечало требованиям нормативных
                    документов, условиям договоров, а также информации о товаре (работе, услуге) представляемой
                    продавцом (изготовителем, исполнителем).<span style='mso-spacerun:yes'>  </span>2. Требования к
                    товару (работе, услуге) относительно его безопасности для жизни, здоровья и имущества потребителей,
                    а также окружающей среды устанавливаются нормативными документами.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>В
                    отношении отдельных групп товаров (работ, услуг) указанные требования устанавливаются
                    законодательными актами Азербайджанской Республики.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:2.45pt;margin-bottom: .1pt;margin-left:.5pt;line-height:106%;mso-list:l1 level1 lfo2'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:106%'><span
                            style='mso-list:Ignore'>3.<span style=''>               </span></span></span><![endif]>Продавец
                    (изготовитель, исполнитель) обязан передать Потребителю товар (работу, услугу), соответствующий по
                    качеству требованиям нормативных документов, условиям договоров, а также предоставленной
                    изготовителем (исполнителем) информации о товаре (работе, услуге).<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>У
                    товаров (работ, услуг), ввозимых на территорию Азербайджанской Республики должен быть
                    предусмотренный законодательством документ, подтверждающий их надлежащее качество.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:2.45pt;mso-list:l1 level1 lfo2'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>4.<span
                                style=''>               </span></span></span><![endif]>Изготовитель (исполнитель) обязан
                    обеспечить возможность использования товара (результатов выполненной работы, услуги) по назначению в
                    течение срока его службы, предусмотренного нормативным документом или установленного им по
                    договоренности с потребителем, а в случае отсутствия таких сроков - в течение 10 лет.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Изготовитель обязан обеспечить техническое обслуживание и гарантийный ремонт товара, а также
                    снабжение предприятий, осуществляющих техническое обслуживание и ремонт, в необходимых объемах и
                    ассортименте запасными частями в течение всего срока его производства, а после снятия с производства
                    - в течение срока службы, а в случае отсутствия такого срока - в течение 10 лет.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 6. Гарантийные обязательства<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l16 level1 lfo3'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>1.<span
                                style=''>               </span></span></span><![endif]>Изготовитель (исполнитель)
                    обеспечивает нормальную работу (применение, использование) товара (работы, услуги), а также
                    комплектующих изделий в течение гарантийного срока, установленного законодательством, а в случае его
                    отсутствия в порядке, определенном договором.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Гарантийные сроки на комплектующие изделия должны быть не меньше гарантийного срока на основное
                    изделие, если иное не предусмотрено законодательством или договором.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l16 level1 lfo3'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>2.<span
                                style=''>               </span></span></span><![endif]>Гарантийный срок указывается в
                    паспорте на товар (работу, услугу) либо на его этикетке (ярлыке) или в любом другом документе,
                    прилагаемом к товару (работе, услуге).<span style='mso-spacerun:yes'>  </span>Для пищевых продуктов,
                    медикаментов, парфюмерно-косметических средств, химической продукции и других товаров (работ,
                    услуг), потребительские свойства которых могут быстро ухудшаться и представлять опасность для жизни,
                    здоровья, имущества и окружающей среды, должен быть установлен срок годности, который указывается на
                    упаковке или в соответствующих документах, прилагаемых к ним. Продажа товаров, срок годности которых
                    истек, запрещается. Гарантийный срок исчисляется от даты продажи, а срок годности от даты
                    изготовления товара.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l16 level1 lfo3'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>3.<span
                                style=''>               </span></span></span><![endif]>О недостатках, обнаруженных в
                    товарах, на которые гарантийные сроки не установлены, потребитель имеет право предъявить продавцу
                    (изготовителю, исполнителю) свои требования не позднее 3-х лет.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l16 level1 lfo3'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>4.<span
                                style=''>               </span></span></span><![endif]>При выполнении гарантийного
                    ремонта гарантийный срок увеличивается на время нахождения товара (работы, услуги) в ремонте.
                    Указанное время исчисляется со дня обращения потребителя с требованием об устранении недостатков.
                    При обмене товара его гарантийный срок исчисляется заново со дня обмена.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:12.65pt;margin-bottom: .25pt;margin-left:-.25pt'>
                    Статья 7. Права потребителя в случае продажи ему товара ненадлежащего качества<span
                        style='mso-spacerun:yes'>  </span>1. Потребитель при обнаружении недостатков либо фальсификации
                    товара в течение гарантийного срока, установленного договором или другими правилами, имеет право по
                    своему выбору требовать от продавца или изготовителя:<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l9 level1 lfo4'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>замены на товар
                    надлежащего качества; </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l9 level1 lfo4'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>соразмерного
                    уменьшения его покупной цены; </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l9 level1 lfo4'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>устранения
                    недостатков товара за счет изготовителя (продавца, потребителя) или возмещения расходов на их
                    исправление потребителем либо третьим лицом;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l9 level1 lfo4'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>замены на такой же
                    товар другой модели (марки, типа и т. д.) с условием перерасчета стоимости; </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l9 level1 lfo4'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>расторжения
                    договора и возмещения нанесенных им убытков.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l6 level1 lfo5'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>2.<span
                                style=''>               </span></span></span><![endif]>Продавец (изготовитель) обязан
                    принять товар ненадлежащего качества у потребителя и удовлетворить одно из требований потребителя,
                    указанных в пункте 1 настоящей статьи.<span style='mso-spacerun:yes'>  </span>Доставка
                    крупногабаритных и тяжелых товаров продавцу (исполнителю), их замена и возврат потребителю
                    осуществляются за счет продавца (исполнителя).<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l6 level1 lfo5'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>3.<span
                                style=''>               </span></span></span><![endif]>При наличии товара требование
                    потребителя о его замене подлежит немедленному удовлетворению, а в случае необходимости, его
                    качество должно быть проверено в течение 14 дней с подачи соответствующего требования или заменено в
                    сроки, согласованные сторонами.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>В
                    случае отсутствия товара требование потребителя о замене подлежит удовлетворению в двухмесячный срок
                    со времени соответствующего заявления.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Если удовлетворить требования потребителя о замене товара в установленный срок невозможно,
                    потребитель вправе предъявить продавцу иные требования, предусмотренные во втором, третьем,
                    четвертом и пятом абзацах пункта 1 настоящей статьи.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l6 level1 lfo5'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>4.<span
                                style=''>               </span></span></span><![endif]>При замене товара с недостатками
                    на товар той же модели (марки, типа и т. д.) надлежащего качества, цена на который изменилась,
                    перерасчет стоимости не производится.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>При
                    замене товара с недостатками на такой же товар другой модели (марки, типа и т. д.), цена на который
                    изменилась, перерасчет стоимости производится, исходя из цен, действовавших на момент приобретения
                    товара.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>При
                    расторжении договора расчеты с потребителем в случае повышения цены на товар производятся, учитывая
                    повышение цен на однородные товары, а в случае снижения цены - исходя из стоимости товара на момент
                    покупки.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l6 level1 lfo5'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>5.<span
                                style=''>               </span></span></span><![endif]>В случае приобретения
                    потребителем продовольственных товаров ненадлежащего качества продавец обязан заменить их на
                    качественные товары или возвратить потребителю сумму стоимости товара, если указанные недостатки
                    обнаружены в пределах срока годности.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>При
                    этом расчеты с потребителем производятся в порядке, указанном в пункте 4 настоящей статьи.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l6 level1 lfo5'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>6.<span
                                style=''>               </span></span></span><![endif]>При предъявлении потребителем
                    требования о безвозмездном устранении недостатков товара они должны быть устранены в течение 14 дней
                    или по соглашению сторон в другой срок.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>По
                    требованию потребителя на время ремонта или замены бытовой техники и транспортных средств продавец
                    (исполнитель) должен безвозмездно предоставить ему аналогичный товар (с доставкой).<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>При
                    задержке выполнения требования о ремонте (замене), а также о предоставлении аналогичного товара на
                    время устранения недостатков или замены товара (14 дней) продавец (исполнитель) вместе с передачей
                    товара потребителю выплачивает ему неустойку в размере одного процента стоимости товара за каждый
                    день задержки сверх установленного срока.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l6 level1 lfo5'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>7.<span
                                style=''>               </span></span></span><![endif]>Требования потребителя
                    рассматриваются по предъявлении потребителем квитанции, товарного либо кассового чека, а по товарам,
                    на которые установлены гарантийные сроки, - технического паспорта либо иного заменяющего его
                    документа.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>При
                    продаже товара продавец обязан выдать потребителю квитанцию, товарный либо кассовый чек, или иной
                    письменный документ.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>В
                    случае утраты потребителем технического паспорта или иного заменяющего его документа их
                    восстановление осуществляется в порядке, предусмотренном законодательством.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l6 level1 lfo5'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>8.<span
                                style=''>               </span></span></span><![endif]>Изготовитель обязан возместить
                    расходы, затраченные продавцом для удовлетворения претензии потребителя к товару.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l6 level1 lfo5'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>9.<span
                                style=''>               </span></span></span><![endif]>Требования, установленные пунктом
                    1 настоящей статьи в отношении товаров, изготовленных за пределами Азербайджанской Республики или
                    приобретенных у посредников, удовлетворяются за счет продавца.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l6 level1 lfo5'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>10.<span
                                style=''>            </span></span></span><![endif]>Требования потребителя,
                    предусмотренные настоящей статьей, не подлежат удовлетворению, если продавец, изготовитель
                    (предприятие, выполняющее их функции) докажут, что недостатки товара возникли вследствие нарушения
                    потребителем правил пользования товаром или его хранения, действий третьих лиц или стихийного
                    бедствия.<span style='mso-spacerun:yes'>  </span>Потребитель имеет право участвовать в проверке
                    качества товара лично или через своего представителя.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 8. Обязанности исполнителя при заключении договора на выполнение работы и оказание услуг<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Изготовитель или исполнитель, занимающий доминирующее положение на рынке обязан заключить с
                    потребителем договор на выполнение работ и оказание услуг, за исключением случаев, если он докажет,
                    что их выполнение (оказание) выходит за рамки его уставной деятельности или производственных
                    возможностей. При этом он должен организовывать свою хозяйственную деятельность так, чтобы
                    потребности населения удовлетворялись надлежащим образом беспрерывно. Исполнитель обязан возместить
                    потребителю убытки, причиненные в результате необоснованного отказа в заключении договора на
                    выполнение работ или оказание услуг.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 9. Права потребителя в случае нарушения исполнителем условий договора о выполнении работ и
                    оказания услуг<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l3 level1 lfo6'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>1.<span
                                style=''>               </span></span></span><![endif]>Потребитель имеет право
                    отказаться от договора о выполнении работ и оказании услуг и требовать возмещения убытков, если
                    исполнитель своевременно не приступает к выполнению договора или выполняет работу настолько
                    медленно, что окончить ее в определенный срок становится невозможным.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l3 level1 lfo6'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>2.<span
                                style=''>               </span></span></span><![endif]>Если при наличии существенных
                    отступлений от условий договора и других существенных недостатков потребитель назначит исполнителю
                    дополнительный срок для устранения этих недостатков, и в эти сроки работа (услуга) также не будет
                    выполнена, потребитель имеет право требовать расторжения договора и возмещение убытков или поручить
                    исправление недостатков третьему лицу за счет исполнителя.<span style='mso-spacerun:yes'>  </span>
                </p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l3 level1 lfo6'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>3.<span
                                style=''>               </span></span></span><![endif]>Если исполнитель отступил от
                    условий договора, что привело к ухудшению выполненной работы (услуги), или допустил иные недостатки
                    в выполненной работе (услуге), потребитель имеет право по своему выбору требовать безвозмездного
                    устранения этих недостатков в соответствующий срок либо возмещения расходов, понесенных им при
                    устранении своими средствами недостатков работы, или соразмерного уменьшения вознаграждения за
                    работу (услугу).<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l3 level1 lfo6'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>4.<span
                                style=''>               </span></span></span><![endif]>Если существенные отступления от
                    условий договора или иные существенные недостатки были обнаружены в работе (услуге), выполненной из
                    материала потребителя, потребитель имеет право требовать по своему выбору или выполнения ее из
                    однородного и качественного материала исполнителя, или расторжения договора и возмещения
                    убытков.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l3 level1 lfo6'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>5.<span
                                style=''>               </span></span></span><![endif]>В случае неустранения в
                    установленный срок недостатков, указанных в пункте 2 настоящей статьи, а также просрочки выполнения
                    работ (услуг), исполнитель уплачивает потребителю при принятии работы (услуги) неустойку в размере
                    одного процента стоимости работы или услуги (стоимости заказа, если их стоимость отдельно не
                    определена) за каждый день просрочки, если договором не предусмотрена иная сумма неустойки.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Уплата исполнителем неустойки (штрафа, пени), установленной в случае неисполнения или ненадлежащего
                    исполнения обязательств, и возмещение убытков не освобождает его от исполнения обязательств.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l3 level1 lfo6'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>6.<span
                                style=''>               </span></span></span><![endif]>Исполнитель не несет
                    ответственности за недостатки в выполненных работах или оказанных услугах, если докажет, что они
                    возникли по вине самого потребителя.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l3 level1 lfo6'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>7.<span
                                style=''>               </span></span></span><![endif]>Требования потребителя,
                    предусмотренные пунктом 2 и 5 настоящей статьи, могут быть предъявлены в случае обнаружения
                    недостатков в ходе выполнения работы (услуги) и при ее принятии потребителем, а также в случае
                    обнаружения недостатков в течение гарантийного срока, а при его отсутствии - в течение одного
                    года.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Гарантийный срок исчисляется со дня принятия работы (услуги) потребителем, а в случае
                    несвоевременного ее принятия по вине потребителя - со дня установленного договором срока исполнения
                    работы (услуги).<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l3 level1 lfo6'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>8.<span
                                style=''>               </span></span></span><![endif]>В случае утраты, порчи,
                    повреждения принятой у потребителя вещи (материала) исполнитель обязан возвратить потребителю вещь
                    аналогичную по качеству и цене (выполнить работу или услугу), а при невозможности - возместить
                    потребителю по его согласию стоимость. вещи (материала) и причиненные убытки в срок не позднее
                    одного месяца.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.1pt; margin-left:.5pt;line-height:106%;mso-list:l3 level1 lfo6'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:106%'><span
                            style='mso-list:Ignore'>9.<span style=''>               </span></span></span><![endif]>Исполнитель
                    обязан предупредить потребителя об особых свойствах вещи (материала), которые могут привести к ее
                    порче, повреждению. Исполнитель не освобождается от ответственности и в том случае, если уровень
                    научных и технических знаний не позволяет выявить особые свойства вещи (материала).<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Стоимость вещи (материала), принимаемой исполнителем для выполнения работ и оказания услуг,
                    определяется потребителем при заключении договора.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l3 level1 lfo6'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>10.<span
                                style=''>            </span></span></span><![endif]>Исполнитель несет ответственность за
                    вред, причиненный жизни, здоровью или имуществу потребителя, который возник в связи с использованием
                    в процессе выполнения работ и оказания услуг вещей, материалов, оборудования, приборов,
                    инструментов, приспособлений либо других средств, не обеспечивающих безопасность жизни, здоровья или
                    имущества потребителя, независимо от знания исполнителем их свойств.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 10. Права потребителей на безопасность товаров (работ, услуг)<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l12 level1 lfo7'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>1.<span
                                style=''>               </span></span></span><![endif]>Потребитель имеет право на то,
                    чтобы товары (работы, услуги) при обычных условиях их использования, хранения и транспортировки были
                    безопасными для его жизни, здоровья, окружающей природной среды, а также не причиняли вреда его
                    имуществу.<span style='mso-spacerun:yes'>  </span>В случае отсутствия нормативных документов,
                    содержащих обязательные требования к товару (работе, услуге), использование которого может причинить
                    вред жизни, здоровью потребителя, окружающей среде, а также имуществу потребителя, соответствующие
                    органы исполнительной власти обязаны безотлагательно обеспечить разработку и принятие таких
                    нормативных документов, немедленно запретить выпуск и реализацию таких товаров, выполнение работ,
                    оказание услуг исполнителем.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l12 level1 lfo7'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>2.<span
                                style=''>               </span></span></span><![endif]>На товары (работы, услуги),
                    использование которых сверх определенного срока представляет опасность для жизни, здоровья
                    потребителя, окружающей среды пли может причинить вред имуществу потребителя, должен быть установлен
                    срок службы (срок годности).<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Изготовитель (исполнитель) обязан указать срок службы (срок годности) в инструкциях на эти
                    товары.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l12 level1 lfo7'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>3.<span
                                style=''>               </span></span></span><![endif]>Если для безопасного
                    использования товаров (работ, услуг) или их транспортировки, хранения необходимо соблюдать
                    специальные правила, изготовитель (исполнитель) обязан определить такие правила, а продавец -
                    довести их до потребителя.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l12 level1 lfo7'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>4.<span
                                style=''>               </span></span></span><![endif]>Товары (работы, услуги), на
                    которые актами законодательства или иными нормативными документами установлены требования по
                    обеспечению безопасности жизни, здоровья потребителей, их имущества, окружающей среды, подлежат
                    обязательной сертификации согласно действующему законодательству. Реализация и использование таких
                    товаров (в том числе импортных) в Азербайджанской Республике без сертификата соответствия
                    запрещаются.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Основанием для разрешения на ввоз таких товаров на территорию Азербайджанской Республики является
                    представленный в таможенные органы сертификат соответствия, выданный или признанный уполномоченными
                    на то органами.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Ответственность за нарушение требований по безопасности товаров (работ, услуг), предусмотренных
                    данным пунктом, определяется настоящим Законом и иными законодательными актами.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l12 level1 lfo7'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>5.<span
                                style=''>               </span></span></span><![endif]>Если установлено, что при
                    соблюдении потребителем правил непользования, хранения или транспортировки товаров (результатов
                    работ) они причиняют либо могут причинить вред жизни, здоровью, имуществу потребителя или окружающей
                    среде, изготовитель (исполнитель, продавец) обязан незамедлительно прекратить их производство
                    (реализацию) до устранения причин вреда, а в необходимых случаях - принять меры к изъятию их из
                    оборота и отзыву от потребителей.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Если причины, наносящие вред, устранить невозможно, изготовитель (исполнитель) обязан снять такие
                    товары (работы, услуги) с производства, изъять из оборота, отозвать от потребителей. В случае
                    неисполнения этих обязанностей изготовителем (исполнителем) решение о снятии товаров (работ, услуг)
                    с производства, изъятии из оборота и отзыве от потребителей должны принимать соответствующие
                    государственные контролирующие органы в пределах своих полномочий. Если в. результате этого действия
                    продовольственное сырье и продукция будут признаны негодными к употреблению в пищу, они должны быть
                    уничтожены или переработаны.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Изготовитель (исполнитель) обязан возместить в полном объеме причиненные потребителям убытки,
                    связанные с отзывом товаров (работ, услуг).<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l12 level1 lfo7'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>6.<span
                                style=''>               </span></span></span><![endif]>В случае нарушения требований,
                    установленных пунктами 4 и 5 настоящей статьи изготовитель (исполнитель) по решению органов
                    осуществляющих государственный контроль за качеством продукции, перечисляют в Государственный бюджет
                    доход, полученный от товаров, реализованных с нарушением этих правил.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l12 level1 lfo7'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>7.<span
                                style=''>               </span></span></span><![endif]>Создавая новый
                    (модернизированный) товар (работу, услугу), изготовитель должен представить нормативную документацию
                    на этот товар соответствующему органу для проведения государственной экспертизы на его соответствие
                    требованиям по безопасности жизни, здоровья и имущества граждан, а также окружающей природной среде.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l12 level1 lfo7'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>8.<span
                                style=''>               </span></span></span><![endif]>Изготовитель (исполнитель) обязан
                    информировать потребителя о возможности риска и о безопасном использовании товара (работы, услуги) с
                    помощью принятых в международной практике обозначений.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 11. Имущественная ответственность за вред, причиненный товарами (работами, услугами)
                    ненадлежащего качества<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Вред, причиненный жизни, здоровью или имуществу потребителя товарами (работами, услугами),
                    содержащими конструктивные производственные, рецептурные или иные недостатки, подлежит возмещению
                    потребителю в полном объеме, если законодательством не предусмотрена более высокая мера
                    ответственности.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 12. Возмещение морального вреда<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Моральный вред, причиненный потребителю вследствие нарушения изготовителем (исполнителем, продавцом)
                    его прав, предусмотренных настоящим Законом, подлежит возмещению причинителем вреда. Размер
                    возмещения вреда определяется судом, если иное не предусмотрено законом.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:4.5pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 13. Права потребителя на информацию о товарах (работах, услугах)<span
                        style='mso-spacerun:yes'>  </span>1. Продавец (исполнитель) обязан предоставить потребителю
                    необходимую и достоверную информацию о цене, потребительских свойствах интересующей его продукции (а
                    в отношении продуктов питания - так же о составе, сроке годности, калорийности, содержании вредных
                    для здоровья веществ по сравнению с обязательными требованиями нормативных документов), условиях ее
                    приобретения, гарантийных обязательствах и порядке предъявления претензий, способах и правилах
                    использования, хранения и безопасной утилизации товара.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Продавец (исполнитель) обязан также предоставить потребителю полную и достоверную информацию о
                    правилах торговли товарами, реализуемыми предприятием и видах обслуживания.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l14 level1 lfo8'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>2.<span
                                style=''>               </span></span></span><![endif]>Информация, предусмотренная в
                    пункте 1 настоящей статьи, доводится до сведения потребителя в технической документации, прилагаемой
                    к продукции, а также маркировкой, указанием даты изготовления и реализации или иным способом,
                    принятым в отдельных сферах обслуживания.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l14 level1 lfo8'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>3.<span
                                style=''>               </span></span></span><![endif]>Произведенные товары должны иметь
                    производственную иди торговую марку, товарный или служебный знак. Производственная марка включает в
                    себя наименование, принадлежность, местонахождение производителя (изготовителя) и обозначение
                    стандартов (нормативных документов). Изготовленный товар должен соответствовать производственной
                    марке. Товар (работа, услуга) изготовленный лицом, осуществляющим предпринимательскую деятельность
                    должен иметь этикетку (ярлык) с указанием сведений о номере документа, удостоверяющего право на
                    осуществление предпринимательской деятельности, и наименования органа, его выдавшего, в случае
                    необходимости - сведения о его сертификации, а по товарам (работам, услугам) отвечающим специальным
                    требованиям - сведения о номерах государственных стандартов.<span style='mso-spacerun:yes'>  </span>
                </p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l14 level1 lfo8'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>4.<span
                                style=''>               </span></span></span><![endif]>Если предоставление недостоверной
                    или недостаточно полной информации о реализуемом товаре повлекло приобретение товара (работы,
                    услуги), не обладающего необходимыми потребителю свойствами, - потребитель вправе расторгнуть
                    договор и потребовать возмещения причиненных ему убытков.<span style='mso-spacerun:yes'>  </span>
                </p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l14 level1 lfo8'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>5.<span
                                style=''>               </span></span></span><![endif]>Реклама продукции, подлежащей
                    обязательной сертификации, но не имеющей сертификат соответствия, запрещена.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l14 level1 lfo8'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>6.<span
                                style=''>               </span></span></span><![endif]>Убытки, причиненные потребителю
                    товарами (работами, услугами) приобретенными в результате недобросовестной рекламы, подлежат
                    возмещению виновными лицами в полном объеме.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l14 level1 lfo8'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>7.<span
                                style=''>               </span></span></span><![endif]>При рассмотрении требований
                    потребителя о возмещении убытков, причиненных недостоверной или неполной информацией о товарах
                    (работах, услугах) либо недобросовестной рекламой, необходимо исходить из предположения об
                    отсутствии у потребителя специальных познаний о свойствах и характеристиках, приобретаемых им
                    товаров (работ, услуг).<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l14 level1 lfo8'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>8.<span
                                style=''>               </span></span></span><![endif]>Государство создает условия для
                    получения потребителями необходимой информации о своих правах и об их защите.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 14. Права потребителя в сфере торгового и иных видов обслуживания<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>1.
                    Все граждане имеют в равной мере право на удовлетворение своих потребностей в сфере торгового и иных
                    видов обслуживания.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:27.9pt;margin-bottom: .25pt;margin-left:-.25pt'>
                    Установление каких-либо преимуществ, прямых или косвенных ограничений прав потребителей не
                    допускается, кроме случаев, предусмотренных законодательными актами. Отдельным категориям граждан,
                    нуждающихся в социальной защите, могут предоставляться льготы и преимущества в торговом и других
                    видах обслуживания в порядке, установленном законодательными актами Азербайджанской Республики,<span
                        style='mso-spacerun:yes'>  </span>2. Потребитель имеет право на свободный выбор товаров и услуг
                    в удобное для него время с учетом режима работы продавца (исполнителя).<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Продавец (исполнитель) обязан всячески содействовать потребителю в свободном выборе товаров и услуг.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Запрещается принуждать потребителя приобретать товары и услуги ненадлежащего качества или ненужного
                    ему ассортимента.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:6.5pt;margin-bottom:.25pt; margin-left:-.25pt'>3.
                    Продавец (исполнитель) обязан предоставить потребителю достоверную и доступную информацию о
                    наименовании, принадлежности и режиме работы своего предприятия.<span
                        style='mso-spacerun:yes'>  </span>4. Потребитель имеет право на проверку качества,
                    комплектности, меры, веса и цены приобретаемых товаров, демонстрацию безопасного и правильного их
                    использования. По требованию потребителя продавец обязан в этих случаях предоставить ему
                    контрольноизмерительные приборы, документы о цене товаров. В том случае, когда во время гарантийного
                    срока необходимо определить причины потери качества товара, продавец, обязан в трехдневный со дня
                    получения письменного заявления от потребителя срок направить этот товар на экспертизу. Экспертиза
                    производится за счет продавца.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:15.4pt;mso-list:l0 level1 lfo9'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>5.<span
                                style=''>               </span></span></span><![endif]>Потребитель имеет право выбрать
                    необходимый ему товар из искусственно созданного продавцом комплекта.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:15.4pt;mso-list:l0 level1 lfo9'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>6.<span
                                style=''>               </span></span></span><![endif]>При нарушении прав потребителя на
                    предприятиях сферы торгового и иных видов обслуживания продавец (исполнитель) и работники этих
                    предприятий несут ответственность в порядке, установленном законодательством.<span
                        style='mso-spacerun:yes'>  </span>Статья 15. Права потребителя на обмен товара ненадлежащего
                    качества<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l10 level1 lfo10'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>1.<span
                                style=''>               </span></span></span><![endif]>Потребитель имеет право обменять
                    непродовольственный товар ненадлежащего качества на аналогичный по месту приобретения, если товар не
                    подошел по форме, размеру, фасону, цвету или по иным причинам не может быть им использован по
                    назначению.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Потребитель имеет право на обмен товара надлежащего качества в течение 14 дней, не считая дня
                    покупки.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Обмен товара надлежащего качества производится, если он не был в употреблении и если сохранены его
                    товарный вид, потребительские свойства, пломбы, ярлыки, а также товарный либо кассовый чек или иные
                    документы, выданные потребителю вместе с проданным товаром.<span style='mso-spacerun:yes'>  </span>
                </p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Перечень товаров, не подлежащих обмену по основаниям, указанным в настоящей статье, утверждается
                    Кабинетом министров Азербайджанской Республики.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l10 level1 lfo10'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>2.<span
                                style=''>               </span></span></span><![endif]>Если в момент обмена аналогичный
                    товар отсутствует в продаже, потребитель вправе приобрести любые другие товары из имеющегося
                    ассортимента с соответствующим перерасчетом стоимости, или получить обратно деньги в размере
                    стоимости возвращенного товара, или осуществить обмен товара на аналогичный при первом же
                    поступлении соответствующего товара в продажу. Продавец, обязан в день поступления товара в продажу
                    сообщить об этом потребителю, требующему обмена товара.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 16. Недействительность условий договоров, ограничивающих права потребителей<span
                        style='mso-spacerun:yes'>  </span>Условия договора, ограничивающие права потребителя по
                    сравнению с правами, установленными законодательством, признаются недействительными. Если в
                    результате применения условий договора, ограничивающих права потребителя, потребителю причинены
                    убытки, то они должны быть возмещены виновным лицом в полном объеме.<span
                        style='mso-spacerun:yes'>  </span>Потребитель имеет право на возмещение убытков, причиненных ему
                    изготовителем (исполнителем, продавцом) в связи с использованием последним преимуществ своего
                    служебного положения в производственной или торговой деятельности.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Глава III. Защита прав потребителей<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 17. Правила торгового и иных видов обслуживания<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Правила торгового, бытового и иных видов обслуживания (выполнения работ, оказания услуг)
                    утверждаются Кабинетом министров Азербайджанской Республики. Указанные правила не должны
                    противоречить Закону Азербайджанской Республики "О защите нрав потребителей" и другим
                    законодательным актам Азербайджанской Республики.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 18. Государственная защита прав потребителей<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l4 level1 lfo11'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>1.<span
                                style=''>               </span></span></span><![endif]>Государство обеспечивает
                    потребителям защиту их интересов, предоставляет возможность свободного выбора товаров (работ,
                    услуг), гарантирует приобретение или получение иными законными способами товаров (работ, услуг), в
                    целях обеспечения на должном уровне здоровья и жизнедеятельности.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l4 level1 lfo11'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>2.<span
                                style=''>               </span></span></span><![endif]>Государственную защиту прав
                    потребителей осуществляют в пределах своих полномочий соответствующие государственные органы,
                    осуществляющие контроль за качеством, безопасностью товаров (работ, услуг), и защитой других
                    охраняемых законом прав потребителей и суд.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:4.15pt;margin-bottom: .25pt;margin-left:-.25pt'>
                    Статья 19. Полномочия государственных органов, осуществляющих контроль за качеством и безопасностью
                    товаров (работ, услуг), предназначенных для потребителей<span style='mso-spacerun:yes'>  </span>1. В
                    целях обеспечения качества и безопасности товаров (работ, услуг), соответствующие органы
                    государственной исполнительной власти, осуществляющие контроль за качеством и безопасностью товаров
                    (работ, услуг) в Азербайджанской Республике, в пределах своей компетенции, установленной Президентом
                    Азербайджанской Республики:<span style='mso-spacerun:yes'>  </span>* устанавливают обязательные
                    требования по безопасности товаров (работ, услуг) и осуществляют контроль за соблюдением этих
                    требований;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>направляют
                    предписания об устранении нарушений требований по безопасности товаров (работ, услуг), снятия с
                    производства, прекращении выпуска и реализации таких товаров (работ, услуг), отзыве их от
                    потребителей, а также информировании об этом потребителей. <span style='mso-spacerun:yes'> </span>
                </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>пресекают
                    недобросовестную конкуренцию на рынке потребительских товаров (работ, услуг) и монополистическую
                    деятельность хозяйствующих субъектов;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>дают официальные
                    разъяснения по вопросам применения законодательства </p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Азербайджанской Республики о защите прав потребителей;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>приводят в норму
                    свойства и санитарно-гигиенические показатели продукции, обеспечивающие безопасность здоровья людей;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>осуществляют
                    согласование нормативно-технической документации и обязательную гигиеническую сертификацию в
                    производственном этапе, товаров, могущих создавать опасность для здоровья людей;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>принимают на
                    основании результатов пробного обследования соответствующее решение о дальнейшем использовании
                    товаров, не отвечающих по показателям безопасности требованиям нормативно-технической
                    документации;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>осуществляют
                    согласование производства, использования и реализации новых технологических процессов, материалов,
                    веществ и изделий;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>утверждают методики
                    гигиенической, экологической, сейсмологической, радиологической, гормональной, фармакологической и
                    токсикологической оценки товаров (работ, услуг), а также способы исследования гигиенических
                    показателей и особенностей уровня вредных факторов;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>осуществляют на
                    территории Азербайджанской Республики контрольные проверки на предприятиях торгового обслуживания,
                    общественного питания и сферы услуг независимо от формы собственности, качества товаров (работ,
                    услуг), соблюдение требований по качеству и безопасности товаров (работ, услуг), а также правил
                    торговли и оказания услуг. В необходимых случаях проверяют на месте качество товаров, сырья,
                    полуфабрикатов, комплектующих материалов или изымают их образцы для проведения независимой
                    экспертизы в соответствующих лабораториях. Стоимость изъятых образцов и проведенных анализов
                    (экспертиз) возмещается за счет проверяемых хозяйствующих субъектов и относится на их
                    производственно-хозяйственные затраты;<span style='mso-spacerun:yes'>  </span>* дают хозяйствующим
                    субъектам обязательные к исполнению предписания о прекращении нарушений прав потребителей, получают
                    от проверяемых субъектов необходимые нормативные документы и иные сведения, характеризующие качество
                    товаров;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>прекращают
                    отгрузку, реализацию (исполнение) товаров (работ, услуг), не отвечающих требованиям нормативных
                    документов, до устранения хозяйствующими субъектами обнаруженных недостатков:<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>в случаях,
                    предусмотренных законодательством, налагают на виновные лица экономические санкции и меры
                    административного взыскания, передают материалы проверок, содержащих признаки преступления,
                    правоохранительным органам;<span style='mso-spacerun:yes'>  </span>* предъявляют в соответствующие
                    суды иски о защите прав потребителей.<span style='mso-spacerun:yes'>  </span>2. Национальный орган
                    по сертификации товаров (работ, услуг) в Азербайджанской Республике:<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>определяет порядок
                    сертификации товаров (работ, услуг); </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>определяет
                    номенклатуру товаров (работ, услуг), подлежащих обязательной сертификации;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>аккредитует органы
                    по сертификации конкретных видов товаров (работ, услуг), а также испытательные лаборатории (центры)
                    для проведения соответствующих испытаний, предоставляет другим юридическим лицам право аккредитации;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>осуществляет
                    контроль за правильностью проведения сертификации товаров (работ, услуг);<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>ведет
                    государственный реестр сертифицированных товаров (работ, услуг), аккредитованных органов по
                    сертификации, испытательных лабораторий (центров);<span style='mso-spacerun:yes'>  </span>*
                    принимает решения о признании сертификатов, выданных зарубежными и международными органами;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l13 level1 lfo12'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>представляет
                    Азербайджанскую Республику во взаимоотношениях с зарубежными странами и международными организациями
                    по вопросам сертификации товаров (работ, услуг).<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>3.
                    Координация деятельности государственных органов исполнительной власти, осуществляющих контроль за
                    безопасностью товаров (работ, услуг), возлагается на соответствующий государственный орган
                    исполнительной власти, осуществляющий деятельность в сфере стандартизации и метрологии.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 20. Обязанности местных органов исполнительной власти по защите прав потребителей<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Местные органы исполнительной власти вправе создавать соответствующие органы по защите прав
                    потребителей. Созданные исполнительной властью соответствующие органы по защите прав
                    потребителей:<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l7 level1 lfo13'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>*<span
                                style=''>                 </span></span></span><![endif]>рассматривают жалобы
                    потребителей, дают консультации по защите их прав, предусмотренных законодательством;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l7 level1 lfo13'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>*<span
                                style=''>                 </span></span></span><![endif]>анализируют договоры,
                    заключаемые продавцами (изготовителями, исполнителями) с потребителями, с целью выявления условий,
                    ущемляющих права потребителей;<span style='mso-spacerun:yes'>  </span>* осуществляют сбор информации
                    о причинении вреда жизни, здоровью или имуществу потребителей, и направляют ее в соответствующие
                    уполномоченные органы;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l7 level1 lfo13'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>*<span
                                style=''>                 </span></span></span><![endif]>при обнаружении потребителями
                    недостатков товаров (работ, услуг) или выявлении ими опасных товаров (работ, услуг) незамедлительно
                    сообщают об этом соответствующим органам государственной исполнительной власти, осуществляющим
                    контроль за безопасностью товаров (работ, услуг);<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l7 level1 lfo13'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>*<span
                                style=''>                 </span></span></span><![endif]>в целях защиты прав
                    потребителей могут предъявлять иски в суды до собственной инициативе или по поручению потребителя
                    (союза потребителей).<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 21. Обязанности и ответственность должностных лиц органов государственной исполнительной
                    власти, осуществляющих защиту прав потребителей<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Должностные лица органов государственной исполнительной власти, осуществляющих защиту прав
                    потребителей, обязаны соблюдать законодательство Азербайджанской Республики, а также охраняемые
                    законом права граждан, предприятий, учреждений и организаций.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>Они
                    обязаны не разглашать сведения, полученные при осуществлении своих полномочий и составляющие
                    государственную или иную охраняемую законом тайну. За неисполнение или ненадлежащее исполнение
                    должностными лицами своих обязанностей, превышение ими своих полномочий они привлекаются к
                    ответственности согласно действующему законодательству.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 22. Рассмотрение жалоб на решения органов государственной исполнительной власти,
                    осуществляющих защиту прав потребителей, их должностных лиц, а также на действия таких лиц </p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Жалобы на решения органов государственной исполнительной власти, осуществляющих защиту прав
                    потребителей, их должностных лиц, а также на действия таких лиц рассматриваются в порядке,
                    определенном законодательством.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Подача жалобы не приостанавливает выполнения решения органа государственной исполнительной власти,
                    осуществляющего защиту прав потребителей, его должностных лиц, а также действий таких лиц.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 23. Отношения органов государственной исполнительной власти, осуществляющих защиту прав
                    потребителей, с правоохранительными органами<span style='mso-spacerun:yes'>  </span>Работники
                    правоохранительных органов оказывают помощь должностным лицам органов государственной исполнительной
                    власти, осуществляющих защиту прав потребителей в исполнении ими служебных обязанностей и пресекают
                    незаконные действия граждан, препятствующих выполнению возложенных на них функций.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 24. Правовая защита должностных лиц органов государственной исполнительной власти,
                    осуществляющих защиту прав потребителей<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Права должностных лиц и специалистов органов государственной исполнительной власти, осуществляющих
                    защиту прав потребителей обеспечиваются соответствующими законодательными актами Азербайджанской
                    Республики.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Запрещается препятствовать им в выполнении своих обязанностей или оказывать </p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    давление в какой-либо форме, а также вмешиваться в их дела.<span style='mso-spacerun:yes'>  </span>
                </p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Органы, должностные лица и граждане, допустившие подобные действия несут ответственность в порядке,
                    определенном законодательством Азербайджанской Республики.<span style='mso-spacerun:yes'>  </span>
                </p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 25. Санкции, применяемые органами государственной исполнительной власти осуществляющими
                    защиту прав потребителей<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    1.<span style='mso-spacerun:yes'>  </span>Органы государственной исполнительной власти,
                    осуществляющие защиту прав потребителей в Азербайджанской Республике, в пределах своих компетенций
                    применяют к лицам, виновным в нарушении законодательства о защите прав потребителей, следующие
                    экономические санкции: </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l5 level1 lfo14'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>за отказ от
                    реализации прав потребителя, установленных пунктом 1 статьи 7 настоящего </p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Закона, - в пятикратном размере стоимости этого товара;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l5 level1 lfo14'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>за невыполнение
                    обязательного к исполнению предписания соответствующего государственного органа об устранении
                    нарушений прав потребителей, - в размере причиненного ущерба, наряду с его возмещением, а в случае
                    невозможности определения размера ущерба - в размере до 100 минимальных заработных плат;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l5 level1 lfo14'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>за выпуск,
                    реализацию товаров, выполнение работ, оказание услуг, не отвечающих требованиям нормативных
                    документов, - в размере 25 процентов стоимости реализованных товаров, выполненных работ, оказанных
                    услуг.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l5 level1 lfo14'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>за реализацию
                    товаров, выполнение работ, оказание услуг, подлежащих, но не прошедших обязательную сертификацию, -
                    в размере 25 процентов стоимости реализованных товаров, выполненных работ, оказанных услуг;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l5 level1 lfo14'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>за выпуск
                    (выполнение) и реализацию представляющих опасность для жизни, здоровья и имущества людей, а также
                    окружающей природной среды товаров (работ, услуг), в следствие нарушения требований стандартов и
                    других нормативных документов, - в размере 100 процентов стоимости выпущенных (выполненных) и
                    реализованных товаров;<span style='mso-spacerun:yes'>  </span>* за реализацию импортных товаров, не
                    отвечающих действующим в Азербайджанской Республике требованиям стандартов, других нормативных
                    документов по безопасности жизни, здоровья и имущества потребителей, а также окружающей среды, - в
                    размере 50 процентов стоимости реализованных товаров;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.1pt; margin-left:9.0pt;text-indent:-9.0pt;line-height:106%;mso-list:l5 level1 lfo14'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:106%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>за реализацию
                    товаров, выполнение работ, оказание услуг, запрещенных к выпуску и реализации, - в размере 100
                    процентов стоимости реализованных товаров, выполненных работ, оказанных услуг;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l5 level1 lfo14'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>за реализацию
                    опасных товаров (яды, химикаты, взрыво- и огнеопасные вещества и т. д.) без надлежащей
                    предупредительной маркировки, а также без информации о правилах и условиях безопасного их
                    использования, - в размере 50 процентов стоимости реализованных товаров;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l5 level1 lfo14'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>за сокрытие от
                    соответствующих органов государственного контроля продукции, подлежащей контролю, - в размере 100
                    процентов стоимости реализованных товаров, выполненных работ и оказанных услуг;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l5 level1 lfo14'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>за нарушение правил
                    сертификации товаров (работ, услуг) органами по сертификации и испытательными лабораториями
                    (центрами) - в размере двукратной стоимости работ по сертификации, а за нарушение этих правил
                    изготовителем (исполнителем, продавцом) - в размере стоимости товаров (работ, услуг), реализованных
                    с нарушениями правил сертификации.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Руководители предприятий и органов по сертификации несут ответственность за вышеуказанные нарушения
                    и подвергаются штрафу в размере трех должностных окладов.<span style='mso-spacerun:yes'>  </span>Уплата
                    штрафов, предусмотренных настоящей статьей, не освобождает изготовителя (исполнителя, продавца) от
                    возмещения ущерба, причиненного потребителям в результате нарушения стандартов, норм, правил,
                    невыполнения договорных обязательств.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l11 level1 lfo15'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>2.<span
                                style=''>               </span></span></span><![endif]>Из сумм штрафов 50 процентов
                    зачисляется в государственный бюджет, 35 процентов - в местный бюджет, а 15 процентов -
                    государственным органам, осуществляющим защиту прав потребителей, наложившим штраф.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-right:.25pt;mso-list:l11 level1 lfo15'><![if !supportLists]><span
                        style='mso-bidi-font-size:12.0pt;line-height:111%'><span style='mso-list:Ignore'>3.<span
                                style=''>               </span></span></span><![endif]>Изготовители (исполнители,
                    продавцы) товаров (работ, услуг) вправе обратиться в суд, арбитражный суд с заявлением о признании
                    недействительными полностью или частично предписаний государственных органов, осуществляющих защиту
                    прав потребителей, либо об отмене или изменении решений о наложении штрафа.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Подача заявления не приостанавливает предписания или решения о наложении штрафа на время его
                    рассмотрения в суде или арбитражном суде, если судом или арбитражным судом не вынесено определение о
                    приостановлении исполнения указанных актов.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Статья 26. Судебная зашита прав потребителей<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Защита прав потребителей, предусмотренных законодательством, осуществляется судом.<span
                        style='mso-spacerun:yes'>  </span>Наряду с удовлетворением требований потребителя суд решает
                    также вопрос возмещения ему морального (неимущественного) вреда.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Потребители освобождаются от уплаты государственной пошлины по искам, связанным с нарушением их
                    прав.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:8.6pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Глава IV.<span style='mso-spacerun:yes'>  </span>Общественные организации потребителей (объединение
                    потребителей) Статья 27. Общественные организации потребителей (объединение потребителей) и их права<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:12.0pt;text-indent:-12.0pt;mso-list:l2 level1 lfo16'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>1.<span style=''>  </span></span></span><![endif]>В целях защиты
                    своих законных прав и интересов граждане Азербайджанской Республики имеют право объединяться на
                    добровольной основе в общественные организации потребителей (объединение потребителей).<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:-.25pt'>
                    Объединение потребителей, являясь общественной организацией, осуществляет свою деятельность в
                    соответствии с Законом Азербайджанской Республики "Об общественных объединениях". </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:12.0pt;text-indent:-12.0pt;mso-list:l2 level1 lfo16'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>2.<span style=''>  </span></span></span><![endif]>Объединение
                    потребителей имеет право: </p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>изучать
                    потребительские свойства товаров, спрос на них, общественное мнение о качестве выпускаемых и
                    реализуемых товаров и ценах на них:<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>обратиться к
                    соответствующим органам государственного контроля для проведения экспертизы и испытания товаров
                    (работ, услуг).<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>получать от
                    соответствующих органов исполнительной власти и хозяйствующих субъектов информацию, необходимую для
                    реализации своих целей ц задач;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>содействовать
                    соответствующим государственным органам в осуществлении контроля за качеством товаров (работ,
                    услуг), торгового и иных видов обслуживания:<span style='mso-spacerun:yes'>  </span>* организовывать
                    юридическую консультацию потребителей в соответствии с законодательством;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>давать предложения
                    и заключения по разработке нормативных документов, устанавливающих требования к качеству товаров
                    (работ, услуг):<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>представлять и
                    защищать интересы потребителей в соответствующих органах государственной исполнительной власти;<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>вносить в
                    соответствующие органы исполнительной власти, предприятия и организации предложения о мерах по
                    повышению качества товаров (работ, услуг) и соблюдению правил ценообразования, о временном
                    приостановлении выпуска и реализации товаров (работ, услуг), не соответствующих установленным
                    требованиям по качеству, о прекращении производства, изъятии из продажи товаров (работ, услуг),
                    опасных для жизни, здоровья и имущества граждан, окружающей среды, о прекращении продажи товаров по
                    неправомерно завышенным ценам,. а также об отмене цен, установленных в нарушение действующего
                    законодательства;<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>в соответствии с
                    законодательством защищать в судебном порядке права граждан, не являющихся членами общественных
                    организаций потребителей (объединения потребителей);<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>обращаться в
                    соответствующие органы исполнительной власти о привлечении к ответственности лиц, виновных в выпуске
                    и продаже некачественных товаров (работ, услуг);<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>информировать
                    общественность о правах потребителей:<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal
                   style='margin-top:0in;margin-right:.25pt;margin-bottom:.25pt; margin-left:9.0pt;text-indent:-9.0pt;mso-list:l8 level1 lfo17'>
                    <![if !supportLists]><span style='mso-bidi-font-size:12.0pt;line-height:111%'><span
                            style='mso-list:Ignore'>*<span style=''>  </span></span></span><![endif]>содействовать
                    развитию международного сотрудничества в области защиты прав потребителей.<span
                        style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:215.65pt;margin-bottom: .25pt;margin-left:-.25pt'>
                    Президент Азербайджанской Республики Гейдар АЛИЕВ.<span style='mso-spacerun:yes'>  </span></p>
                <p class=MsoNormal style='margin-top:0in;margin-right:311.0pt;margin-bottom: .25pt;margin-left:-.25pt'>
                    г. Баку, 19 сентября 1995 г.<span style='mso-spacerun:yes'>  </span>№ 1113. </p>
                <p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-indent:0in; line-height:107%'><b
                        style='mso-bidi-font-weight:normal'><span
                            style='font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:107%'><span
                                style='mso-spacerun:yes'> </span></span></b></p></div>
        @endif
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <style>
        .stars .star.active:before {
            color: #79a6e9;
            content: "\e020";
        }
        .avg_rating {
            position: relative !important;
            float: initial !important;
            font-size: 15px !important;
        }
    </style>
    <script>
        var wc_single_product_params = {
            "i18n_required_rating_text": "Please select a rating",
            "review_rating_required": "yes",
            "flexslider": {
                "rtl": false,
                "animation": "slide",
                "smoothHeight": true,
                "directionNav": false,
                "controlNav": "thumbnails",
                "slideshow": false,
                "animationSpeed": 500,
                "animationLoop": false,
                "allowOneSlide": false
            },
            "zoom_enabled": "",
            "zoom_options": [],
            "photoswipe_enabled": "",
            "photoswipe_options": {
                "shareEl": false,
                "closeOnScroll": false,
                "history": false,
                "hideAnimationDuration": 0,
                "showAnimationDuration": 0
            },
            "flexslider_enabled": ""
        };
    </script>
    <script type='text/javascript'
            src='/wp-content/plugins/woocommerce/assets/js/frontend/single-product.min7e2e.js?ver=3.8.1'></script>
    <script src="{{asset('js/order.js')}}"></script>
    <div class="row">
        <div class="large-12 columns">
            <div class="woocommerce-notices-wrapper"></div>
        </div>
    </div>
    <div id="product-{{$product->id}}"
         class="product type-product post-1865 status-publish first instock product_cat-smartwatch has-post-thumbnail featured shipping-taxable purchasable product-type-simple">

        <div class="single-product with-sidebar right-sidebar">
            <div class="row">
                <div class="xlarge-2 large-3 columns show-for-large-up sidebar-pos">
                    <div class="shop_sidebar wpb_widgetised_column">
                        <aside class="widget master-slider-main-widget">
                            <!-- MasterSlider -->
                            <div id="P_MS5e256dd2798b9" class="master-slider-parent ms-parent-id-3"
                                 style="max-width: 400px; position: relative;">
                                <!-- MasterSlider Main -->
                                <div id="MS5e256dd2798b9" class="master-slider ms-skin-default ms-wk"
                                     style="visibility: visible; opacity: 1; margin: 0px; background-color: #f8f8f8;">
                                    <div class="ms-container">
                                        <div class="ms-inner-controls-cont" style="max-width: 400px;">
                                            <div class="ms-bullets ms-dir-h ms-align-bottom ms-ctrl-hide"
                                                 style="bottom: 10px; opacity: 0; width: 32px;">
                                                <div class="ms-bullets-count">
                                                    <div class="ms-bullet ms-bullet-selected"
                                                         style="margin: 3px;"></div>
                                                    <div class="ms-bullet" style="margin: 3px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ms-timerbar ms-align-bottom"
                                         style="height: 4px; top: auto; bottom: 0px;">
                                        <div class="ms-time-bar"
                                             style="height: 4px; background-color: rgb(255, 255, 255); width: 0%;"></div>
                                    </div>
                                </div>
                                <!-- END MasterSlider Main -->
                            </div>
                            <!-- END MasterSlider -->

                            <script>
                                (window.MSReady = window.MSReady || []).push(function ($) {

                                    "use strict";
                                    var masterslider_98b9 = new MasterSlider();

                                    // slider controls
                                    masterslider_98b9.control('bullets', {
                                        autohide: true,
                                        overVideo: true,
                                        dir: 'h',
                                        align: 'bottom',
                                        space: 6,
                                        margin: 10
                                    });
                                    masterslider_98b9.control('timebar', {
                                        autohide: false,
                                        overVideo: true,
                                        align: 'bottom',
                                        color: '#FFFFFF',
                                        width: 4
                                    });
                                    // slider setup
                                    masterslider_98b9.setup("MS5e256dd2798b9", {
                                        width: 400,
                                        height: 800,
                                        minHeight: 0,
                                        space: 0,
                                        start: 1,
                                        grabCursor: true,
                                        swipe: true,
                                        mouse: true,
                                        keyboard: false,
                                        layout: "boxed",
                                        wheel: false,
                                        autoplay: true,
                                        instantStartLayers: false,
                                        mobileBGVideo: false,
                                        loop: true,
                                        shuffle: false,
                                        preload: 0,
                                        heightLimit: true,
                                        autoHeight: false,
                                        smoothHeight: true,
                                        endPause: false,
                                        overPause: true,
                                        fillMode: "fill",
                                        centerControls: true,
                                        startOnAppear: false,
                                        layersMode: "center",
                                        autofillTarget: "",
                                        hideLayers: false,
                                        fullscreenMargin: 0,
                                        speed: 200,
                                        dir: "v",
                                        parallaxMode: 'swipe',
                                        view: "basic"
                                    });
                                    //$("head").append("<link rel='stylesheet' id='ms-fonts'  href='//fonts.googleapis.com/css?family=Lato:300,regular,900|Great+Vibes:regular' type='text/css' media='all' />");

                                    window.masterslider_instances = window.masterslider_instances || [];
                                    window.masterslider_instances.push(masterslider_98b9);
                                });
                            </script>

                        </aside>
                    </div>
                </div>
                <!--.columns-->

                <div class="xlarge-10 large-9 columns content-pos">


                    <div class="single-product-images with_sidebar">
                        <div class="featured_img_temp">
                            <img width="570" height="606"
                                 src="{{config('app.uploads_location')}}/{{$product->front_photo_url}}"
                                 class="attachment-shop_single size-shop_single wp-post-image"
                                 alt=""
                                 sizes="(max-width: 570px) 100vw, 570px"/>
                        </div>
                        <div class="images single-images">

                            <div class="product_images">

                                <div id="product-images-carousel"
                                     class="owl-carousel owl-theme woocommerce-product-gallery__wrapper"
                                     data-slider-id="1">
                                    <div class="easyzoom el_zoom woocommerce-product-gallery__image">
                                        <a data-fresco-group="product-gallery"
                                           data-fresco-options="fit: 'width'"
                                           class="fresco"
                                           href="{{config('app.uploads_location')}}/{{$product->front_photo_url}}">


                                            <img width="570" height="606"
                                                 src="{{config('app.uploads_location')}}/{{$product->front_photo_url}}"
                                                 class="attachment-shop_single size-shop_single wp-post-image"
                                                 alt=""
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                            <span class="product_image_zoom_button"><i
                                                    class="fa fa-expand"></i></span>

                                        </a>
                                    </div>

                                    @if(isset($product->back_photo_url))
                                        <div class="easyzoom el_zoom">
                                            <a data-fresco-group="product-gallery"
                                               data-fresco-options="fit: 'width'"
                                               class="fresco"
                                               href="{{config('app.uploads_location')}}/{{$product->back_photo_url}}">
                                                <img data-src="{{config('app.uploads_location')}}/{{$product->back_photo_url}}"
                                                     class="owl-lazy" alt="">
                                                <span class="product_image_zoom_button"><i
                                                        class="fa fa-expand"></i></span>
                                            </a>
                                        </div>
                                    @endif


                                    @if(isset($product->additional_photo1_url))
                                        <div class="easyzoom el_zoom">
                                            <a data-fresco-group="product-gallery"
                                               data-fresco-options="fit: 'width'"
                                               class="fresco"
                                               href="{{config('app.uploads_location')}}/{{$product->additional_photo1_url}}">
                                                <img data-src="{{config('app.uploads_location')}}/{{$product->additional_photo1_url}}"
                                                     class="owl-lazy" alt="">
                                                <span class="product_image_zoom_button"><i
                                                        class="fa fa-expand"></i></span>
                                            </a>
                                        </div>
                                    @endif

                                    @if(isset($product->additional_photo2_url))
                                        <div class="easyzoom el_zoom">
                                            <a data-fresco-group="product-gallery"
                                               data-fresco-options="fit: 'width'"
                                               class="fresco"
                                               href="{{config('app.uploads_location')}}/{{$product->additional_photo2_url}}">
                                                <img data-src="{{config('app.uploads_location')}}/{{$product->additional_photo2_url}}"
                                                     class="owl-lazy" alt="">
                                                <span class="product_image_zoom_button"><i
                                                        class="fa fa-expand"></i></span>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            </div><!-- /.product_images -->


                        </div>


                        <div class="product_summary_thumbnails_wrapper with-sidebar">
                            <div>


                                <div class="product_thumbnails">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"><img width="350"
                                                                           height="380"
                                                                           src="{{config('app.uploads_location')}}/{{$product->front_photo_url}}"
                                                                           class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image"
                                                                           alt="" title="{{$product->name}}"
                                                                           sizes="(max-width: 350px) 100vw, 350px"/>
                                            </div>
                                            @if(isset($product->back_photo_url))
                                                <div class="swiper-slide"><img width="350"
                                                                               height="380"
                                                                               src="{{config('app.uploads_location')}}/{{$product->back_photo_url}}"
                                                                               class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image"
                                                                               alt="" title="{{$product->name}}"
                                                                               sizes="(max-width: 350px) 100vw, 350px"/>
                                                </div>
                                            @endif

                                            @if(isset($product->additional_photo1_url))
                                                <div class="swiper-slide"><img width="350"
                                                                               height="380"
                                                                               src="{{config('app.uploads_location')}}/{{$product->additional_photo1_url}}"
                                                                               class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image"
                                                                               alt="" title="{{$product->name}}"
                                                                               sizes="(max-width: 350px) 100vw, 350px"/>
                                                </div>
                                            @endif
                                            @if(isset($product->additional_photo2_url))
                                                <div class="swiper-slide"><img width="350"
                                                                               height="380"
                                                                               src="{{config('app.uploads_location')}}/{{$product->additional_photo2_url}}"
                                                                               class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image"
                                                                               alt="" title="{{$product->name}}"
                                                                               sizes="(max-width: 350px) 100vw, 350px"/>
                                                </div>
                                            @endif
                                        </div><!-- /.swiper-wrapper -->

                                        <style>
                                            .product_thumbnails .swiper-container {
                                                height: 333.14285714286px;
                                            }
                                        </style>


                                        <div class="pagination"></div>

                                    </div><!-- /.swiper-container -->


                                </div><!-- /.product_images -->

                            </div>
                        </div><!-- .product_summary_thumbnails_wrapper-->

                    </div>

                    <!-- Product Content -->

                    <div class="single-product-infos">
                        <div class="product_infos">


                            <script>
                                jQuery(document).ready(function ($) {
                                    jQuery('.social-sharing').socialShare({
                                        social: 'twitter,facebook',
                                        animation: 'launchpadReverse',
                                        blur: true
                                    });
                                });
                            </script>

                            <div class="box-share-master-container" data-name="Share">
                                <a href="javascript:;" class="social-sharing"
                                   data-name="{{$product->name}}"
                                   data-shareimg="{{config('app.uploads_location')}}/{{$product->front_photo_url}}">
                                    <i class="fa fa-share-alt"></i>
                                    <span>Share</span>
                                </a>
                            </div>
                            <!--.box-share-master-container-->


                            <h1 class="product_title entry-title">{{$product->name}}</h1>
                            <p class="price">
                                @if($product->sale_price>0)
                                    <del>
                                        <span class="woocommerce-Price-amount amount">
                                            <span class="woocommerce-Price-currencySymbol">₼</span>
                                            {{sprintf("%.2f",$product->price + (int)$product->sale_price)}}
                                        </span>
                                    </del>
                                @endif
                                <ins>
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol">₼</span>
                                        {{ $product->price }}
                                    </span>
                                </ins>
                            </p>
                            <div class="woocommerce-product-details__short-description">
                                <p>{{$product->description}}</p>
                            </div>

                            <script>
                                let view_cart_message = "@lang('product.view_cart')";
                                let view_cart_link = "{{route('view_cart')}}";
                                let product_added_message = "{{__('product.product_added', ['product' => $product->name])}}";
                            </script>

                            <div class="archive-product-rating">
                                <label
                                    for="rating">@lang('product.average_rating')</label>
                                <div class="star-rating avg_rating" role="img" aria-label="Rated {{$product->avg_star}} out of 5"><span
                                        style="width:{{(round($product->avg_star/5*100))}}%">Rated <strong class="rating">{{$product->avg_star}}</strong> / 5</span>
                                </div>
                            </div>

                            <form class="cart"
                                  method="post" enctype='multipart/form-data'>

                                @if($product->in_stock == 'no')
                                    <button type="button" onclick='makeOrder($(this),"{{route('makeOrder')}}","{{route('checkout')}}")' data-product-id="{{$product->id}}"
                                       class="button alt" >@lang('ordersNotInStock.make_order')</button>
                                @else
                                    <button type="button" name="add-to-cart"
                                            onclick="single_add_to_cart({{$product->id}},view_cart_message,view_cart_link,product_added_message)"
                                            class="single_add_to_cart_button button alt">@lang('product.add_to_cart')
                                    </button>
                                @endif

                            </form>


                            <div
                                class="yith-wcwl-add-to-wishlist add-to-wishlist-1865 exists wishlist-fragment on-first-load"
                                data-fragment-ref="{{$product->id}}"
                                data-fragment-options="{&quot;base_url&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/product\/moto-360?page&amp;product=moto-360&amp;post_type=product&amp;name=moto-360&quot;,&quot;wishlist_url&quot;:&quot;https:\/\/woodstock.temashdesign.com\/electronics\/wishlist\/&quot;,&quot;in_default_wishlist&quot;:true,&quot;is_single&quot;:true,&quot;show_exists&quot;:false,&quot;product_id&quot;:1865,&quot;parent_product_id&quot;:1865,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:true,&quot;browse_wishlist_text&quot;:&quot;Browse Wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in the wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:false,&quot;item&quot;:&quot;add_to_wishlist&quot;}">

                                <!-- ADD TO WISHLIST -->
                                @if($product->in_stock == 'yes')
                                    @if(!in_array($product->id,request()->wish_list['ids']))
                                        <div class="yith-wcwl-add-button">
                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                            <a href="{{route('add_to_wish_list',['id' => $product->id])}}"   rel="nofollow"
                                               data-product-id="{{$product->id}}" data-product-type="simple"
                                               data-original-product-id="{{$product->id}}" class=""
                                               data-title="@lang('product.add_to_wish_list')">
                                                <span class="feedback">@lang('product.add_to_wish_list')</span>
                                            </a>
                                        </div>
                                    @else
                                        <div class="yith-wcwl-wishlistexistsbrowse">
                                            <a href="{{route('wishList')}}" rel="nofollow"
                                               data-title="@lang('product.browse_wish_list')">
                                                @lang('product.browse_wish_list') </a>
                                        </div>
                                    @endif
                                @endif

                            <!-- COUNT TEXT -->

                            </div>
                            <div class="comment-form-rating" data-value="{{$product->star}}"
                                 data-product-id="{{$product->id}}"><label
                                    for="rating">@lang('product.your_rating')</label><select
                                    name="rating" id="rating"
                                    required>
                                    <option value="">Rate&hellip;
                                    </option>
                                    <option value="5">Perfect
                                    </option>
                                    <option value="4">Good</option>
                                    <option value="3">Average
                                    </option>
                                    <option value="2">Not that bad
                                    </option>
                                    <option value="1">Very poor
                                    </option>
                                </select></div>
                            {{--                            <a--}}
                            {{--                                href="../../index.html?action=yith-woocompare-add-product&amp;id=1865"--}}
                            {{--                                class="compare button" data-product_id="1865"--}}
                            {{--                                rel="nofollow">Compare</a>--}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!--.columns-->
            </div>
            <!--.row-->
        </div>
        <!--.single-product .with-sidebar-->
        <meta itemprop="url" content="index.html"/>
    </div>
@endsection

<nav class="off-menu st-mobnav slide-from-left snd-light">
    <div class="nano">
        <div class="nano-content">
            <div id="mobiles-menu-offcanvas" class="offcanvas-left-content">
                <!-- Close Icon -->
                <a href="#" class="close-icon"></a>
                <div class="clearfix"></div>

                <!-- Search Section -->
                <div class="l-search">
                    <div class="woodstock-search-form">
                        <form role="search" method="get" class="searchform  woodstock-ajax-search"
                              action="/" data-thumbnail="1"
                              data-price="1" data-post_type="product" data-count="20">
                            <input type="text" class="s ajax-search-input" placeholder="Search for products"
                                   value="" name="s" />
                            <input type="hidden" name="post_type" value="product">
                            <div class="ajax-loading spinner-circle">
                                <div class="spinner"></div>
                            </div>
                            <button type="submit" class="searchsubmit">
                                Search </button>
                        </form>
                        <div class="search-results-wrapper sd-light">
                            <div class="woodstock-scroll nano">
                                <div class="woodstock-search-results woodstock-scroll-content nano-content">
                                </div>
                            </div>
                            <div class="woodstock-search-loader"></div>
                        </div>

                    </div>




                </div>

                <!-- Contact Section -->
                <div class="contact-info">
                    <div class="inside-content">
                        <span class="contact-info-icon"></span>

                        <span class="contact-info-title">

															<span
                                                                class="contact-info-subtitle">@lang('header.contact_us')</span>
															{{request()->contact_info['phone']}}
														</span>

                        <span class="contact-info-arrow"></span>

                        <div class="inside-area">
                            <div class="inside-area-content">

                                <div class="contact-item">

                                    <span class="town">Baku</span>

                                    <span class="phone">{{request()->contact_info['phone']}}</span>

                                    <a
                                        href="mailto:{{request()->contact_info['email']}}">{{request()->contact_info['email']}}</a>

                                </div>
                                <div class="after-clear"></div>


                                <hr/>
                                <div class="after-clear"></div>


                                <ul class="social-icons">

                                    <li class="instagram"><a href="https://www.instagram.com/watchcenter.az/?hl=en"
                                                             rel="nofollow"
                                                             target="_blank"
                                                             title="Follow us on Instagram"></a></li>
                                    <li class="whatsapp"><a
                                            href="https://api.whatsapp.com/send?phone={{request()->contact_info['phone']}}&text=Salam.%20Saat%20sifariş%20vermək%20isteyirem,%20zəhmət%20olmasa%20köməklik%20edərdiz.%20&3f&source=&data="
                                            rel="nofollow"
                                            target="_blank"
                                            title="Follow us on WhatsApp"></a></li>

                                </ul>
                                <div class="after-clear"></div>

                                <div class="after-clear"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <nav id="mobile-main-navigation" class="mobile-navigation">
                    <ul id="mob-main-menu">
                        <li id="menu-item-2259"
                            class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">
                            <a href="{{ route('home') }}"><span class="menu-item-text"><span
                                        class="menu-item-main-title">@lang('header.home')</span></span></a>
                        </li>
                        <li id="menu-item-2259"
                            class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">
                            <a href="{{ route('orders_not_in_stock') }}"><span class="menu-item-text"><span
                                        class="menu-item-main-title">@lang('header.orders_not_in_stock')</span></span></a>
                        </li>
                        <li id="menu-item-2259"
                            class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">
                            <a href="{{ route('refund_policy') }}"><span class="menu-item-text"><span
                                        class="menu-item-main-title">@lang('header.refund_policy')</span></span></a>
                        </li>
{{--                        <li id="menu-item-2259"--}}
{{--                            class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">--}}
{{--                            <a href="{{ route('about_us') }}"><span class="menu-item-text"><span--}}
{{--                                        class="menu-item-main-title">@lang('header.about_us')</span></span></a>--}}
{{--                        </li>--}}
                    </ul>
                </nav>

                <nav id="mobile-top-bar-navigation" class="mobile-navigation">
                    <ul id="menu-top-bar-navigation-1">
                        <li id="menu-item-2220"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2220">
                            <a href="{{ route('home') }}">@lang('header.home')</a></li>
                        <li
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1295">
                            <a href="#">@lang('header.language')</a>
                            <ul class="sub-menu">
                                <li id="menu-item-1880"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1880">
                                    <a href="{{ route('language',['lang'=> 'az']) }}">Azerbaycan</a></li>
                                <li id="menu-item-1880"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1880">
                                    <a href="{{ route('language',['lang'=> 'ru']) }}">Русский</a></li>
                                <li id="menu-item-1879"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1879">
                                    <a href="{{ route('language',['lang'=> 'en']) }}">English</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-1878"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1878">
                            <a href="{{ route('contactUs') }}">@lang('header.contact_us')</a></li>
                    </ul>
                </nav>



                <nav id="mobile-right-top-bar-navigation" class="mobile-navigation">
                    <ul id="my-account">
                        @if(auth()->check())
                            <li class="login-link"><a href="{{route('logout')}}" ><i class="logout-icon"></i>@lang('header.logout') </a>
                        @else
                            <li class="login-link"><a href="{{route('login')}}"
                                                      class="acc-link"><i class="login-icon"></i>@lang('header.login')
                                    / @lang('header.register') </a>
                            </li>

                        @endif
                            <li class="wishlist-link"><a href="{{route('wishList')}}"
                                                         class="acc-link"><i class="wishlist-icon"></i>@lang('header.wish_list')
                                </a></li>
                    </ul>
                </nav><!-- .myacc-navigation -->



                <div class="mob-language-and-currency">



                </div>
                <!--.language-and-currency-->

                <div class="sidebar-social-icons-wrapper">
                    <ul class="social-icons">
                        <li class="instagram"><a target="_blank" title="Instagram"
                                                 href="https://www.instagram.com/watchcenter.az/?hl=en"></a></li>
                    </ul>
                </div>


            </div>

            <!-- Shop Sidebar Offcanvas -->


            @if(isset($watches_count))
            <div id="filters-offcanvas" class="offcanvas-left-content wpb_widgetised_column">
                <!-- Close Icon -->
                <a href="#" class="close-icon"></a>
                <div class="clearfix"></div>

                <aside class="widget woocommerce widget_product_categories">
                    <h3 class="widget-title">@lang('home.filters')</h3>
                    <ul class="product-categories">
                        <li class="cat-item cat-item-167 @if(Request::get('product_type')=='watches') current-cat @endif cat-parent">
                            <a
                                    data-filter="product_type"
                                    data-value="watches"
                                    href="">@lang('home.watches')</a> <span
                                    class="count">({{$watches_count}})</span>
                            <ul class='children'>
                                <li class="cat-item cat-item-168 @if(Request::get('watch_type')=='mens') current-cat @endif">
                                    <a
                                            data-filter="watch_type"
                                            data-value="mens"
                                            href="">@lang('home.mens_watches')</a>
                                    <span class="count">({{$mens_watches_count}})</span></li>
                                <li class="cat-item cat-item-169 @if(Request::get('watch_type')=='ladies') current-cat @endif">
                                    <a
                                            data-filter="watch_type"
                                            data-value="ladies"
                                            href="">@lang('home.ladies_watches')</a>
                                    <span
                                            class="count">({{$womens_watches_count}})</span></li>
                            </ul>
                        </li>
                        <li class="cat-item cat-item-180 @if(Request::get('product_type')=='watch_boxes') current-cat  @endif "><a
                                    href="?product_type=watch_boxes">@lang('home.watch_boxes')</a> <span
                                    class="count">({{$watch_boxes_count}})</span></li>
                    </ul>
                </aside>
                <aside class="widget woocommerce widget_price_filter">
                    <h3 class="widget-title">@lang('home.filter_by_price')</h3>

                    <form method="get"
                          action="/">
                          <input type="number" id="min_price_mob" name="min_price_mob" style="display:block;"
                                       value="{{ $min_price }}" data-min="0" placeholder="Min price"/>
                                <input type="number" id="max_price_mob" name="max_price_mob"
                                       value="{{ $max_price }}" data-max="1500" placeholder="Max price"/>
                                <button type="button" class="button filter_price"
                                        onclick='document.querySelector("#filters-offcanvas [data-filter]").click();'>@lang('home.filter')</button>
                    </form>

                </aside>
                <aside
                        class="widget woocommerce woodstock_attributes_filter widget_layered_nav">
                    <h3 class="widget-title">@lang('home.filter_by_color')</h3>
                    <ul>
                        <li class="wc-layered-nav-term show-color @if(in_array('black',$colors) ) chosen @endif "
                            data-color="black"><a
                                    data-filter="filter_colors"
                                    data-value="black"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#000000;color:rgba(0,0,0,0.5);"
                                        title="@lang('home.black')"></span><span
                                        class="nav-title">@lang('home.black')</span></a> <span
                                    class="count">12</span></li>
                        <li class="wc-layered-nav-term show-color @if(in_array('brown',$colors) ) chosen @endif  "
                            data-color="brown"><a
                                    data-filter="filter_colors"
                                    data-value="brown"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#a52a2a;color:rgba(165, 42, 42, 0.5);"
                                        title="@lang('home.brown')"></span><span
                                        class="nav-title">@lang('home.brown')</span></a> <span
                                    class="count">12</span></li>
                        <li class="wc-layered-nav-term show-color @if(in_array('gold',$colors) ) chosen @endif "
                            data-color="gold"><a
                                    data-filter="filter_colors"
                                    data-value="gold"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#efbe37;color:rgba(239,190,55,0.5);"
                                        title="@lang('home.gold')"></span><span
                                        class="nav-title">@lang('home.gold')</span></a> <span
                                    class="count">4</span></li>
                        <li class="wc-layered-nav-term show-color @if(in_array('red',$colors) ) chosen @endif "
                            data-color="red"><a
                                    data-filter="filter_colors"
                                    data-value="red"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#dd3333;color:rgba(221,51,51,0.5);"
                                        title="@lang('home.red')"></span><span
                                        class="nav-title">@lang('home.red')</span></a> <span
                                    class="count">4</span></li>
                        <li class="wc-layered-nav-term show-color @if(in_array('silver',$colors) ) chosen @endif "
                            data-color="silver"><a
                                    data-filter="filter_colors"
                                    data-value="silver"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#cccccc;color:rgba(204,204,204,0.5);"
                                        title="@lang('home.silver')"></span><span
                                        class="nav-title">@lang('home.silver')</span></a> <span
                                    class="count">1</span></li>
                        <li class="wc-layered-nav-term show-color @if(in_array('white',$colors) ) chosen @endif "
                            data-color="white"><a
                                    data-filter="filter_colors"
                                    data-value="white"
                                    href=""><span
                                        class="swatch swatch-color"
                                        style="background-color:#ffffff;color:rgba(255,255,255,0.5);box-shadow: 0 0 0 1px #00000014;"
                                        title="@lang('home.white')"></span><span
                                        class="nav-title">@lang('home.white')</span></a> <span
                                    class="count">14</span></li>
                    </ul>
                </aside>
                <aside
                        class="widget woocommerce widget_layered_nav woocommerce-widget-layered-nav">
                    <h3 class="widget-title">@lang('home.mechanism')</h3>
                    <ul class="woocommerce-widget-layered-nav-list">
                        <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term ">
                            <a rel="nofollow"
                               data-filter="mech"
                               data-value="all"
                               href="">@lang('home.all')</a> <span
                                    class="count">({{$watches_count}})</span></li>
                        <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term ">
                            <a rel="nofollow"
                               data-filter="mech"
                               data-value="mechanical"
                               href="">@if(Request::get('mech')=='mechanical') <i
                                        class="fa fa-check"></i> @endif @lang('home.mechanical')</a> <span
                                    class="count">({{$mech_count}})</span></li>
                        <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term ">
                            <a
                                    rel="nofollow"
                                    href=""
                                    data-filter="mech"
                                    data-value="quartz"
                            >@if(Request::get('mech')=='quartz') <i class="fa fa-check"></i> @endif  @lang('home.automatic')
                            </a>
                            <span class="count">({{$watches_count - $mech_count}})</span></li>
                    </ul>
                </aside>
                <aside
                        class="widget woocommerce widget_layered_nav woocommerce-widget-layered-nav">
                    <h3 class="widget-title">@lang('home.on_sale')</h3>
                    <ul class="woocommerce-widget-layered-nav-list">
                        <li
                                class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term ">
                            <a rel="nofollow"
                               data-filter="on_sale"
                               data-value="on_sale"
                               href=""> @if(Request::get('on_sale')=='on_sale') <i
                                        class="fa fa-check"></i> @endif @lang('home.on_sale')</a> <span
                                    class="count">({{$on_sale_count}})</span></li>
                        <li
                                class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term ">
                            <a rel="nofollow"
                               data-filter="on_sale"
                               data-value="all"
                               href="">@lang('home.all')</a>
                            <span class="count">({{$watches_count}})</span></li>
                    </ul>
                </aside>
        </div>
            @endif

    </div>
</nav>

<nav class="off-menu slide-from-right scd-light">
    <div class="nano">
        <div class="nano-content">
            <div id="minicart-offcanvas" class="offcanvas-right-content"
                 data-empty-bag-txt="@lang('checkout.cart_is_empty')" data-singular-item-txt="@lang('header.items')"
                 data-multiple-item-txt="items">
                <div class="loading-overlay">
                    <div class="spinner spinner-circle"></div>
                </div>
                <div class="widget woocommerce widget_shopping_cart">
                    <h2 class="widgettitle">Cart</h2>
                    <div class="widget_shopping_cart_content"></div>
                </div>
            </div>
        </div>
    </div>
</nav>

<script>

const mobElements = document.querySelectorAll("#filters-offcanvas [data-filter]");
let mob_filter_colors_chosen = document.querySelectorAll("#filters-offcanvas .show-color.chosen");
let mob_selected_colors = '';
for (const el of mob_filter_colors_chosen) {
    mob_selected_colors += el.getAttribute('data-color') + ',';
}

for (const el of mobElements) {
    el.addEventListener('click', function (event) {
        let filter = el.getAttribute('data-filter');
        let value = el.getAttribute('data-value');

        let max_value = document.querySelector("#max_price_mob").value;
        let min_value = document.querySelector("#min_price_mob").value;

        if (filter === 'product_type') {
            full_url = updateQueryStringParameter(full_url, 'watch_type', '');
        }
        if (filter === 'watch_type') {
            full_url = updateQueryStringParameter(full_url, 'product_type', 'watches');
        }
        if (filter === 'filter_colors') {
            if (mob_selected_colors.match(value)) {

                mob_selected_colors = mob_selected_colors.replace(value, '');
                value = '';
            }
            value = mob_selected_colors + value;
        }
        full_url = updateQueryStringParameter(full_url, 'min_price', min_value);
        full_url = updateQueryStringParameter(full_url, 'max_price', max_value);

        el.setAttribute('href', updateQueryStringParameter(full_url, filter, value));
    })
}


</script>

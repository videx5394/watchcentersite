<div id="header-top-bar" class="td_dark tbd_light">

    <div class="row">

        <div class="large-6 columns topbar-menu">

            <nav id="left-site-navigation-top-bar" class="main-navigation">

                <ul id="menu-top-bar-navigation">
                    <li id="menu-item-2220"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2220">
                        <a href="{{ route('home') }}">@lang('header.home')</a></li>
                    <li id="menu-item-1295"
                        class="menu-item menu-item-type-custom menu-item-object-custom  menu-item-1295">
                        <a href="#">@lang('header.language')</a>
                        <ul class="sub-menu">
                            <li id="menu-item-1880"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1880">
                                <a href="{{ route('language',['lang'=> 'az']) }}">Azerbaycan</a></li>
                            <li id="menu-item-1880"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1880">
                                <a href="{{ route('language',['lang'=> 'ru']) }}">Русский</a></li>
                            <li id="menu-item-1879"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1879">
                                <a href="{{ route('language',['lang'=> 'en']) }}">English</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-1878"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1878">
                        <a href="{{ route('contactUs') }}">@lang('header.contact_us')</a></li>

                </ul>

            </nav><!-- #site-navigation -->

        </div><!-- .large-6 .columns -->

        <div class="large-6 columns topbar-right">

            <div class="topbar-social-icons-wrapper">
                <ul class="social-icons">
                    <li class="instagram"><a target="_blank" title="Instagram"
                                           href="https://www.instagram.com/watchcenter.az/?hl=en"></a></li>
                </ul>
            </div>


            <div class="language-and-currency">


            </div>
            <!--.language-and-currency-->


            <nav id="site-navigation-top-bar" class="main-navigation myacc-navigation">
                <ul id="my-account">
                    @if(auth()->check())
                        <li class="login-link"><a href="{{route('logout')}}" ><i class="logout-icon"></i>@lang('header.logout') </a>
                    @else
                        <li class="login-link"><a href="{{route('login')}}"
                                                  class="acc-link"><i class="login-icon"></i>@lang('header.login')
                                / @lang('header.register') </a>
                        </li>

                    @endif
                        <li class="wishlist-link"><a href="{{route('wishList')}}"
                                                     class="acc-link"><i class="wishlist-icon"></i>@lang('header.wish_list')
                            </a></li>
                </ul>
            </nav><!-- .myacc-navigation -->

        </div><!-- .large-6 .columns -->

    </div><!-- .row -->

</div><!-- #site-top-bar -->

<div class="header-main-section row" style="padding-bottom: 0;">
    <div class="large-12 columns">




        <div class="header-tools" style="width: 100%">
            <div class="l-logo">
                <a href="/" rel="home">
                    <img class="site-logo" src="{{asset('images').'/wclogo.png'}}" title="Watch Center" alt="Watch Center"/>
                </a>
                <div class="logo-word">Watch Center</div>
            </div><!-- .site-branding -->

            <script>
                //<![CDATA[

                // Set pixelRatio to 1 if the browser doesn't offer it up.
                var pixelRatio = !!window.devicePixelRatio ? window.devicePixelRatio : 1;

                logo_image = new Image();

                // jQuery(window).load(function () {
                //
                //     if (pixelRatio > 1) {
                //         jQuery('.site-logo').each(function () {
                //
                //             var logo_image_width = jQuery(this).width();
                //             var logo_image_height = jQuery(this).height();
                //
                //             jQuery(this).css("width", logo_image_width);
                //             jQuery(this).css("height", logo_image_height);
                //
                //             //jQuery(this).attr('src', 'wp-content/uploads/sites/4/2016/03/logo_retina.png');
                //         });
                //     }
                //     ;
                //
                // });

                //]]>
            </script>
            <ul style="float: right; transform: translate(0px, 55px);">

                <li class="mobile-menu-button mb-light"><a><i
                            class="mobile-menu-icon"></i><span
                            class="mobile-menu-text">Menu</span></a></li>

                <li class="mobile-search mb-light">
                    <a href="#" class="mobile-search-button"></a>
                </li>

                <li class="search-area">

                    <div class="l-search">


                        <div class="woodstock-search-form">


                            <form role="search" method="get"
                                  class="searchform  woodstock-ajax-search"
                                  action=""
                                  data-thumbnail="1" data-price="1" data-post_type="product"
                                  data-count="20">
                                <input type="text" class="s ajax-search-input"
                                       placeholder="@lang('header.search')" value="" name="s"/>
                                <input type="hidden" name="post_type" value="product">
                                <div class="ajax-loading spinner-circle">
                                    <div class="spinner"></div>
                                </div>
                                <button type="button" class="searchsubmit">
                                    Search
                                </button>
                            </form>
                            <div class="search-results-wrapper sd-light">
                                <div class="woodstock-scroll nano">
                                    <div
                                        class="woodstock-search-results woodstock-scroll-content nano-content">
                                    </div>
                                </div>
                                <div class="woodstock-search-loader"></div>
                            </div>

                        </div>


                    </div>
                </li>

                <li class="contact-area  hc-light  csd-light"  >
                    <!-- Contact Section -->

                    <div class="contact-info">
                        <div class="inside-content">
                            <span class="contact-info-icon"></span>

                            <span class="contact-info-title">

															<span
                                                                class="contact-info-subtitle">@lang('header.contact_us')</span>
															{{request()->contact_info['phone']}}
														</span>

                            <span class="contact-info-arrow"></span>

                            <div class="inside-area">
                                <div class="inside-area-content">

                                    <div class="contact-item">

                                        <span class="town">Baku</span>

                                        <span class="phone">{{request()->contact_info['phone']}}</span>

                                        <a
                                            href="mailto:{{request()->contact_info['email']}}">{{request()->contact_info['email']}}</a>

                                    </div>
                                    <div class="after-clear"></div>


                                    <hr/>
                                    <div class="after-clear"></div>


                                    <ul class="social-icons">

                                        <li class="instagram"><a href="https://www.instagram.com/watchcenter.az/?hl=en"
                                                                 rel="nofollow"
                                                                 target="_blank"
                                                                 title="Follow us on Instagram"></a></li>
                                        <li class="whatsapp"><a
                                                href="https://api.whatsapp.com/send?phone={{request()->contact_info['phone']}}&text=Salam.%20Saat%20sifariş%20vermək%20istəyirem,%20zəhmət%20olmasa%20köməklik%20edərdiz&source=&data="
                                                rel="nofollow"
                                                target="_blank"
                                                title="Follow us on WhatsApp"></a></li>

                                    </ul>
                                    <div class="after-clear"></div>

                                    <div class="after-clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Shop Section -->
                <li class="shop-bag shc-light"  >
                    <a>
                        <div class="l-header-shop">
                            <span class="shopbag_items_number">{{ request()->shopping_cart['count'] }}</span>
                            <i class="icon-shop"></i>
                            <div class="overview">
                                <span class="bag-items-number">{{ request()->shopping_cart['count'] }} @lang('header.items')</span>
                                <span class="woocommerce-Price-amount amount"><span
                                        class="woocommerce-Price-currencySymbol">₼</span>{{ request()->shopping_cart['price'] }}</span>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- Main Navigation -->


<div id="site-nav" style="position: initial;" class="l-nav h-nav mn-light  mnd-light">
    <div class="nav-container row">
        <nav id="nav" class="nav-holder">
            <ul class="navigation menu tdl-navbar-nav mega_menu">

                <li id="menu-item-2259"
                    class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">
                    <a href="{{ route('home') }}"><span class="menu-item-text"><span
                                class="menu-item-main-title">@lang('header.home')</span></span></a>
                </li>
                <li id="menu-item-2259"
                    class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">
                    <a href="{{ route('orders_not_in_stock') }}"><span class="menu-item-text"><span
                                class="menu-item-main-title">@lang('header.orders_not_in_stock')</span></span></a>
                </li>
                <li id="menu-item-2259"
                    class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">
                    <a href="{{ route('refund_policy') }}"><span class="menu-item-text"><span
                                class="menu-item-main-title">@lang('header.refund_policy')</span></span></a>
                </li>
{{--                <li id="menu-item-2259"--}}
{{--                    class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">--}}
{{--                    <a href="{{ route('about_us') }}"><span class="menu-item-text"><span--}}
{{--                                class="menu-item-main-title">@lang('header.about_us')</span></span></a>--}}
{{--                </li>--}}

            </ul>
        </nav>
    </div>
</div> <!-- End Main Navigation -->



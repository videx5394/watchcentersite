
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="og:description" content="Watch center" />
		<meta property="og:url" content="www.watchcenter.az" />
		<meta property="og:title" content="Watch center" />
		<meta property="og:type" content="website" />
		<meta property="og:site_name" content="Watch center" />
		<meta property="og:image" content="{{asset('images/wclogo.png')}}" />
		<meta property="og:image:type" content="image/jpeg" />
    <title>{{ config('app.name', 'WatchCenter') }}</title>

    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="///fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&text=%21%22%23%24%25%26%27%28%29%30+,-./0123456789:;%3C=%3E%3F@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`%E2%82%AC„‘’“”™©®°µ±÷abcdefghijklmnopqrstuvwxyz{|}~%C3%9C%C3%96%C4%9E%C4%B0%C6%8F%C3%87%C5%9E%C3%BC%C3%B6%C4%9F%C4%B1%C9%99%C3%A7%C5%9F" rel="stylesheet">
    <link rel="profile" href="/https://gmpg.org/xfn/11">
    <link rel="pingback" href="/xmlrpc.php">

    <script>
        function updateQueryStringParameter(uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            } else {
                return uri + separator + key + "=" + value;
            }
        }
        function roundTo(n, digits) {
            if (digits === undefined) {
                digits = 0;
            }

            var multiplicator = Math.pow(10, digits);
            n = parseFloat((n * multiplicator).toFixed(11));
            var test =(Math.round(n) / multiplicator);
            return +(test.toFixed(digits));
        }
        document.documentElement.className = document.documentElement.className + ' yes-js js_active js';
    </script>
    <title>Watch Center &#8211; Watch Center</title>
    <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
    <link rel='dns-prefetch' href='http://s.w.org/' />
    <link rel="alternate" type="application/rss+xml" title="Watch Center &raquo; Feed"
          href="/feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Watch Center &raquo; Comments Feed"
          href="/comments/feed/index.html" />
    <script type="text/javascript">
        window._wpemojiSettings = { "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/", "ext": ".png", "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/", "svgExt": ".svg", "source": { "concatemoji": "{{asset('js/wp-emoji-release.min.js')}}" } };
        !function (e, a, t) { var r, n, o, i, p = a.createElement("canvas"), s = p.getContext && p.getContext("2d"); function c(e, t) { var a = String.fromCharCode; s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, e), 0, 0); var r = p.toDataURL(); return s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, t), 0, 0), r === p.toDataURL() } function l(e) { if (!s || !s.fillText) return !1; switch (s.textBaseline = "top", s.font = "600 32px Arial", e) { case "flag": return !c([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) && (!c([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !c([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447])); case "emoji": return !c([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340]) }return !1 } function d(e) { var t = a.createElement("script"); t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t) } for (i = Array("flag", "emoji"), t.supports = { everything: !0, everythingExceptFlag: !0 }, o = 0; o < i.length; o++)t.supports[i[o]] = l(i[o]), t.supports.everything = t.supports.everything && t.supports[i[o]], "flag" !== i[o] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[i[o]]); t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function () { t.DOMReady = !0 }, t.supports.everything || (n = function () { t.readyCallback() }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function () { "complete" === a.readyState && t.readyCallback() })), (r = t.source || {}).concatemoji ? d(r.concatemoji) : r.wpemoji && r.twemoji && (d(r.twemoji), d(r.wpemoji))) }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='jckqv-minstyles-css'
          href='/wp-content/plugins/jck-woo-quickview/assets/frontend/css/main.min9dff.css' type='text/css'
          media='all' />
    <link rel='stylesheet' id='wp-block-library-css'
          href='/wp-includes/css/dist/block-library/style.min9dff.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-block-library-theme-css'
          href='/wp-includes/css/dist/block-library/theme.min9dff.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-style-css'
          href='/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style747d.css' type='text/css'
          media='all' />
    <link rel='stylesheet' id='jquery-selectBox-css'
          href='/wp-content/plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox7359.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='yith-wcwl-font-awesome-css'
          href='/wp-content/plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min1849.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='yith-wcwl-main-css'
          href='/wp-content/plugins/yith-woocommerce-wishlist/assets/css/style8d18.css' type='text/css'
          media='all' />
    <link rel='stylesheet' id='contact-form-7-css'
          href='/wp-content/plugins/contact-form-7/includes/css/stylesb62d.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css'
          href='/wp-content/plugins/revslider/public/assets/css/rs6b5e1.css' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        #rs-demo-id {}
    </style>
    <link rel='stylesheet' id='woocommerce-layout-css'
          href='/css/woocommerce-layout7e2e.css?ver=3.8.1' type='text/css'
          media='all' />
    <link rel='stylesheet' id='woocommerce-smallscreen-css'
          href='/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen7e2e.css' type='text/css'
          media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='woocommerce-general-css'
          href='/wp-content/plugins/woocommerce/assets/css/woocommerce7e2e.css' type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>
    <link rel='stylesheet' id='rs-particles-front-css'
          href='/wp-content/plugins/revslider-particles-addon/public/assets/css/revolution.addon.particles20b9.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='jquery-colorbox-css'
          href='/wp-content/plugins/yith-woocommerce-compare/assets/css/colorbox9dff.css' type='text/css'
          media='all' />
    <link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'
          href='/wp-content/plugins/woocommerce/assets/css/prettyPhoto9dff.css' type='text/css' media='all' />
    <link rel='stylesheet' id='tawcvs-frontend-css'
          href='/wp-content/plugins/variation-swatches-for-woocommerce/assets/css/frontendd0f1.css?ver=20160615'
          type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_front-css'
          href='/wp-content/plugins/js_composer/assets/css/js_composer.min9b2d.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ms-main-css'
          href='/wp-content/plugins/masterslider/public/assets/css/masterslider.main6b10.css?ver=3.2.14' type='text/css'
          media='all' />
    <link rel='stylesheet' id='ms-custom-css' href='/wp-content/uploads/sites/4/masterslider/customcf1b.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='tooltipster-css' href='/wp-content/themes/woodstock/css/tooltipster9b70.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='fresco-css' href='/wp-content/themes/woodstock/css/fresco/fresco6f3e.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='easyzoom-css' href='/wp-content/themes/woodstock/css/easyzoom5152.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='swiper-css' href='/wp-content/themes/woodstock/css/idangerous.swiper9776.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='nanoscroller-css' href='/wp-content/themes/woodstock/css/nanoscroller4297.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='select2-css' href='/wp-content/plugins/woocommerce/assets/css/select27e2e.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='wstock-app-css' href='/wp-content/themes/woodstock/css/app5152.css'
          type='text/css' media='all' />
    <link rel='stylesheet' id='stylesheet-css' href='/wp-content/themes/woodstock/style5152.css' type='text/css'
          media='all' />
    <!--[if IE]>
    <link rel='stylesheet' id='wstock-IE-css'  href="/css/ie.css" type='text/css' media='all' />
    <![endif]-->
    <!-- <link rel='stylesheet' id='redux-google-fonts-tdl_options-css'
          href='https://fonts.googleapis.com/css?family=Lato%3A100%2C300%2C400%2C700%2C900%2C100italic%2C300italic%2C400italic%2C700italic%2C900italic&amp;subset=latin%2Clatin-ext&amp;ver=1578691331'
          type='text/css' media='all' /> -->
    <script type='text/javascript' src='/wp-includes/js/jquery/jquery4a5f.js?ver=1.12.4-wp'></script>
    <script type='text/javascript' src='/wp-includes/js/jquery/jquery-migrate.min330a.js'></script>
    <script type='text/javascript' src='/wp-content/plugins/wp-retina-2x/js/picturefill.min5b75.js'></script>
    <script type='text/javascript'
            src='/wp-content/plugins/revslider/public/assets/js/revolution.tools.minf049.js'></script>
    <script type='text/javascript'
            src='/wp-content/plugins/revslider/public/assets/js/rs6.minb5e1.js'></script>
    <script type='text/javascript'
            src='/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = { "ajax_url": "\/electronics\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/add-to-card-ajax\/?wc-ajax=%%endpoint%%", "i18n_view_cart": "View cart", "cart_url": "{{route('view_cart')}}", "is_cart": "", "cart_redirect_after_add": "no" };
        window.$ = jQuery;
        /* ]]> */
    </script>
    <script type='text/javascript'
            src='/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min7e2e.js'></script>
    <script type='text/javascript'
            src='/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart9b2d.js'></script>
    <link rel='https://api.w.org/' href='/wp-json/index.html' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" />
    <link rel="canonical" href="/index.html" />
    <link rel='shortlink' href='/index.html' />
    <link rel="alternate" type="application/json+oembed"
          href="/wp-json/oembed/1.0/embed1037.json?url=https%3A%2F%2Fwoodstock.temashdesign.com%2Felectronics%2F" />
    <link rel="alternate" type="text/xml+oembed"
          href="/wp-json/oembed/1.0/embed7917?url=https%3A%2F%2Fwoodstock.temashdesign.com%2Felectronics%2F&amp;format=xml" />
    <script>var ms_grabbing_curosr = 'wp-content/plugins/masterslider/public/assets/css/common/grabbing.html', ms_grab_curosr = 'wp-content/plugins/masterslider/public/assets/css/common/grab.html';</script>
    <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
        </style>
    </noscript>
    <style type="text/css">
        .recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important;
        }
    </style>
    <style>
        /* QV Button */

        .jckqvBtn {
            display: table;

            float: left;
            margin: 0px 0px 10px 0px;
            padding: 8px 10px 8px 10px;
            background: #66cc99;
            color: #ffffff;

            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .jckqvBtn:hover {
            background: #47C285;
            color: #ffffff;
        }

        /* Magnific Specific */

        .mfp-bg {
            background: #000000;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=8)";
            filter: alpha(opacity=8);
            -moz-opacity: 0.8;
            -khtml-opacity: 0.8;
            opacity: 0.8;
        }
    </style>
    <script
        type="text/javascript">function setREVStartSize(t) { try { var h, e = document.getElementById(t.c).parentNode.offsetWidth; if (e = 0 === e || isNaN(e) ? window.innerWidth : e, t.tabw = void 0 === t.tabw ? 0 : parseInt(t.tabw), t.thumbw = void 0 === t.thumbw ? 0 : parseInt(t.thumbw), t.tabh = void 0 === t.tabh ? 0 : parseInt(t.tabh), t.thumbh = void 0 === t.thumbh ? 0 : parseInt(t.thumbh), t.tabhide = void 0 === t.tabhide ? 0 : parseInt(t.tabhide), t.thumbhide = void 0 === t.thumbhide ? 0 : parseInt(t.thumbhide), t.mh = void 0 === t.mh || "" == t.mh || "auto" === t.mh ? 0 : parseInt(t.mh, 0), "fullscreen" === t.layout || "fullscreen" === t.l) h = Math.max(t.mh, window.innerHeight); else { for (var i in t.gw = Array.isArray(t.gw) ? t.gw : [t.gw], t.rl) void 0 !== t.gw[i] && 0 !== t.gw[i] || (t.gw[i] = t.gw[i - 1]); for (var i in t.gh = void 0 === t.el || "" === t.el || Array.isArray(t.el) && 0 == t.el.length ? t.gh : t.el, t.gh = Array.isArray(t.gh) ? t.gh : [t.gh], t.rl) void 0 !== t.gh[i] && 0 !== t.gh[i] || (t.gh[i] = t.gh[i - 1]); var r, a = new Array(t.rl.length), n = 0; for (var i in t.tabw = t.tabhide >= e ? 0 : t.tabw, t.thumbw = t.thumbhide >= e ? 0 : t.thumbw, t.tabh = t.tabhide >= e ? 0 : t.tabh, t.thumbh = t.thumbhide >= e ? 0 : t.thumbh, t.rl) a[i] = t.rl[i] < window.innerWidth ? 0 : t.rl[i]; for (var i in r = a[0], a) r > a[i] && 0 < a[i] && (r = a[i], n = i); var d = e > t.gw[n] + t.tabw + t.thumbw ? 1 : (e - (t.tabw + t.thumbw)) / t.gw[n]; h = t.gh[n] * d + (t.tabh + t.thumbh) } void 0 === window.rs_init_css && (window.rs_init_css = document.head.appendChild(document.createElement("style"))), document.getElementById(t.c).height = h, window.rs_init_css.innerHTML += "#" + t.c + "_wrapper { height: " + h + "px }" } catch (t) { console.log("Failure at Presize of Slider:" + t) } };</script>
    <!-- ******************************************************************** -->
    <!-- Custom CSS Styles -->
    <!-- ******************************************************************** -->
    <style>
        /***************************************************************/
        /*  Content Width  *********************************************/
        /***************************************************************/
        .row {
            max-width: 92.857rem;
        }

        /***************************************************************/
        /*  Color Styling  *********************************************/
        /***************************************************************/
        /* Main Theme Color */
        .woocommerce a.button,
        .woocommerce-page a.button,
        .woocommerce button.button,
        .woocommerce-page button.button,
        .woocommerce input.button,
        .woocommerce-page input.button,
        .woocommerce #respond input#submit,
        .woocommerce-page #respond input#submit,
        .woocommerce #content input.button,
        .woocommerce-page #content input.button,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt,
        .woocommerce #respond input#submit.alt,
        .woocommerce #content input.button.alt,
        .woocommerce-page a.button.alt,
        .woocommerce-page button.button.alt,
        .woocommerce-page input.button.alt,
        .woocommerce-page #respond input#submit.alt,
        .woocommerce-page #content input.button.alt,
        .woocommerce #respond input#submit.alt.disabled,
        .woocommerce #respond input#submit.alt.disabled:hover,
        .woocommerce #respond input#submit.alt:disabled,
        .woocommerce #respond input#submit.alt:disabled:hover,
        .woocommerce #respond input#submit.alt[disabled]:disabled,
        .woocommerce #respond input#submit.alt[disabled]:disabled:hover,
        .woocommerce a.button.alt.disabled,
        .woocommerce a.button.alt.disabled:hover,
        .woocommerce a.button.alt:disabled,
        .woocommerce a.button.alt:disabled:hover,
        .woocommerce a.button.alt[disabled]:disabled,
        .woocommerce a.button.alt[disabled]:disabled:hover,
        .woocommerce button.button.alt.disabled,
        .woocommerce button.button.alt.disabled:hover,
        .woocommerce button.button.alt:disabled,
        .woocommerce button.button.alt:disabled:hover,
        .woocommerce button.button.alt[disabled]:disabled,
        .woocommerce button.button.alt[disabled]:disabled:hover,
        .woocommerce input.button.alt.disabled,
        .woocommerce input.button.alt.disabled:hover,
        .woocommerce input.button.alt:disabled,
        .woocommerce input.button.alt:disabled:hover,
        .woocommerce input.button.alt[disabled]:disabled,
        .woocommerce input.button.alt[disabled]:disabled:hover,
        input[type="button"],
        input[type="reset"],
        input[type="submit"],
        #minicart-offcanvas .widget_shopping_cart .buttons a.view_cart,
        .woocommerce #minicart-offcanvas .widget_shopping_cart .buttons a.view_cart,
        .select2-drop.orderby-drop .select2-results .select2-highlighted,
        .select2-drop.count-drop .select2-results .select2-highlighted,
        .select2-dropdown .select2-results .select2-results__option--highlighted[data-selected],
        .select2-container--default .select2-results__option--highlighted[aria-selected],
        .select2-container--default .select2-results__option--highlighted[data-selected],
        #button_offcanvas_sidebar_left,
        #button_offcanvas_sidebar_left i,
        .woocommerce .products a.button,
        .woocommerce-page .products a.button,
        .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
        .woocommerce .widget_price_filter .price_slider_amount .button,
        .woocommerce-page .widget_price_filter .price_slider_amount .button,
        .my_account_container table.shop_table tbody td.order-actions .account_view_link,
        .my_account_container .my_address_wrapper .shipping_billing_wrapper .edit-link a,
        .entry-meta .edit-link a,
        .widget_calendar tbody tr>td a,
        .vc_grid-container-wrapper .vc_grid .vc_btn3,
        .woocommerce .widget_layered_nav .woocommerce-widget-layered-nav-dropdown button,
        .woocommerce-page .widget_layered_nav .woocommerce-widget-layered-nav-dropdown button {
            background: #6990cb;
        }

        #jckqv .button {
            background: #6990cb !important;
        }

        .woocommerce .star-rating span:before,
        .woocommerce-page .star-rating span:before,
        #jckqv .woocommerce-product-rating .star-rating span::before,
        .arthref .icon-container .share-title h4,
        .woocommerce p.stars a:hover::before,
        .woocommerce p.stars.selected a:not(.active)::before,
        .woocommerce p.stars.selected a.active::before,
        .woocommerce p.stars:hover a::before,
        .woocommerce .widget_layered_nav ul li.chosen a::before,
        .woocommerce .widget_layered_nav_filters ul li a::before {
            color: #6990cb;
        }

        .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
        .woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle {
            border-color: #6990cb;
        }

        /* Links */
        a {
            color: #6990cb;
        }

        a:hover,
        a:focus {
            color: #79a6e9;
        }

        /* Main Color Hover */
        #minicart-offcanvas .widget_shopping_cart .buttons a.view_cart:hover,
        .woocommerce .products a.button:hover,
        .woocommerce-page .products a.button:hover,
        .woocommerce .widget_price_filter .price_slider_amount .button:hover,
        .woocommerce-page .widget_price_filter .price_slider_amount .button:hover,
        .woocommerce a.button:hover,
        .woocommerce-page a.button:hover,
        .woocommerce button.button:hover,
        .woocommerce-page button.button:hover,
        .woocommerce input.button:hover,
        .woocommerce-page input.button:hover,
        .woocommerce #respond input#submit:hover,
        .woocommerce-page #respond input#submit:hover,
        .woocommerce #content input.button:hover,
        .woocommerce-page #content input.button:hover,
        .woocommerce a.button.alt:hover,
        .woocommerce button.button.alt:hover,
        .woocommerce input.button.alt:hover,
        .woocommerce #respond input#submit.alt:hover,
        .woocommerce #content input.button.alt:hover,
        .woocommerce-page a.button.alt:hover,
        .woocommerce-page button.button.alt:hover,
        .woocommerce-page input.button.alt:hover,
        .woocommerce-page #respond input#submit.alt:hover,
        .woocommerce-page #content input.button.alt:hover,
        .my_account_container table.shop_table tbody td.order-actions .account_view_link:hover,
        .my_account_container .my_address_wrapper .shipping_billing_wrapper .edit-link a:hover,
        input[type="button"]:hover,
        input[type="reset"]:hover,
        input[type="submit"]:hover,
        .entry-meta .edit-link a:hover,
        .widget_calendar tbody tr>td a:hover,
        .vc_grid-container-wrapper .vc_grid .vc_btn3:hover,
        .woocommerce .widget_layered_nav .woocommerce-widget-layered-nav-dropdown button:hover,
        .woocommerce-page .widget_layered_nav .woocommerce-widget-layered-nav-dropdown button:hover {
            background-color: #79a6e9;
        }

        #jckqv .button:hover {
            background: #79a6e9 !important;
        }

        /* Content background */
        body,
        #page-wrap,
        #archive-categories .category-box,
        #products li.product-item figure.product-inner:hover,
        #content .widget_product_categories .product-categories li.cat-parent>a .child-indicator,
        .woocommerce #content .widget_price_filter .ui-slider .ui-slider-handle,
        .woocommerce-page #content .widget_price_filter .ui-slider .ui-slider-handle,
        .woocommerce .quantity .qty-plus,
        .woocommerce .quantity .qty-minus,
        .product_navigation .nav-fillslide div,
        .product_navigation .nav-fillslide .icon-wrap::before,
        #products li.product-item,
        #page-wrap.tdl-boxed .boxed-layout,
        .slide-from-right,
        .single-product-infos .variation-select select option {
            background-color: #ffffff;
        }

        .bordered::before,
        .bordered::after {
            background: -webkit-linear-gradient(45deg, rgba(0, 0, 0, 0.03) 0, rgba(0, 0, 0, 0.03) 25%, rgba(0, 0, 0, 0) 25%, rgba(0, 0, 0, 0) 100%), linear-gradient(-45deg, rgba(0, 0, 0, 0.03) 0, rgba(0, 0, 0, 0.03) 25%, rgba(0, 0, 0, 0) 25%, rgba(0, 0, 0, 0) 100%), #ffffff;
            background: -moz-linear-gradient(45deg, rgba(0, 0, 0, 0.03) 0, rgba(0, 0, 0, 0.03) 25%, rgba(0, 0, 0, 0) 25%, rgba(0, 0, 0, 0) 100%), linear-gradient(-45deg, rgba(0, 0, 0, 0.03) 0, rgba(0, 0, 0, 0.03) 25%, rgba(0, 0, 0, 0) 25%, rgba(0, 0, 0, 0) 100%), #ffffff;
            background: linear-gradient(45deg, rgba(0, 0, 0, 0.03) 0, rgba(0, 0, 0, 0.03) 25%, rgba(0, 0, 0, 0) 25%, rgba(0, 0, 0, 0) 100%), linear-gradient(-45deg, rgba(0, 0, 0, 0.03) 0, rgba(0, 0, 0, 0.03) 25%, rgba(0, 0, 0, 0) 25%, rgba(0, 0, 0, 0) 100%), #ffffff;
            background-position: 50% 50%;
            -webkit-background-size: 20px 20px;
            background-size: 20px 20px;
        }

        .mc-dark .bordered::before,
        .mc-dark .bordered::after {
            background: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.03) 0, rgba(255, 255, 255, 0.03) 25%, rgba(255, 255, 255, 0) 25%, rgba(255, 255, 255, 0) 100%), linear-gradient(-45deg, rgba(255, 255, 255, 0.03) 0, rgba(255, 255, 255, 0.03) 25%, rgba(255, 255, 255, 0) 25%, rgba(255, 255, 255, 0) 100%), #ffffff;
            background: -moz-linear-gradient(45deg, rgba(255, 255, 255, 0.03) 0, rgba(255, 255, 255, 0.03) 25%, rgba(255, 255, 255, 0) 25%, rgba(255, 255, 255, 0) 100%), linear-gradient(-45deg, rgba(255, 255, 255, 0.03) 0, rgba(255, 255, 255, 0.03) 25%, rgba(255, 255, 255, 0) 25%, rgba(255, 255, 255, 0) 100%), #ffffff;
            background: linear-gradient(45deg, rgba(255, 255, 255, 0.03) 0, rgba(255, 255, 255, 0.03) 25%, rgba(255, 255, 255, 0) 25%, rgba(255, 255, 255, 0) 100%), linear-gradient(-45deg, rgba(255, 255, 255, 0.03) 0, rgba(255, 255, 255, 0.03) 25%, rgba(255, 255, 255, 0) 25%, rgba(255, 255, 255, 0) 100%), #ffffff;
            background-position: 50% 50%;
            -webkit-background-size: 20px 20px;
            background-size: 20px 20px;
        }

        #products li.product-item:hover,
        #content .widget_product_categories .product-categories li {
            border-color: #ffffff;
        }

        .product-item:not(.product_hover_disable):hover .product_after_shop_loop {
            border-top-color: #ffffff;
        }

        #products li.product-item.product_hover_disable:hover {
            border-color: #f5f5f5;
        }

        .product-item.product_hover_disable:hover .product_after_shop_loop,
        .product-item.display_buttons:hover .product_after_shop_loop {
            border-top-color: #f5f5f5;
        }

        /*@media only screen and (min-width: 40em) and (max-width: 61.94em) {*/
        /*  #products li.product-item:hover,#content .widget_product_categories .product-categories li {border-color: #f5f5f5;}.product-item:hover .product_after_shop_loop {border-top-color: #f5f5f5;}*/
        /*}*/
        #products li.product-item {
            margin-right: -1px;
            margin-bottom: -1px;
            border: 1px solid #f5f5f5;
        }

        /* Top Bar Colors */
        .main-navigation ul ul,
        .main-navigation ul ul li:first-child ul {
            /*    border-top: 1px solid rgba(255,255,255,0.05);*/
        }

        #header-top-bar {
            background: rgba(51, 51, 51, 1);
        }

        #header-top-bar {
            border-bottom: 0px solid rgba(51, 51, 51, 1);
        }

        /* Top Bar Dropdown Background Color */
        #header-top-bar .main-navigation ul ul,
        .select2-drop.topbar,
        .select2-drop.topbar .select2-results,
        .select2-dropdown.topbar,
        .select2-dropdown.topbar .select2-results,
        .topbar-right .wcml-dropdown-click .wcml-cs-submenu,
        .topbar-right .language-and-currency .wcml-dropdown .wcml-cs-submenu {
            background: rgba(255, 255, 255, 1) !important;
        }

        /***************************************************************/
        /*  Header Colors  *********************************************/
        /***************************************************************/
        /* Header Styling */
        .l-header {
            background-color: #ffffff;
        }

        /* Search Styling */
        .l-search .woodstock-search-form form input[type=text] {
            background-color: rgba(245, 245, 245, 1);
        }

        /* Search Box Ajax DropDown Background Color */
        .ajax-search-results,
        .l-search .woodstock-search-form .search-results-wrapper .woodstock-scroll {
            background-color: rgba(255, 255, 255, 1);
        }

        .l-search .widget_product_search input.search-field,
        .l-search .widget_search input.search-field,
        .l-search .woodstock-search-form form input.ajax-search-input {
            border-left: 0px solid rgba(245, 245, 245, 1);
            border-right: 0px solid rgba(245, 245, 245, 1);
            border-top: 0px solid rgba(245, 245, 245, 1);
            border-bottom: 0px solid rgba(245, 245, 245, 1);
        }

        .l-search .woodstock-search-form form input.ajax-search-input::-webkit-input-placeholder {
            color: #000000;
        }

        .l-search .woodstock-search-form form input.ajax-search-input {
            color: #000000;
        }

        /* Customer Support Styling */
        .contact-info {
            background-color: rgba(245, 245, 245, 1);
            border-left: 0px solid rgba(245, 245, 245, 1);
            border-right: 0px solid rgba(245, 245, 245, 1);
            border-top: 0px solid rgba(245, 245, 245, 1);
            border-bottom: 0px solid rgba(245, 245, 245, 1);
        }

        /* Customer Support DropDown Background Color */
        .contact-info .inside-area .inside-area-content {
            background-color: rgba(255, 255, 255, 1);
        }

        /* Mobile Menu Button Styling */
        .mobile-menu-button a,
        .mobile-search .mobile-search-button {
            background-color: rgba(245, 245, 245, 1);
            border-left: 0px solid rgba(245, 245, 245, 1);
            border-right: 0px solid rgba(245, 245, 245, 1);
            border-top: 0px solid rgba(245, 245, 245, 1);
            border-bottom: 0px solid rgba(245, 245, 245, 1);
        }

        /* Main Menu Styling */
        .l-nav {
            background-color: rgba(255, 255, 255, 1);
            border-top: 1px solid rgba(245, 245, 245, 1);
            border-bottom: 1px solid rgba(245, 245, 245, 1);
        }

        nav#nav ul ul.sub-menu,
        #page_header_wrap .tdl-megamenu-wrapper {
            background-color: rgba(255, 255, 255, 1);
        }

        /* Stocky Header Styling */
        #header-st,
        #header-st.sticky-header-not-top {
            background-color: rgba(255, 255, 255, 1);
        }

        /* Sticky Header Menu Styling */
        #header-st nav#st-nav ul ul.sub-menu,
        #header-st .tdl-megamenu-wrapper {
            background-color: rgba(255, 255, 255, 1);
        }

        #header-st .tdl-megamenu-wrapper .sub-menu {
            background-color: transparent !important;
        }

        /*  Default Main Title Area Styling  */
        .site_header.without_featured_img {
            background-color: #f5f5f5;
        }

        .blog-content-area .site_header.without_featured_img {
            background-color: #f5f5f5;
        }

        .shop-page .site_header.without_featured_img,
        .site_header.woo-pages.without_featured_img {
            background-color: #f5f5f5;
        }

        /***************************************************************/
        /*  Footer Colors  *********************************************/
        /***************************************************************/
        footer#site-footer {
            background-color: #333333;
        }

        footer#site-footer .f-copyright {
            background-color: #2f2f2f;
        }

        /***************************************************************/
        /*  Fonts  *****************************************************/
        /***************************************************************/
        .woocommerce a.button,
        .woocommerce-page a.button,
        .woocommerce button.button,
        .woocommerce-page button.button,
        .woocommerce input.button,
        .woocommerce-page input.button,
        .woocommerce #respond input#submit,
        .woocommerce-page #respond input#submit,
        .woocommerce #content input.button,
        .woocommerce-page #content input.button,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt,
        .woocommerce #respond input#submit.alt,
        .woocommerce #content input.button.alt,
        .woocommerce-page a.button.alt,
        .woocommerce-page button.button.alt,
        .woocommerce-page input.button.alt,
        .woocommerce-page #respond input#submit.alt,
        .woocommerce-page #content input.button.alt,
        .ajax-search-results .all-results {
            font-family: 'Roboto Condensed';
        }

        /***************************************************************/
        /*  Header *****************************************************/
        /***************************************************************/
        .header-main-section .l-logo {
            height: auto;
            border: 0;
            padding: 0;
        }

        .header-main-section .header-tools,
        .header-centered .search-area {
            padding-top: 10px;
        }

        .header-main-section .l-logo img {
            width: 240px;
            height: 175px;
            position: relative;
            top: -15px;
        }

        .header-main-section {
            padding-top: 30px;
            padding-bottom: 30px;
        }

        /***************************************************************/
        /*  Page Loader Colors *****************************************/
        /***************************************************************/
        #wstock-loader-wrapper {
            background: #ffffff;
        }

        .wstock-loader-1 {
            background-color: #9395a4;
        }

        .wstock-loader-2 {
            border-top: 0.3em solid rgba(147, 149, 164, .3);
            border-right: 0.3em solid rgba(147, 149, 164, .3);
            border-bottom: 0.3em solid rgba(147, 149, 164, .3);
            border-left: 0.3em solid #9395a4;
        }

        .wstock-loader-3 {
            border-top-color: #9395a4;
        }

        .wstock-loader-3:before {
            border-top-color: #9395a4;
            opacity: 0.5;
        }

        .wstock-loader-3:after {
            border-top-color: #9395a4;
            opacity: 0.2;
        }

        .wstock-loader-4 {
            border: 3px solid #9395a4;
        }

        .wstock-loader-4:before,
        .wstock-loader-4:after {
            background-color: #9395a4;
        }

        /***************************************************************/
        /*  Sticky Header ***********************************************/
        /***************************************************************/
        #header-st {
            -webkit-animation-duration: 0.3s;
            -moz-animation-duration: 0.3s;
            -o-animation-duration: 0.3s;
            animation-duration: 0.3s;
            -webkit-animation-fill-mode: both;
            -moz-animation-fill-mode: both;
            -o-animation-fill-mode: both;
            animation-fill-mode: both;
        }

        @media only screen and (max-width: 74.94em) {
            #header-st.sticky-header-not-top {
                display: none;
            }
        }

        /***************************************************************/
        /*  Custom Icons ***********************************************/
        /***************************************************************/
        /*  Search Icon  */
        .l-search button.searchsubmit:after,
        .woocommerce-product-search:after,
        .widget_search .search-form:after,
        .l-search .woodstock-search-form form.woodstock-ajax-search .searchsubmit:after,
        .l-search .woodstock-search-form form .searchsubmit:after,
        .mobile-search .mobile-search-button:before {
            content: "\e604";
            color: #000000;
        }

        /*  Spinners Color  */
        .l-search .ajax-loading.spinner-bounce .spinner,
        .l-search .ajax-loading.spinner-bounce .spinner:before,
        .l-search .ajax-loading.spinner-bounce .spinner:after {
            background-color: #000000;
        }

        .l-search .ajax-loading.spinner-circle .spinner {
            border-color: #000000;
            border-right-color: transparent;
        }

        .l-search .ajax-loading.spinner-dots .spinner:after {
            background: rgba(0, 0, 0, 0.5);
            box-shadow: -13px 0 0 0 #000000, 13px 0 0 0 #000000;
            animation: alter 1s ease-in-out infinite;
        }

        @keyframes alter {

            0%,
            100% {
                background-color: rgba(0, 0, 0, 0.5);
                box-shadow: -13px 0 0 0 #000000, 13px 0 0 0 #000000;
            }

            50% {
                background-color: rgba(0, 0, 0, 0.5);
                box-shadow: 0 0 0 0 #000000, 0 0 0 0 #000000;
            }
        }

        /*  Search Custom Icon  */
        /*  Customer Support Icon  */
        .contact-info .contact-info-icon::after {
            content: "\e602";
            color: #000000;
        }

        /*  Shopping Cart Icon  */
        .l-header-shop .icon-shop::before {
            content: "\e600";
            color: #000000;
        }

        .l-header-shop .shopbag_items_number {
            color: #000000;
            border-color: #000000;
            background-color: #ffffff;
        }

        .l-header-shop:hover .shopbag_items_number {
            color: #ffffff;
            background-color: #000000;
        }

        /*  Sticky Header Shopping Cart Icon  */
        #header-st .l-header-shop .shopbag_items_number {
            background: #ffffff;
        }

        #header-st .l-header-shop:hover .shopbag_items_number {
            color: #ffffff;
        }

        /*========== Custom CSS ==========*/
    </style>
    <style type="text/css" title="dynamic-css" class="options-output">
        .header-main-section .l-logo .logo h1 {
            font-family: 'Roboto Condensed';
            line-height: 40px;
            letter-spacing: 0px;
            font-weight: 700;
            font-style: normal;
            color: #333333;
            font-size: 40px;
        }

        .header-main-section .l-logo small {
            font-family: 'Roboto Condensed';
            line-height: 14px;
            letter-spacing: 0px;
            font-weight: 300;
            font-style: normal;
            color: #666666;
            font-size: 14px;
        }

        body,
        p,
        .contact-info .contact-info-title .contact-info-subtitle,
        nav#nav ul ul li a,
        nav#st-nav ul ul li a,
        .ajax_autosuggest_item_description,
        input[type="search"],
        .tooltipster-default .tooltipster-content,
        .arthref .icon-container ul li span,
        .blog-list-comment i span {
            font-family: 'Roboto Condensed';
            line-height: 26px;
            letter-spacing: 0px;
            font-weight: 300;
            font-style: normal;
            font-size: 16px;
        }

        h1,
        #jckqv h1 {
            font-family: 'Roboto Condensed';
            line-height: 50px;
            letter-spacing: 0px;
            font-weight: 700;
            font-style: normal;
            font-size: 36px;
        }

        h2,
        .widget_shopping_cart .total .amount,
        .account-tab-link {
            font-family: 'Roboto Condensed';
            line-height: 42px;
            letter-spacing: 0px;
            font-weight: 700;
            font-style: normal;
            font-size: 30px;
        }

        h3,
        .contact-info .contact-info-title,
        .contact-info .inside-area .inside-area-content span.phone,
        .mobile-menu-button a span,
        #mobiles-menu-offcanvas .mobile-menu-text {
            font-family: 'Roboto Condensed';
            line-height: 34px;
            letter-spacing: 0px;
            font-weight: 700;
            font-style: normal;
            font-size: 24px;
        }

        h4,
        .ajax_autosuggest_suggestions .ajax_autosuggest_category,
        #minicart-offcanvas .widget .widget_shopping_cart_content .product-name a,
        .woocommerce div.product .woocommerce-tabs ul.tabs li a,
        .woocommerce #content div.product .woocommerce-tabs ul.tabs li a,
        .woocommerce-page div.product .woocommerce-tabs ul.tabs li a,
        .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li a,
        .shop_sidebar ul.product_list_widget li a .product-title,
        .woocommerce table.shop_table th,
        .woocommerce-page table.shop_table th,
        .cart-collaterals .shipping-calculator-button {
            font-family: 'Roboto Condensed';
            line-height: 25px;
            letter-spacing: 0px;
            font-weight: 700;
            font-style: normal;
            font-size: 18px;
        }

        h5,
        .ajax_autosuggest_suggestions li span.searchheading,
        .l-header-shop span.amount {
            font-family: 'Roboto Condensed';
            line-height: 25px;
            letter-spacing: 0px;
            font-weight: 700;
            font-style: normal;
            font-size: 18px;
        }

        h6 {
            font-family: 'Roboto Condensed';
            line-height: 17px;
            letter-spacing: 3px;
            font-weight: 300;
            font-style: normal;
            font-size: 12px;
        }

        nav#nav ul li>a,
        nav#st-nav ul li>a,
        #page_header_wrap .tdl-megamenu-wrapper .tdl-megamenu-title,
        #page_header_wrap .tdl-megamenu-wrapper .tdl-megamenu-title a,
        .mobile-navigation a,
        .mob-language-and-currency .select2-chosen {
            font-family: 'Roboto Condensed';
            text-transform: uppercase;
            line-height: 26px;
            letter-spacing: 0px;
            font-weight: 700;
            font-style: normal;
            font-size: 17px;
        }
    </style>
    <style type="text/css" data-type="vc_shortcodes-custom-css">
        .vc_custom_1455536398215 {
            padding-top: 10px !important;
            padding-bottom: 70px !important;
            background-color: #f5f5f5 !important;
        }

        .vc_custom_1455479961033 {
            margin-top: 50px !important;
            margin-bottom: 30px !important;
        }

        .vc_custom_1457817253445 {
            padding-right: 20% !important;
            padding-left: 20% !important;
            background-image: url(wp-content/uploads/sites/4/2015/06/home-block-bg-202c0.jpg?id=2225) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }

        .vc_custom_1455477515175 {
            padding-top: 0px !important;
            padding-right: 10% !important;
            padding-bottom: 30px !important;
            padding-left: 10% !important;
        }

        .vc_custom_1456691369411 {
            margin-top: -30px !important;
            margin-bottom: 30px !important;
            margin-left: 10px !important;
        }

        .vc_custom_1457817288059 {
            margin-top: 40px !important;
            margin-left: 2px !important;
        }


        .vc_custom_1457817759035 {
            margin-top: -15px !important;
        }

        .vc_custom_1457817775052 {
            margin-top: -20px !important;
        }

        .vc_custom_1457817790284 {
            margin-top: -20px !important;
        }

        .vc_custom_1455477551962 {
            padding-top: 30px !important;
            padding-right: 10% !important;
            padding-bottom: 0px !important;
            padding-left: 10% !important;
        }


        .vc_custom_1456733451382 {
            margin-top: -30px !important;
            margin-right: 10px !important;
            margin-bottom: 30px !important;
        }

        .vc_custom_1457817325707 {
            margin-top: 40px !important;
            margin-right: 4px !important;
        }
    </style><noscript>
        <style>
            .wpb_animate_when_almost_visible {
                opacity: 1;
            }
        </style>
    </noscript>

    <script type='text/javascript' src='/wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript' src='/wp-includes/js/jquery/ui/widget.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript' src='/wp-includes/js/jquery/ui/button.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript' src='/wp-includes/js/jquery/ui/spinner.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript' src='/wp-includes/js/jquery/ui/effect.mine899.js?ver=1.11.4'></script>
    <script type='text/javascript' src='/wp-includes/js/underscore.min4511.js?ver=1.8.3'></script>

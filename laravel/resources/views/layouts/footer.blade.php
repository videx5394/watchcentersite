<style>
    .cards img
    {
        position: relative;
        top: -8px;
    }
    .cards i
    {
        font-size: 35px;
    }
    .cards .cash
    {
        font-size: 42px !important;
        position: relative;
        top: 3px;
    }
</style>
<div class="f-copyright">
    <div class="row">
        <div class="large-6 columns copytxt">
            <p>Copyright © Watch Center Baku {{date('Y')}}.</p>
        </div>
        <div class="large-6 columns cards" style="display: none;">
            <img class="birkart" alt="birkart" src="{{asset('images/birkart.png')}}" />
            <i title="visa" class="fa fa-cc-visa" aria-hidden="true"></i>
            <i title="mastercard" class="fa fa-cc-mastercard" aria-hidden="true"></i>
            <i title="cash" class="fa fa-money cash" aria-hidden="true"></i>
        </div>
    </div>
</div>

<script>
    window.onload = function (e) {
        document.querySelector('.birkart').setAttribute('title', 'birkart');
    };
</script>

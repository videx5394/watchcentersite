<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.head')
</head>
<body data-rsssl=1
      class="archive tax-product_cat term-smartphones term-170 wp-embed-responsive theme-woodstock _masterslider _msp_version_3.2.14 woocommerce woocommerce-page woocommerce-js wpb-js-composer js-comp-ver-6.1 vc_responsive loaded">

<div id="wstock-loader-wrapper">
    <div class="wstock-loader-section">
        <div class="wstock-loader-3"></div>
    </div>
</div>

<div id="off-container" class="off-container">
    <div class="off-drop">
        <div class="off-drop-after"></div>
        <div class="off-content">


            <div id="page-wrap" class="fullwidth	mc-light">

                <div class="boxed-layout sd-light snd-light">


                    <header id="page_header_wrap" class="l-header header-default">
                        @include('layouts.header')
                    </header>

                    <div id="primary" class="content-area" style="">



                        <div id="content" class="site-content" role="main" style="">
                            @yield('content')

                        </div><!-- #content -->

                    </div><!-- #primary -->


                    <footer id="site-footer" class="fc-dark active">

                        @include('layouts.footer')

                    </footer>

                    <!-- ******************************************************************** -->
                    <!-- * Sticky Header **************************************************** -->
                    <!-- ******************************************************************** -->
                    <header id="header-st">
                        @include('layouts.stickyHeader')
                    </header>


                </div><!-- /boxed-layout -->
            </div><!-- /page-wrap -->

        </div><!-- /off-content -->
    </div><!-- /off-drop -->

    @include('layouts.mobNavigation')

</div><!-- /off-container -->

<!-- ******************************************************************** -->
<!-- * Custom Footer JavaScript Code ************************************ -->
<!-- ******************************************************************** -->


<script>(function () {
        function addEventListener(element, event, handler) {
            if (element.addEventListener) {
                element.addEventListener(event, handler, false);
            } else if (element.attachEvent) {
                element.attachEvent('on' + event, handler);
            }
        }
        function maybePrefixUrlField() {
            if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
                this.value = "http://" + this.value;
            }
        }

        var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
        if (urlFields && urlFields.length > 0) {
            for (var j = 0; j < urlFields.length; j++) {
                addEventListener(urlFields[j], 'blur', maybePrefixUrlField);
            }
        }
        /* test if browser supports date fields */
        var testInput = document.createElement('input');
        testInput.setAttribute('type', 'date');
        if (testInput.type !== 'date') {

            /* add placeholder & pattern to all date fields */
            var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
            for (var i = 0; i < dateFields.length; i++) {
                if (!dateFields[i].placeholder) {
                    dateFields[i].placeholder = 'YYYY-MM-DD';
                }
                if (!dateFields[i].pattern) {
                    dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
                }
            }
        }
    })();</script>
<script type="text/javascript">
    var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;

    function addLoading(element) {
        element.fadeTo("400", "0.6").block({
            message: null,
            overlayCSS: {
                backgroundSize: "40px 40px",
                opacity: 1
            }
        });
    }


    function removeLoading(element) {
        element.stop(!0).css("opacity", "1").unblock();
    }

</script>
<!-- <link rel='stylesheet' id='vc_google_fonts_lato100100italic300300italicregularitalic700700italic900900italic-css'
      href='https://fonts.googleapis.com/css?family=Lato%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic&amp;ver=6.1'
      type='text/css' media='all' /> -->
<link rel='stylesheet' id='vc_animate-css-css'
      href='/wp-content/plugins/js_composer/assets/lib/bower/animate-css/animate.min9b2d.css?ver=6.1' type='text/css'
      media='all' />

<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpUtilSettings = { "ajax": { "url": "{{route('add_to_wish_list_ajax')}}" } };
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-includes/js/wp-util.min9dff.js?ver=5.3.2'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var jckqv_vars = { "ajaxurl": "\/electronics\/wp-admin\/admin-ajax.php", "nonce": "a7cd30dfdf", "settings": { "trigger_general_method": "click", "trigger_position_autoinsert": "1", "trigger_position_position": "beforetitle", "trigger_position_align": "left", "trigger_position_margins": ["0", "0", "10", "0"], "trigger_styling_autohide": "0", "trigger_styling_hoverel": ".product", "trigger_styling_icon": "eye", "trigger_styling_text": "Quickview", "trigger_styling_btnstyle": "flat", "trigger_styling_padding": ["8", "10", "8", "10"], "trigger_styling_btncolour": "#66cc99", "trigger_styling_btnhovcolour": "#47C285", "trigger_styling_btntextcolour": "#ffffff", "trigger_styling_btntexthovcolour": "#ffffff", "trigger_styling_borderradius": ["4", "4", "4", "4"], "popup_general_gallery": "1", "popup_general_overlaycolour": "#000000", "popup_general_overlayopacity": "0.8", "popup_imagery_imgtransition": "horizontal", "popup_imagery_transitionspeed": "600", "popup_imagery_autoplay": "0", "popup_imagery_autoplayspeed": "3000", "popup_imagery_infinite": "1", "popup_imagery_navarr": "1", "popup_imagery_thumbnails": "thumbnails", "popup_content_showtitle": "1", "popup_content_showprice": "1", "popup_content_showrating": "1", "popup_content_showbanner": "1", "popup_content_showdesc": "short", "popup_content_showatc": "1", "popup_content_ajaxcart": "0", "popup_content_autohidepopup": "1", "popup_content_showqty": "1", "popup_content_showmeta": "1", "popup_content_themebtn": "0", "popup_content_btncolour": "#66cc99", "popup_content_btnhovcolour": "#47C285", "popup_content_btntextcolour": "#ffffff", "popup_content_btntexthovcolour": "#ffffff" }, "imgsizes": { "catalog": { "width": "350", "height": "380", "crop": 1 }, "single": { "width": "570", "height": "619", "crop": 1 }, "thumbnail": { "width": "80", "height": "80", "crop": 1 } }, "url": "https:\/\/woodstock.temashdesign.com\/electronics", "text": { "added": "Added!", "adding": "Adding to Cart...", "loading": "Loading..." }, "rtl": "" };
    /* ]]> */
</script>
<script type='text/javascript'
        src='/wp-content/plugins/jck-woo-quickview/assets/frontend/js/main9dff.js?ver=5.3.2'></script>
<script type='text/javascript' src='/wp-includes/js/comment-reply.min9dff.js?ver=5.3.2'></script>
<script type='text/javascript'
        src='/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min7359.js?ver=1.2.0'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var yith_wcwl_l10n = { "remove_ajax_url": "{{route('remove_from_wish_list')}}","ajax_url": "{{route('add_to_wish_list_ajax')}}",'success_message': "@lang('home.product_added_to_wish_list')", "redirect_to_cart": "no", "multi_wishlist": "", "hide_add_button": "1", "enable_ajax_loading": "", "ajax_loader_url": "https:\/\/woodstock.temashdesign.com\/electronics\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader-alt.svg", "remove_from_wishlist_after_add_to_cart": "1", "labels": { "cookie_disabled": "We are sorry, but this feature is available only if cookies on your browser are enabled.", "added_to_cart_message": "<div class=\"woocommerce-notices-wrapper\"><div class=\"woocommerce-message\" role=\"alert\">@lang('home.product_added_to_wish_list')<\/div><\/div>" }, "actions": { "add_to_wishlist_action": "add_to_wishlist", "remove_from_wishlist_action": "remove_from_wishlist", "reload_wishlist_and_adding_elem_action": "reload_wishlist_and_adding_elem", "load_mobile_action": "load_mobile", "delete_item_action": "delete_item", "load_fragments": "load_fragments" } };
    /* ]]> */
</script>
<script type='text/javascript'
        src='/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl8d18.js?ver=3.0.5'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = { "apiSettings": { "root": "https:\/\/woodstock.temashdesign.com\/electronics\/wp-json\/contact-form-7\/v1", "namespace": "contact-form-7\/v1" }, "cached": "1" };
    /* ]]> */
</script>
<script type='text/javascript'
        src='/wp-content/plugins/contact-form-7/includes/js/scriptsb62d.js?ver=5.1.6'></script>
<script type='text/javascript'
        src='/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var woocommerce_params = { "ajax_url": "\/electronics\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/electronics\/?wc-ajax=%%endpoint%%" };
    /* ]]> */
</script>
<script type='text/javascript'
        src='/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min7e2e.js?ver=3.8.1'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = { "ajax_url": "\/electronics\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/electronics\/?wc-ajax=%%endpoint%%", "cart_hash_key": "wc_cart_hash_fe3642afebec2a66167fb82ae5fb4bbd", "fragment_name": "wc_fragments_fe3642afebec2a66167fb82ae5fb4bbd", "request_timeout": "5000" };
    /* ]]> */
</script>
{{--<script type='text/javascript'--}}
{{--        src='/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min7e2e.js?ver=3.8.1'></script>--}}
<script type='text/javascript'
        src='/wp-content/plugins/revslider-particles-addon/public/assets/js/revolution.addon.particles.min20b9.js?ver=1.0.2'></script>
<script type='text/javascript'
        src='/wp-content/plugins/revslider-slicey-addon/public/assets/js/revolution.addon.slicey.min8a54.js?ver=1.0.0'></script>
<script type='text/javascript'
        src='/wp-content/plugins/revslider-snow-addon/public/assets/js/revolution.addon.snow.min4bf4.js?ver=1.0.3'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var yith_woocompare = { "ajaxurl": "\/electronics\/?wc-ajax=%%endpoint%%", "actionadd": "yith-woocompare-add-product", "actionremove": "yith-woocompare-remove-product", "actionview": "yith-woocompare-view-table", "actionreload": "yith-woocompare-reload-product", "added_label": "Added", "table_title": "Product Comparison", "auto_open": "yes", "loader": "https:\/\/woodstock.temashdesign.com\/electronics\/wp-content\/plugins\/yith-woocommerce-compare\/assets\/images\/loader.gif", "button_text": "Compare", "cookie_name": "yith_woocompare_list_4", "close_label": "Close" };
    /* ]]> */
</script>
<script type='text/javascript'
        src='/wp-content/plugins/yith-woocommerce-compare/assets/js/woocompare.min7ebe.js?ver=2.3.18'></script>
<script type='text/javascript'
        src='/wp-content/plugins/yith-woocommerce-compare/assets/js/jquery.colorbox-min13ac.js?ver=1.4.21'></script>
<script type='text/javascript'
        src='/wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min005e.js?ver=3.1.6'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var woodstock_scripts_vars = { "ajaxurl": "findProduct", "live_search_empty_msg":  "@lang('home.live_search_empty_msg')", "all_results":  "@lang('home.all_results')", "mapApiKey": "AIzaSyDGeoKldACxhsXuQ6IN5W5fYGfFTll1coU" };
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/themes/woodstock/js/wstock-plugins4e44.js?ver=1.3'></script>
<script type='text/javascript'
        src='/wp-content/themes/woodstock/js/jquery.autocomplete.min8759.js?ver=1.9.9.8'></script>
<script type='text/javascript' src='/wp-content/themes/woodstock/js/wstock-custom.scriptsad05.js?ver=1.4'></script>
<script type='text/javascript'
        src='/wp-content/plugins/variation-swatches-for-woocommerce/assets/js/frontendd0f1.js?ver=20160615'></script>
<script type='text/javascript' src='/wp-includes/js/wp-embed.min9dff.js?ver=5.3.2'></script>
<script type='text/javascript'
        src='/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min9b2d.js?ver=6.1'></script>
<script type='text/javascript'
        src='/wp-content/plugins/masterslider/public/assets/js/jquery.easing.min6b10.js?ver=3.2.14'></script>
<script type='text/javascript'
        src='/wp-content/plugins/masterslider/public/assets/js/masterslider.min6b10.js?ver=3.2.14'></script>
<script type='text/javascript'
        src='/wp-content/plugins/js_composer/assets/lib/vc_waypoints/vc-waypoints.min9b2d.js?ver=6.1'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mc4wp_forms_config = [];
    /* ]]> */
</script>
{{--<script type='text/javascript'--}}
{{--        src='/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min125b.js?ver=4.7.4'></script>--}}
<!--[if lte IE 9]>
<script type='text/javascript' src='https://woodstock.temashdesign.com/electronics/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=4.7.4'></script>
<![endif]-->


</body>

</html>


<div class="row sth-light shd-light">
    <div class="large-12 columns">

        <div class="mobile-menu-button"><a><i class="mobile-menu-icon"></i><span
                    class="mobile-menu-text">Menu</span></a></div>

        <div id="sticky-site-nav" class="l-nav h-nav">
            <div class="nav-container row">
                <nav id="nav" class="nav-holder">
                    <ul class="navigation menu tdl-navbar-nav mega_menu">
                        <li id="menu-item-2259"
                            class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">
                            <a href="{{ route('home') }}"><span class="menu-item-text"><span
                                            class="menu-item-main-title">@lang('header.home')</span></span></a>
                        </li>
                        <li id="menu-item-2259"
                            class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">
                            <a href="{{ route('orders_not_in_stock') }}"><span class="menu-item-text"><span
                                            class="menu-item-main-title">@lang('header.orders_not_in_stock')</span></span></a>
                        </li>
                        <li id="menu-item-2259"
                            class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">
                            <a href="{{ route('refund_policy') }}"><span class="menu-item-text"><span
                                            class="menu-item-main-title">@lang('header.refund_policy')</span></span></a>
                        </li>
{{--                        <li id="menu-item-2259"--}}
{{--                            class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-2259">--}}
{{--                            <a href="{{ route('about_us') }}"><span class="menu-item-text"><span--}}
{{--                                            class="menu-item-main-title">@lang('header.about_us')</span></span></a>--}}
{{--                        </li>--}}

                    </ul>
                </nav>
            </div>
        </div> <!-- End Main Navigation -->



        <!-- Shop Section -->
        <div class="shop-bag">
            <a>
                <div class="l-header-shop">
                    <span class="shopbag_items_number">{{ request()->shopping_cart['count'] }}</span>
                    <i class="icon-shop"></i>
                    <div class="overview">
                        <span class="bag-items-number">{{ request()->shopping_cart['count'] }} @lang('header.items')</span>
                        <span class="woocommerce-Price-amount amount"><span
                                class="woocommerce-Price-currencySymbol">₼</span>{{ request()->shopping_cart['price'] }}</span>
                    </div>
                </div>
            </a>
        </div>



    </div>

</div>

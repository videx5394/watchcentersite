@extends('layouts.app')

@section('content')
    <style>
        .wrapper {
            margin: 10px auto;
            width: 80%;
            text-align: center;
        }
    </style>
    <div class="wrapper">
        <h2 style="min-height: 300px;"> @lang('paymentApproved.invalid_payment')</h2>
    </div>
@endsection

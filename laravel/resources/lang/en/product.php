<?php

return [
    'product' => 'Product',
    'add_to_cart' => 'Add to cart',
    'average_rating' => 'Average rating',
    'add_to_wish_list' => 'Add to Wishlist',
    'view_cart' => 'View cart',
    'product_added' => '“:product” has been added to your cart.',
    'already_added' => 'The product is already in the wishlist!',
    'browse_wish_list' => 'Browse Wishlist',
    'your_rating' => 'Your rating',
    'flat_rate' => 'Flat rate'
];

<?php

return [
    'search' => 'Search',
    'contact number' => 'Contact number',
    'items' => 'Items',
    'login' => 'Login',
    'register' => 'Register',
    'wish_list' => 'Wishlist',
    'home' => 'Home',
    'language' => 'Language',
    'contact_us' => 'Contact Us',
    'orders_not_in_stock' => 'Products to order',
    'refund_policy' => 'Refund policy',
    'watch_boxes' => 'Watch boxes',
    'about_us' => 'About us',
    'logout' => 'Logout'
 ];

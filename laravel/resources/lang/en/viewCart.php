<?php

return [
    'product' => 'Product',
    'price' => 'Price',
    'name' => 'Name',
    'total_price' => 'Total price',
    'cart_totals' => 'CART TOTALS',
    'coupon' => 'Coupon',
    'coupon_code' => 'Coupon code',
    'apply_coupon' => 'Apply coupon',
    'shipping' => 'Shipping',
    'address' => 'Address',
    'update_cart' => 'Update cart',
    'proceed_to_checkout' => 'Proceed to checkout',
    'subtotal' => 'Subtotal',
    'other_cities' => 'Other cities',
    'baku' => 'Baku',
    'month_count' => 'Month',
    'monthly' => 'Price per month',
    'total' => 'Total',
];

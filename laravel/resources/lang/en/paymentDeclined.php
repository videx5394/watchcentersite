<?php

return [
    'invalid_payment' => 'Unsuccessful payment!',
    'error_happened' => 'An error occurred while ordering and paying',
    'error_reasons' => 'This may be due to the following reasons',
    'possible_reasons' => 'Possible reasons:',
    'insufficient_fund' => 'Lack of funds',
    'confirmation_error' => 'Error during confirmation',
    'check_other_payment_methods' => 'Please check again with other payment methods',
    'contact_us' => 'If you are unable to resolve the issue, please contact usZ'
   ];

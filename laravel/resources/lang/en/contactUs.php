<?php

return [
    'get' => 'Get',
    'in_touch' => 'In Touch',
    'your_name' => 'Your Name',
    'subject' => 'Subject',
    'your_email' => 'Your Email',
    'message' => 'Message',
    'our_address' => 'OUR ADDRESS',
    'working_hours' => 'WORKING HOURS',
    'contact_us' => 'Contact Us',
    'send' => 'Send',
    'form_error_message' => 'Your message has not been sent because of internal error. Please try again later',
    'form_error_title' => 'Error',
    'form_success_message' => 'Your message has been sent. Our service manager will contact you soon',
    'form_success_title' => 'Thank you for your opinion',
    'everyday' => 'Everyday'
];

<?php

return [
    'orders_not_in_stock' => 'Products to order',
    'make_order' => 'Make order',
    'order_conditions_text' => 'You can order watches in this section. The selected watch will be delivered to the specified address in Baku within 45 days after payment. Thank you for choosing us!',
    'order_rules_title' => 'Order rules',
    'order_conditions' => 'Please confirm order conditions :',
    ];

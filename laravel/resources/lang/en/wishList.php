<?php

return [
    'my_wish_list' => 'My wishlist',
    'product_name' => 'Product name',
    'unit_price' => 'Unit price',
    'add_to_cart' => 'Add to cart',
    'remove_from_cart' => 'Remove from cart'
 ];

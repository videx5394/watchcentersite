<?php

return [
    'product' => 'Məhsul',
    'add_to_cart' => 'Səbətə əlavə et',
    'average_rating' => 'Orta reytinq', // Orta reytinq
    'add_to_wish_list' => 'Seçilmişlərə əlavə et',
    'view_cart' => 'Səbətə baxış',
    'product_added' => '“:product” səbətinizə ələvə olundu',
    'already_added' => 'Seçilmiş məhsul', // secilmish mesul
    'browse_wish_list' => 'Seçilmişlərə bax', // secilmihslere bax
    'your_rating' => 'Sizin reytinqiniz', //  sizin reytiqiniz
    'flat_rate' => 'Flat rate'
];

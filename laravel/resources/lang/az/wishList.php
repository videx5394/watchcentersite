<?php

return [
    'my_wish_list' => 'Seçilmişlər',
    'product_name' => 'Məhsulun adı',
    'unit_price' => 'Qiymət',
    'add_to_cart' => 'Səbətə əlavə et',
    'remove_from_cart' => 'Səbətdən sil',
 ];

<?php

return [
    'invalid_payment' => 'Uğursuz ödəmə!',
    'error_happened' => 'Sifariş və ödəmə zamanı xəta yarandı',
    'error_reasons' => 'Buna aşağıda qeyd edilənlər səbəb ola bilər',
    'possible_reasons' => 'Mümkün səbəblər:',
    'insufficient_fund' => 'Məbləğin çatışmamazlığı',
    'confirmation_error' => 'Təsdiqləmə zamanı xəta',
    'check_other_payment_methods' => 'Zəhmət olmasa, digər ödəmə növləri ilə bir daha yoxlayın',
    'contact_us' => 'Əgər problemi həll etməyə nail ola bilmirsinizsə, bizimlə əlaqə saxlayın'
   ];

<?php

return [
    'get' => 'Bizimlə', // bizimle
    'in_touch' => 'Əlaqə', // elaqe
    'your_name' => 'Ad',
    'subject' => 'Mövzu',
    'your_email' => 'Email',
    'message' => 'Mesaj',
    'our_address' => 'Ünvanımız',
    'working_hours' => 'İş saatları',
    'contact_us' => 'Bizimlə əlaqə',
    'send' => 'Göndər',
    'form_error_message' => 'Daxili səhv olduğuna görə mesaj göndərilmədi. Zəhmət olmasa bir az sonra yenə cəhd edin',
    'form_error_title' => 'Xəta',
    'form_success_message' => 'Mesajınız göndərildi. Əməkdaşımız tezliklə sizinlə əlaqə saxlayacaq',
    'form_success_title' => 'Müraciyyətiniz üçün təşəkkür edirik', // muraciyyetiniz ucun teshekkur edirik
    'everyday' => 'Hər gün'
];

<?php

return [
    'product' => 'Məhsul',
    'price' => 'Qiymət',
    'name' => 'Ad',
    'total_price' => 'Ödəniləcək məbləğ',
    'cart_totals' => 'Sifarişin melumatlari', // Sifarishin melumatlari
    'coupon' => 'Kupon',
    'coupon_code' => 'Kupon kodu',
    'apply_coupon' => 'Kupon əlavə et',
    'shipping' => 'Çatdırılma',
    'address' => 'Ünvan',
    'update_cart' => 'Update cart',
    'proceed_to_checkout' => 'Ödəmək', // odemek
    'subtotal' => 'Qiymət', //
    'other_cities' => 'Digər şəhərlər', // diger sheherler
    'baku' => 'Bakı',
    'month_count' => 'Ay sayı',
    'monthly' => 'Aylıq ödəniş',
    'total' => 'Yekun',
];

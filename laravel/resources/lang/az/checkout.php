<?php

return [
    'have_a_coupon' => 'Kupon əlavə et', // kupon elave et
    'click_here' => 'Kupon əlavə etmək üçün bura tıklayın', // kupon elave etmek ucun bura tiklayin
    'billing_details' => 'Sifariş məlumatları', // sifarish melumatlari
    'firstName' => 'Ad',
    'firstName_placeholder' => 'Adınız',
    'surname' => 'Soyad',
    'surname_placeholder' => 'Soyadınız',
    'phone' => 'Əlaqə nömrəsi', // elaqe nomresi
    'phone_placeholder' => 'Əlaqə nömrəniz',
    'address' => 'Ünvan',
    'address_placeholder' => 'Ünvanınız',
    'email' => 'Email',
    'email_placeholder' => 'Sizin email',  // sizin email
    'note' => 'Qeyd', // elave melumat
    'your_order' => 'Sifarişiniz',
    'product' => 'Məhsul', // mesul
    'price' => 'Qiymət',
    'shipping' => 'Çatdırılma', // catdirilma
    'apply_coupon' => 'Kupon əlavə et', //
    'coupon_code' => 'Kupon', // kupon
    'place_order' => 'Sifariş et', // sifarish et
    'within_the_city_limits' => 'Within the city limits',
    'different_city' => 'Different city',
    'order_notes' => 'Qeyd',  // Qeyd
    'visa_master' => 'Visa / Master',
    'bircart' => 'BirKart',
    'cash' => 'Nəğd ödəniş', // negd odenish
    'order_notes_placeholder' => 'Sifarişinin və çatdırılmanın özəllikləri', // sifarishin ve catdirilmanin ozellikleri
    'form_error_message' => 'Daxili xəta. Sifarişiniz qəbul olunmadı', // Daxili xeta. Sifarishiniz qebul olunmadi
    'form_error_title' => 'Xəta',
    'form_success_message' => 'Sifarişiniz qəbul olundu. Yaxin zamanda əlaqə saxlanalacaq', // Sifarishiniz qebul olundu . Yaxin zamanda elaqe saxlanalacaq
    'form_success_title' => 'Sifarişiniz üçün təşəkkür edirik',
    'coupon_added_title' => 'Sifarish olundu', // Sifarish olundu
    'coupon_added_message' => 'Kupon əlavə edildi', // Kupon elave edildi
    'checkout_is_not_available' => 'Səbətinizdə məhsul olmamadığına görə ödəniş mümkün deyil', // Sebetinizde mesul olmadigina gore odenish mumkun deyil
    'cart_is_empty' => 'Hal hazırda səbətiniz boşdur',
    'return_to_shop' => 'Əsas səhifəyə keçid', //  esas sehifeye kecid
    'month_count' => 'Ay sayı',
    'monthly' => 'Aylıq ödəniş',
    'total' => 'Yekun',
    'back' => 'Geri qayıt',
    'confirm_order' => 'Sifarişi təsdiqlə',
    'month' => 'ay'
 ];

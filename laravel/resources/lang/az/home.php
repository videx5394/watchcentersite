<?php

return [
    'default_sorting' => 'Standart çeşidləmə', //standart çeşidləmə
    'sort_by_price_h_l' => 'Bahadan-ucuza', //Ucuzdan-bahaya
    'sort_by_price_l_h' => 'Ucuzdan-bahaya',
    'login' => 'Login',
    'register' => 'Register',
    'wish_list' => 'Seçilmişlər',
    'home' => 'Əsas səhifə',
    'language' => 'Dil seçimi',
    'contact_us' => 'Bizimlə əlaqə',
    'watches' => 'Saatlar',
    'mens_watches' => 'Kişi saatları',
    'ladies_watches' => 'Qadın saatları',
    'watch_boxes' => 'Saat qabları',
    'filters' => 'Filtrlər',
    'filter_by_price' => 'Qiymət', // Qiymete gore
    'filter' => 'Filtr',
    'filter_by_color' => 'Rəng', // rege gore
    'black'     => 'Qara',
    'silver'    => 'Gümüş',
    'gold'      => 'Qızıl',
    'red'       => 'Qırmızı',
    'brown'     => 'Qəhvəyi',
    'white'     => 'Ağ',
    'mechanism' => 'Mexanizm', // mexanizm
    'mechanical'=> 'Mexaniki', // mexaniki
    'automatic' => 'Kvars', //Kvars
    'all'       => 'Hamısı',
    'on_sale'   => 'Endirimli', // endirimli
    'results'   => 'Gosterildi - :from-:to Ümumi - :all', // Gosterildi : 1-20 Umumi : 30
    'add_to_card' => 'Səbətə əlavə et', //
    'add_to_wish_list' => 'Seçilmişlərə əlavə et', // secilmishlere elave et
    'best_sellers' => 'Ən cox satılanlar', // en cox satilanlar
    'all_products' => 'Bütün məhsullar', //
    'all_results' => 'Bütün nəticələr', //
    'live_search_empty_msg' => 'Unable to find any products that match the currenty query',
    'shopping_cart' => 'Səbət',
    'view_cart' => 'Səbətə baxış', // sebete baxmaq
    'checkout' => 'Ödəniş', // odenish
    'total_price' => 'Ödəniləcək məbləğ', // odenilecek mebleg
    'product_added_to_wish_list' => 'Seçilmiş məhsullara əlavə edildi', // secilmish mesullara elave edildi
];

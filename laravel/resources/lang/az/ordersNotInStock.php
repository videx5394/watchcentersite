<?php

return [
    'orders_not_in_stock' => 'Sifarişli məhsullar',
    'make_order' => 'Sifariş et',
    'order_conditions_text' => 'Bu bölümdə olan saatları sifariş edə bilərsiz. Seçdiyiniz saat, ödəniş etdikdən sonra 45 gün ərzində Baki şəhəri üzrə bildirdiyiviz ünvana çatdırılır. Bizi seçdiyiviz üçun sizə təşəkkür edirik!',
    'order_rules_title' => 'Sifariş qaydaları',
    'order_conditions' => 'Sifariş şərtləri təsdiq edin :', // sifarish sertleri tesdeq edin
    ];

<?php

return [
    'search' => 'Axtarış',
    'contact number' => 'Əlaqə nömrəsi',
    'items' => 'Məhsul', // mesullar
    'login' => 'Daxil ol',
    'register' => 'Qeydiyyat',
    'wish_list' => 'Seçilmişlər', // Secilmishler
    'home' => 'Əsas səhifə',
    'language' => 'Dil seçimi', // Dil secimi
    'contact_us' => 'Bizimlə əlaqə',
    'orders_not_in_stock' => 'Sifarişli məhsullar', // Sifarishli mesullar
    'refund_policy' => 'Qaytarılma şərtləri', // Qaytarilma shertleri
    'watch_boxes' => 'Saat qabları', // saat qablari
    'about_us' => 'Haqqımızda', // haqqimizda
    'logout' => 'Çıxış'
 ];

<?php

return [
    'get' => 'Свяжитесь', // svajites
    'in_touch' => 'С нами', // s nami
    'your_name' => 'Имя',
    'subject' => 'Заголовок',
    'your_email' => 'Ваш email',
    'message' => 'Сообщение',
    'our_address' => 'Наш адрес',
    'working_hours' => 'Рабочие часы',
    'contact_us' => 'Обратная связь',
    'send' => 'Отправить',
    'form_error_message' => 'Ваше сообщение не было отправлено из за системной ошибки. Пожалуста попробуйте еще раз позже',
    'form_error_title' => 'Ошибка',
    'form_success_message' => 'Ваше сообщение отправлено. Мы свяжемся с вами в ближайшее время',
    'form_success_title' => 'Спасибо за ваше обращение', // spasibo za vawe obraweniye
    'everyday' => 'Круглосуточно' // kruglosutocno
];

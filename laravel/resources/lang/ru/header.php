<?php

return [
    'search' => 'Поиск',
    'contact number' => 'Контактный номер',
    'items' => 'Товаров', // prducti
    'login' => 'Логин',
    'register' => 'Регистрация',
    'wish_list' => 'Избранное', // izbrannoe
    'home' => 'Главная', // glavnaya
    'language' => 'Язык', // yazik
    'contact_us' => 'Обратная связь', // obratnaya svaz
    'orders_not_in_stock' => 'Товары на заказ', // Tovari na zakaz
    'refund_policy' => 'Условия возврата', // Usloviya vozvrata
    'watch_boxes' => 'Коробки для часов', // korobki dla casov
    'about_us' => 'О нас',
    'logout' => 'Выход'
 ];

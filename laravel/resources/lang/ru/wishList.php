<?php

return [
    'my_wish_list' => 'Избранное',
    'product_name' => 'Название товара',
    'unit_price' => 'Цена товара', // cena tovara
    'add_to_cart' => 'Добавить в корзину',
    'remove_from_cart' => 'Удалить из корзины'
 ];

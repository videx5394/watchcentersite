<?php

return [
    'have_a_coupon' => 'Добавить купон',
    'click_here' => 'Нвжите чтобы ввести купон',
    'billing_details' => 'Детали оплаты',
    'firstName' => 'Имя',
    'firstName_placeholder' => 'Ваше имя',
    'surname' => 'Фамилия',
    'surname_placeholder' => 'Ваша фамилия',
    'phone' => 'Контактный номер',
    'phone_placeholder' => 'Ваш контактный номер',
    'address' => 'Адресс',
    'address_placeholder' => 'Ваш адресс',
    'email' => 'Email',
    'email_placeholder' => 'Ваш email',
    'note' => 'Детали заказа',
    'your_order' => 'Ваш заказ',
    'product' => 'Продукт',
    'price' => 'Цена',
    'shipping' => 'Доставка',
    'apply_coupon' => 'Добавить купон',
    'coupon_code' => 'Код купона',
    'place_order' => 'Сделать заказ', //Sdelat zakaz
    'within_the_city_limits' => 'Within the city limits',
    'different_city' => 'Different city',
    'order_notes' => 'Примечания к заказу', //Primecaniya k zakazu
    'visa_master' => 'Visa / Master',
    'bircart' => 'BirKart',
    'cash' => 'Оплата наличными', // Oplata nalicnimi
    'order_notes_placeholder' => 'Примечания к заказу. Желательные условия к заказу',
    'form_error_message' => 'Заказ не был осуществлен из за системной ошибки. Пожалуйста попробуйте позже', // Primecaniya k zakazu . Jelatelnie usloviya k zakazu
    'form_error_title' => 'Ошибка',
    'form_success_message' => 'Ваш заказ был осуществлен. Мы свяжемся с вами в ближайшее время',
    'form_success_title' => 'Спасибо за заказ',
    'coupon_added_title' => '',
    'coupon_added_message' => 'Купон успешно добавлен',
    'checkout_is_not_available' => 'Заказ невозможен.', //
    'cart_is_empty' => 'Ваша карта пуста',
    'return_to_shop' => 'Вернуться на главную страницу',
    'month_count' => 'Месяц',
    'monthly' => 'Месячная оплата',
    'total' => 'Итог',
    'back' => 'Назад',
    'confirm_order' => 'Подтвердить заказ',
    'month' => 'месяц'
 ];

<?php

return [
    'product' => 'Товар',
    'add_to_cart' => 'Добавить в корзину',
    'average_rating' => 'Средняя оценка', // srednaya otcenka
    'add_to_wish_list' => 'Добавить в избранное',
    'view_cart' => 'Посмотреть карту',
    'product_added' => '“:product” был добавлен в список', // tovar bil dobavlen v spisok
    'already_added' => 'Товар уже в избранном!',
    'browse_wish_list' => 'Посмотреть избранное', // prosmotret izbrannoe
    'your_rating' => 'Ващ рейтинг', //
    'flat_rate' => 'Flat rate'
];

<?php

return [
    'orders_not_in_stock' => 'Товары нв заказ', // tovari na zakaz
    'make_order' => 'Заказать', // zalazat
    'order_rules_title' => 'Условия заказа',
    'order_conditions_text' => 'Вы можете заказать часы в этом разделе. Выбранные часы будут доставлены по указанному адресу в Баку в течение 45 дней после оплаты. Спасибо, что выбрали нас!',
    'order_conditions' => 'Подтвердите условия заказа :', // podtverdite usloviya zakaza
    ];

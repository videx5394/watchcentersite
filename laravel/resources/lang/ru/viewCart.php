<?php

return [
    'product' => 'Товар',
    'price' => 'Цена',
    'name' => 'Название',
    'total_price' => 'Итоговая цена',
    'cart_totals' => 'Детали покупки', // detali pokupki
    'coupon' => 'Купон',
    'coupon_code' => 'Код купона',
    'apply_coupon' => 'Добавить купон',
    'shipping' => 'Доставка',
    'address' => 'Адрес',
    'update_cart' => 'Update cart',
    'proceed_to_checkout' => 'Оплатить', // oplatit
    'subtotal' => 'Цена', // cena
    'other_cities' => 'Другие города', // drugie goroda
    'baku' => 'Баку',
    'month_count' => 'Месяц',
    'monthly' => 'Месячная оплата',
    'total' => 'Итог',
];

<?php

return [
    'default_sorting' => 'Сортировка по умолчанию',
    'sort_by_price_h_l' => 'Сортировка от дорогого к дешевому', // ot dorogogo  k dewevomu
    'sort_by_price_l_h' => 'Сортировка от дешевого к дорогому',
    'login' => 'Логин',
    'register' => 'Регистрация',
    'wish_list' => 'Избранное',
    'home' => 'Главная',
    'language' => 'Язык',
    'contact_us' => 'Обратная связь',
    'watches' => 'Часы',
    'mens_watches' => 'Мужские часы',
    'ladies_watches' => 'Жениские часы',
    'watch_boxes' => 'Коробки для часов',
    'filters' => 'Фильтры',
    'filter_by_price' => 'Фильтр по цене',
    'filter' => 'Фильтр',
    'filter_by_color' => 'Фильтр цвету',
    'black'     => 'Черный',
    'silver'    => 'Серебряный',
    'gold'      => 'Золотой',
    'red'       => 'Красный',
    'brown'     => 'Коричневый',
    'white'     => 'Белый',
    'mechanism' => 'Механизм',
    'mechanical'=> 'Механические',
    'automatic' => 'Кварцевые',
    'all'       => 'Все',
    'on_sale'   => 'На скидке',
    'results'   => 'Показано :from-:to из :all', // Pokazano 1-16 iz 200
    'add_to_card' => 'Добавить в корзину',
    'add_to_wish_list' => 'Добавить в избранное',
    'best_sellers' => 'Топ продаж', // top prodaj
    'all_products' => 'Топ товаров',
    'all_results' => 'Посмотреть все',
    'live_search_empty_msg' => 'Unable to find any products that match the currenty query',
    'shopping_cart' => 'Корзина',
    'view_cart' => 'Посмотреть корзину',
    'checkout' => 'Покупка', // pokupka
    'total_price' => 'Итоговая уена', // itogovaya cena
    'product_added_to_wish_list' => 'Товар был успешно добавлен',
];

<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CouponTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testApplyCoupon()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
            ->json('POST', '/applyCoupon', ['coupon' => 'Sally'])
            ->assertStatus(200)
            ->json();
        $this->assertIsNumeric($response['sale']);
    }
}

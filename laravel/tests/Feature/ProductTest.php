<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    public function getFirstProductId()
    {
        $testProduct = DB::select("SELECT id from `products` LIMIT 1");
        if(count($testProduct)>0)
        {
            return $testProduct[0]->id;
        }
        else
            return 0;
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetProduct()
    {
        $testProductId = $this->getFirstProductId();
        $response = $this->get(route('product',['id'=>$testProductId]));
        if($testProductId>0)
        {
            $response->assertStatus(200);
        }
        else
        {
            $response->assertStatus(302);
        }
    }

    public function testRateProduct()
    {
        $user = factory(User::class)->create();
        $testProductId = $this->getFirstProductId();
        $response = $this->actingAs($user)->get(route('product',['id'=>$testProductId]));
        $response->assertStatus(200);
        $response->assertSee('ok');
    }
}

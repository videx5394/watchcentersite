/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : wca

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-02-18 18:30:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for backups
-- ----------------------------
DROP TABLE IF EXISTS `backups`;
CREATE TABLE `backups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `backup_size` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `backups_name_unique` (`name`),
  UNIQUE KEY `backups_file_name_unique` (`file_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of backups
-- ----------------------------

-- ----------------------------
-- Table structure for coupons
-- ----------------------------
DROP TABLE IF EXISTS `coupons`;
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `sale` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of coupons
-- ----------------------------

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `color` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `departments_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES ('1', 'Administration', '[]', '#000', null, '2020-02-12 16:31:42', '2020-02-12 16:31:42');

-- ----------------------------
-- Table structure for infos
-- ----------------------------
DROP TABLE IF EXISTS `infos`;
CREATE TABLE `infos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of infos
-- ----------------------------
INSERT INTO `infos` VALUES ('1', null, '2020-02-14 20:11:44', '2020-02-16 14:14:33', '+994551234568', 'watchcenter@gmail.com');

-- ----------------------------
-- Table structure for la_configs
-- ----------------------------
DROP TABLE IF EXISTS `la_configs`;
CREATE TABLE `la_configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of la_configs
-- ----------------------------
INSERT INTO `la_configs` VALUES ('1', 'sitename', '', 'Watch center', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('2', 'sitename_part1', '', 'Watch', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('3', 'sitename_part2', '', 'center', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('4', 'sitename_short', '', 'WC', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('5', 'site_description', '', '', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('6', 'sidebar_search', '', '0', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('7', 'show_messages', '', '0', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('8', 'show_notifications', '', '0', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('9', 'show_tasks', '', '0', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('10', 'show_rightsidebar', '', '0', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('11', 'skin', '', 'skin-black', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('12', 'layout', '', 'fixed', '2020-02-12 16:31:42', '2020-02-13 18:21:58');
INSERT INTO `la_configs` VALUES ('13', 'default_email', '', 'emil9453@gmail.com', '2020-02-12 16:31:42', '2020-02-13 18:21:58');

-- ----------------------------
-- Table structure for la_menus
-- ----------------------------
DROP TABLE IF EXISTS `la_menus`;
CREATE TABLE `la_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'module',
  `parent` int(10) unsigned NOT NULL DEFAULT 0,
  `hierarchy` int(10) unsigned NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of la_menus
-- ----------------------------
INSERT INTO `la_menus` VALUES ('2', 'Users', 'users', 'fa-group', 'module', '1', '0', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `la_menus` VALUES ('3', 'Uploads', 'uploads', 'fa-files-o', 'module', '0', '0', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `la_menus` VALUES ('6', 'Roles', 'roles', 'fa-user-plus', 'module', '1', '0', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `la_menus` VALUES ('8', 'Permissions', 'permissions', 'fa-magic', 'module', '1', '0', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `la_menus` VALUES ('10', 'Products', 'products', 'fa fa-cube', 'module', '0', '0', '2020-02-12 17:28:06', '2020-02-12 17:28:06');
INSERT INTO `la_menus` VALUES ('11', 'Coupons', 'coupons', 'fa fa-cube', 'module', '0', '0', '2020-02-12 19:34:35', '2020-02-12 19:34:35');
INSERT INTO `la_menus` VALUES ('12', 'Infos', 'infos', 'fa fa-info-circle', 'module', '0', '0', '2020-02-12 19:48:32', '2020-02-12 19:48:32');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2014_05_26_050000_create_modules_table', '1');
INSERT INTO `migrations` VALUES ('2014_05_26_055000_create_module_field_types_table', '1');
INSERT INTO `migrations` VALUES ('2014_05_26_060000_create_module_fields_table', '1');
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('2014_12_01_000000_create_uploads_table', '1');
INSERT INTO `migrations` VALUES ('2016_05_26_064006_create_departments_table', '1');
INSERT INTO `migrations` VALUES ('2016_05_26_064007_create_employees_table', '1');
INSERT INTO `migrations` VALUES ('2016_05_26_064446_create_roles_table', '1');
INSERT INTO `migrations` VALUES ('2016_07_05_115343_create_role_user_table', '1');
INSERT INTO `migrations` VALUES ('2016_07_06_140637_create_organizations_table', '1');
INSERT INTO `migrations` VALUES ('2016_07_07_134058_create_backups_table', '1');
INSERT INTO `migrations` VALUES ('2016_07_07_134058_create_menus_table', '1');
INSERT INTO `migrations` VALUES ('2016_09_10_163337_create_permissions_table', '1');
INSERT INTO `migrations` VALUES ('2016_09_10_163520_create_permission_role_table', '1');
INSERT INTO `migrations` VALUES ('2016_09_22_105958_role_module_fields_table', '1');
INSERT INTO `migrations` VALUES ('2016_09_22_110008_role_module_table', '1');
INSERT INTO `migrations` VALUES ('2016_10_06_115413_create_la_configs_table', '1');
INSERT INTO `migrations` VALUES ('2020_01_12_192941_create_watches_table', '1');

-- ----------------------------
-- Table structure for modules
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_db` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `view_col` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fa_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `is_gen` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO `modules` VALUES ('1', 'Users', 'Users', 'users', 'name', 'User', 'UsersController', 'fa-group', '1', '2020-02-12 16:31:17', '2020-02-12 16:31:42');
INSERT INTO `modules` VALUES ('2', 'Uploads', 'Uploads', 'uploads', 'name', 'Upload', 'UploadsController', 'fa-files-o', '1', '2020-02-12 16:31:17', '2020-02-12 16:31:42');
INSERT INTO `modules` VALUES ('3', 'Departments', 'Departments', 'departments', 'name', 'Department', 'DepartmentsController', 'fa-tags', '1', '2020-02-12 16:31:17', '2020-02-12 16:31:42');
INSERT INTO `modules` VALUES ('5', 'Roles', 'Roles', 'roles', 'name', 'Role', 'RolesController', 'fa-user-plus', '1', '2020-02-12 16:31:18', '2020-02-12 16:31:42');
INSERT INTO `modules` VALUES ('7', 'Backups', 'Backups', 'backups', 'name', 'Backup', 'BackupsController', 'fa-hdd-o', '1', '2020-02-12 16:31:18', '2020-02-12 16:31:42');
INSERT INTO `modules` VALUES ('8', 'Permissions', 'Permissions', 'permissions', 'name', 'Permission', 'PermissionsController', 'fa-magic', '1', '2020-02-12 16:31:18', '2020-02-12 16:31:42');
INSERT INTO `modules` VALUES ('10', 'Products', 'Products', 'products', 'name', 'Product', 'ProductsController', 'fa-cube', '1', '2020-02-12 16:38:38', '2020-02-12 17:28:12');
INSERT INTO `modules` VALUES ('11', 'Coupons', 'Coupons', 'coupons', 'name', 'Coupon', 'CouponsController', 'fa-cube', '1', '2020-02-12 19:32:12', '2020-02-12 19:34:35');
INSERT INTO `modules` VALUES ('12', 'Infos', 'Infos', 'infos', 'phone', 'Info', 'InfosController', 'fa-info-circle', '1', '2020-02-12 19:45:40', '2020-02-12 19:48:32');

-- ----------------------------
-- Table structure for module_fields
-- ----------------------------
DROP TABLE IF EXISTS `module_fields`;
CREATE TABLE `module_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `colname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module` int(10) unsigned NOT NULL,
  `field_type` int(10) unsigned NOT NULL,
  `unique` tinyint(1) NOT NULL DEFAULT 0,
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minlength` int(10) unsigned NOT NULL DEFAULT 0,
  `maxlength` int(10) unsigned NOT NULL DEFAULT 0,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `popup_vals` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) unsigned NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_fields_module_foreign` (`module`),
  KEY `module_fields_field_type_foreign` (`field_type`),
  CONSTRAINT `module_fields_field_type_foreign` FOREIGN KEY (`field_type`) REFERENCES `module_field_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `module_fields_module_foreign` FOREIGN KEY (`module`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of module_fields
-- ----------------------------
INSERT INTO `module_fields` VALUES ('1', 'name', 'Name', '1', '16', '0', '', '5', '250', '1', '', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('2', 'context_id', 'Context', '1', '13', '0', '0', '0', '0', '0', '', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('3', 'email', 'Email', '1', '8', '1', '', '0', '250', '0', '', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('4', 'password', 'Password', '1', '17', '0', '', '6', '250', '1', '', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('5', 'type', 'User Type', '1', '7', '0', 'Employee', '0', '0', '0', '[\"Employee\",\"Client\"]', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('6', 'name', 'Name', '2', '16', '0', '', '5', '250', '1', '', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('7', 'path', 'Path', '2', '19', '0', '', '0', '250', '0', '', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('8', 'extension', 'Extension', '2', '19', '0', '', '0', '20', '0', '', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('9', 'caption', 'Caption', '2', '19', '0', '', '0', '250', '0', '', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('10', 'user_id', 'Owner', '2', '7', '0', '1', '0', '0', '0', '@users', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('11', 'hash', 'Hash', '2', '19', '0', '', '0', '250', '0', '', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('12', 'public', 'Is Public', '2', '2', '0', '0', '0', '0', '0', '', '0', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_fields` VALUES ('30', 'name', 'Name', '5', '16', '1', '', '1', '250', '1', '', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('31', 'display_name', 'Display Name', '5', '19', '0', '', '0', '250', '1', '', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('32', 'description', 'Description', '5', '21', '0', '', '0', '1000', '0', '', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('33', 'parent', 'Parent Role', '5', '7', '0', '1', '0', '0', '0', '@roles', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('34', 'dept', 'Department', '5', '7', '0', '1', '0', '0', '0', '@departments', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('46', 'name', 'Name', '7', '16', '1', '', '0', '250', '1', '', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('47', 'file_name', 'File Name', '7', '19', '1', '', '0', '250', '1', '', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('48', 'backup_size', 'File Size', '7', '19', '0', '0', '0', '10', '1', '', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('49', 'name', 'Name', '8', '16', '1', '', '1', '250', '1', '', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('50', 'display_name', 'Display Name', '8', '19', '0', '', '0', '250', '1', '', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('51', 'description', 'Description', '8', '21', '0', '', '0', '1000', '0', '', '0', '2020-02-12 16:31:18', '2020-02-12 16:31:18');
INSERT INTO `module_fields` VALUES ('53', 'name', 'name', '10', '22', '0', '', '0', '256', '1', '', '0', '2020-02-12 16:40:28', '2020-02-12 16:40:28');
INSERT INTO `module_fields` VALUES ('54', 'product_type', 'product_type', '10', '18', '0', 'watches', '0', '0', '1', '[\"watches\",\"watch_boxes\"]', '0', '2020-02-12 16:52:58', '2020-02-13 18:39:08');
INSERT INTO `module_fields` VALUES ('55', 'price', 'price', '10', '10', '0', '', '0', '11', '0', '', '0', '2020-02-12 16:53:44', '2020-02-14 19:03:59');
INSERT INTO `module_fields` VALUES ('56', 'sale_price', 'sale_price', '10', '10', '0', '', '0', '11', '0', '', '0', '2020-02-12 16:54:11', '2020-02-14 19:03:19');
INSERT INTO `module_fields` VALUES ('57', 'description', 'description', '10', '21', '0', '', '0', '0', '0', '', '0', '2020-02-12 16:54:49', '2020-02-12 16:54:49');
INSERT INTO `module_fields` VALUES ('58', 'front_photo', 'front_photo', '10', '12', '0', '', '0', '0', '1', '', '0', '2020-02-12 16:55:37', '2020-02-12 16:56:52');
INSERT INTO `module_fields` VALUES ('59', 'front_photo_small', 'front_photo_small', '10', '12', '0', '', '0', '0', '1', '', '0', '2020-02-12 16:56:24', '2020-02-12 16:57:05');
INSERT INTO `module_fields` VALUES ('60', 'back_photo', 'back_photo', '10', '12', '0', '', '0', '0', '1', '', '0', '2020-02-12 16:57:33', '2020-02-12 16:57:33');
INSERT INTO `module_fields` VALUES ('61', 'back_photo_small', 'back_photo_small', '10', '12', '0', '', '0', '0', '1', '', '0', '2020-02-12 16:57:49', '2020-02-12 16:57:49');
INSERT INTO `module_fields` VALUES ('62', 'additional_photo1', 'additional_photo1', '10', '12', '0', '', '0', '0', '0', '', '0', '2020-02-12 16:58:18', '2020-02-12 16:58:18');
INSERT INTO `module_fields` VALUES ('63', 'additional_photo2', 'additional_photo2', '10', '12', '0', '', '0', '0', '0', '', '0', '2020-02-12 16:58:48', '2020-02-12 16:58:48');
INSERT INTO `module_fields` VALUES ('64', 'colors', 'colors', '10', '15', '0', '', '0', '0', '0', '[\"black\",\"silver\",\"gold\",\"red\",\"brown\",\"white\"]', '0', '2020-02-12 17:05:32', '2020-02-12 17:05:32');
INSERT INTO `module_fields` VALUES ('65', 'mech', 'mechanism type', '10', '18', '0', 'mechanical', '0', '0', '1', '[\"mechanical\",\"quartz\"]', '0', '2020-02-12 17:07:02', '2020-02-13 18:41:24');
INSERT INTO `module_fields` VALUES ('66', 'watch_type', 'watch type', '10', '7', '0', '', '0', '256', '1', '[\"mens\",\"ladies\"]', '0', '2020-02-12 17:10:24', '2020-02-13 18:47:55');
INSERT INTO `module_fields` VALUES ('67', 'is_best_seller', 'is best seller', '10', '18', '0', '', '0', '0', '1', '[\"yes\",\"no\"]', '0', '2020-02-12 17:12:00', '2020-02-13 18:42:34');
INSERT INTO `module_fields` VALUES ('68', 'in_stock', 'in stock', '10', '18', '0', '', '0', '0', '1', '[\"yes\",\"no\"]', '0', '2020-02-12 17:12:30', '2020-02-13 18:43:42');
INSERT INTO `module_fields` VALUES ('69', 'name', 'name', '11', '22', '0', '', '0', '256', '0', '', '0', '2020-02-12 19:33:21', '2020-02-12 19:33:21');
INSERT INTO `module_fields` VALUES ('70', 'sale', 'sale', '11', '13', '0', '', '0', '100', '1', '', '0', '2020-02-12 19:34:20', '2020-02-12 19:34:20');
INSERT INTO `module_fields` VALUES ('71', 'phone', 'phone', '12', '22', '0', '', '0', '256', '1', '', '0', '2020-02-12 19:47:54', '2020-02-12 19:47:54');
INSERT INTO `module_fields` VALUES ('72', 'email', 'email', '12', '8', '1', '', '0', '256', '1', '', '0', '2020-02-12 19:48:12', '2020-02-12 19:48:12');

-- ----------------------------
-- Table structure for module_field_types
-- ----------------------------
DROP TABLE IF EXISTS `module_field_types`;
CREATE TABLE `module_field_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of module_field_types
-- ----------------------------
INSERT INTO `module_field_types` VALUES ('1', 'Address', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('2', 'Checkbox', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('3', 'Currency', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('4', 'Date', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('5', 'Datetime', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('6', 'Decimal', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('7', 'Dropdown', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('8', 'Email', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('9', 'File', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('10', 'Float', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('11', 'HTML', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('12', 'Image', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('13', 'Integer', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('14', 'Mobile', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('15', 'Multiselect', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('16', 'Name', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('17', 'Password', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('18', 'Radio', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('19', 'String', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('20', 'Taginput', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('21', 'Textarea', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('22', 'TextField', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('23', 'URL', '2020-02-12 16:31:17', '2020-02-12 16:31:17');
INSERT INTO `module_field_types` VALUES ('24', 'Files', '2020-02-12 16:31:17', '2020-02-12 16:31:17');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `coupon_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for order_product
-- ----------------------------
DROP TABLE IF EXISTS `order_product`;
CREATE TABLE `order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of order_product
-- ----------------------------

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'ADMIN_PANEL', 'Admin Panel', 'Admin Panel Permission', null, '2020-02-12 16:31:42', '2020-02-12 16:31:42');

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES ('1', '1');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `product_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'watches',
  `price` double NOT NULL,
  `sale_price` double NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `front_photo` int(11) NOT NULL DEFAULT 1,
  `front_photo_small` int(11) NOT NULL DEFAULT 1,
  `back_photo` int(11) NOT NULL DEFAULT 1,
  `back_photo_small` int(11) NOT NULL DEFAULT 1,
  `additional_photo1` int(11) NOT NULL,
  `additional_photo2` int(11) NOT NULL,
  `colors` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `mech` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'mechanical',
  `watch_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_best_seller` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `in_stock` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', null, '2020-02-12 17:29:48', '2020-02-16 18:39:08', 'Swatch', 'watches', '560.15', '0', 'First product', '15', '6', '10', '7', '15', '5', '[\"black\",\"silver\"]', 'mechanical', 'mens', 'yes', 'yes');
INSERT INTO `products` VALUES ('2', null, '2020-02-14 18:06:31', '2020-02-16 07:10:17', 'Tissot', 'watches', '420.1', '20', 'tissot', '18', '17', '15', '14', '13', '12', '[\"silver\"]', 'quartz', 'mens', 'no', 'no');
INSERT INTO `products` VALUES ('3', null, '2020-02-14 19:26:03', '2020-02-16 07:10:50', 'Patek Philip', 'watches', '780.1', '150', '150 man skidka na tovar', '12', '16', '14', '9', '13', '8', '[\"silver\"]', 'mechanical', 'mens', 'no', 'yes');
INSERT INTO `products` VALUES ('4', null, '2020-02-16 14:18:50', '2020-02-16 14:18:50', 'Lige', 'watches', '89', '0', 'Opisanie profucta lige vigladit tak', '21', '9', '16', '13', '12', '20', '[\"silver\"]', 'quartz', 'mens', 'yes', 'yes');

-- ----------------------------
-- Table structure for product_rating
-- ----------------------------
DROP TABLE IF EXISTS `product_rating`;
CREATE TABLE `product_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `star` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of product_rating
-- ----------------------------
INSERT INTO `product_rating` VALUES ('1', '2', '4', null, '5');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) unsigned NOT NULL DEFAULT 1,
  `dept` int(10) unsigned NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`),
  KEY `roles_parent_foreign` (`parent`),
  KEY `roles_dept_foreign` (`dept`),
  CONSTRAINT `roles_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`),
  CONSTRAINT `roles_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'SUPER_ADMIN', 'Super Admin', 'Full Access Role', '1', '1', null, '2020-02-12 16:31:42', '2020-02-12 16:31:42');

-- ----------------------------
-- Table structure for role_module
-- ----------------------------
DROP TABLE IF EXISTS `role_module`;
CREATE TABLE `role_module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `acc_view` tinyint(1) NOT NULL,
  `acc_create` tinyint(1) NOT NULL,
  `acc_edit` tinyint(1) NOT NULL,
  `acc_delete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_module_role_id_foreign` (`role_id`),
  KEY `role_module_module_id_foreign` (`module_id`),
  CONSTRAINT `role_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_module_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_module
-- ----------------------------
INSERT INTO `role_module` VALUES ('1', '1', '1', '1', '1', '1', '1', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module` VALUES ('2', '1', '2', '1', '1', '1', '1', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module` VALUES ('3', '1', '3', '1', '1', '1', '1', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module` VALUES ('5', '1', '5', '1', '1', '1', '1', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module` VALUES ('7', '1', '7', '1', '1', '1', '1', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module` VALUES ('8', '1', '8', '1', '1', '1', '1', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module` VALUES ('10', '1', '10', '1', '1', '1', '1', '2020-02-12 17:13:26', '2020-02-12 17:13:26');
INSERT INTO `role_module` VALUES ('11', '1', '11', '1', '1', '1', '1', '2020-02-12 19:34:35', '2020-02-12 19:34:35');
INSERT INTO `role_module` VALUES ('12', '1', '12', '1', '1', '1', '1', '2020-02-12 19:48:32', '2020-02-12 19:48:32');

-- ----------------------------
-- Table structure for role_module_fields
-- ----------------------------
DROP TABLE IF EXISTS `role_module_fields`;
CREATE TABLE `role_module_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `field_id` int(10) unsigned NOT NULL,
  `access` enum('invisible','readonly','write') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_module_fields_role_id_foreign` (`role_id`),
  KEY `role_module_fields_field_id_foreign` (`field_id`),
  CONSTRAINT `role_module_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `module_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_module_fields_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_module_fields
-- ----------------------------
INSERT INTO `role_module_fields` VALUES ('1', '1', '1', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('2', '1', '2', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('3', '1', '3', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('4', '1', '4', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('5', '1', '5', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('6', '1', '6', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('7', '1', '7', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('8', '1', '8', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('9', '1', '9', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('10', '1', '10', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('11', '1', '11', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('12', '1', '12', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('30', '1', '30', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('31', '1', '31', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('32', '1', '32', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('33', '1', '33', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('34', '1', '34', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('46', '1', '46', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('47', '1', '47', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('48', '1', '48', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('49', '1', '49', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('50', '1', '50', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('51', '1', '51', 'write', '2020-02-12 16:31:42', '2020-02-12 16:31:42');
INSERT INTO `role_module_fields` VALUES ('53', '1', '53', 'write', '2020-02-12 16:40:28', '2020-02-12 16:40:28');
INSERT INTO `role_module_fields` VALUES ('54', '1', '54', 'write', '2020-02-12 16:52:58', '2020-02-12 16:52:58');
INSERT INTO `role_module_fields` VALUES ('55', '1', '55', 'write', '2020-02-12 16:53:44', '2020-02-12 16:53:44');
INSERT INTO `role_module_fields` VALUES ('56', '1', '56', 'write', '2020-02-12 16:54:11', '2020-02-12 16:54:11');
INSERT INTO `role_module_fields` VALUES ('57', '1', '57', 'write', '2020-02-12 16:54:49', '2020-02-12 16:54:49');
INSERT INTO `role_module_fields` VALUES ('58', '1', '58', 'write', '2020-02-12 16:55:37', '2020-02-12 16:55:37');
INSERT INTO `role_module_fields` VALUES ('59', '1', '59', 'write', '2020-02-12 16:56:24', '2020-02-12 16:56:24');
INSERT INTO `role_module_fields` VALUES ('60', '1', '60', 'write', '2020-02-12 16:57:33', '2020-02-12 16:57:33');
INSERT INTO `role_module_fields` VALUES ('61', '1', '61', 'write', '2020-02-12 16:57:49', '2020-02-12 16:57:49');
INSERT INTO `role_module_fields` VALUES ('62', '1', '62', 'write', '2020-02-12 16:58:18', '2020-02-12 16:58:18');
INSERT INTO `role_module_fields` VALUES ('63', '1', '63', 'write', '2020-02-12 16:58:48', '2020-02-12 16:58:48');
INSERT INTO `role_module_fields` VALUES ('64', '1', '64', 'write', '2020-02-12 17:05:32', '2020-02-12 17:05:32');
INSERT INTO `role_module_fields` VALUES ('65', '1', '65', 'write', '2020-02-12 17:07:03', '2020-02-12 17:07:03');
INSERT INTO `role_module_fields` VALUES ('66', '1', '66', 'write', '2020-02-12 17:10:24', '2020-02-12 17:10:24');
INSERT INTO `role_module_fields` VALUES ('67', '1', '67', 'write', '2020-02-12 17:12:00', '2020-02-12 17:12:00');
INSERT INTO `role_module_fields` VALUES ('68', '1', '68', 'write', '2020-02-12 17:12:30', '2020-02-12 17:12:30');
INSERT INTO `role_module_fields` VALUES ('69', '1', '69', 'write', '2020-02-12 19:33:21', '2020-02-12 19:33:21');
INSERT INTO `role_module_fields` VALUES ('70', '1', '70', 'write', '2020-02-12 19:34:20', '2020-02-12 19:34:20');
INSERT INTO `role_module_fields` VALUES ('71', '1', '71', 'write', '2020-02-12 19:47:54', '2020-02-12 19:47:54');
INSERT INTO `role_module_fields` VALUES ('72', '1', '72', 'write', '2020-02-12 19:48:12', '2020-02-12 19:48:12');

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES ('1', '1', '1', null, null);

-- ----------------------------
-- Table structure for shopping_cart
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE `shopping_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of shopping_cart
-- ----------------------------

-- ----------------------------
-- Table structure for shopping_cart_not_in_stock
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart_not_in_stock`;
CREATE TABLE `shopping_cart_not_in_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of shopping_cart_not_in_stock
-- ----------------------------
INSERT INTO `shopping_cart_not_in_stock` VALUES ('2', '4', '2020-02-18 17:47:30', '2');

-- ----------------------------
-- Table structure for shopping_cart_totals
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart_totals`;
CREATE TABLE `shopping_cart_totals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `shipping` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of shopping_cart_totals
-- ----------------------------

-- ----------------------------
-- Table structure for uploads
-- ----------------------------
DROP TABLE IF EXISTS `uploads`;
CREATE TABLE `uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT 1,
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uploads_user_id_foreign` (`user_id`),
  CONSTRAINT `uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3219 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of uploads
-- ----------------------------
INSERT INTO `uploads` VALUES ('5', 'xperia_z2_03-350x380.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\watchcenter\\storage\\uploads\\2020-02-16-070845-xperia_z2_03-350x380.jpg', 'jpg', '', '1', 'mp3nla4cgb4zpte3dvje', '0', null, '2020-02-16 07:08:45', '2020-02-16 07:08:45');
INSERT INTO `uploads` VALUES ('6', 'ant-head-01.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\watchcenter\\storage\\uploads\\2020-02-16-070916-ant-head-01.jpg', 'jpg', '', '1', 'yg7ptwdbec9ifilavljv', '0', null, '2020-02-16 07:09:16', '2020-02-16 07:09:16');
INSERT INTO `uploads` VALUES ('3218', 'ant-head-01.jpg', 'C:\\Users\\ebaghirov.KIBRIT\\Documents\\watchcenter\\storage\\uploads\\2020-02-16-070916-ant-head-01.jpg', 'jpg', '', '1', 'yg7ptwdbec9ifilavljv', '0', null, '2020-02-16 07:09:16', '2020-02-16 07:09:16');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `context_id` int(10) unsigned NOT NULL DEFAULT 0,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Employee',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Emil', '1', 'emil9453@gmail.com', '$2y$10$HXzhEddHOx/wkr79zBSfwu67B6pViZmir9J0Lr79R/D8LbEX3ytvm', 'Employee', 'Ra6lGzVBMaOWj7DJmRyf9JqacVOf8kHX79QSwCoQGKO6QzT9ndpwdKN54YQW', null, '2020-02-12 16:33:10', '2020-02-16 12:45:15');
INSERT INTO `users` VALUES ('2', 'User', '0', 'emil5394@gmail.com', '$2y$10$p.qO7k.fyErQINccM2uoBOvuxyhDYP4BgqzoMnvmbZKGMIZuYm.ku', 'Employee', null, null, '2020-02-18 05:59:35', '2020-02-18 05:59:35');

-- ----------------------------
-- Table structure for wish_list
-- ----------------------------
DROP TABLE IF EXISTS `wish_list`;
CREATE TABLE `wish_list` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of wish_list
-- ----------------------------
INSERT INTO `wish_list` VALUES ('0', '4', '2020-02-18 17:46:37', '2');

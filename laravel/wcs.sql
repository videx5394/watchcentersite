-- MariaDB dump 10.17  Distrib 10.4.12-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: wcs
-- ------------------------------------------------------
-- Server version	10.4.12-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `coupon`
--

DROP TABLE IF EXISTS `coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT current_timestamp(),
  `sale` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon`
--

LOCK TABLES `coupon` WRITE;
/*!40000 ALTER TABLE `coupon` DISABLE KEYS */;
INSERT INTO `coupon` (`id`, `name`, `created_at`, `sale`) VALUES (1,'hi','2020-02-05 22:24:47',17);
/*!40000 ALTER TABLE `coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `coupon_id` int(11) DEFAULT 0,
  `status` int(11) DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` (`id`, `user_id`, `created_at`, `coupon_id`, `status`, `name`, `surname`, `address`, `phone`, `email`, `note`, `shipping`, `payment`, `total_price`) VALUES (33,2,'2020-02-08 20:12:25',1,0,'a','a','a','a','emil9453@gmail.com','a','cities','cash',1662.17),(34,2,'2020-02-08 20:26:36',1,0,'q','q','q','q','emil9453@gmail.com','q','baku','cash',1659.17),(35,2,'2020-02-08 20:27:15',NULL,0,'a','a','a','a','emil9453@gmail.com','a','baku','cash',NULL),(36,2,'2020-02-08 20:29:51',1,0,'a','a','a','a','emil9453@gmail.com','a','cities','cash',1247.17),(37,2,'2020-02-08 20:33:10',1,0,'a','a','a','a','emil9453@gmail.com','a','cities','cash',1247.17),(38,2,'2020-02-08 20:34:46',1,0,'a','a','a','a','emil9453@gmail.com','a','cities','cash',1247.17),(39,2,'2020-02-08 20:36:05',1,0,'a','a','a','a','emil9453@gmail.com','a','cities','cash',1247.17),(40,2,'2020-02-08 20:37:37',1,0,'a','b','s','c','emil9453@gmail.com','b','baku','cash',1244.17),(41,2,'2020-02-08 20:39:33',1,0,'a','a','a','a','emil9453@gmail.com','a','cities','cash',1247.17),(42,2,'2020-02-08 20:42:00',1,0,'a','a','a','a','emil9453@gmail.com','a','cities','cash',1247.17),(43,2,'2020-02-08 20:42:51',1,0,'a','a','a','a','emil9453@gmail.com','a','cities','cash',1247.17),(44,2,'2020-02-11 23:01:34',NULL,0,'Emil','Bagirov','sadfsadfasf','asdfasdf','emil9453@gmail.com','Hello','baku','cash',NULL),(45,2,'2020-02-11 23:46:57',NULL,0,'Emil','Bagirov','address','phone','emil9453@gmail.com','','baku','cash',NULL),(46,2,'2020-02-12 01:18:18',NULL,0,'name','Bagirov','address','phone','emil9453@gmail.com','note','baku','cash',NULL),(47,2,'2020-02-12 01:19:59',NULL,0,'name','g','address','phone','emil9453@gmail.com','hi','baku','cash',NULL),(48,2,'2020-02-12 01:30:09',NULL,0,'qq','surnamed','dd','s','emil9453@gmail.com','','baku','cash',NULL),(49,2,'2020-02-12 01:34:47',NULL,0,'q','q','q','q','emil9453@gmail.com','q','baku','cash',899.00);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_product_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_product`
--

LOCK TABLES `order_product` WRITE;
/*!40000 ALTER TABLE `order_product` DISABLE KEYS */;
INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `created_at`, `price`) VALUES (48,33,2,'2020-02-08 16:12:25',999.00),(49,33,6,'2020-02-08 16:12:25',600.00),(50,33,7,'2020-02-08 16:12:25',500.00),(51,34,2,'2020-02-08 16:26:36',999.00),(52,34,6,'2020-02-08 16:26:36',600.00),(53,34,7,'2020-02-08 16:26:36',500.00),(54,35,2,'2020-02-08 16:27:15',999.00),(55,35,6,'2020-02-08 16:27:15',600.00),(57,36,2,'2020-02-08 16:29:51',999.00),(58,36,6,'2020-02-08 16:29:51',600.00),(60,37,2,'2020-02-08 16:33:10',999.00),(61,37,6,'2020-02-08 16:33:10',600.00),(63,38,2,'2020-02-08 16:34:46',999.00),(64,38,6,'2020-02-08 16:34:46',600.00),(66,39,2,'2020-02-08 16:36:05',999.00),(67,39,6,'2020-02-08 16:36:05',600.00),(69,40,2,'2020-02-08 16:37:37',999.00),(70,40,6,'2020-02-08 16:37:37',600.00),(72,41,2,'2020-02-08 16:39:33',999.00),(73,41,6,'2020-02-08 16:39:33',600.00),(75,42,2,'2020-02-08 16:42:00',899.00),(76,42,6,'2020-02-08 16:42:00',600.00),(78,43,2,'2020-02-08 16:42:51',899.00),(79,43,6,'2020-02-08 16:42:51',600.00),(80,44,6,'2020-02-11 19:01:34',600.00),(81,46,15,'2020-02-11 21:18:18',899.00),(82,47,15,'2020-02-11 21:19:59',899.00),(83,48,15,'2020-02-11 21:30:09',899.00),(84,49,15,'2020-02-11 21:34:47',899.00);
/*!40000 ALTER TABLE `order_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `product_type` tinyint(4) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `sale_price` decimal(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `front_photo` varchar(255) DEFAULT NULL,
  `front_photo_small` varchar(255) DEFAULT NULL,
  `back_photo` varchar(255) DEFAULT NULL,
  `back_photo_small` varchar(255) DEFAULT NULL,
  `additional_photo1` varchar(255) DEFAULT NULL,
  `additional_photo2` varchar(255) DEFAULT NULL,
  `colors` varchar(255) DEFAULT '',
  `mech` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  `watch_type` tinyint(4) DEFAULT NULL,
  `is_best_seller` tinyint(4) DEFAULT NULL,
  `in_stock` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='watch_type: \r\n1-mens watch\r\n2-ladies watch\r\n3-watch boxes\r\n\r\ncolors:\r\n''black''  -1\r\n''silver''  -2\r\n''gold''   -3\r\n''red''     -4\r\n''brown''-5 \r\n''white'' -6\r\n\r\nmech: \r\nMechanical - 1\r\nAutomatic -2\r\n\r\n\r\nis_best_seller:\r\nyes - 1 \r\nno - 0';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `name`, `product_type`, `price`, `sale_price`, `description`, `front_photo`, `front_photo_small`, `back_photo`, `back_photo_small`, `additional_photo1`, `additional_photo2`, `colors`, `mech`, `created_at`, `deleted_at`, `watch_type`, `is_best_seller`, `in_stock`) VALUES (1,'iphone5s',1,500.00,NULL,'Meet Xperia Z2. An Android phone that allows you to take photos and videos like never before. For moments you’ll keep looking back on forever.','iphone5s-1.jpg','moto360-02-350x380.jpg','iphone5s_gray.jpg','moto360-03-350x380.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',black,white,',1,'2020-01-24 13:56:30',NULL,1,0,1),(2,'hermes',1,999.00,100.00,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','tag-carrer-01-768x816.jpg','iphone5s_gray.jpg','radomir-02.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',brown,',2,'2020-01-24 13:56:30',NULL,1,1,1),(3,'linux',1,700.00,100.00,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','marina-01-350x380.jpg','iphone5s_gray.jpg','marina-02.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',black,',2,'2020-01-24 13:56:30',NULL,1,0,1),(4,'videl',1,800.00,100.00,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','globemaster-01-768x816.jpg','iphone5s_gray.jpg','globemaster-02.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',gold,silver,',2,'2020-01-24 13:56:30',NULL,2,0,1),(5,'box',2,700.00,100.00,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','louis-02-350x380.jpg','iphone5s_gray.jpg','louis-03.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',red,',2,'2020-01-24 13:56:30',NULL,3,0,1),(6,'best seller',1,600.00,NULL,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','louis-03.jpg','iphone5s_gray.jpg','apwatch-02-768x816.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',black,',2,'2020-01-24 13:56:30',NULL,1,1,1),(7,'vumo',1,500.00,NULL,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','marina-04.jpg','iphone5s_gray.jpg','tag-heuer-04.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',black,',2,'2020-01-24 13:56:30',NULL,1,1,1),(8,'hermes',1,999.00,100.00,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','seamaster-sp-02-350x380.jpg','iphone5s_gray.jpg','radomir-02.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',brown,',2,'2020-01-24 13:56:30',NULL,1,1,1),(9,'iphone5s',1,500.00,NULL,'Meet Xperia Z2. An Android phone that allows you to take photos and videos like never before. For moments you’ll keep looking back on forever.','iphone5s-1.jpg','radomir-04.jpg','iphone5s_gray.jpg','moto360-03-350x380.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',black,white,',1,'2020-01-24 13:56:30',NULL,1,1,1),(10,'best seller',1,600.00,NULL,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.
5 mm thick, 21 jewels,\r\nGlucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','louis-03.jpg','iphone5s_gray.jpg','apwatch-02-768x816.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',black,',2,'2020-01-24 13:56:30',NULL,1,0,1),(11,'vumo',1,500.00,NULL,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','radomir-02.jpg','iphone5s_gray.jpg','tag-heuer-04.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',black,',2,'2020-01-24 13:56:30',NULL,1,0,1),(12,'hermes',1,999.00,100.00,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','marina-04.jpg','iphone5s_gray.jpg','radomir-02.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',brown,',2,'2020-01-24 13:56:30',NULL,1,0,1),(13,'best seller',1,600.00,NULL,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','passession-03-350x380.jpg','iphone5s_gray.jpg','apwatch-02-768x816.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',black,',2,'2020-01-24 13:56:30',NULL,1,1,1),(14,'vumo',1,500.00,NULL,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','marina-04.jpg','iphone5s_gray.jpg','tag-heuer-04.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',black,',2,'2020-01-24 13:56:30',NULL,1,1,1),(15,'hermes',1,999.00,100.00,'Hand-wound mechanical, P.5000 calibre, executed entirely by Panerai, 15 ½ lignes, 4.5 mm thick, 21 jewels, Glucydur® balance, 21,600 alternations/hour. KIF Parechoc® anti-shock device. Power reserve 8 days, two barrels. 127 components.','iphone5s_gold.jpg','tag-carrer-01-768x816.jpg','iphone5s_gray.jpg','radomir-02.jpg','iphone5s-1.jpg','iphone5s-1.jpg',',brown,',2,'2020-01-24 13:56:30',NULL,1,1,0);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_rating`
--

DROP TABLE IF EXISTS `product_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `star` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_rating`
--

LOCK TABLES `product_rating` WRITE;
/*!40000 ALTER TABLE `product_rating` DISABLE KEYS */;
INSERT INTO `product_rating` (`id`, `user_id`, `product_id`, `created_at`, `star`) VALUES (1,2,2,'2020-02-01 10:58:31',2),(2,3,2,'2020-02-01 11:28:34',4),(3,5,2,'2020-02-01 14:18:24',5),(4,2,14,'2020-02-03 19:29:36',4),(5,2,15,'2020-02-12 01:48:12',3);
/*!40000 ALTER TABLE `product_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_cart`
--

DROP TABLE IF EXISTS `shopping_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopping_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_cart`
--

LOCK TABLES `shopping_cart` WRITE;
/*!40000 ALTER TABLE `shopping_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `shopping_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_cart_not_in_stock`
--

DROP TABLE IF EXISTS `shopping_cart_not_in_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopping_cart_not_in_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_cart_not_in_stock`
--

LOCK TABLES `shopping_cart_not_in_stock` WRITE;
/*!40000 ALTER TABLE `shopping_cart_not_in_stock` DISABLE KEYS */;
INSERT INTO `shopping_cart_not_in_stock` (`id`, `product_id`, `created_at`, `user_id`) VALUES (99,15,'2020-02-12 01:42:20',2);
/*!40000 ALTER TABLE `shopping_cart_not_in_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_cart_totals`
--

DROP TABLE IF EXISTS `shopping_cart_totals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopping_cart_totals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `shipping` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_cart_totals`
--

LOCK TABLES `shopping_cart_totals` WRITE;
/*!40000 ALTER TABLE `shopping_cart_totals` DISABLE KEYS */;
/*!40000 ALTER TABLE `shopping_cart_totals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES (1,'emil','dr.bakhtiyar.aliyev@gmail.com',NULL,'$2y$10$f4mc8YZcXn1ciz/tO6Q73.bvH8Yyr0j9NfqintDt3fwG3FB.juMfu',NULL,'2020-01-20 11:43:33','2020-01-20 11:43:33'),(2,'emil','emil9453@gmail.com',NULL,'$2y$10$Et7R9mEkf9d9UN8ynsSnbuRZixZnThYHvUIIOLtFJFGxbKUQYksPW',NULL,'2020-01-26 03:42:39','2020-01-26 03:42:39');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wish_list`
--

DROP TABLE IF EXISTS `wish_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wish_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wish_list`
--

LOCK TABLES `wish_list` WRITE;
/*!40000 ALTER TABLE `wish_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `wish_list` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-12  1:48:56

<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConfirmBircartOrder;
use App\Http\Requests\getBircartTable;
use App\Http\Requests\PlaceOrder;
use App\Models\Home;
use App\Models\KapitalApi;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    public function handlePaymentResponse(Request $request)
    {
        $xmlmsg = $request->input('xmlmsg');
        if($xmlmsg!='')
        {
             $redirect_route = KapitalApi::handlePaymentResponse($xmlmsg);
             return redirect()->to($redirect_route);
        }
        else
        {
            Log::error('no xmlmsg in handlePaymentResponse');
        }
    }

    public function paymentProcessing($OrderID,$SessionID)
    {
        sleep(6);
        $redirect_route = KapitalApi::handlePaymentStatusAndRedirect([
            'OrderID' => $OrderID,
            'SessionID' => $SessionID
        ]);
        return redirect()->to($redirect_route);
    }

    public function invalidPayment()
    {
        return view('paymentDeclined');
    }

    public function successfulPayment()
    {
        return view('paymentApproved');
    }

    public function placeOrder(PlaceOrder $request)
    {
        $user_id = Auth::id();

        $validated = $request->validated();
        $not_in_stock = (int)trim($request->input('not_in_stock'));

        if(isset($validated['errors']))
        {
            return response()->json($validated['errors']);
        }

        Log::info('Try place order (placeOrder Controller) user_id : '.$user_id.' request : '.json_encode($request->all()));

        if($not_in_stock == 1)
        {
            $userHasProductsInShoppingCart = Order::checkShoppingCartNotInStockForOrder($user_id);
        }
        else
        {
            $userHasProductsInShoppingCart = Order::checkShoppingCartForOrder($user_id);
        }

        if(!$userHasProductsInShoppingCart)
            return response(['message'=>'no products in shopping cart'], 400);

        $to_name = $request->session()->get('contact_info')['phone'];
        $to_email = $request->session()->get('contact_info')['email'];

        $request = $request->all();
        $request['user_id'] = $user_id;

        $order_id = Order::placeOrder($request);

        if($order_id>0)
        {
            // send mail
            $request['order_id'] = $order_id;
            $request['to_name'] = $to_name;
            $request['to_email'] = $to_email;
            Order::sendOrderCompletedMail($request);
            return response(null, 200);
        }
        else
            return response(['message'=>'could not apply order'], 400);
    }

    public function getBircartTable(getBircartTable $request)
    {
        $user_id = Auth::id();
        $validated = $request->validated();
        $not_in_stock = (int)trim($request->input('not_in_stock'));

        if(isset($validated['errors']))
        {
            return response()->json($validated['errors']);
        }

        if($not_in_stock == 1)
        {
            $userHasProductsInShoppingCart = Order::checkShoppingCartNotInStockForOrder($user_id);
        }
        else
        {
            $userHasProductsInShoppingCart = Order::checkShoppingCartForOrder($user_id);
        }

        if(!$userHasProductsInShoppingCart)
            return response(['message'=>'no products in shopping cart'], 400);

        $request = $request->all();
        $request['user_id'] = $user_id;
        $tableInfo = Order::getInfoForBircartTable($request);

        if($tableInfo['totalPrice'] == 0)
        {
            return response(['message'=>'no products in shopping cart'], 400);
        }

        return response()->json($tableInfo);
    }

    public function makeOrder(Request $request)
    {
        if (!Auth::check()) return response()->json(['redirect' => 'login']);

        $user_id = Auth::id();

        $product_id = (int)$request->get('product_id');

        if ($product_id == 0) return response('',400);

        Log::info('Try make order (makeOrder Controller) user_id: '.$user_id.' request : '.json_encode($request->all()));

        Order::insertToShoppingCartNotInStock($user_id, $product_id);

        return response()->json(['status' => 'ok']);
    }

    public function confirmApiOrder(ConfirmBircartOrder $request)
    {
        $user_id = Auth::id();
        $validated = $request->validated();
        $not_in_stock = (int)trim($request->input('not_in_stock'));

        if(isset($validated['errors']))
        {
            return response()->json($validated['errors']);
        }

        Log::info('Try make order (confirmApiOrder Controller) user_id: '.$user_id.' request : '.json_encode($request->all()));

        if($not_in_stock == 1)
        {
            $userHasProductsInShoppingCart = Order::checkShoppingCartNotInStockForOrder($user_id);
        }
        else
        {
            $userHasProductsInShoppingCart = Order::checkShoppingCartForOrder($user_id);
        }

        if(!$userHasProductsInShoppingCart)
            return response(['message'=>'no products in shopping cart'], 400);

        $request = $request->all();
        $request['user_id'] = $user_id;

        $result = Order::confirmApiOrder($request);

        if($result['status'] == 'ok')
        {
            return  response()->json(['url' => $result['url']]);
        }
        else
        {
            return response(['message'=>$result['message']], 503);
        }
    }
}

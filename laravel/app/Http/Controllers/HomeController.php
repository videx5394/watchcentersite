<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConfirmBircartOrder;
use App\Http\Requests\contactUsMail;
use App\Http\Requests\getBircartTable;
use App\Http\Requests\PlaceOrder;
use App\Models\Home;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        //pagination
        $perPage = in_array($request->get('count'), [16, 28, 40]) ? (int)$request->get('count') : 16;
        $order_by = in_array($request->get('orderby'), ['price-desc', 'price-asc']) ? explode('-', $request->get('orderby')) : explode('-', 'price-asc');
        $order_by_column = $order_by[0];
        $order_by_direction = $order_by[1];
        $page = (int)$request->get('page') > 0 ? (int)$request->get('page') : 1;
        $offset = ($page - 1) * $perPage;

        //filters
        $product_type = in_array($request->get('product_type'),['watches','watch_boxes']) ? $request->get('product_type') : 'watches';
        $watch_type = in_array($request->get('watch_type'), ['mens', 'ladies']) ? $request->get('watch_type') : null;
        $min_price = (int)$request->get('min_price') > 0 ? (int)$request->get('min_price') : 0;
        $max_price = (int)$request->get('max_price') > 0 ? (int)$request->get('max_price') : 1500;
        $colors = Home::filterColorsArray($request->get('filter_colors'));
        $mech = in_array($request->get('mech'), ['mechanical','quartz']) ? $request->get('mech') : null;
        $on_sale = in_array($request->get('on_sale'), ['on_sale', 'all']) ? $request->get('on_sale') : 'all';


        $products = Home::getProductsWithPagination($order_by_column, $order_by_direction, $perPage, $offset, $product_type, $watch_type, $min_price, $max_price, $colors, $mech, $on_sale);
        $total_count = Home::getProductsCount($product_type, $watch_type, $min_price, $max_price, $colors, $mech, $on_sale);
        $total_page_count = ceil($total_count / $perPage);
        //dd($products);
        $grouped_count = Home::getGroupedCount();
        $watches_count = Home::getWatchesCountByGroupedArray($grouped_count);
        $mens_watches_count = Home::getMensWatchCountByGroupedArray($grouped_count);
        $womens_watches_count = Home::getWomensWatchCountByGroupedArray($grouped_count);
        $watch_boxes_count = Home::getWatchBoxesCountByGroupedArray($grouped_count);
        $on_sale_count = Home::getOnSaleCount();
        $mech_count = Home::getMechCount();
        $best_sellers = Home::getBestSellers();

        return view('home', [
            'products' => collect($products),
            'total_count' => $total_count,
            'total_page_count' => $total_page_count,
            'count' => $request->get('count'),
            'orderby' => $request->get('orderby'),
            'perPage' => $perPage,
            'page' => $page,
            'from' => $offset,
            'to' => $offset + $perPage,
            'request' => $request,
            'full_url' => $request->fullUrl(),
            'watches_count' => $watches_count,
            'mens_watches_count' => $mens_watches_count,
            'womens_watches_count' => $womens_watches_count,
            'watch_boxes_count' => $watch_boxes_count,
            'on_sale_count' => $on_sale_count,
            'min_price' => $min_price,
            'max_price' => $max_price,
            'mech_count' => $mech_count,
            'colors' => $colors,
            'allColors' => Home::$allColors,
            'best_sellers' => $best_sellers
        ]);
    }

    public function findProduct(Request $request)
    {
        $search = $request->get('query');
        $results = Home::getProductByProductName($search);

        $suggestions = Home::getSuggestionsFromQueryResult($results);

        return response()->json(['suggestions' => $suggestions]);
    }

    public function contactUs(Request $request)
    {
        return view('contactUs');
    }

    public function ordersNotInStock(Request $request)
    {
        //pagination
        $perPage = in_array($request->get('count'), [16, 28, 40]) ? (int)$request->get('count') : 16;
        $order_by = in_array($request->get('orderby'), ['price-desc', 'price-asc']) ? explode('-', $request->get('orderby')) : explode('-', 'price-asc');
        $order_by_column = $order_by[0];
        $order_by_direction = $order_by[1];
        $page = (int)$request->get('page') > 0 ? (int)$request->get('page') : 1;
        $offset = ($page - 1) * $perPage;

        $total_count = Home::getNotInStockProductsCount();
        $total_page_count = floor($total_count / $perPage);

        $products = Home::getNotInStockProducts($order_by_column, $order_by_direction, $perPage, $offset);
        return view('ordersNotInStock',[
            'products' => $products,
            'total_count' => $total_count,
            'total_page_count' => $total_page_count,
            'count' => $request->get('count'),
            'orderby' => $request->get('orderby'),
            'perPage' => $perPage,
            'page' => $page,
            'from' => $offset,
            'to' => $offset + $perPage,
            'request' => $request,
            'full_url' => $request->fullUrl(),
        ]);

    }

    public function refundPolicy(Request $request)
    {
        return view('refundPolicy');
    }

    public function aboutUs(Request $request)
    {
        return view('aboutUs');
    }

    public function product(Request $request, $id)
    {
        $product = Home::getProductById($id);

        if (count($product) > 0) {
            //dd($product);
            return view('product', [
                'product' => $product[0]
            ]);
        } else
            return redirect()->back();
    }

    public function addToCart(Request $request, $id)
    {
        return;
    }

    public function addToCartAjax(Request $request)
    {

        if (!Auth::check()) return response()->json(['redirect' => 'login']);

        $user_id = Auth::id();

        $product_id = (int)$request->get('product_id');

        if ($product_id == 0) return;

        Home::insertToShoppingCart($user_id, $product_id);

        $products_in_card = Home::getProductsFromShoppingCart($user_id);


        $html = Home::getProductsHtmlForShoppingCart($products_in_card);

        return response()->json($html);
    }

    public function addToWishList(Request $request, $id)
    {
        if (!Auth::check()) return response()->json(['redirect' => 'login']);

        $user_id = Auth::id();

        if ($id != 0)
        {
            Home::insertToWishList($user_id, $id);
        }

        return redirect()->back();

    }

    public function addToWishListAjax(Request $request)
    {
        if (!Auth::check()) return response()->json(['redirect' => 'login']);

        $user_id = Auth::id();

        $product_id = (int)$request->get('product_id');

        if ($product_id == 0) return;

        Home::insertToWishList($user_id, $product_id);

        $html = '{"prod_id":359,"result":"true","message":"Product added!","fragments":{"yith-wcwl-add-to-wishlist add-to-wishlist-359  wishlist-fragment on-first-load":"\n<div class=\"yith-wcwl-add-to-wishlist add-to-wishlist-359 exists wishlist-fragment on-first-load\" data-fragment-ref=\"359\" data-fragment-options=\"{&quot;base_url&quot;:&quot;https:\\\/\\\/woodstock.temashdesign.com\\\/electronics\\\/product-category\\\/cellphones\\\/smartphones?product_cat=cellphones\\\/smartphones&quot;,&quot;wishlist_url&quot;:&quot;https:\\\/\\\/woodstock.temashdesign.com\\\/electronics\\\/wishlist\\\/view\\\/IBJU1AILKZ1B\\\/&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:&quot;359&quot;,&quot;parent_product_id&quot;:&quot;359&quot;,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse Wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in the wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:false,&quot;&quot;:&quot;add_to_wishlist&quot;}\">\n            \n            <!-- ADD TO WISHLIST -->\n            \n<!-- ADDED TO WISHLIST MESSAGE -->\n<div class=\"yith-wcwl-wishlistaddedbrowse\">\n\t<span class=\"feedback\">\n                Product added!    <\/span>\n\t<a href=\"https:\/\/woodstock.temashdesign.com\/electronics\/wishlist\/view\/IBJU1AILKZ1B\/\" rel=\"nofollow\" data-title=\"Browse Wishlist\">\n\t\t\t\tBrowse Wishlist\t<\/a>\n<\/div>\n            <!-- COUNT TEXT -->\n            \n        \t<\/div>"},"user_wishlists":[{"id":241591,"wishlist_name":"My wishlist on Woodstock Electronics","add_to_this_wishlist_url":"\/electronics\/wp-admin\/admin-ajax.php?add_to_wishlist=359&wishlist_id=241591"}],"wishlist_url":"https:\/\/woodstock.temashdesign.com\/electronics\/wishlist\/view\/IBJU1AILKZ1B\/"}';

        return response()->json($html);
    }

    public function getShoppingCartContent()
    {
        $user_id = Auth::id();

        if (isset($user_id)) {
            $products_in_card = Home::getProductsFromShoppingCart($user_id);

            $html = Home::getProductsHtmlForShoppingCart($products_in_card);

            return response()->json($html);
        }
        return '{}';
    }

    public function rateProduct(Request $request)
    {
        $product_id = $request->input('product_id');
        $star = $request->input('star');

        $user_id = Auth::id();

        if (isset($user_id)) {
            Home::rateProduct($product_id, $user_id, $star);
            return 'ok';
        }
        return 'login';
    }

    public function viewCart(Request $request)
    {
        $user_id = Auth::id();

        $products = Home::getProductsFromShoppingCart($user_id);

        $price = 0;

        foreach ($products as $product) {
            $price += $product->price - (int)$product->sale_price;
        }

        return view('viewCart', [
            'products' => $products,
            'price' => $price
        ]);
    }

    public function applyCoupon(Request $request)
    {
        $couponName = trim($request->input('coupon'));
        $user_id = Auth::id();
        Home::applyCoupon($user_id, $couponName);

        if ($couponName == '') return response()->json(['sale'=>0]);
        $coupon = Home::getCouponSaleAndId($couponName);
        return response()->json(['sale'=>$coupon->sale]);
    }

    public function checkout(Request $request)
    {
        $user = Auth::user();
        $email = $user->email;
        $user_id = $user->id;

        $shipping = trim($request->get('shipping'));
        $not_in_stock = (int)trim($request->get('not-in-stock'));

        if (in_array($shipping,['baku','cities']))
        {
            Home::applyShoppingCartTotals($user_id,$shipping);
        }

        if($not_in_stock == 1)
        {
            $products = Home::getProductsFromShoppingCartNotInStock($user_id);
        }
        else
        {
            $products = Home::getProductsFromShoppingCart($user_id);
        }

        $totals = Home::getUserShoppingCartTotals($user_id);

        $price = 0;

        foreach ($products as $product) {
            $price += $product->price - (int)$product->sale_price;
        }

        return view('checkout', [
            'email' => $email,
            'products' => $products,
            'price' => $price,
            'totals' => $totals,
            'not_in_stock' => $not_in_stock
        ]);
    }

    public function contactUsMail(contactUsMail $request)
    {
        $validated = $request->validated();

        if(isset($validated['errors']))
        {
            return response()->json($validated['errors']);
        }
        $mailResult = Home::contactUsMail($request);

        if(!$mailResult)
            return response(['message'=>'mail is not sent'], 400);
    }

    public function removeFromCardAjax(Request $request)
    {
        $product_id = (int)$request->input('product_id');

        $user_id = Auth::id();

        if ($product_id == 0) response(null, 400);

        Home::removeProductFromCart($product_id, $user_id);

        $products_in_card = Home::getProductsFromShoppingCart($user_id);

        $html = Home::getProductsHtmlForShoppingCart($products_in_card);

        return response()->json($html);
    }

    public function removeFromWishLIst(Request $request)
    {
        $product_id = (int)$request->input('product_id');

        $user_id = Auth::id();

        if ($product_id == 0) response(null, 400);

        Home::removeFromWishLIst($product_id, $user_id);

        return 'ok';
    }

    public function wishList(Request $request)
    {
        $user_id = Auth::id();

        $products = Home::getProductsFromWishList($user_id);

        $subtotal = 0;

        return view('wishList', ['products' => $products]);
    }
}

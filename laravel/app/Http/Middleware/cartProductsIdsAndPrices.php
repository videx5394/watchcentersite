<?php

namespace App\Http\Middleware;

use App\Models\Home;
use Closure;
use Illuminate\Support\Facades\Auth;

class cartProductsIdsAndPrices
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_id = Auth::id();

        $request->shopping_cart = [
            'ids' => [],
            'price' => 0,
            'count' => 0
        ];

        $request->wish_list = [
            'ids' => [],
            'price' => 0,
            'count' => 0
        ];


        if(isset($user_id))
        {
            $request->shopping_cart = Home::getShoppingCartProductsIdsAndPrices($user_id);
            $request->wish_list = Home::getWishListProductsIdsAndPrices($user_id);
        }

        return $next($request);
    }
}

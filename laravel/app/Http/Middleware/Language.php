<?php

namespace App\Http\Middleware;

use App\Language as LanguageModel;
use Closure;
use Illuminate\Support\Facades\App;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = session('locale');
        $locale = in_array($locale, ['az','ru','en']) ? $locale : config('app.fallback_locale');
        App::setLocale($locale);
        $request->attributes->add(['locale'=>$locale]);
        return $next($request);
    }
}

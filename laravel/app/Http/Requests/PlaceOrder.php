<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlaceOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|max:255',
            'surname' => 'required|max:255',
            'address' => 'required|max:1000',
            'phone' => 'required|max:100',
            'email' => 'required|max:100|email',
            'shipping' => 'required|in:' . implode(',', ['baku','cities']),
            'coupon' => 'nullable',
            'note' => 'nullable',
            'payment' => 'required|in:' . implode(',', ['visa_master','bircart','cash']),
        ];
    }
}

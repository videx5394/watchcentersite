<?php

namespace App\Models;

use App\Interfaces\Orderable;
use Illuminate\Support\Facades\Log;

/*The order can have the following statuses:
CREATED (assigned after the OrderID and SessionID parameters are generated but
before the order is paid)
ON-LOCK (assigned to avoid payment duplication)
The ON-LOCK status is assigned when executing the PayOrder procedure. Once the
authorization process is complete; the ON-LOCK status is changed to APPROVED
or DECLINED.
ON-PAYMENT
APPROVED
CANCELED (operation execution is interrupted by customer)
DECLINED (for example, the Prefix not found error occurred)
*/

class KapitalApi implements Orderable
{
    private static $statuses = [
        'CREATED' => 'CREATED',
        'APPROVED' => 'APPROVED',
        'CANCELED' => 'CANCELED',
        'DECLINED' => 'DECLINED',
        'ON-LOCK' => 'ON-LOCK',
        'ON-PAYMENT' => 'ON-PAYMENT',
    ];

    private static $merchantName = 'E1000010';
    private static $serviceUrl = 'https://e-commerce.kapitalbank.az:5443/Exec';
    private static $sslKeyLocation = '';
    private static $sslCertLocation = '';

    public static function getSllKeyLocation()
    {
        return config('app.payment_api_ssl_key_location');
    }

    public static function getHostingUrl()
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    }


    public static function getSllCertLocation()
    {
        return config('app.payment_api_ssl_cert_location');
    }

    public static function handlePaymentStatusAndRedirect($data)
    {
        $orderStatusResult = KapitalApi::getPaymentStatus($data);

        $redirect_route = KapitalApi::handlePaymentProcessing($data);

        if($orderStatusResult['status'] == 'ok')
        {
            switch ($orderStatusResult['order_status'])
            {
                case KapitalApi::$statuses['DECLINED']:
                    $redirect_route = KapitalApi::handlePaymentDeclined($data);
                    break;
                case KapitalApi::$statuses['APPROVED']:
                    $redirect_route = KapitalApi::handlePaymentApproved($data);
                    break;
                case KapitalApi::$statuses['CANCELED']:
                    $redirect_route = KapitalApi::handlePaymentCanceled($data);
                    break;
                default:
                    $redirect_route = KapitalApi::handlePaymentProcessing($data);
            }
            return $redirect_route;
        }
        else
            return $redirect_route;
    }

    public static function handlePaymentResponse($xmlmsg)
    {
        $redirect_route = 'invalidPayment';
        try {
            $xml = simplexml_load_string($xmlmsg);
            $json = json_encode($xml);
            $result = json_decode($json, TRUE);
            $redirect_route = KapitalApi::handlePaymentStatusAndRedirect($result);
            return $redirect_route;
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' - ' . $e->getFile() . ' ' . $e->getLine());
            return $redirect_route;
        }
    }

    public static function handlePaymentDeclined($data)
    {
        return 'invalidPayment';
    }

    public static function handlePaymentApproved($data)
    {
        $request = Order::copyFromTempOrderToOrderByOrderId($data["OrderID"]);
        if($request['order_id']>0)
        {
            // send mail
            $request['to_name'] = session()->get('contact_info')['phone'];
            $request['to_email'] = session()->get('contact_info')['email'];
            Order::sendOrderCompletedMail($request);
            return 'successfulPayment';
        }
        else
        {
            Log::error('could not handlePaymentApproved');
            return 'errors.503';
        }
    }

    public static function handlePaymentProcessing($data)
    {
        return sprintf('paymentProcessing/%s/%s',$data['OrderID'],$data['SessionID']);
    }

    public static function handlePaymentCanceled($data)
    {
        return 'invalidPayment';
    }

    public static function getPaymentStatuses(): array
    {
        return KapitalApi::$statuses;
    }

    public static function createPayment($data): array
    {
        $division_count = $data['division_count'] == 1 ? '' : $data['division_count'];
        $string = '<?xml version="1.0" encoding="UTF-8"?>
                        <TKKPG>
                           <Request>
                              <Operation>CreateOrder</Operation>
                              <Language>AZ</Language>
                              <Order>
                                 <OrderType>Purchase</OrderType>
                                 <Merchant>' . KapitalApi::$merchantName . '</Merchant>
                                 <Amount>' . $data['totalPrice'] . '</Amount>
                                 <Currency>944</Currency>
                                 <Description>' . $data['transaction_string'] . '/TAKSIT=' . $division_count . '</Description>
                                 <ApproveURL>'.KapitalApi::getHostingUrl().'/checkout/approved</ApproveURL>
                                 <CancelURL>'.KapitalApi::getHostingUrl().'/checkout/canceled</CancelURL>
                                 <DeclineURL>'.KapitalApi::getHostingUrl().'/checkout/declined</DeclineURL>
                              </Order>
                           </Request>
                        </TKKPG>';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => KapitalApi::$serviceUrl,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSLKEY => KapitalApi::getSllKeyLocation(),
            CURLOPT_SSLCERT => KapitalApi::getSllCertLocation(),
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $string,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/xml",
            ),
        ));

        $response = curl_exec($curl);
        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $result = json_decode($json, TRUE);
        curl_close($curl);


        Log::info('CreateOrder: '.$string.' - CreateOrder-Result: '.$json);

        $status = $result['Response']['Status'];

        if ($status != '00') {
            Log::error('CreateOrder api responded with status' . $status);
            return ['status' => 'error', 'message' => 'CreateOrder api responded with status' . $status];
        }

        $order_id = $result['Response']['Order']['OrderID'];
        $session_id = $result['Response']['Order']['SessionID'];
        $order_url = $result['Response']['Order']['URL'];

        $url = sprintf("%s?ORDERID=%s&SESSIONID=%s&expm=05expy=17", $order_url, $order_id, $session_id);
        return ['status' => 'ok', 'url' => $url,'order_id'=>$order_id,'session_id'=>$session_id];
    }

    public static function getPaymentStatus($data)
    {
        $string = '<?xml version="1.0" encoding="UTF-8"?>
                    <TKKPG>
                        <Request>
                            <Operation>GetOrderStatus</Operation>
                            <Language>AZ</Language>
                            <Order>
                                <Merchant>' . KapitalApi::$merchantName . '</Merchant>
                                <OrderID>' . $data['OrderID'] . '</OrderID>
                            </Order>
                            <SessionID>' . $data['SessionID'] . '</SessionID>
                        </Request>
                    </TKKPG>';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => KapitalApi::$serviceUrl,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSLKEY => KapitalApi::getSllKeyLocation(),
            CURLOPT_SSLCERT => KapitalApi::getSllCertLocation(),
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $string,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/xml",
            ),
        ));

        $response = curl_exec($curl);
        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $result = json_decode($json, TRUE);
        curl_close($curl);

        Log::info('GetOrderStatus: '.$string.' - GetOrderStatus-Result: '.$json);

        $status = $result['Response']['Status'];

        if ($status != '00') {
            Log::error('GetOrderStatus api responded with status' . $status);
            return ['status' => 'error', 'message' => 'GetOrderStatus api responded with status' . $status];
        }

        $order_status = $result['Response']['Order']['OrderStatus'];
        return ['status' => 'ok', 'order_status' => $order_status];
    }
}

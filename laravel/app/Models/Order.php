<?php

namespace App\Models;

use http\Env\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class Order
{
    public static function sendOrderCompletedMail(array $request)
    {
        $to_name = $request['to_name'];
        $to_email = $request['to_email'];

        try {
            $products = Order::getDataForOrderMail($request['order_id']);

            $data = [
                'request' => $request,
                'products' => $products
            ];

            Mail::send("orderMail", $data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject("New Order!");
                $message->from(config('app.from_mail'), "New Order!");
            });

            if (Mail::failures()) {
                return false;
                // return response showing failed emails
            }
            return true;
        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }
    }

    public static function insertToShoppingCartNotInStock($user_id, $product_id)
    {
        DB::transaction(function () use ($user_id, $product_id) {
            Order::removeFromShoppingCartNotInStock($user_id);
            DB::insert("INSERT INTO `shopping_cart_not_in_stock` (`user_id`,`product_id`) VALUES($user_id,$product_id)");
        });
    }

    public static function getOrdersTotalPrice($user_id, $shipping, $coupon, $table)
    {
        try {
            $totalPriceQuery = "SELECT 
                  ROUND((SUM(price - COALESCE(sale_price,0)) - 
                  SUM(price - COALESCE(sale_price,0))*COALESCE(coupons.sale,0)/100 + 
                  CASE WHEN $shipping = 'baku' THEN 0 WHEN $shipping =  'cities' THEN 3 ELSE 0 END ),2) as total
                  FROM $table 
                  LEFT JOIN products ON $table.product_id = products.id
                  LEFT JOIN coupons ON coupons.id = (SELECT id FROM coupons WHERE name=$coupon)
                  WHERE $table.user_id = $user_id";

            $result = DB::select($totalPriceQuery);

            if (count($result) == 0) {
                return 0;
            } else {
                return (int)$result[0]->total;
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' - ' . $e->getFile() . ' ' . $e->getLine());
            return 0;
        }
    }

    public static function getInfoForBircartTable(array $request): array
    {
        $user_id = DB::connection()->getPdo()->quote($request['user_id']);
        $shipping = DB::connection()->getPdo()->quote($request['shipping']);
        $coupon = DB::connection()->getPdo()->quote($request['coupon']);
        $not_in_stock = (int)$request['not_in_stock'];
        $table = $not_in_stock == 0 ? 'shopping_cart' : 'shopping_cart_not_in_stock';

        $totalPrice = Order::getOrdersTotalPrice($user_id, $shipping, $coupon, $table);
        return [
            'totalPrice' => $totalPrice,
            'departed' => [
                '2' => round($totalPrice / 2, 2),
                '3' => round($totalPrice / 3, 2),
                '6' => round($totalPrice / 6, 2),
            ]
        ];
    }

    public static function placeOrder(array $request): int
    {
        $user_id = DB::connection()->getPdo()->quote($request['user_id']);
        $coupon = DB::connection()->getPdo()->quote($request['coupon']);
        $name = DB::connection()->getPdo()->quote($request['firstName']);
        $surname = DB::connection()->getPdo()->quote($request['surname']);
        $address = DB::connection()->getPdo()->quote($request['address']);
        $phone = DB::connection()->getPdo()->quote($request['phone']);
        $email = DB::connection()->getPdo()->quote($request['email']);
        $note = DB::connection()->getPdo()->quote($request['note']);
        $shipping = DB::connection()->getPdo()->quote($request['shipping']);
        $payment = DB::connection()->getPdo()->quote($request['payment']);
        $not_in_stock = (int)$request['not_in_stock'];

        $table = $not_in_stock == 0 ? 'shopping_cart' : 'shopping_cart_not_in_stock';

        return DB::transaction(function () use ($not_in_stock, $table, $user_id, $coupon, $name, $surname, $address, $phone, $email, $note, $shipping, $payment) {

            try {
                $insertOrderQuery = "INSERT INTO `order` (user_id, coupon_id, status, `name`, surname, address, phone, email, note, shipping, payment,total_price) 
                            VALUES ($user_id ,(SELECT id FROM coupons WHERE name=$coupon), 0, $name, $surname, $address, $phone, $email, $note, $shipping, $payment,(SELECT 
                                                ROUND((SUM(price - COALESCE(sale_price,0)) - 
                                                SUM(price - COALESCE(sale_price,0))*COALESCE(coupons.sale,0)/100 + 
                                                CASE WHEN $shipping = 'baku' THEN 0 WHEN $shipping =  'cities' THEN 3 ELSE 0 END ),2)
                                                FROM $table 
                                                LEFT JOIN products ON $table.product_id = products.id
                                                LEFT JOIN coupons ON coupons.id = (SELECT id FROM coupons WHERE name=$coupon)
                                                WHERE $table.user_id = $user_id))";


                DB::statement($insertOrderQuery);

                $order_id = DB::select("SELECT id FROM `order` WHERE user_id = $user_id ORDER BY id DESC LIMIT 1");

                if ($not_in_stock == 1) {
                    Order::copyProductsFromCartNotInStockToOrder($user_id, $order_id[0]->id);
                    Order::removeFromShoppingCartNotInStock($user_id);
                } else {
                    Order::copyProductsFromCartToOrder($user_id, $order_id[0]->id);
                    Order::removeAllFromShoppingCartTotals($user_id);
                    Order::removeFromShoppingCart($user_id);
                }
                return $order_id[0]->id;
            } catch (\Exception $e) {
                Log::error($e->getMessage() . ' - ' . $e->getFile() . ' ' . $e->getLine());
                return 0;
            }
        });
    }

    public static function removeAllFromShoppingCartTotals($user_id)
    {
        DB::delete("DELETE FROM `shopping_cart_totals` WHERE user_id = $user_id");
    }

    public static function removeFromShoppingCart($user_id)
    {
        DB::delete("DELETE FROM `shopping_cart` WHERE user_id = $user_id");
    }

    public static function copyProductsFromCartToOrder($user_id, $order_id)
    {
        DB::insert("INSERT INTO order_product (product_id, order_id,price) SELECT product_id, $order_id,(SELECT (price - COALESCE(sale_price,0)) as price FROM products WHERE  products.id = shopping_cart.product_id) FROM shopping_cart WHERE user_id = $user_id;");
    }

    public static function removeFromShoppingCartNotInStock($user_id)
    {
        DB::delete("DELETE FROM `shopping_cart_not_in_stock` WHERE user_id = $user_id");
    }

    public static function copyProductsFromCartNotInStockToOrder($user_id, $order_id)
    {
        DB::insert("INSERT INTO order_product (product_id, order_id,price) SELECT product_id, $order_id,(SELECT (price - COALESCE(sale_price,0)) as price FROM products WHERE  products.id = shopping_cart_not_in_stock.product_id) FROM shopping_cart_not_in_stock WHERE user_id = $user_id;");
    }

    public static function checkShoppingCartForOrder($user_id): bool
    {
        $data = DB::select("SELECT id FROM shopping_cart WHERE user_id = $user_id");

        if (count($data) > 0) {
            return true;
        } else
            return false;
    }

    public static function checkShoppingCartNotInStockForOrder($user_id): bool
    {
        $data = DB::select("SELECT id FROM shopping_cart_not_in_stock WHERE user_id = $user_id");

        if (count($data) > 0) {
            return true;
        } else
            return false;
    }

    public static function removeOrderTransaction($transaction_id)
    {
        DB::delete("DELETE FROM `temp_order_transactions` WHERE id = $transaction_id");
    }

    public static function saveDataToTempOrder(array $request): int
    {
        $user_id = DB::connection()->getPdo()->quote($request['user_id']);
        $coupon = DB::connection()->getPdo()->quote($request['coupon']);
        $name = DB::connection()->getPdo()->quote($request['firstName']);
        $surname = DB::connection()->getPdo()->quote($request['surname']);
        $address = DB::connection()->getPdo()->quote($request['address']);
        $phone = DB::connection()->getPdo()->quote($request['phone']);
        $email = DB::connection()->getPdo()->quote($request['email']);
        $note = DB::connection()->getPdo()->quote($request['note']);
        $shipping = DB::connection()->getPdo()->quote($request['shipping']);
        $payment = DB::connection()->getPdo()->quote($request['payment']);
        $not_in_stock = (int)$request['not_in_stock'];

        $insertOrderQuery = "INSERT INTO `temp_order` (user_id, coupon, status, `name`, surname, address, phone, email, note, shipping, payment,not_in_stock) 
                            VALUES ($user_id ,$coupon, 0, $name, $surname, $address, $phone, $email, $note, $shipping, $payment,$not_in_stock)";
        DB::statement($insertOrderQuery);
        $temp_order_id = DB::select("SELECT id FROM `temp_order` WHERE user_id = $user_id ORDER BY id DESC LIMIT 1");
        return $temp_order_id[0]->id;
    }

    public static function createOrderTransaction($user_id, $temp_order_id)
    {
        return DB::transaction(function () use ($user_id, $temp_order_id) {
            try {
                DB::insert("INSERT INTO `temp_order_transactions` (user_id,transaction_status_id,temp_order_id) VALUES($user_id,1,$temp_order_id)");
                DB::select("SELECT id FROM `temp_order_transactions` WHERE user_id = $user_id ORDER BY id DESC LIMIT 1");
                $lastId = DB::select("SELECT id FROM `temp_order_transactions` WHERE user_id = $user_id ORDER BY id DESC LIMIT 1");
                return $lastId[0]->id;
            } catch (\Exception $e) {
                Log::error($e->getMessage() . ' - could not createOrderTransaction');
                return 0;
            }
        });
    }

    public static function setTransactionOrderIdAndSessionIdFromApi($transaction_id,$order_id,$session_id)
    {
        return DB::update("UPDATE `temp_order_transactions` SET transaction_order_id='$order_id',transaction_session_id='$session_id' WHERE id=$transaction_id");
    }

    public static function removeFromTempOrderByTransactionOrderId($transactionOrderId)
    {
        DB::delete("DELETE temp_order , temp_order_transactions  FROM temp_order  INNER JOIN temp_order_transactions  
                            WHERE temp_order.id = temp_order_transactions.temp_order_id AND temp_order_transactions.transaction_order_id = $transactionOrderId");
    }

    public static function confirmApiOrder(array $request)
    {
        try {
            $temp_order_id = Order::saveDataToTempOrder($request);
            $transaction_id = Order::createOrderTransaction($request['user_id'], $temp_order_id);

            if ($transaction_id == 0) {
                return ['status' => 'error', 'message' => 'could not place order'];
            }
            $transaction_string = str_pad($transaction_id, 5, '0', STR_PAD_LEFT);
            $user_id = DB::connection()->getPdo()->quote($request['user_id']);
            $shipping = DB::connection()->getPdo()->quote($request['shipping']);
            $coupon = DB::connection()->getPdo()->quote($request['coupon']);
            $not_in_stock = (int)$request['not_in_stock'];
            $table = $not_in_stock == 0 ? 'shopping_cart' : 'shopping_cart_not_in_stock';
            $totalPrice = Order::getOrdersTotalPrice($user_id, $shipping, $coupon, $table)*100;
            $division_count = isset($request['divisionCount']) ? $request['divisionCount'] : 1;

            $apiResult = KapitalApi::createPayment([
                'totalPrice' => $totalPrice,
                'transaction_string' => $transaction_string,
                'division_count' => $division_count,
            ]);

            if($apiResult['status'] == 'ok')
            {
                $updateStatus = Order::setTransactionOrderIdAndSessionIdFromApi($transaction_id,$apiResult['order_id'],$apiResult['session_id']);
                if($updateStatus == 1)
                {
                    return $apiResult;
                }
                else
                {
                    $message = 'message could not setTransactionOrderIdAndSessionIdFromApi';
                    Log::error($message);
                    return ['status'=> 'error', $message];
                }
            }
            else
                return $apiResult;

        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' - ' . $e->getFile() . ' ' . $e->getLine());
            return ['status'=>'error','message'=>$e->getMessage() . ' - ' . $e->getFile() . ' ' . $e->getLine()];
        }
    }

    public static function getDataForOrderMail($order_id)
    {
        return DB::select("SELECT order_product.price,products.name as name,`order`.total_price,coupons.`name` as coupon FROM `products` 
                        LEFT JOIN order_product ON products.id = order_product.product_id
                        LEFT JOIN `order` ON order_product.order_id = `order`.id
                        LEFT JOIN `coupons` ON coupons.id = `order`.coupon_id
                        WHERE `order`.id = $order_id ");
    }

    public static function copyFromTempOrderToOrderByOrderId($transactionOrderId)
    {
        try {
            $order = DB::select("SELECT * FROM temp_order_transactions LEFT JOIN temp_order ON temp_order.id = temp_order_transactions.temp_order_id WHERE temp_order_transactions.transaction_order_id = $transactionOrderId");

            if (count($order) > 0) {
                $request = [
                    'user_id' => $order[0]->user_id,
                    'coupon' => $order[0]->coupon,
                    'firstName' => $order[0]->name,
                    'surname' => $order[0]->surname,
                    'address' => $order[0]->address,
                    'phone' => $order[0]->phone,
                    'email' => $order[0]->email,
                    'note' => $order[0]->note,
                    'shipping' => $order[0]->shipping,
                    'payment' => $order[0]->payment,
                    'not_in_stock' => $order[0]->not_in_stock,
                ];

                $order_id = Order::placeOrder($request);
                if ($order_id > 0) {
                    $request['order_id'] = $order_id;
                    Order::removeFromTempOrderByTransactionOrderId($transactionOrderId);
                    return $request;
                } else {
                    Log::error('Could not place Order with transaction_id ' . $transactionOrderId);
                    return [];
                }
            } else {
                Log::error('Error in transaction_id ' . $transactionOrderId);
                return [];
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' - ' . $e->getFile() . ' ' . $e->getLine());
            return [];
        }
    }
}

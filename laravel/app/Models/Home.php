<?php


namespace App\Models;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class Home
{
    public static $allColors = ['black', 'silver', 'gold', 'red', 'brown', 'white'];

    private static function getColorsFilter($colors)
    {
        $colors_filter = '1=1';

        if (count($colors) > 0) {
            $colors_filter = ' ( ';
            foreach ($colors as $key => $color) {
                if ($key == 0)
                    $colors_filter .= 'colors LIKE "%' . $color . '%"';
                else
                    $colors_filter .= ' OR colors LIKE "%' . $color . '%"';
            }
            $colors_filter .= ' ) ';
        }
        return $colors_filter;
    }

    public static function getProductsWithPagination($order_by_column, $order_by_direction, $perPage, $offset, $product_type, $watch_type, $min_price, $max_price, $colors, $mech, $on_sale)
    {
        $product_type = DB::connection()->getPdo()->quote($product_type);
        $min_price = DB::connection()->getPdo()->quote($min_price);
        $max_price = DB::connection()->getPdo()->quote($max_price);

        $colors_filter = Home::getColorsFilter($colors);

        $query = "select `products`.*,
                           concat(u2.hash, '/', u2.name) as front_photo_small_url,
                           concat(u4.hash, '/', u4.name) as back_photo_small_url
                    from `products`
                             LEFT JOIN uploads as u2 ON u2.id = products.front_photo_small
                             LEFT JOIN uploads as u4 ON u4.id = products.back_photo_small
                    WHERE product_type = $product_type
                        " . (!is_null($watch_type) ? 'AND watch_type = "' . $watch_type . '"' : '') . "
                      AND price - COALESCE(sale_price, 0) >= $min_price
                      AND price - COALESCE(sale_price, 0) <= $max_price
                      AND $colors_filter
                        " . (!is_null($mech) ? 'AND mech = "' . $mech . '"' : '') . " " . ($on_sale == 'all' ? '' : 'AND sale_price !=0') . "
                      AND `products`.deleted_at IS NULL
                      AND in_stock = 'yes'
                    ORDER BY $order_by_column $order_by_direction
                    LIMIT $perPage OFFSET $offset";
        //dd($query);
        return DB::select($query);
    }

    public static function getNotInStockProducts($order_by_column, $order_by_direction, $perPage, $offset)
    {
        $query = "select `products`.*,
                           concat(u2.hash, '/', u2.name) as front_photo_small_url,
                           concat(u4.hash, '/', u4.name) as back_photo_small_url
                    from `products`
                             LEFT JOIN uploads as u2 ON u2.id = products.front_photo_small
                             LEFT JOIN uploads as u4 ON u4.id = products.back_photo_small
                    WHERE in_stock = 'no'
                      AND `products`.deleted_at IS NULL
                    
                    ORDER BY $order_by_column $order_by_direction
                    LIMIT $perPage OFFSET $offset";
        //dd($query);
        return DB::select($query);
    }

    public static function getNotInStockProductsCount()
    {
        $query = "SELECT COUNT(0) as total_count FROM `products` WHERE
                                     in_stock = 'no'
                                    AND deleted_at IS NULL 
                                    ";
        //dd($query);

        return DB::select($query)['0']->total_count;
    }

    public static function getProductsCount($product_type, $watch_type, $min_price, $max_price, $colors, $mech, $on_sale)
    {
        $product_type = DB::connection()->getPdo()->quote($product_type);
        $min_price = DB::connection()->getPdo()->quote($min_price);
        $max_price = DB::connection()->getPdo()->quote($max_price);

        $colors_filter = Home::getColorsFilter($colors);

        $query = "SELECT COUNT(0) as total_count FROM `products` WHERE
                                    product_type = $product_type
                                    " . (!is_null($watch_type) ? 'AND watch_type = "' . $watch_type . '"' : '') . "
                                    AND price - COALESCE(sale_price, 0) >= $min_price
                                    AND price - COALESCE(sale_price, 0) <= $max_price
                                    AND $colors_filter
                                     " . (!is_null($mech) ? 'AND mech = "' . $mech . '"' : '') . " 
                                     " . ($on_sale == 'all' ? '' : 'AND sale_price !=0') . "
                                    AND deleted_at IS NULL 
                                    AND in_stock = 'yes'
                                    ";

        //dd($query);

        return DB::select($query)['0']->total_count;
    }

    public static function getProductByProductName($search)
    {
        $query = 'SELECT
                        `products`.id,
                        `products`.price,
                        `products`.sale_price,
                        concat(u2. HASH, "/", u2. NAME) AS front_photo_small_url,
                        `products`.`name`
                    FROM
                        `products`
                 LEFT JOIN uploads AS u2 ON u2.id = `products`.front_photo_small
                    WHERE
                        `products`.`name` LIKE "%' . $search . '%"
                    AND `products`.deleted_at IS NULL
                    AND `products`.in_stock = "yes"';
        return DB::select($query);
    }

    public static function getSuggestionsFromQueryResult($result)
    {
        $suggestions = [];

        foreach ($result as $item) {
            $suggestions[] = [
                "value" => $item->name,
                "permalink" => route('product', ['id' => $item->id]),
                "price" => '<span class="price">' . (!is_null($item->sale_price) ? '<del>
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">₼</span>' . $item->price . '
                                            </span>
                                        </del>' : '') . '
                                    <ins>
                                        <span class="woocommerce-Price-amount amount">
                                            <span class="woocommerce-Price-currencySymbol">₼</span>' . ($item->price - (int)$item->sale_price) . '
                                        </span>
                                    </ins>
                                </span>',
                "thumbnail" => "<img width=\"350\" height=\"380\" src=\" " . config('app.uploads_location') . "/" . $item->front_photo_small_url . "\" class=\"attachment-woocommerce_thumbnail size-woocommerce_thumbnail\" alt=\"\" />"
            ];
        }
        return $suggestions;
    }

    public static function getBestSellers()
    {
        $query = "SELECT `products`.*,concat(u2.hash, '/', u2.name) as front_photo_small_url,
                           concat(u4.hash, '/', u4.name) as back_photo_small_url FROM `products` LEFT JOIN uploads as u2 ON u2.id = products.front_photo_small
                             LEFT JOIN uploads as u4 ON u4.id = products.back_photo_small WHERE is_best_seller = 'yes' AND `products`.deleted_at IS NULL AND in_stock = 'yes' ";

        return DB::select($query);
    }

    public static function getProductsCountByGroupedArray($array)
    {
        $result = 0;
        foreach ($array as $group) {
            $result += $group->count;
        }
        return $result;
    }

    public static function getWatchesCountByGroupedArray($array)
    {
        $result = 0;
        foreach ($array as $group) {
            if ($group->product_type == 'watches') {
                $result += $group->count;
            }
        }
        return $result;
    }

    public static function getMensWatchCountByGroupedArray($array)
    {
        foreach ($array as $group) {
            if ($group->watch_type == 'mens' && $group->product_type == 'watches') {
                return $group->count;
            }
        }
        return 0;
    }

    public static function getWomensWatchCountByGroupedArray($array)
    {
        foreach ($array as $group) {
            if ($group->watch_type == 'ladies' && $group->product_type == 'watches') {
                return $group->count;
            }
        }
        return 0;
    }

    public static function getWatchBoxesCountByGroupedArray($array)
    {
        foreach ($array as $group) {
            if ($group->product_type == 'watch_boxes') {
                return $group->count;
            }
        }
        return 0;
    }

    public static function getGroupedCount()
    {
        return DB::select("SELECT watch_type,product_type, COUNT(0) as count FROM `products` WHERE deleted_at IS NULL AND in_stock = 'yes'  GROUP BY watch_type ,product_type");
    }

    public static function getOnSaleCount()
    {
        return DB::select("SELECT COUNT(0) as count FROM `products` WHERE sale_price !=0 AND deleted_at IS NULL AND in_stock = 'yes' ")[0]->count;
    }

    public static function getMechCount()
    {
        return DB::select("SELECT COUNT(0) as count FROM `products` WHERE mech='mechanical' AND product_type='watches' AND deleted_at IS NULL AND in_stock = 'yes' ")[0]->count;
    }

    public static function filterColorsArray($rawColors)
    {
        $filter_colors_arr_filtered = [];
        if (!is_null($rawColors)) {
            $filter_colors_arr = explode(',', $rawColors);
            foreach ($filter_colors_arr as $color) {
                if (in_array($color, ['black', 'silver', 'gold', 'red', 'brown', 'white'])) {
                    $filter_colors_arr_filtered[] = $color;
                }
            }
        }
        return $filter_colors_arr_filtered;
    }

    public static function insertToShoppingCart($user_id, $product_id)
    {
        $checkIfExist = DB::select("SELECT id FROM `shopping_cart` WHERE `user_id`=$user_id AND `product_id`=$product_id LIMIT 1");
        if (!isset($checkIfExist[0])) {
            DB::transaction(function () use ($user_id, $product_id) {
                Home::removeFromWishLIst($product_id, $user_id);
                DB::insert("INSERT INTO `shopping_cart` (`user_id`,`product_id`) VALUES($user_id,$product_id)");
            });
        }
    }

    public static function insertToWishList($user_id, $product_id)
    {
        $checkIfExist = DB::select("SELECT id FROM `wish_list` WHERE `user_id`=$user_id AND `product_id`=$product_id LIMIT 1");
        if (!isset($checkIfExist[0])) {
            DB::insert("INSERT INTO `wish_list` (`user_id`,`product_id`) VALUES($user_id,$product_id)");
        }
    }

    public static function getShoppingCartProductsIdsAndPrices($user_id)
    {
        $collection = DB::select("SELECT
                                product_id,
                                (price - COALESCE(sale_price, 0)) as price
                            FROM
                                shopping_cart AS s
                            JOIN products AS p ON s.product_id = p.id
                            WHERE
                                user_id = $user_id");

        $result = ['ids' => [], 'price' => 0, 'count' => 0];

        foreach ($collection as $item) {
            $result['ids'][] = $item->product_id;
            $result['price'] += $item->price;
            $result['count']++;
        }
        return $result;
    }

    public static function getWishListProductsIdsAndPrices($user_id)
    {
        $collection = DB::select("SELECT
                                product_id,
                                (price - COALESCE(sale_price, 0)) as price
                            FROM
                                wish_list AS w
                            JOIN products AS p ON w.product_id = p.id
                            WHERE
                                user_id = $user_id");

        $result = ['ids' => [], 'price' => 0, 'count' => 0];

        foreach ($collection as $item) {
            $result['ids'][] = $item->product_id;
            $result['price'] += $item->price;
            $result['count']++;
        }
        return $result;
    }

    public static function applyCoupon($user_id, $coupon)
    {
        $coupon = DB::connection()->getPdo()->quote($coupon);
        $checkTotals = DB::select("SELECT id FROM `shopping_cart_totals` WHERE user_id = $user_id");
        if (count($checkTotals) > 0) {
            DB::update("UPDATE `shopping_cart_totals` SET coupon_id = (SELECT id FROM coupons WHERE name=$coupon) WHERE user_id = $user_id");
        } else {
            DB::insert("INSERT INTO `shopping_cart_totals` (user_id,coupon_id) VALUES ($user_id,(SELECT id FROM coupons WHERE name=$coupon))");

        }
    }

    public static function applyShoppingCartTotals($user_id, $shipping)
    {
        $shipping = DB::connection()->getPdo()->quote($shipping);
        $checkTotals = DB::select("SELECT id FROM `shopping_cart_totals` WHERE user_id = $user_id");

        if (count($checkTotals) > 0) {
            DB::update("UPDATE `shopping_cart_totals` SET shipping = $shipping WHERE user_id = $user_id");
        } else {
            DB::insert("INSERT INTO `shopping_cart_totals` (user_id,shipping) VALUES ($user_id,$shipping)");
        }
    }

    public static function getProductsFromShoppingCart($user_id)
    {
        return DB::select("SELECT
                                p.*,
                                concat(u2.hash, '/', u2.name) as front_photo_small_url
                            FROM
                                shopping_cart AS s
                            JOIN products AS p ON s.product_id = p.id
                            LEFT JOIN uploads as u2 ON u2.id = p.front_photo_small
                            WHERE
                                s.user_id = $user_id");
    }

    public static function getProductsFromShoppingCartNotInStock($user_id)
    {
        return DB::select("SELECT
                                p.*,
                                concat(u2.hash, '/', u2.name) as front_photo_small_url
                            FROM
                                shopping_cart_not_in_stock AS s
                            JOIN products AS p ON s.product_id = p.id
                            LEFT JOIN uploads as u2 ON u2.id = p.front_photo_small
                            WHERE
                                s.user_id = $user_id");
    }

    public static function getUserShoppingCartTotals($user_id)
    {
        $data = DB::select("SELECT
                                c.sale, c.id, uc.shipping,c.name as coupon
                            FROM
                                shopping_cart_totals AS uc
                            LEFT JOIN coupons AS c ON c.id = uc.coupon_id
                            WHERE
                                uc.user_id = $user_id");

        if (count($data) > 0) {
            return $data[0];
        }
        return (object)['sale' => 0, 'coupon' => '', 'id' => 0, 'shipping' => 'baku'];
    }

    public static function getProductsFromWishList($user_id)
    {
        return DB::select("SELECT
                                p.*,
                                concat(u2.hash, '/', u2.name) as front_photo_small_url
                            FROM
                                wish_list AS s
                            JOIN products AS p ON s.product_id = p.id
                            LEFT JOIN uploads as u2 ON u2.id = p.front_photo_small
                            WHERE
                                s.user_id = $user_id");
    }

    public static function rateProduct($product_id, $user_id, $star)
    {
        $checkIfExist = DB::select("SELECT id FROM `product_rating` WHERE user_id=$user_id AND product_id=$product_id");
        if (count($checkIfExist) > 0) {
            DB::update("UPDATE `product_rating` SET star=$star WHERE user_id=$user_id AND product_id=$product_id");
        } else
            DB::insert("INSERT INTO `product_rating` (`user_id`,`product_id`,`star`) VALUES($user_id,$product_id,$star)");
    }

    public static function getProductById($id)
    {
        $user_id = (int)Auth::id();
        $query = "SELECT (SELECT AVG(star) FROM `product_rating` AS pr WHERE pr.product_id = '$id')                as avg_star,
                           p.*,
                           (SELECT pr.star FROM `product_rating` AS pr WHERE pr.product_id = '$id' AND pr.user_id = $user_id) as star,
                            concat(u1.hash, '/', u1.name) as front_photo_url,
                                               concat(u2.hash, '/', u2.name) as front_photo_small_url,
                                               concat(u3.hash, '/', u3.name) as back_photo_url,
                                               concat(u4.hash, '/', u4.name) as back_photo_small_url,
                                               concat(u5.hash, '/', u5.name) as additional_photo1_url,
                                               concat(u6.hash, '/', u6.name) as additional_photo2_url  
                    FROM `products` AS p
                    
                     LEFT JOIN uploads as u1 ON u1.id = p.front_photo
                                                 LEFT JOIN uploads as u2 ON u2.id = p.front_photo_small
                                                 LEFT JOIN uploads as u3 ON u3.id = p.back_photo
                                                 LEFT JOIN uploads as u4 ON u4.id = p.back_photo_small
                                                 LEFT JOIN uploads as u5 ON u5.id = p.additional_photo1
                                                 LEFT JOIN uploads as u6 ON u6.id = p.additional_photo2
                    where p.id = '$id'
                      AND p.deleted_at IS NULL";
        //dd($query);
        return DB::select($query);
    }

    public static function getCouponSaleAndId($coupon)
    {
        $coupon = DB::connection()->getPdo()->quote($coupon);
        $data = DB::select("SELECT sale,id FROM `coupons` WHERE name=$coupon");

        if (count($data) > 0) {
            return $data[0];
        }
        return (object)['sale' => 0, 'id' => 0];
    }

    public static function removeProductFromCart($product_id, $user_id)
    {
        DB::delete("DELETE FROM `shopping_cart` WHERE product_id=$product_id AND user_id=$user_id");
    }

    public static function removeAllProductsFromCart($user_id)
    {
        DB::delete("DELETE FROM `shopping_cart` WHERE  user_id=$user_id");
    }

    public static function removeFromWishLIst($product_id, $user_id)
    {
        DB::delete("DELETE FROM `wish_list` WHERE product_id=$product_id AND user_id=$user_id");
    }

    public static function getProductsHtmlForShoppingCart($products)
    {
        $products_count = count($products);

        $bag_items_number = "<script>
                            (function($){
                                $('.shop-bag').trigger('click');
                            })(jQuery);
                            </script>

                            <span class=\"bag-items-number\">$products_count " . __('header.items') . " </span>";


        $items_number = "<span class=\"shopbag_items_number\" style='display: block;'>$bag_items_number</span>";

        $card_content = "<div class=\"widget_shopping_cart_content\">
                        <div class=\"minicart_title\">
                            <a href=\"#\" class=\"close-icon\"></a>
                            <div class=\"l-header-shop\">
                                <span class=\"shopbag_items_number\" style='display: block;'>$products_count</span>
                                <i class=\"icon-shop\"></i>
                            </div>
                            <h2 class=\"cart-title\">" . __('home.shopping_cart') . "</h2>
                        </div>
                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"woocommerce-mini-cart cart_list product_list_widget \">";

        $total_price = 0;

        foreach ($products as $product) {
            $total_price += ($product->price - $product->sale_price);
            $card_content .= "<tr class=\"woocommerce-mini-cart-item bag-product clearfix product-id-$product->id mini_cart_item\">
                        <td class=\"product-thumbnail\"><a href=\"" . route('product', ['id' => $product->id]) . "\"><img width=\"350\" height=\"380\" src=\"" . config('app.uploads_location') . "/" . $product->front_photo_small_url . "\" class=\"attachment-woocommerce_thumbnail size-woocommerce_thumbnail\" alt=\"\"/></a></td>

                        <td class=\"product-name\">
                            <h4><a href=\"" . route('product', ['id' => $product->id]) . "\">$product->name</a></h4>

                           <span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">₼</span>" . ((int)$product->price - (int)$product->sale_price) . "</span>                     </td>

                        <td class=\"product-remove\">
							<a href=\"%s\" class=\"remove-product remove\" data-ajaxurl=\"" . route('remove_from_card_ajax') . "?id=" . $product->id . "\" data-product-id=\"" . $product->id . "\"   data-variation-id=\"0\" data-product-qty=\"1\" title=\"Remove this item\"><i class=\"icon-close-regular\"></i></a>
                        </td>
                    </tr>";
        }

        $amount = "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">₼</span>$total_price</span>";

        $card_content .= "</table>
					<p class=\"total\"><strong class=\"subtotal_name\">" . __('home.total_price') . ":</strong> <span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">₼</span>$total_price</span></p>
					<p class=\"buttons\">
						<a href=\"" . route('view_cart') . "\" class=\"button view_cart wc-forward\">" . __('home.view_cart') . "</a>
						<a href=\"" . route('checkout') . "\" class=\"button checkout wc-forward\">" . __('home.checkout') . "</a>
					</p>
				</div>";

        return [
            'cart_hash' => "3e049c94eacd4a2ff8d4925447c33347",
            'fragments' => [
                '.bag-items-number' => $bag_items_number,
                '.shop-bag .overview .amount' => $amount,
                '.shopbag_items_number' => $items_number,
                'div.widget_shopping_cart_content' => $card_content
            ]
        ];
    }

    public static function contactUsMail($request)
    {
        try {
            $to_name = $request->session()->get('contact_info')['phone'];
            $to_email = $request->session()->get('contact_info')['email'];

            $data = [
                'request' => $request
            ];

            Mail::send("contactUsMail", $data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject("Contact Us");
                $message->from(config('app.from_mail'), "Contact Us");
            });

            if (Mail::failures()) {
                return false;
                // return response showing failed emails
            }
            return true;
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' - ' . $e->getFile() . ' ' . $e->getLine());
            return false;
        }
    }
}

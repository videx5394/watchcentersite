<?php
namespace App\Interfaces;

interface Orderable
{
    public static function getPaymentStatuses():array;
    public static function createPayment($data) :array;
    public static function handlePaymentResponse($data);
    public static function handlePaymentDeclined($data);
    public static function handlePaymentApproved($data);
    public static function handlePaymentCanceled($data);
    public static function getPaymentStatus($data);
}

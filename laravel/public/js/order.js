const makeOrder = async (element,makeOrderRoute,checkoutRoute) => {

    const product = element.closest('figure');

    addLoading(product);

    const data = {
        'product_id' : element.attr('data-product-id')
    };

    const response = await fetch(makeOrderRoute, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json',
            'Accept':'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });

    if(response.status === 200)
    {
        window.location.href = checkoutRoute+"?not-in-stock=1";
    }
    else
    {

    }
    removeLoading(product);
}

class Coupon {
    price = 0;
    sale = 0;
    applyRoute = '';
    shipping = 'baku';
    coupon_added_message = () => {};

    constructor(price,sale,shipping,applyRoute,coupon_added_message)
    {
        this.price = price;
        this.applyRoute = applyRoute;
        this.sale = sale;
        this.shipping = shipping ? shipping : 'baku';
        this.coupon_added_message = coupon_added_message;
    }

    shippingMethidChanged = (element) => {
        this.shipping = element.val();
        this.updateTotalprice();
    };

    updateTotalprice = () =>
    {
        let total_price = this.price;

        total_price = roundTo(total_price - total_price*this.sale/100,2);

        let formatted_total_price = total_price.toString().split(/\./);

        if(this.shipping == 'cities')
        {
            total_price += 3;
        }
        else
        {
            //total_price += -3;
        }

        if(formatted_total_price[1] && formatted_total_price[1].length === 1)
        {
            total_price = parseFloat(total_price.toString() + '0');
        }

        $('.total_price').text(total_price);
    };

    applyCoupon = async () => {
        let value = $('#coupon_code').val();

        const data = {
            coupon: value
        };

        addLoading($('.coupon_code_wrapper'));


        const response = await fetch( this.applyRoute, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });

        if(response.status === 200)
        {
            $('.cart-subtotal .coupon').text('');

            const result = await response.json();
            this.sale = parseInt(result.sale);

            this.updateTotalprice();

            if(this.sale > 0)
            {
                this.coupon_added_message();
                $('.cart-subtotal .coupon').text(this.sale);
            }
            else {
                $('#coupon_code').val('');
            }

            removeLoading($('.coupon_code_wrapper'));
        }
    };

}

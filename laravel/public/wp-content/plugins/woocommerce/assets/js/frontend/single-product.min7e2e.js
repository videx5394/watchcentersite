function activateStarts(rating)
{
    let stars = jQuery('.comment-form-rating .stars .star');
    stars.removeClass('active');
    stars.each(function() {
        let value = jQuery(this).attr('data-value');
        if(parseInt(value) <= parseInt(rating))
        {
            jQuery(this).addClass('active');
        }
    });
}

async function single_add_to_cart(product_id,view_cart_message,view_cart_link,product_added_message)
{
    const result = await fetch('/add-to-card-ajax?product_id='+product_id);

    const response = await result.json();

    if(response.redirect && response.redirect === 'login')
    {
        window.location.href = '/login';
        return;
    }

    if(response.fragments)
    {
        $('.widget_shopping_cart_content').html(response.fragments['div.widget_shopping_cart_content']);

        let product_count = $('.widget_shopping_cart_content .product_list_widget tbody tr').length;
        $('.shop-bag .shopbag_items_number').text(product_count);

        $('.shop-bag .overview .amount').html(response.fragments['.shop-bag .overview .amount']);
        $('.shop-bag .bag-items-number').text($('.shop-bag .bag-items-number')[0].innerText.replace(/[0-9]+/g,product_count));
    }

    $('.woocommerce-notices-wrapper').html('<div class="woocommerce-message" role="alert">\n' +
        '\t\t<a href="'+view_cart_link+'" tabindex="1" class="button wc-forward">'+view_cart_message+'</a> '+product_added_message+'.\t</div>');

    setTimeout(function () {
        //$( ".woocommerce-notices-wrapper .woocommerce-message" ).fadeOut( 3000);
    },3000);

    $('.shop-bag').click();
}

jQuery(function (s) {
    if ("undefined" == typeof wc_single_product_params) return !1;
    s("body").on("init", ".product_infos", function () {
        s(".wc-tab, .woocommerce-tabs .panel:not(.panel .panel)").hide();
        var t = window.location.hash, e = window.location.href, i = s(this).find(".wc-tabs, ul.tabs").first();
        0 <= t.toLowerCase().indexOf("comment-") || "#reviews" === t || "#tab-reviews" === t ? i.find("li.reviews_tab a").click() : 0 < e.indexOf("comment-page-") || 0 < e.indexOf("cpage=") ? i.find("li.reviews_tab a").click() : "#tab-additional_information" === t ? i.find("li.additional_information_tab a").click() : i.find("li:first a").click()
    }).on("click", ".wc-tabs li a, ul.tabs li a", function (t) {
        t.preventDefault();
        var e = s(this), i = e.closest(".wc-tabs-wrapper, .woocommerce-tabs");
        i.find(".wc-tabs, ul.tabs").find("li").removeClass("active"), i.find(".wc-tab, .panel:not(.panel .panel)").hide(), e.closest("li").addClass("active"), i.find(e.attr("href")).show()
    }).on("click", "a.woocommerce-review-link", async function ()
    {
        return s(".reviews_tab a").click(), !0
    }).on("init", "#rating", function () {
        s("#rating").hide().before('<p class="stars">\t\t\t\t\t\t<span>\t\t\t\t\t\t\t<a data-value="1" class="star star-1">1</a>\t\t\t\t\t\t\t<a data-value="2" class="star star-2">2</a>\t\t\t\t\t\t\t<a data-value="3" class="star star-3">3</a>\t\t\t\t\t\t\t<a data-value="4" class="star star-4">4</a>\t\t\t\t\t\t\t<a data-value="5" class="star star-5">5</a>\t\t\t\t\t\t</span>\t\t\t\t\t</p>')
        const star = jQuery('.comment-form-rating').attr('data-value');
        if(star>0)
        {
            activateStarts(star);
        }
    }).on("click", ".comment-form-rating p.stars a", async function () {
        const value = jQuery(this).attr('data-value');
        let data = {
            'product_id' :   jQuery(this).closest('.comment-form-rating').attr('data-product-id'),
            'star' : value
        };
        const result = await fetch('/rateProduct', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });

        let response =  await result.text();

        if (response === 'login')
        {
            window.location.href = '/login';
        }
        else if (response === 'ok')
        {
            activateStarts(value);
        }

        // var t = s(this), e = s(this).closest("#respond").find("#rating"), i = s(this).closest(".stars");
        // return e.val(t.text()), t.siblings("a").removeClass("active"), t.addClass("active"), i.addClass("selected"), !1
    }).on("click", "#respond #submit", function () {
        var t = s(this).closest("#respond").find("#rating"), e = t.val();
        if (0 < t.length && !e && "yes" === wc_single_product_params.review_rating_required) return window.alert(wc_single_product_params.i18n_required_rating_text), !1
    }), s(".wc-tabs-wrapper, .woocommerce-tabs, #rating").trigger("init");

    function e(t, e) {
        this.$target = t, this.$images = s(".woocommerce-product-gallery__image", t), 0 !== this.$images.length ? (t.data("product_gallery", this), this.flexslider_enabled = s.isFunction(s.fn.flexslider) && wc_single_product_params.flexslider_enabled, this.zoom_enabled = s.isFunction(s.fn.zoom) && wc_single_product_params.zoom_enabled, this.photoswipe_enabled = "undefined" != typeof PhotoSwipe && wc_single_product_params.photoswipe_enabled, e && (this.flexslider_enabled = !1 !== e.flexslider_enabled && this.flexslider_enabled, this.zoom_enabled = !1 !== e.zoom_enabled && this.zoom_enabled, this.photoswipe_enabled = !1 !== e.photoswipe_enabled && this.photoswipe_enabled), 1 === this.$images.length && (this.flexslider_enabled = !1), this.initFlexslider = this.initFlexslider.bind(this), this.initZoom = this.initZoom.bind(this), this.initZoomForTarget = this.initZoomForTarget.bind(this), this.initPhotoswipe = this.initPhotoswipe.bind(this), this.onResetSlidePosition = this.onResetSlidePosition.bind(this), this.getGalleryItems = this.getGalleryItems.bind(this), this.openPhotoswipe = this.openPhotoswipe.bind(this), this.flexslider_enabled ? (this.initFlexslider(e.flexslider), t.on("woocommerce_gallery_reset_slide_position", this.onResetSlidePosition)) : this.$target.css("opacity", 1), this.zoom_enabled && (this.initZoom(), t.on("woocommerce_gallery_init_zoom", this.initZoom)), this.photoswipe_enabled && this.initPhotoswipe()) : this.$target.css("opacity", 1)
    }

    e.prototype.initFlexslider = function (t) {
        var e = this.$target, i = this, o = s.extend({
            selector: ".woocommerce-product-gallery__wrapper > .woocommerce-product-gallery__image",
            start: function () {
                e.css("opacity", 1)
            },
            after: function (t) {
                i.initZoomForTarget(i.$images.eq(t.currentSlide))
            }
        }, t);
        e.flexslider(o), s(".woocommerce-product-gallery__wrapper .woocommerce-product-gallery__image:eq(0) .wp-post-image").one("load", function () {
            var i = s(this);
            i && setTimeout(function () {
                var t = i.closest(".woocommerce-product-gallery__image").height(), e = i.closest(".flex-viewport");
                t && e && e.height(t)
            }, 100)
        }).each(function () {
            this.complete && s(this).trigger("load")
        })
    }, e.prototype.initZoom = function () {
        this.initZoomForTarget(this.$images.first())
    }, e.prototype.initZoomForTarget = function (t) {
        if (!this.zoom_enabled) return !1;
        var i = this.$target.width(), o = !1;
        if (s(t).each(function (t, e) {
            if (s(e).find("img").data("large_image_width") > i) return !(o = !0)
        }), o) {
            var e = s.extend({touch: !1}, wc_single_product_params.zoom_options);
            "ontouchstart" in document.documentElement && (e.on = "click"), t.trigger("zoom.destroy"), t.zoom(e), setTimeout(function () {
                t.find(":hover").length && t.trigger("mouseover")
            }, 100)
        }
    }, e.prototype.initPhotoswipe = function () {
        this.zoom_enabled && 0 < this.$images.length ? (this.$target.prepend('<a href="#" class="woocommerce-product-gallery__trigger">🔍</a>'), this.$target.on("click", ".woocommerce-product-gallery__trigger", this.openPhotoswipe), this.$target.on("click", ".woocommerce-product-gallery__image a", function (t) {
            t.preventDefault()
        }), this.flexslider_enabled || this.$target.on("click", ".woocommerce-product-gallery__image a", this.openPhotoswipe)) : this.$target.on("click", ".woocommerce-product-gallery__image a", this.openPhotoswipe)
    }, e.prototype.onResetSlidePosition = function () {
        this.$target.flexslider(0)
    }, e.prototype.getGalleryItems = function () {
        var t = this.$images, r = [];
        return 0 < t.length && t.each(function (t, e) {
            var i = s(e).find("img");
            if (i.length) {
                var o = {
                    src: i.attr("data-large_image"),
                    w: i.attr("data-large_image_width"),
                    h: i.attr("data-large_image_height"),
                    title: i.attr("data-caption") ? i.attr("data-caption") : i.attr("title")
                };
                r.push(o)
            }
        }), r
    }, e.prototype.openPhotoswipe = function (t) {
        t.preventDefault();
        var e, i = s(".pswp")[0], o = this.getGalleryItems(), r = s(t.target);
        e = r.is(".woocommerce-product-gallery__trigger") || r.is(".woocommerce-product-gallery__trigger img") ? this.$target.find(".flex-active-slide") : r.closest(".woocommerce-product-gallery__image");
        var a = s.extend({
            index: s(e).index(), addCaptionHTMLFn: function (t, e) {
                return t.title ? (e.children[0].textContent = t.title, !0) : (e.children[0].textContent = "", !1)
            }
        }, wc_single_product_params.photoswipe_options);
        new PhotoSwipe(i, PhotoSwipeUI_Default, o, a).init()
    }, s.fn.wc_product_gallery = function (t) {
        return new e(this, t || wc_single_product_params), this
    }, s(".woocommerce-product-gallery").each(function () {
        s(this).trigger("wc-product-gallery-before-init", [this, wc_single_product_params]), s(this).wc_product_gallery(wc_single_product_params), s(this).trigger("wc-product-gallery-after-init", [this, wc_single_product_params])
    })
});

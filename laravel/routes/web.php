<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Artisan;

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/clear-cache', function () {
//     $exitCode = Artisan::call('cache:clear');
//     Artisan::call('vendor:publish --provider="Greggilbert\Recaptcha\RecaptchaServiceProvider"');
//     // return what you want
// });

Auth::routes();

Route::get('/', function () {
    return redirect('home');
});

Route::get('/lang/{lang}', 'LanguageController@index')->name('language');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contact-us', 'HomeController@contactUs')->name('contactUs');
Route::get('/orders-not-in-stock', 'HomeController@ordersNotInStock')->name('orders_not_in_stock');
Route::post('/makeOrder', 'OrderController@makeOrder')->name('makeOrder');
Route::get('/refund-policy', 'HomeController@refundPolicy')->name('refund_policy');
Route::get('/about-us', 'HomeController@index')->name('about_us');
Route::get('/product/{id}', 'HomeController@product')->name('product');
Route::get('/view-cart', 'HomeController@viewCart')->name('view_cart')->middleware('auth');
Route::get('/checkout', 'HomeController@checkout')->name('checkout')->middleware('auth');
Route::post('/checkout/approved', 'OrderController@handlePaymentResponse')->name('checkoutApproved');
Route::post('/checkout/canceled', 'OrderController@handlePaymentResponse')->name('checkoutCanceled');
Route::post('/checkout/declined', 'OrderController@handlePaymentResponse')->name('checkoutDeclined');
Route::get('/paymentProcessing/{orderId}/{sessionId}', 'OrderController@paymentProcessing')->middleware('throttle:5,1')->name('paymentProcessing');
Route::get('/invalidPayment', 'OrderController@invalidPayment')->name('invalidPayment');
Route::get('/successfulPayment', 'OrderController@successfulPayment')->name('successfulPayment');
Route::get('/getShoppingCartContent', 'HomeController@getShoppingCartContent')->name('getShoppingCartContent');
Route::get('/findProduct', 'HomeController@findProduct')->name('findProduct');
Route::get('/wish-list', 'HomeController@wishList')->name('wishList')->middleware('auth');

Route::post('/remove_from_wish_list', 'HomeController@removeFromWishLIst')->name('remove_from_wish_list');
Route::post('/applyCoupon', 'HomeController@applyCoupon')->name('applyCoupon')->middleware('auth');
Route::post('/placeOrder', 'OrderController@placeOrder')->name('placeOrder')->middleware('auth');
Route::post('/getBircartTable', 'OrderController@getBircartTable')->name('getBircartTable')->middleware('auth');
Route::post('/confirmApiOrder', 'OrderController@confirmApiOrder')->name('confirmApiOrder')->middleware('auth');
Route::post('/contactUsMail', 'HomeController@contactUsMail')->name('contactUsMail');


Route::get('/add-to-card/{id}', 'HomeController@addToCart')->name('add_to_card');
Route::get('/add-to-card-ajax', 'HomeController@addToCartAjax')->name('add_to_card_ajax');
Route::post('/remove-from-card-ajax', 'HomeController@removeFromCardAjax')->name('remove_from_card_ajax');
Route::get('/add-to-wish-list/{id}', 'HomeController@addToWishList')->name('add_to_wish_list');
Route::get('/add-to-wish-list-ajax', 'HomeController@addToWishListAjax')->name('add_to_wish_list_ajax');
Route::get('logout', 'Auth\LoginController@logout')->middleware('auth');


Route::post('/rateProduct', 'HomeController@rateProduct')->name('rateProduct');

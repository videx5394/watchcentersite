function ErrorSwall() {
	Toast.fire({
		title: 'Xəta baş verdi!',
		text: 'Zəhmət olmasa bir daha cəhd edin',
		type: 'error',
		confirmButtonText: 'OK'
	});
}

function SuccessEmailSwall() {
	Toast.fire({
		title: 'Mesaj göndərildi!',
		type: 'success',
		confirmButtonText: 'OK'
	});
}

$('#contact-form').submit(e => {
	e.preventDefault();

	let error = false;
	const re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

	if ($('#contact-name-input').val().length < 1) {
		$('#contact-name-input').addClass('input-error');
		error = true;
	}

	if (
		$('#contact-email').val().length < 1 ||
		!re.test(
			$('#contact-email')
				.val()
				.toLowerCase()
		)
	) {
		$('#contact-email').addClass('input-error');
		error = true;
	}

	if ($('#contact-subject').val().length < 1) {
		$('#contact-subject').addClass('input-error');
		error = true;
	}

	if ($('#contact-message').val().length < 1) {
		$('#contact-message').addClass('input-error');
		error = true;
	}

	if (error) return;

	$('.send-btn-description').toggleClass('d-none');
	$('.send-btn-spinner').toggleClass('d-none');

	$.ajax({
		method: 'POST',
		contentType: 'application/json',
		url: `http://${emailPath}/landing/send-email`,
		data: JSON.stringify({
			name: $('#contact-name-input').val(),
			email: $('#contact-email').val(),
			subject: $('#contact-subject').val(),
			message: $('#contact-message').val()
		}),
		method: 'POST',
		success: function(data, textStatus, jqXHR) {
			if (jqXHR.status == 200) {
				SuccessEmailSwall();
				$('#contact-name-input').val('');
				$('#contact-email').val('');
				$('#contact-subject').val('');
				$('#contact-message').val('');
			} else {
				ErrorSwall();
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			ErrorSwall();
		},
		complete: function() {
			$('.send-btn-description').toggleClass('d-none');
			$('.send-btn-spinner').toggleClass('d-none');
		}
	});

	// setTimeout(() => {
	// 	$('.send-btn-description').toggleClass('d-none');
	// 	$('.send-btn-spinner').toggleClass('d-none');
	// 	ErrorSwall();
	// }, 2500);
});

$('#contact-name-input').keypress(() => {
	$('#contact-name-input').removeClass('input-error');
});

$('#contact-email').keypress(() => {
	$('#contact-email').removeClass('input-error');
});

$('#contact-subject').keypress(() => {
	$('#contact-subject').removeClass('input-error');
});

$('#contact-message').keypress(() => {
	$('#contact-message').removeClass('input-error');
});

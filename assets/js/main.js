const Toast = Swal.mixin({
	customClass: {
		container: 'swal-container-class',
		popup: 'swal-popup-class',
		header: 'header-class',
		title: 'swall-title-class',
		closeButton: 'close-button-class',
		icon: 'swall-icon-class',
		image: 'image-class',
		content: 'swall-content-class mt-2',
		input: 'input-class',
		actions: 'actions-class mt-2',
		confirmButton: 'swall-confirm-btn-class',
		cancelButton: 'swall-cancel-btn-class',
		footer: 'footer-class'
	}
});

function RegistrationInfoSwall() {
	Toast.fire({
		title: 'Qeydiyyat hələ başlamayıb',
		text: 'Saytımızı izləməyə davam edin. Yaxın günlərdə məlumat veriləcək',
		type: 'info',
		confirmButtonText: 'OK'
	});
}

$('#login-btn').click(() => {
	RegistrationInfoSwall();
});

$('#login-footer-btn').click(() => {
	RegistrationInfoSwall();
});

$('#reg-btn').click(() => {
	RegistrationInfoSwall();
});

$('#reg-btn-2').click(() => {
	RegistrationInfoSwall();
});

$('#reg-link').click(() => {
	RegistrationInfoSwall();
});

$('.reg-link').click(e => {
	e.stopPropagation();
	RegistrationInfoSwall();
});

function toPortal() {
	window.location = `${portalPath}`;
}

function toPortalReg() {
	window.location = `${portalPath}/#/signup`;
}

function toIndex() {
	window.location = `${rootPath}index.html`;
}

function toCompetitionParticipants() {
	window.location = `${rootPath}competition.html#participants`;
}

function toCompetitionObtain() {
	window.location = `${rootPath}competition.html#obtain`;
}

function toCompetition() {
	window.location = `${rootPath}competition.html`;
}

function toStages() {
	window.location = `${rootPath}stages.html`;
}

function toMentors() {
	window.location = `${rootPath}mentors.html`;
}

function toNews() {
	window.location = `${rootPath}news.html`;
}

function toContacts() {
	window.location = `${rootPath}contacts.html`;
}

function toFaq() {
	window.location = `${rootPath}faq.html`;
}
function toDocuments() {
	window.location = `${rootPath}documents.html`;
}

$('.head-img').click(function() {
	toIndex();
});

$('.nav-competition').click(function() {
	toCompetition();
});

$('#all-mentors').click(function() {
	toMentors();
});

$('.nav-mentors').click(function() {
	toMentors();
});

$('.nav-stages').click(function() {
	toStages();
});

$('.nav-news').click(function() {
	toNews();
});

$('.nav-contacts').click(function() {
	toContacts();
});
$('.nav-documents').click(function() {
	toDocuments();
});

$('.nav-faq').click(function() {
	toFaq();
});

function updateHeight(mythis) {
	var height = $('.answear', mythis).outerHeight();
	$('.answear-container', mythis).css({ height: height });
}

$('.question-block').click(function() {
	if ($(this).hasClass('active')) {
		$('.answear-container', this).css({ height: 0 });
		$('.question-dot div', this).html('+');
	} else {
		updateHeight(this);
		$('.question-dot div', this).html('-');
	}
	$(this).toggleClass('active');
});

$(document).ready(function() {
	if (window.location.pathname == '/' || window.location.pathname.indexOf('index') > 0) {
		$('#main-container')
			.removeClass('d-none')
			.addClass('d-flex');
		$('#main-carousel').owlCarousel({
			animateOut: 'fadeOutDown',
			animateIn: 'fadeInUp',
			items: 1,
			autoplay: true,
			margin: 100,
			autoplayTimeout: 10000,
			autoplaySpeed: 1000,
			smartSpeed: 1000,
			fluidSpeed: 1000,
			navSpeed: 1000,
			loop: true,
			dots: true,
			lazyLoad: true,
			mouseDrag: false,
			touchDrag: false,
			autoplayHoverPause: true
		});
		$('#secondary-carousel').owlCarousel({
			animateOut: 'fadeOutRight',
			animateIn: 'fadeInRight',
			items: 1,
			autoplay: false,
			margin: 100,
			loop: true,
			dots: false,
			lazyLoad: true,
			mouseDrag: false,
			touchDrag: false,
			autoplayHoverPause: true
		});

		sync1 = $('#main-carousel');
		sync2 = $('#secondary-carousel');

		sync1.on('change.owl.carousel', function(event) {
			if (event.namespace && event.property.name === 'position') {
				var target = event.relatedTarget.relative(event.property.value, true);
				sync2.owlCarousel('to', target, 1000, true);
			}
		});

		$('.slide1-btn').click(function() {
			toCompetitionParticipants();
		});

		$('.slide2-btn').click(function() {
			toCompetitionObtain();
		});

		$('.slide3-btn').click(function() {
			toStages();
		});
	}
	$('.mentor-container').each(function() {
		var height = $('.mentor-description', this).outerHeight();
		$(this).css({ bottom: `-${height}px` });
	});
});

function detectUnsuportedBrowser() {
    // navigator.userAgent.match(/Android/i) ||
    if (
        
		window.location.pathname.indexOf('not-supported') == -1 &&
		['Firefox', 'Safari'].indexOf(getBrowserName()) == -1
	)
		window.location = rootPath + 'not-supported.html';
}

function getBrowserName() {
    const userAgent = window.navigator.userAgent.split(' ');
	return userAgent[userAgent.length - 1].match(/[A-Za-z]+/g)[0];
}

detectUnsuportedBrowser();

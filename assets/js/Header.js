class MyHeader extends HTMLElement {
	connectedCallback() {
		this.innerHTML = `<div class="container-fluid px-5 pt-3 text-uppercase header">
			<div class="row justify-content-between mt-2 mx-0">
				<div class="d-flex align-items-center">
					<img class="head-img" src="${rootPath}assets/images/yukselis.svg" alt="yukselish-img" />
				</div>
				<div class="d-flex justify-content-between">
					<div class="d-flex align-items-center pr-lg-3 mr-3  navigation">
						<div class="py-2 mr-3  mr-lg-4 nav-competition">
							<a href="#">Haqqında</a>
						</div>
						<div class="py-2 mr-3 mr-lg-4 nav-mentors">
							<a href="#">İdarəçİ rəhbərlər</a>
						</div>
						<div class="py-2 mr-3 mr-lg-4 nav-stages">
							<a href="#">Mərhələlər</a>
						</div>
						<div class="py-2 mr-3 mr-lg-4 nav-news">
							<a href="#">Xəbərlər</a>
						</div>
						<div class="py-2 mr-3 mr-lg-4 nav-contacts">
							<a href="#">BİZİMLƏ ƏLAQƏ</a>
						</div>
					</div>
					<div class="d-flex justify-content-center align-items-center">
						<button id="login-btn" class="px-3 py-2 border-0 text-uppercase secondary-color controls">
							Şəxsİ kabİnet
						</button>
					</div>
				</div>
			</div>
		</div>`;
	}
}

customElements.define('my-header', MyHeader);
